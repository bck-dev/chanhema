$(document).ready(function () {

    $(window).scroll(function () {

        // if ($(document).scrollTop() > 10) {

        //     $('.top_row_menu').addClass('menu_display_none');
        //     $('.navcol').addClass('fixed_menu_margin');
        //     $('.main-menu').addClass('fixed_menu_height');
        //     $('.navig').addClass('fixed_menu');

        // } else {
            
        //     $('.top_row_menu').removeClass('menu_display_none');
        //     $('.navcol').removeClass('fixed_menu_margin');
        //     $('.main-menu').removeClass('fixed_menu_height');
        //     $('.navig').removeClass('fixed_menu');

        // }

        if ($(document).scrollTop() > 20) {
            $('.new-nav').addClass('new-scroll-height');            
            $('.new-main-menu').addClass('new-scroll-height');                          
            $('.new-top-bar').addClass('new-top-bar-transition');                 
            $('.new-logo').addClass('logo-transition');               
            $('.new-small-logo').addClass('logo-small-transition').delay(800);
        } else {
            $('.new-nav').removeClass('new-scroll-height');            
            $('.new-main-menu').removeClass('new-scroll-height');                           
            $('.new-top-bar').removeClass('new-top-bar-transition');                           
            $('.new-small-logo').removeClass('logo-small-transition');              
            $('.new-logo').removeClass('logo-transition');
        }


        if ($(document).scrollTop() > 10) {

            $('.sticky').addClass('stickyshow');

        } else {

            $('.sticky').removeClass('stickyshow');

        }

    });

    $('#aboutExpand').click(function () {
        if($('.aboutMenuMobile').css('display')=="none"){
            $('.aboutMenuMobile').removeClass('d-none');
            $('.aboutMenuMobile').addClass('d-block');
        }
        else{
            $('.aboutMenuMobile').removeClass('d-block');
            $('.aboutMenuMobile').addClass('d-none');
        }
        
        $('.collectionMenu').removeClass('d-block');
        $('.collectionMenu').addClass('d-none');
    });

    $('#collectionExpand').click(function () {
        if($('.collectionMenu').css('display')=="none"){
            $('.collectionMenu').removeClass('d-none');
            $('.collectionMenu').addClass('d-block');
        }
        else{
            $('.collectionMenu').removeClass('d-block');
            $('.collectionMenu').addClass('d-none');
        }
        
        $('.aboutMenuMobile').removeClass('d-block');
        $('.aboutMenuMobile').addClass('d-none');
    });

    $('.search-ico').click(function () {
        // var searchtextbox = $('.search');
        // searchtextbox.toggle("slide");

        $(".search").toggle("slide");

    });
    $("#open-menu").click(function () {
        $("#overlay").slideToggle("slow")

    });


    // policy slide

    $(".policy-close").click(function () {
        $(".policy").animate({
            height: "toggle"
        });

    });

    // drop section

    // $('.dorp-collect').hover(function(){
    // $('#drop-one').toggle();
    // });


    // first first section
    $(".panel-about, #drop-two").hover(function () {
        $('#drop-two').addClass('display-on');
    });
    $(".panel-about").mouseleave(function () {
        $('#drop-two').removeClass('display-on');
    });
    $("#drop-two").mouseleave(function () {
        $('#drop-two').removeClass('display-on');
    });


    // first second section
    $(".second-about, #first-drop1").hover(function () {
        $('#first-drop1').addClass('display-on');
    });
    $(".second-about").mouseleave(function () {
        $('#first-drop1').removeClass('display-on');
    });
    $("#first-drop1").mouseleave(function () {
        $('#first-drop1').removeClass('display-on');
    });


    // third FIRST section

    $(".dow-panel-sec, #drop-one").hover(function () {
        $('#drop-one').addClass('display-on');
    });
    $(".dow-panel-sec").mouseleave(function () {
        $('#drop-one').removeClass('display-on');
    });
    $("#drop-one").mouseleave(function () {
        $('#drop-one').removeClass('display-on');
    });


    // third second section

    $(".dow-panel-first, #drop-thirdTwo").hover(function () {
//            $('#first-drop1').addClass('display-on');
        $('#drop-thirdTwo').addClass('display-on');
    });
    $(".dow-panel-first").mouseleave(function () {
//            $('#first-drop1').removeClass('display-on');
        $('#drop-thirdTwo').removeClass('display-on');
    });
    $("#drop-thirdTwo").mouseleave(function () {
//              $('#first-drop1').removeClass('display-on');
        $('#drop-thirdTwo').removeClass('display-on');
    });

    // toglle section services
    // function initAccordion(accordionElem) {

    //     //when panel is clicked, handlePanelClick is called.          

    //     function handlePanelClick(event) {
    //         showPanel(event.currentTarget);
    //     }

    //     //Hide currentPanel and show new panel.  

    //     function showPanel(panel) {
    //         //Hide current one. First time it will be null. 
    //         var expandedPanel = accordionElem.querySelector(".active");
    //         if (expandedPanel) {
    //             expandedPanel.classList.remove("active");
    //         }

    //         //Show new one
    //         panel.classList.add("active");

    //     }

    //     var allPanelElems = accordionElem.querySelectorAll(".panel");
    //     for (var i = 0, len = allPanelElems.length; i < len; i++) {
    //         allPanelElems[i].addEventListener("click", handlePanelClick);
    //     }

    //     //By Default Show first panel
    //     //   showPanel(allPanelElems[1])

    // }

    // initAccordion(document.getElementById("accordion"));

});