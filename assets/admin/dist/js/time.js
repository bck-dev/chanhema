$(function() {
  
  setInterval( function() {

    //create new date
    var today = new Date();

    //get hours & AM/PM
    var hours = today.getHours();
    var pmhours = hours % 12;
    if(hours == 0){
      $(".hours").html("12");
    }
    else{
      $(".zero").html(pmhours < 10 && hours != 12 ? "0" : "" );
      $(".hours").html(hours <= 12 ? hours : pmhours);
    }
    $(".ampm").html(hours >= 12 ? "PM" : "AM");

    //get minutes
    var minutes = today.getMinutes();
    $(".minutes").html(( minutes < 10 ? "0" : "" ) + minutes);

    //get seconds 
    var seconds = today.getSeconds();
    $(".seconds").html(( seconds < 10 ? "0" : "" ) + seconds);

    //get year
    var year = today.getFullYear();
    $(".year").html(year);

    //get month
    var month = today.getMonth();
    var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    $(".month").html(months[month] + " ");

    //get day
    var date = today.getDay();
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    $(".day").html(days[date]);

    //get date
    var date = today.getDate();
    $(".date").html(date + ",");

  }, 1000);
});