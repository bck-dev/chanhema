-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2020 at 07:06 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hemachandra`
--

-- --------------------------------------------------------

--
-- Table structure for table `best_practices`
--

CREATE TABLE `best_practices` (
  `best_practices_id` int(11) NOT NULL,
  `best_practices_name` text DEFAULT NULL,
  `best_practices_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `best_practices_images`
--

CREATE TABLE `best_practices_images` (
  `best_practices_images_id` int(11) NOT NULL,
  `best_practices_images_name` text NOT NULL,
  `best_practices_best_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `best_practices_images`
--

INSERT INTO `best_practices_images` (`best_practices_images_id`, `best_practices_images_name`, `best_practices_best_id`) VALUES
(9, '1525757615aa.jpeg', 3),
(10, '1526534086cja-logo.png', 1),
(11, '1526534086gia-logo.png', 1),
(12, '1526534086Depositphotos_8110132_xs.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `celebrate_images`
--

CREATE TABLE `celebrate_images` (
  `celebrate_image_id` int(11) NOT NULL,
  `celebrate_images_name` varchar(100) DEFAULT NULL,
  `celebrate_celebrate_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `celebrate_images`
--

INSERT INTO `celebrate_images` (`celebrate_image_id`, `celebrate_images_name`, `celebrate_celebrate_id`) VALUES
(3, '1543301544collection3.jpeg', 1),
(4, '1543301553collection2.jpg', 2),
(6, '1543317912collection1.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `collection`
--

CREATE TABLE `collection` (
  `collection_id` int(11) NOT NULL,
  `first_image` varchar(255) DEFAULT NULL,
  `Main_description` varchar(255) NOT NULL,
  `catagory_id` int(11) DEFAULT NULL,
  `sub_catagory_id` int(11) DEFAULT NULL,
  `second_image` varchar(255) DEFAULT NULL,
  `second_description` varchar(255) DEFAULT NULL,
  `second_section_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collection`
--

INSERT INTO `collection` (`collection_id`, `first_image`, `Main_description`, `catagory_id`, `sub_catagory_id`, `second_image`, `second_description`, `second_section_status`) VALUES
(16, 'c90a68207427267010b1f8cceb7ecc19.png', 'Inspired by the decorative art and architecture of Sri Lanka’s last kingdom, which came to an end in 1815, the Kandyan Collection fuses contemporary design with stunning historical motifs. These include majestic gem-dusted peacocks and elegant floral desi', 1, 8, '362a9e47e097e94b0c742d16c4626be9.png', 'Inspired by the decorative art and architecture of Sri Lanka’s last kingdom, which came to an end in 1815, the Kandyan Collection fuses contemporary design with stunning historical motifs. These include majestic gem-dusted peacocks and elegant floral desi', 1),
(17, 'd86f3d622152f153eb47422b3d040bad.png', 'hfshfsf fsffhufffdf ffffffffffffffffffffffffffffff yryhfsuf ufufifbsdf', 5, 3, '542f2e363e079445e4e19d5f1809751e.png', 'hfhfbfh jfjbjfb jdjjdfvndjnvdjvndjvndjvnfvjdvndjfvn vdnvjvndjvndjv', 1),
(22, '2bd3540aec4366ce4f629293a1de9634.png', 'jdjdjd djdjd bdbncnc cnndcscnc nscscsndcc&nbsp;', 2, 4, NULL, NULL, NULL),
(23, '689dacecf50a42c05120ea7103d36079.png', 'Breitling has its very own place among watch brands: the highly exclusive ‘stronghold’ of technical watches, mainly chronographs. A longstanding partner of aviation, a field where reliability and precision play a vital role, the brand has always devoted p', 1, 6, 'b6e6353296d8e76b6ebd4d1597aac635.png', 'watch brands: the highly exclusive ‘stronghold’ of technical watches, mainly.', 1),
(24, '83d2338c3d51bf21a1fb4440904e9b7c.png', 'Breitling has its very own place among watch brands: the highly exclusive ‘stronghold’ of technical watches, mainly chronographs. A longstanding partner of aviation, a field where reliability and precision play a vital role, the brand has always devoted p', 1, 2, NULL, NULL, NULL),
(25, '7d0f8e1fe919e00caff47978f7fa8dfa.png', 'Breitling has its very own place among watch brands: the highly exclusive ‘stronghold’ of technical watches, mainly chronographs. A longstanding partner of aviation, a field where reliability and precision play a vital role, the brand has always devoted.', 1, 5, 'd551e015fd5d7ee3457106f4af8d7e58.png', 'Breitling has its very own place among,\r\nwatch brands: the highly exclusive ‘stronghold’ of technical watches, mainly.', 1),
(26, '25ce2b4656a28dcf99d0eb90f176824c.png', 'From tropical leaf patterns to rare indigenous flowers, this collection celebrates the richness and natural beauty of our island home.', 2, 9, NULL, NULL, NULL),
(27, 'b39cd6675987d743841374679d140b5c.jpg', 'Blending contemporary refinement  with exquisite craftsmanship, the Cluster Collection brings together unique pieces in our signature style. The vibrant shades of the finest Ceylon gemstones are offset with complementing hues.', 3, 12, '3fcd3b21aedff1d26d0735f1b723ae88.png', 'Blending contemporary refinement  with exquisite craftsmanship, the Cluster Collection brings together unique pieces in our signature style. The vibrant shades of the finest Ceylon gemstones are offset with complementing hues.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `collection_images`
--

CREATE TABLE `collection_images` (
  `collection_image_id` int(11) NOT NULL,
  `collection_images_name` varchar(100) DEFAULT NULL,
  `collection_collection_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collection_images`
--

INSERT INTO `collection_images` (`collection_image_id`, `collection_images_name`, `collection_collection_id`) VALUES
(2, '1543319427collection3.jpeg', 3),
(3, '1543319436collection2.jpg', 2),
(4, '1543319446collection1.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `collection_page_main_category`
--

CREATE TABLE `collection_page_main_category` (
  `celebrate_id` int(11) NOT NULL,
  `celebrate_name` varchar(100) DEFAULT NULL,
  `celebrate_description` text DEFAULT NULL,
  `celebrate_materials` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collection_page_main_category`
--

INSERT INTO `collection_page_main_category` (`celebrate_id`, `celebrate_name`, `celebrate_description`, `celebrate_materials`) VALUES
(1, 'Kandyan Collection', '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div><div>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', '<div>test,test,test,test</div>'),
(2, 'Nature', NULL, NULL),
(3, 'Cluster', NULL, NULL),
(5, 'Test 4', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `collection_page_sub_catagory`
--

CREATE TABLE `collection_page_sub_catagory` (
  `page_sub_catagory_id` int(11) NOT NULL,
  `page_catagory_name` varchar(255) NOT NULL,
  `page_catagory_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collection_page_sub_catagory`
--

INSERT INTO `collection_page_sub_catagory` (`page_sub_catagory_id`, `page_catagory_name`, `page_catagory_id`) VALUES
(2, 'Love & Care', 1),
(3, 'test test11', 5),
(4, 'Traditional', 2),
(5, 'Ceylone Doll', 1),
(6, 'Heritage', 1),
(7, 'test wedding3', 1),
(8, 'test wedding4', 1),
(9, 'Adamantine', 2),
(10, 'Celestial Art', 2),
(11, 'Contemporary', 2),
(12, 'Accessories', 3),
(13, 'Bracelets', 3),
(14, 'Charms', 3),
(15, 'Engagement Rings', 3),
(16, 'test 1', 5),
(17, 'test2', 5);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `faq_id` int(11) NOT NULL,
  `faq_name` varchar(100) DEFAULT NULL,
  `faq_description` text NOT NULL,
  `faq_category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `faq_name`, `faq_description`, `faq_category_id`) VALUES
(2, 'test', '<span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">Lorem Ipsum</span><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>', 6),
(3, 'test 2', '<span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">second sample text<b>&nbsp;</b>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>', 5);

-- --------------------------------------------------------

--
-- Table structure for table `faq_category`
--

CREATE TABLE `faq_category` (
  `faq_category_id` int(11) NOT NULL,
  `faq_category_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_category`
--

INSERT INTO `faq_category` (`faq_category_id`, `faq_category_name`) VALUES
(6, 'Terms and Conditions'),
(5, 'Privacy Policies');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `gallery_id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`gallery_id`, `image`) VALUES
(9, '1559710588news.jpg'),
(8, '1559710577news.jpg'),
(13, '1559710682news.jpg'),
(11, '1559710659news.jpg'),
(12, '1559710669news.jpg'),
(7, '1559710566news.jpg'),
(10, '1559710602news.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_type` int(11) NOT NULL,
  `email` text NOT NULL,
  `password` varchar(100) NOT NULL,
  `verification_status` tinyint(4) NOT NULL,
  `security_code` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`user_id`, `user_name`, `user_type`, `email`, `password`, `verification_status`, `security_code`) VALUES
(1, 'Yasas Malinga', 1, 'yasasmalinga@ymail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, '5a6c344fb871ea7d0f5945fd55c940ae025aa260'),
(2, 'admin', 1, 'admin@hemachandra.com', '21232f297a57a5a743894a0e4a801fc3', 1, '5a6c344fb871ea7d0f5945fd55c940ae025aa260');

-- --------------------------------------------------------

--
-- Table structure for table `main_banner`
--

CREATE TABLE `main_banner` (
  `main_banner_id` int(11) NOT NULL,
  `main_banner_image` text DEFAULT NULL,
  `main_banner_text` varchar(100) DEFAULT NULL,
  `main_banner_link` text DEFAULT NULL,
  `main_banner_status` int(11) DEFAULT NULL COMMENT '1-Active, 2-Deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_banner`
--

INSERT INTO `main_banner` (`main_banner_id`, `main_banner_image`, `main_banner_text`, `main_banner_link`, `main_banner_status`) VALUES
(7, '95a1381fec452447761bd6780f2d7ab6.png', 'banner', 'http://bckonnect.com/hemachandracmsnew/', 1),
(8, '80f1e59a5222fa7f7bca027ebfbd56d5.png', 'banner', 'http://bckonnect.com/hemachandracmsnew/', 1),
(9, 'a8970b59307f3767d69a9aa3bffc1c2f.png', 'banner', 'http://bckonnect.com/hemachandracmsnew/', 1),
(10, '4bb079b00af7fca591180909903941f8.png', 'banner', 'http://bckonnect.com/hemachandracmsnew/', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_title` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `news_description` text DEFAULT NULL,
  `news_images` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_title`, `date`, `news_description`, `news_images`) VALUES
(2, 'Hemachandras to open new flagship store on Flower Road, Colombo in 2020', '05/31/2019', '<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\">Located in the heart of Colombo 7, the city’s most exclusive district,\r\nthe new Hemachandras flagship store will be unveiled in late 2020. The boutique\r\nwill welcome clients into a private and harmonious space housing an assortment\r\nof signature pieces including the brand’s emblematic cluster designs, as well\r\nas a range of new collections which take inspiration from the island’s diverse\r\nflora, as well as the rich artistic heritage of the Kandyan kingdom.<o:p></o:p></span></p>', ''),
(3, 'HEMACHANDRA unveils new TALI collection with interactive astrology app', '05/29/2019', 'Hemachandra first presented its new TALI collection during the Fall/ Winter 2019 show. Inspired by a spiritual world, the luck-bringing models in the range were revealed in a virtual constellation via a mobile app created specially for the occasion.<p></p><p class=\"feed-sec-text\" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 15px;=\"\" color:=\"\" rgb(69,=\"\" 69,=\"\" 69);=\"\" line-height:=\"\" 1.8;=\"\" font-weight:=\"\" 600;=\"\" padding-top:=\"\" 22px;=\"\" padding-bottom:=\"\" 5px;\"=\"\" style=\"margin-bottom: 1rem;\">This service is always undertaken with respect to the original technical and aesthetic specifications of the piece.</p><p class=\"feed-sec-text\" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 15px;=\"\" color:=\"\" rgb(69,=\"\" 69,=\"\" 69);=\"\" line-height:=\"\" 1.8;=\"\" font-weight:=\"\" 600;=\"\" padding-top:=\"\" 22px;=\"\" padding-bottom:=\"\" 5px;\"=\"\" style=\"margin-bottom: 1rem;\">Hemachandra first presented its new TALI collection during the Fall/ Winter 2019 show. Inspired by a spiritual world, the luck-bringing models in the range were revealed in a virtual constellation via a mobile app created specially for the occasion.</p><p></p><p class=\"feed-sec-text\" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 15px;=\"\" color:=\"\" rgb(69,=\"\" 69,=\"\" 69);=\"\" line-height:=\"\" 1.8;=\"\" font-weight:=\"\" 600;=\"\" padding-top:=\"\" 22px;=\"\" padding-bottom:=\"\" 5px;\"=\"\" style=\"margin-bottom: 1rem;\">This service is always undertaken with respect to the original technical and aesthetic specifications of the piece.</p><p></p><p></p>Hemachandra first presented its new TALI collection during the Fall/ Winter 2019 show. Inspired by a spiritual world, the luck-bringing models in the range were revealed in a virtual constellation via a mobile app created specially for the occasion.<p></p><p class=\"feed-sec-text\" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 15px;=\"\" color:=\"\" rgb(69,=\"\" 69,=\"\" 69);=\"\" line-height:=\"\" 1.8;=\"\" font-weight:=\"\" 600;=\"\" padding-top:=\"\" 22px;=\"\" padding-bottom:=\"\" 5px;\"=\"\" style=\"margin-bottom: 1rem;\">This service is always undertaken with respect to the original technical and aesthetic specifications of the piece.</p>', '');

-- --------------------------------------------------------

--
-- Table structure for table `news_images`
--

CREATE TABLE `news_images` (
  `news_image_id` int(11) NOT NULL,
  `image_name` text NOT NULL,
  `news_news_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_images`
--

INSERT INTO `news_images` (`news_image_id`, `image_name`, `news_news_id`) VALUES
(1, '1559716194box1.png', 2),
(2, '1559716194box2.png', 2),
(3, '1559716194box4.png', 2),
(5, '1559717713box2.png', 3),
(6, '1559717713box4.png', 3),
(7, '1559724605box1.png', 2),
(8, '1559724605box2.png', 2),
(9, '1559724605box4.png', 2),
(10, '1559725584box1.png', 3),
(11, '1559725584box2.png', 3),
(12, '1559725584box4.png', 3);

-- --------------------------------------------------------

--
-- Table structure for table `notification_bar`
--

CREATE TABLE `notification_bar` (
  `notification_bar_id` int(11) NOT NULL,
  `notification_bar_description` text NOT NULL,
  `notification_bar_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_bar`
--

INSERT INTO `notification_bar` (`notification_bar_id`, `notification_bar_description`, `notification_bar_status`) VALUES
(1, 'We Just Update our <a href=\"http://bckonnect.com/hemachandra/return\" title=\"Return policy\" target=\"\">Return policy</a>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `people_id` int(11) NOT NULL,
  `people_first_name` varchar(100) NOT NULL,
  `people_last_name` varchar(100) NOT NULL,
  `people_image` text DEFAULT NULL,
  `people_age` int(11) NOT NULL,
  `people_address` text NOT NULL,
  `people_contact_number` varchar(100) NOT NULL,
  `people_email` text NOT NULL,
  `people_description` text NOT NULL,
  `people_category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`people_id`, `people_first_name`, `people_last_name`, `people_image`, `people_age`, `people_address`, `people_contact_number`, `people_email`, `people_description`, `people_category`) VALUES
(1, 'Ranjit', 'Madivila', '5b87e6fce8456eeabb800eb705ab3ca2.png', 0, 'Ranjit Madivila  address', '0', '0', '<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Jewellery\r\nruns in Ranjit’s blood; his father made gold jewellery in Jaffna, and his son\r\nis now a qualified gemmologist. With over forty-two years of experience in\r\nsales, Ranjit is one of our longest-serving employees and a real treasure to\r\nthe company. Over the decades he has learnt five languages: Chinese is his\r\nfavourite, but he also speaks English, French, German and Italian.<o:p></o:p></span></p>', '2'),
(2, 'P.B. ', 'Kaduruwewa', 'f92519ae3335361c518d3f7dca135eca.png', 0, 'P.B.  Kaduruwewa address', '0', '0', '<div><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Another\r\nstalwart in the sales team, Kaduruwewa is known for his expert knowledge which\r\nstems from his background as a gemmologist, as well as his thirty-seven years\r\nof experience at Hemachandras.. Kaduruwewa himself wears a ring set with a\r\nyellow sapphire which he bought as a rough stone. Astrologically, he believes\r\nit brings him good fortune.&nbsp;<o:p></o:p></span></p></div><div><br></div>', '1'),
(3, 'Sripathi ', 'Senanayake', '604434ed6bbb6c445fd3e184ddf3e333.png', 0, 'Sripathi Senanayake addres', '0', '0', '<div><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Sripathi’s\r\npassion for gemstones began early in life. He pinpoints the birth of his\r\ncuriosity to a time when he visited the mines in Ratnapura as a child. Sripathi\r\nis our senior gemmologist and has been with us for the past thirty-five years. He\r\nis particularly fond of the blue sapphire because of its beautiful lustre,\r\nvivid colour and hardness. His favourite part of the job is gem testing; he\r\nloves seeing what he describes as ‘the internal world of gemstones’.&nbsp;<o:p></o:p></span></p></div>', '0'),
(4, 'D.C. Sanath ', 'Deepal', 'a45f2d8394cb88986d82f1ad5848dfab.png', 0, '0', '0', 'default@gmail.com', '<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Deepal\r\ndescends from a long line of goldsmiths – his father and grandfather both\r\ncrafted intricate gold jewellery in the southern city of Galle. His father\r\nmoved to Kandy and was an integral part of the Hemachandras’ workshop. This\r\nheritage is clearly important to Deepal. He wears an intricate, gold-filigree\r\nring made by his father set with a diamond. He explains that white stones help\r\nto keep the wearer cool and calm. During his thirty years at Hemachandras, Deepal\r\nhas learnt to speak eight languages allowing him to communicate freely with our\r\ncustomers who visit from around the world.&nbsp;<o:p></o:p></span></p>', '0'),
(5, 'Gayan ', 'Danthanarayana', '4c9e42de55a7595f194fe2cdf6fd9e4e.png', 0, '0', '0', 'default@gmail.com', '<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Gayan’s\r\nfather learnt how to make jewellery from his mother at their family home in\r\nGalle. This was unusual, particularly at the time in the 1950s. Since the age\r\nof ten, Gayan has been creating jewellery, beginning with pieces of copper his\r\nfather gave him to practise with. Today he is one of our most highly skilled goldsmiths.\r\nOver the past fifteen years, he has honed his craft and become expert at making\r\nthe cluster design which emblematises our signature style. It takes him 2-3\r\ndays to make a pair of cluster earrings; Gayan enjoys the satisfaction of\r\nfinishing pieces explaining how much care and attention is needed.<o:p></o:p></span></p>', '0'),
(6, 'Nishanka ', 'H.G. V ', 'add9bfe2bd48834abaf2815dedd6f592.png', 0, '0', '0', 'default@gmail.com', '<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Patience and\r\nspeed are two of the skills Nishanka attributes to his ability to craft fine\r\ngold jewellery. It’s a challenging combination but one that he has mastered and\r\nuses to make highly detailed pieces. One of the most memorable works he’s\r\ncreated for us includes a four-petalled peridot, tsavorite and white sapphire\r\nflower that formed the centrepiece of a necklace. Nishanka enjoys having the\r\nfreedom and independence to make a living from a skill which he’s developed\r\nover the past three decades. Despite his expert knowledge, he explains that\r\nhe’s still honing his skill and improving every day.<o:p></o:p></span></p>', '0'),
(7, 'Chinthana ', 'Ranga Werellegama', '9b3516a36badc134a35605b2cec80208.png', 0, '0', '0', 'default@gmail.com', '<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">After a\r\ncareer in the army, Chinthana now enjoys the creativity of jewellery making\r\nwhich he describes as an artistic process. His grandfather and father were both\r\ngoldsmiths, and his uncle owned a jewellery store in Kandy so he’s returned to\r\na family tradition. Drawn to more contemporary designs, one of the pieces he’s\r\nmost proud of is a blue topaz pendant -its vivid colour accentuated by diamonds.&nbsp;<o:p></o:p></span></p>', '0'),
(8, 'S.K. Daminda', 'Samanalakumbu', '967c924473b478de74877225b55c2f9b.png', 0, '0', '0', 'default@gmail.com', '<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Known for his\r\ninnovation, Daminda explains that there is tension at the beginning of the\r\njewellery-making process and then an increasing satisfaction as a piece takes\r\nshape. To be a good designer, he believes an innate ability to imagine the\r\nfinal piece is needed. He describes how hand-making jewellery is such a unique\r\nprocess – you can give the same design to two different craftsmen and they will\r\ncreate two different pieces. At the company, he is renowned for creating a signature\r\nring set with four blue sapphires, each surrounded by diamonds.<o:p></o:p></span></p>', '0'),
(9, 'Dhanuka ', 'Sahanpriya', '88984ffa8c37d4eaff7a456f231c608c.png', 0, '0', '0', 'default@gmail.com', '<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">The resident\r\nexpert in statement pieces, Dhanuka is drawn to larger, more complex jewellery.\r\nHis accomplishments include a gem-studded peacock necklace which took him <span style=\"background:yellow;mso-highlight:yellow\">XXX</span> days to complete. He\r\nenjoys the independence of the jewellery-making process and explains how a\r\ncraftsman’s work can never be replicated by another.<o:p></o:p></span></p>', '0'),
(10, 'Mahinda ', 'Piyatilleke', 'dad2af6e0b704640d715ee426c0eb2d7.png', 0, '0', '0', 'default@gmail.com', '<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Mahinda\r\nenjoys a challenge, and never shies away from accepting an urgent or intricate\r\norder from our discerning customers, often working through the night to meet\r\nthese deadlines. He enjoys that each day of jewellery crafting is different –\r\nthere is always something new to learn and it’s never monotonous. Mahinda\r\nlearnt everything he knows from his father who worked for Hemachandras in the\r\n1960s and 70s.<o:p></o:p></span></p>', '0'),
(11, 'Krishanthi  ', 'Wittatchy', 'dc257c54b99e5383cc6daa9a8205ec9a.png', 0, '0', '0', 'default@gmail.com', '<span lang=\"EN-GB\" style=\"font-size:12.0pt;font-family:\r\n\" calibri\",\"sans-serif\";mso-ascii-theme-font:minor-latin;mso-fareast-font-family:=\"\" calibri;mso-fareast-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;=\"\" mso-bidi-font-family:calibri;mso-bidi-theme-font:minor-latin;color:black;=\"\" mso-themecolor:text1;mso-ansi-language:en-gb;mso-fareast-language:en-us;=\"\" mso-bidi-language:ar-sa\"=\"\">Famous for her thirty-four years of service, as well\r\nas her efficiency and attention to detail, Krishanthi is a real gem in the\r\nHemachandras team. She has been at the company so long that she describes the\r\nstore as ‘home’ and the directors as ‘her family’. Krishanthi originally joined\r\nthe team as a sales assistant but has progressed through the ranks and now\r\nbears the&nbsp;</span><span style=\"color: black;\">responsibility\r\nof Head Cashier. She enjoys training the younger staff members and is pleased\r\nto see more women joining the team.</span><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\"><o:p></o:p></span></p>', '0');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `product_description` text DEFAULT NULL,
  `product_matterials` text DEFAULT NULL,
  `product_journey` text DEFAULT NULL,
  `product_catagory` int(10) DEFAULT NULL,
  `product_sub_catagory` int(10) DEFAULT NULL,
  `collection_page` int(10) DEFAULT NULL,
  `collection_sub_page` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_description`, `product_matterials`, `product_journey`, `product_catagory`, `product_sub_catagory`, `collection_page`, `collection_sub_page`) VALUES
(16, 'CLUSTERS', 'Anatomatal threaded and threadless Gem Clusters allow a personal look to your piercings', 'test', 'klsndkgaskg', 2, 1, 1, 5),
(19, 'CLUSTERS', 'Anatomatal threaded and threadless Gem Clusters allow a personal look to your piercings', 'sdgsd', 'adgsdg', 2, 1, 1, 2),
(20, 'CLUSTERS', 'Anatomatal threaded and threadless Gem Clusters allow a personal look to your piercings', 'asgsdg', 'aegas', 2, 1, 1, 2),
(23, 'CLUSTERS', 'Anatomatal threaded and threadless Gem Clusters allow a personal look to your piercings', 'hytyjt', '<br>', 2, 1, 1, 6),
(24, 'Diamond ring', 'test tst test tst', 'Diamond', '<br>', 2, 1, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`category_id`, `category_name`, `status`) VALUES
(1, 'Necklaces', 1),
(2, 'Rings', 1),
(3, 'Bracelets', 1),
(4, 'Test 445', 1),
(5, 'Test 555', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_images_id` int(11) NOT NULL,
  `product_images_name` varchar(100) NOT NULL,
  `product_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`product_images_id`, `product_images_name`, `product_product_id`) VALUES
(31, '1558517351product3.png', 16),
(32, '1558517525BC-logo.png', 17),
(34, '1558517667product3.png', 19),
(36, '1558518053product4.png', 20),
(37, '1559796819Collection_04 (2).jpg', 21),
(38, '1559797835diamond.png', 22),
(39, '15598002621559797835diamond.png', 23),
(40, '15598286221558518053product4.png', 24),
(41, '155982872715598002621559797835diamond.png', 25);

-- --------------------------------------------------------

--
-- Table structure for table `product_sub_catagory`
--

CREATE TABLE `product_sub_catagory` (
  `sub_catagory_id` int(11) NOT NULL,
  `sub_catagory` varchar(100) NOT NULL,
  `catagory_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_sub_catagory`
--

INSERT INTO `product_sub_catagory` (`sub_catagory_id`, `sub_catagory`, `catagory_id`) VALUES
(1, 'Diamond/Gold', 2),
(2, 'gold', 3),
(3, 'test sub', 5);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `services_id` int(11) NOT NULL,
  `services_name` text DEFAULT NULL,
  `services_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`services_id`, `services_name`, `services_description`) VALUES
(2, 'Jewellery Repair', '\r\n\r\n\r\n<style type=\"text/css\">\r\np.p1 {margin: 0.0px 0.0px 10.0px 0.0px; line-height: 45.5px; font: 18.0px Arial; color: #50585f; -webkit-text-stroke: #50585f}\r\np.p2 {margin: 0.0px 0.0px 0.0px 0.0px; line-height: 18.0px; font: 15.0px Verdana; color: #5e5e5e; -webkit-text-stroke: #5e5e5e}\r\nli.li3 {margin: 0.0px 0.0px 7.5px 0.0px; line-height: 18.0px; font: 15.0px Verdana; color: #5e5e5e; -webkit-text-stroke: #5e5e5e}\r\nspan.s1 {font-kerning: none}\r\nul.ul1 {list-style-type: disc}\r\n</style>\r\n\r\n\r\n<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Blue Box: </span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\ncolor:black;mso-themecolor:text1;mso-bidi-font-weight:bold\">We wish to offer\r\nyou the ultimate ease of service whether you would like to customise a gift for\r\na loved one, order a special piece online or ask us a question about how to\r\ncare for your jewels. Our expert team look forward to giving you the best\r\npossible shopping and after-care experience.<b><o:p></o:p></b></span></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">&nbsp;</span></b></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Customise – </span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\ncolor:black;mso-themecolor:text1;mso-bidi-font-weight:bold\">For a truly unique\r\ngift, any of our pieces can be customised using the precious metal and stone of\r\nyour choice. With the personalised assistance of our in-house team, you can\r\nalso create your own bespoke design.<o:p></o:p></span></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">&nbsp;</span></b></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Order online\r\n– </span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:\r\nminor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:bold\">All of\r\nthe pieces displayed here are available for purchase with prices provided on\r\nrequest. Please e-mail us at order@hemachandras.com and one of our friendly\r\nteam will get in touch to assist you with the process. For further details,\r\nplease see our shipping and insurance pages.<o:p></o:p></span></p><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\">&nbsp;</span></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">After-sales</span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\ncolor:black;mso-themecolor:text1;mso-bidi-font-weight:bold\"> - Our commitment\r\nis to provide you with the highest level of jewellery care services. Our team\r\nis well-equipped to offer you expert advice on how to personalize your jewels,\r\nrestore them or simply preserve their beauty and longevity.&nbsp;All our pieces\r\ncome with a lifetime guarantee.<b> </b>Should you have any questions or need\r\nmore information, we invite you to get in touch (Link to inquiries) or visit us\r\nin store.<o:p></o:p></span></p><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\">&nbsp;</span></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Cleaning and\r\nPolishing</span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\">: Depending on the piece and signs of wear it shows, we may recommend\r\nsimple cleaning, ultrasonic cleaning or polishing to remove scratches and\r\npreserve the shine. For jewels in white gold, the polishing service includes\r\nrhodium plating to enhance the brilliance of the metal. For each service\r\nperformed, a professional quality inspection ensures that each stone is safely\r\nset and that functional parts, such as clasps and safety locks, work properly.<o:p></o:p></span></p><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\">&nbsp;</span></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Engraving:</span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\ncolor:black;mso-themecolor:text1;mso-bidi-font-weight:bold\"> Jewellery can be\r\npersonalized by engraving a name, a date or a message on the precious metal\r\n(service subject to technical constraints and space availability). Engraving is\r\na discretionary service offered by our store if requested at the time of\r\npurchase.<o:p></o:p></span></p><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\">&nbsp;</span></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Re-sizing:</span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\ncolor:black;mso-themecolor:text1;mso-bidi-font-weight:bold\"> Performed on a\r\nbracelet, necklace, chain or ring, this service consists of increasing or\r\nreducing the size of the item within limitations. Some jewels, due to their\r\nunique design, cannot be re-sized. Please get in touch with us&nbsp;for advice\r\nand resizing options.<o:p></o:p></span></p><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\">&nbsp;</span></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Earring\r\nServices:</span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\"> Your earring clips can be adjusted for better comfort. The adjustment\r\nincreases or decreases the clip tension and gap, and therefore the hold on the\r\nearlobe. <o:p></o:p></span></p><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\">&nbsp;</span></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Plating\r\nServices:</span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\"> For plated jewellery which is showing signs of wear, we offer plating\r\nservices to restore the original finish. You can also to opt to have your\r\njewellery pieces plated to alter the appearance. <o:p></o:p></span></p><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\">&nbsp;</span></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Repair\r\nServices:</span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\"> Proper care and handling will protect the appearance of your jewellery\r\nover time. If your jewellery becomes damaged, please avoid wearing it until\r\nexamined by one of our staff in store. After a careful assessment by one of our\r\nexperts, a quotation will be given, and if accepted repairs and replacements\r\nwill be made. <o:p></o:p></span></p><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">&nbsp;</span></p><p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">(Grey bar) FAQs:</span></b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\"> Queries/concerns/clarifications to be directed to our\r\ninquires page.<o:p></o:p></span></p><p class=\"p1\">\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1;mso-bidi-font-weight:\r\nbold\">&nbsp;</span></p><ul class=\"ul1\">\r\n</ul>');

-- --------------------------------------------------------

--
-- Table structure for table `services_images`
--

CREATE TABLE `services_images` (
  `services_images_id` int(11) NOT NULL,
  `services_images_name` text NOT NULL,
  `services_services_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services_images`
--

INSERT INTO `services_images` (`services_images_id`, `services_images_name`, `services_services_id`) VALUES
(1, '1526533885IMG_0733.jpg', 2),
(21, '1525417458coverpage.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stones`
--

CREATE TABLE `stones` (
  `stones_id` int(11) NOT NULL,
  `stones_name` text DEFAULT NULL,
  `stones_sub_title` varchar(100) DEFAULT NULL,
  `stones_description` text DEFAULT NULL,
  `stones_status` int(5) DEFAULT NULL COMMENT '1-cut, 2-uncut'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stones`
--

INSERT INTO `stones` (`stones_id`, `stones_name`, `stones_sub_title`, `stones_description`, `stones_status`) VALUES
(9, 'Garnet', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">January<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Ranging from\r\nruby red and dark brown to pink and orange, garnets are energising stones,\r\nwhich add vitality to life. Synonymous with passion, they are believed to\r\ninspire love and deepen personal relationships. As warming, protective gems\r\nlinked to the heart chakra, they are often used to mark &nbsp;2<sup>nd</sup> anniversaries.<o:p></o:p></span></p>', 2),
(10, 'Amethyst', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">February<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">A variety of\r\nquartz, amethyst’s vibrant wine-purple hue means it has been linked with luxury\r\nsince ancient times when it was used to adorn royal crowns and insignia.\r\nRanging from pale violet to deep lavender in shade, this majestic 6<sup>th</sup>\r\nanniversary stone is deeply connected to beliefs of abundance and prosperity. Amethyst\r\nalso has a calming and spiritual quality.<o:p></o:p></span></p>', 1),
(11, 'Blue Sapphire', NULL, '<span style=\"color: #808080;font-family: Consolas, \" lucida=\"\" console\",=\"\" \"courier=\"\" new\",=\"\" monospace;=\"\" font-size:=\"\" 12px;=\"\" white-space:=\"\" pre-wrap;\"=\"\">The deep blue of the Sapphire protects any Virgo who should wear it. This stone is quite impervious to damage – and you will be too. </span>', 2),
(12, 'Blue Sapphire', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">April<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Purity and\r\nclarity are two mesmerising qualities of this resplendent sapphire. A\r\ncolourless sapphire, commonly described as white, it is linked to the higher\r\nmind. The stone is believed to have the power to enhance wisdom, as well as\r\nopen thought channels. White sapphires are also thought to be protective\r\nstones.<b><o:p></o:p></b></span></p>', 1),
(13, 'Blue Sapphire', NULL, '<span style=\"color: #808080;font-family: Consolas, \" lucida=\"\" console\",=\"\" \"courier=\"\" new\",=\"\" monospace;=\"\" font-size:=\"\" 12px;=\"\" white-space:=\"\" pre-wrap;\"=\"\">The deep blue of the Sapphire protects any Virgo who should wear it. This stone is quite impervious to damage – and you will be too. </span>', 2),
(14, 'Cat’s Eye', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">June<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Long associated\r\nwith protection, this mystical stone is believed to have the power to enhance\r\ncreativity and positivity. Cat’s Eye is found in shades as varied as golden\r\nyellow, yellowish brown and green. Chatoyancy - the distinctive narrow band of\r\nconcentrated light that falls across the surface - is what imbues the gem with\r\nsuch a mesmerising quality. Cat’s Eye also displays another impressive effect\r\nknown as ‘milk and honey’: when light is shone over the chatoyant band, the\r\nside nearest the light shows the stone’s original body colour while the other\r\nside has a milky appearance.<o:p></o:p></span></p>', 0),
(15, 'Moonstone ', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">June<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">The\r\nadularescent glow of the moonstone is reminiscent of a full moon shining\r\nthrough lightly parted clouds. Hindu mythology describes the stone as made of\r\nsolidified moonbeams. Believed to bring good luck, in some cultures the\r\nethereal gem is believed to show the future if held under the light of the\r\nmoon. Ranging from semi-transparent to opaque, moonstone occurs in hues as\r\nvaried as colourless, white, blue, grey, light green, yellow and even black.<o:p></o:p></span></p>', 0),
(16, 'Ruby', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">July<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Associated\r\nwith the Sun, as well as the element of fire, this deep red stone is believed\r\nto inspire passion, joy and confidence. </span><span lang=\"EN-GB\" style=\"mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">It </span><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;\r\ncolor:black;mso-themecolor:text1\">has been loved by rulers and royalty for\r\ncenturies. Although the gems are rarely found on the island today, according to\r\nlegend, it was a Sri Lankan ruby that was given by King Solomon to the Queen of\r\nSheba. In the thirteenth century, Marco Polo, the medieval Italian explorer,\r\ndescribed a fabulous ruby – “about a palm in length and of the thickness of a\r\nman’s arm” set in the spire of the Ruvanvelisaya dagoba in\r\nAnuradhapura.&nbsp;The ruby is a 15<sup>th</sup> and 40<sup>th</sup>\r\nanniversary stone.<o:p></o:p></span></p>', 0),
(17, 'Star Ruby', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">July<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">According to Eastern folklore, these opulent, highly-prized\r\ngems bring good fortune to their owners. Occurring in shades that range from\r\nlight to dark and even purple-red they display a stunning phenomenon called\r\nasterism: tiny inclusions cause light to scatter across the surface, normally\r\ndisplaying as six ethereal rays.&nbsp;<o:p></o:p></span></p>', 0),
(18, 'Peridot', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">August<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Peridot has\r\nalways been associated with light. The ancient Egyptians called it the ‘gem of\r\nthe sun’. At this time, some believed that it protected its owner from ‘terrors\r\nof the night’, especially when set in gold. While the intensity of the majestic\r\nolive-green shade can vary, peridot is one of the few gemstones that occurs in\r\nonly one colour. The radiance of the gem lends to its association with\r\npositivity and balance. Long famed for its healing properties, this 16<sup>th</sup>\r\nanniversary stone is believed to enhance compassion, health and peace.&nbsp;<o:p></o:p></span></p>', 0),
(19, 'Blue Sapphire', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">September<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Sri Lanka has\r\nbeen famous for millennia for producing some of the world’s largest and best\r\nblue sapphires, many of which decorate the crown jewels of monarchies around\r\nthe world. The quality, lustre and deep blue of the Ceylon sapphire is\r\nunparalleled. Linked to the planet Saturn, the regal gem is symbolic of good\r\nluck and wealth. Added to their association with strength and powerful\r\nself-expression, this makes blue sapphires a fitting 5<sup>th</sup> and 45<sup>th</sup>\r\nanniversary gift.<o:p></o:p></span></p>', 0),
(20, 'Star Sapphire', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">September<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">The mystical\r\nstar sapphire is believed by many to be a stone of destiny – one that acts as a\r\nguiding light and protects its wearer against harm. Ranging from\r\nsemi-transparent to opaque, they come in a spectrum of colours with green,\r\nyellow and orange being the rarest.<o:p></o:p></span></p>', 0),
(21, 'Tourmaline', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">October<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">A stone of\r\ninspiration and happiness, tourmaline derives its name from ‘toramalli’ meaning\r\n‘mixed gems’ in Sinhala, one of Sri Lanka’s national languages. Few other gems\r\ncan match the tourmaline’s dazzling rainbow of colours, which range from rich\r\nreds to pastel pinks, peaches, intense greens, vivid yellows and deep blues. Uniquely,\r\ntourmalines can have colour zones, including pink and green, which is a variety\r\ncommonly known as watermelon tourmaline. In many cultures it is seen as a\r\nbalancing, cleansing gem which is given to mark 8<sup>th</sup> anniversaries.<o:p></o:p></span></p>', 0),
(22, 'Yellow sapphire', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">November<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:\r\nCalibri;mso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Long\r\nassociated with the planet Jupiter, the stunning yellow sapphire stimulates the\r\nintellect and fulfilment of ambition. In Sri Lankan culture, the stone’s\r\nillustrious qualities are also believed to attract wealth and prosperity.<o:p></o:p></span></p>', 1),
(23, 'Blue Topaz', NULL, '<p class=\"MsoNormal\"><b><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:black;\r\nmso-themecolor:text1\">December<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Linked to the\r\nelement of air with its sky-blue shades, this ethereal topaz is a 4<sup>th</sup>\r\nanniversary gemstone. The stone has calming properties which promote\r\nrelaxation. Believed to aid clear communication, topaz is also connected to\r\ntruth and wisdom. For centuries, many people in the Indian subcontinent have\r\nbelieved that a topaz worn above the heart assures long life, beauty, and\r\nintelligence.<o:p></o:p></span></p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stones_images`
--

CREATE TABLE `stones_images` (
  `stones_images_id` int(11) NOT NULL,
  `stones_images_name` text NOT NULL,
  `stones_stones_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stones_images`
--

INSERT INTO `stones_images` (`stones_images_id`, `stones_images_name`, `stones_stones_id`) VALUES
(26, '1573708437rhodolite-garnet-gemstone-500x500.jpg', 9),
(28, '1573708702amethyst-stone-500x500.gif', 10),
(29, '1573708775asd.jpg', 11),
(30, '1573708953white_sapphire.jpg', 12),
(31, '1573709087a470c834460a5f5d9e23c26f108be693.jpg', 13),
(32, '1573709182cats-eye.png', 14),
(33, '157370928351467541-tumbled-blue-moonstone-adularia-natural-mineral-gem-stone-isolated-on-white-background.jpg', 15),
(34, '1573709341gemstone-172548-ruby-pear-red-cf763.jpg', 16),
(35, '15737094253952bca90ae2b5178cb94bfd5dad118c.jpg', 17),
(36, '1573709497peridot-gemstone-500x500.jpg', 18),
(37, '1573709571blue-sapphire-gemstone-500x500.jpg', 19),
(38, '1573709646593a23a5ea03b.jpg', 20),
(39, '15737097275d1f9ced7e274.jpg', 21),
(43, '1573710041yellow-sapphire-gemstone.jpg', 22),
(44, '1573710129bluetopaz_famous.jpg', 23);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `stores_id` int(11) NOT NULL,
  `stores_title` varchar(100) NOT NULL,
  `stores_address` text NOT NULL,
  `stores_telephone_number` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longtuide` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`stores_id`, `stores_title`, `stores_address`, `stores_telephone_number`, `latitude`, `longtuide`) VALUES
(1, 'Blue box:', '<p class=\"MsoNormal\"><span style=\"color: black;\">Flagship\r\nStore</span><br></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Flower Road<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Colombo 7<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span lang=\"EN-GB\" style=\"mso-bidi-font-family:Calibri;\r\nmso-bidi-theme-font:minor-latin;color:black;mso-themecolor:text1\">Sri Lanka<o:p></o:p></span></p>', '+94112564896', '210.3256', '12.032569'),
(2, 'Jewel Villa', 'No 7,<div>Ward Place,</div><div>Colombo 7,</div><div>Sri Lanka.</div>', '0114589632', '100.2356', '250.3258');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `best_practices`
--
ALTER TABLE `best_practices`
  ADD PRIMARY KEY (`best_practices_id`);

--
-- Indexes for table `best_practices_images`
--
ALTER TABLE `best_practices_images`
  ADD PRIMARY KEY (`best_practices_images_id`);

--
-- Indexes for table `celebrate_images`
--
ALTER TABLE `celebrate_images`
  ADD PRIMARY KEY (`celebrate_image_id`);

--
-- Indexes for table `collection`
--
ALTER TABLE `collection`
  ADD PRIMARY KEY (`collection_id`);

--
-- Indexes for table `collection_images`
--
ALTER TABLE `collection_images`
  ADD PRIMARY KEY (`collection_image_id`);

--
-- Indexes for table `collection_page_main_category`
--
ALTER TABLE `collection_page_main_category`
  ADD PRIMARY KEY (`celebrate_id`);

--
-- Indexes for table `collection_page_sub_catagory`
--
ALTER TABLE `collection_page_sub_catagory`
  ADD PRIMARY KEY (`page_sub_catagory_id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `faq_category`
--
ALTER TABLE `faq_category`
  ADD PRIMARY KEY (`faq_category_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `main_banner`
--
ALTER TABLE `main_banner`
  ADD PRIMARY KEY (`main_banner_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `news_images`
--
ALTER TABLE `news_images`
  ADD PRIMARY KEY (`news_image_id`);

--
-- Indexes for table `notification_bar`
--
ALTER TABLE `notification_bar`
  ADD PRIMARY KEY (`notification_bar_id`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`people_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_images_id`);

--
-- Indexes for table `product_sub_catagory`
--
ALTER TABLE `product_sub_catagory`
  ADD PRIMARY KEY (`sub_catagory_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`services_id`);

--
-- Indexes for table `services_images`
--
ALTER TABLE `services_images`
  ADD PRIMARY KEY (`services_images_id`);

--
-- Indexes for table `stones`
--
ALTER TABLE `stones`
  ADD PRIMARY KEY (`stones_id`);

--
-- Indexes for table `stones_images`
--
ALTER TABLE `stones_images`
  ADD PRIMARY KEY (`stones_images_id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`stores_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `best_practices`
--
ALTER TABLE `best_practices`
  MODIFY `best_practices_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `best_practices_images`
--
ALTER TABLE `best_practices_images`
  MODIFY `best_practices_images_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `celebrate_images`
--
ALTER TABLE `celebrate_images`
  MODIFY `celebrate_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `collection`
--
ALTER TABLE `collection`
  MODIFY `collection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `collection_images`
--
ALTER TABLE `collection_images`
  MODIFY `collection_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `collection_page_main_category`
--
ALTER TABLE `collection_page_main_category`
  MODIFY `celebrate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `collection_page_sub_catagory`
--
ALTER TABLE `collection_page_sub_catagory`
  MODIFY `page_sub_catagory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `faq_category`
--
ALTER TABLE `faq_category`
  MODIFY `faq_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `main_banner`
--
ALTER TABLE `main_banner`
  MODIFY `main_banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `news_images`
--
ALTER TABLE `news_images`
  MODIFY `news_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `notification_bar`
--
ALTER TABLE `notification_bar`
  MODIFY `notification_bar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `people_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `product_images_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `product_sub_catagory`
--
ALTER TABLE `product_sub_catagory`
  MODIFY `sub_catagory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `services_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services_images`
--
ALTER TABLE `services_images`
  MODIFY `services_images_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `stones`
--
ALTER TABLE `stones`
  MODIFY `stones_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `stones_images`
--
ALTER TABLE `stones_images`
  MODIFY `stones_images_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `stores_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
