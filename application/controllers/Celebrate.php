<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Celebrate extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->check_isvalidated();
}

// **************************************** CELEBRATE SECTION'S NAME CHANGED TO COLLECTION PAGE CATAGORY SECTION ***********************************************************

    public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
        $this->load->view('admin/components/header', $headerData);
        $this->load->view('admin/components/sidebar', $sidebarData);
        $this->load->view($page, $mainData);
        $this->load->view('admin/components/footer', $footerData);
    }

    //validate session start
    private function check_isvalidated(){
        if(!$this->session->userdata('validated')){
            redirect('login');
        }
    }
    //validate session end
    //add celebrate function start --ishara sewwandi
    public function addcelebrate() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/celebrate/addcelebrate';
        $mainData = array(
            'pagetitle' => 'Add New Collection Page Catagory');
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //add celebrate function end --ishara sewwandi

    //insert celebrate function start --ishara sewwandi
    public function insertcelebrate() {
        $data = array(
            'celebrate_name' =>$this->input->post('home_page_celebrate_name'),
        );

        $data1 = $this->setting_model->insert($data, 'collection_page_main_category');

        $this->session->set_flashdata('msg','<div class="alert alert-success">Celebrate Added Successfully!</div>');

        redirect('celebrate/addcelebrate');
    }
    //insert celebrate function end --ishara sewwandi

    //View celebrate function start --ishara sewwandi
    public function viewcelebrate() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/celebrate/viewcelebrate';
        $mainData = array(
            'celebrate_name' => $this->setting_model->Get_All('collection_page_main_category')
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //View celebrate function end --ishara sewwandi

    //edit celebrate function start --ishara sewwandi
    public function editcelebrate() {
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/celebrate/editcelebrate';
        $mainData = array(
            'pagetitle' => 'Edit Collection Catagory',
            'celebrate' => $this->setting_model->Get_Single('collection_page_main_category','celebrate_id',$id)
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    //edit celebrate function end --ishara sewwandi

    //update celebrate function start --ishara sewwandi
    public function updatecelebrate() {

        $id = $this->uri->segment(3);
        $data = array(
            'celebrate_name' =>$this->input->post('celebrate_name'),
        );

        $data1 = $this->setting_model->update($data,'collection_page_main_category','celebrate_id',$id);

        redirect('celebrate/viewcelebrate');
    }
    //update celebrate function end --ishara sewwandi

    //delete celebrate function start --ishara sewwandi
    public function deletecelebrate(){
        $celebrate_id = $this->input->post('id');
        $this->setting_model->delete('collection_page_main_category','celebrate_id',$celebrate_id);
        redirect('celebrate/viewcelebrate');
    }
    //delete celebrate function end --ishara sewwandi
    
    //page sub catagory section ---------------------------------------------------------------------------------------------- coded by gihan
    
    public function addPageSubCat() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/celebrate/addsubcatagory';
        $mainData = array(
            'pagetitle' => 'Add New Collection Sub catagory',
            'celebrate' => $this->setting_model->Get_All('collection_page_main_category')
            );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    
    public function insertpageSubCatagory(){
        $data = array(
            'page_catagory_name' => $this->input->post('sub_catagory_name'),
            'page_catagory_id' => $this->input->post('pageCatagory')
        );
        
        $this->setting_model->insert($data,'collection_page_sub_catagory');
        
        $this->session->set_flashdata('msg','<div class="alert alert-success">Page Sub Catagory Added Successfully!</div>');
        redirect('celebrate/addPageSubCat');
        
    }
    
    //View celebrate function start ---------------------------------------------------------------------------------------- coded by Gihan 
    public function viewPageSubCatagory() {
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/celebrate/viewSubCatagory';
        $mainData = array(
            'pagetitle' => 'View Page Sub catagory',
            'celebrate_name' => $this->setting_model->Get_All('collection_page_main_category'),
            'pageSubCatagory' => $this->setting_model->Get_All('collection_page_sub_catagory')
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    
    public function editPageSubcategory(){
        $id = $this->uri->segment(3);
        $headerData = null;
        $sidebarData = null;
        $page = 'admin/celebrate/editPageSubCatagory';
        $mainData = array(
            'pagetitle' => 'Edit Collection Sub catagory',
            'celebrate_name' => $this->setting_model->Get_All('collection_page_main_category'),
            'pageSubCatagory' => $this->setting_model->Get_Single('collection_page_sub_catagory','page_sub_catagory_id',$id)
        );
        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }
    
    public function updateSubCatagory(){
        $id = $this->uri->segment(3);
        $data = array(
            'page_catagory_name' =>$this->input->post('sub_catagory_edit'),
            'page_catagory_id' =>$this->input->post('categoryname_edit'),
        );

        $data1 = $this->setting_model->update($data,'collection_page_sub_catagory','page_sub_catagory_id',$id);

        redirect('celebrate/viewPageSubCatagory');
        
    }
    // delete celebrate function start --ishara sewwandi
    // public function deletesubcelebrate(){
    //     $celebrate_id = $this->input->post('id');
    //     $this->setting_model->delete('collection_page_sub_catagory','page_sub_catagory_id',$celebrate_id);
    //     redirect('celebrate/viewPageSubCatagory');
    // }
    
}