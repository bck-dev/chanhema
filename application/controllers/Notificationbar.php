<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificationbar extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	//Start Template
	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}
	//End Template

	//edit notification function start
	public function editnotification() {
		$id = $this->uri->segment(3);
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/notification/editnotification';
		$mainData = array(
			'pagetitle' => 'Edit Notification Bar',
			'notification_bar' => $this->setting_model->Get_All('notification_bar')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}
	//edit notification function end

	//update notification function start
	public function updatnotification() {
		$id = $this->uri->segment(3);
		$data = array(
			'notification_bar_description'=>$this->input->post('notofocation_description'),
			'notification_bar_status'=>$this->input->post('notification_radio')
		);
		$this->setting_model->update($data,'notification_bar','notification_bar_id',$id);
		$this->session->set_flashdata('msg','<div class="alert alert-success">Notification Bar Updated Successfully!</div>');
		redirect('notificationbar/viewnotification');
	}
	//update notification function end

	//view notification function start
	public function viewnotification() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/notification/viewnotification';
		$mainData = array(
			'pagetitle' => 'View Notification Bar',
			'notification_bar' => $this->setting_model->Get_All('notification_bar')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}
	//view notification function end
}