<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubCatagories extends CI_Controller{

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

//============== start add collcetion function =====================Gihan Kaveendra==============

	public function addsubcatagory() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/sub_catagory/addnewsubcatagory';
		$mainData = array(
			'pagetitle' => 'Add New Material',
			// 'catagory' => $this->setting_model->Get_All('product_category')
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
//=========================== end add collection function ==========================

//============================Start insert collection function ======Gihan Kaveendra=====

	public function insertsubcatagory() {
		$data = array(
		    
			// 'catagory_id' => $this->input->post('catagory'),
			'sub_catagory' =>$this->input->post('sub_catagory')
			);

		$data1 = $this->setting_model->insert($data, 'product_sub_catagory');

		$this->session->set_flashdata('msg','<div class="alert alert-success">Material Added Successfully!</div>');

		redirect('SubCatagories/addsubcatagory');
	}
//==============================End insertcollection function ========Gihan kaveendra=====

//==============================Start View collection function ========Gihan Kaveendra====

	public function viewSubCatagory() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/sub_catagory/viewsubcatagory';
		$mainData = array(
			'pagetitle' => 'View Material',
			'sub_cat' => $this->setting_model->Get_All('product_sub_catagory'),
			// 'catagory' => $this->setting_model->Get_All('product_category')
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}
//=============================End View collection function ===========Gihan Kaveendra======

//=============================Start edit collection function =========Gihan Kaveendra======
	public function editSubcategory() {
		$id = $this->uri->segment(3);
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/sub_catagory/editsubcatagory';
		$mainData = array(
			'pagetitle' => 'Edit Material',
			'sub_cat' => $this->setting_model->Get_Single('product_sub_catagory','sub_catagory_id',$id),
			// 'catagory1' => $this->setting_model->Get_All('product_category')
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}
//============================End edit collectin function ============= Gihan kaveeendra====

//============================Start Update Collection function ======== Gihan kaveendra=====

	public function updateSubcategory(){
	    
		$id = $this->uri->segment(3);
		$data = array(
			'sub_catagory' =>$this->input->post('categoryname_edit'),
			// 'catagory_id' =>$this->input->post('sub_catagory_edit')
			);
		
		$data1 = $this->setting_model->update($data,'product_sub_catagory','sub_catagory_id',$id);
		$this->session->set_flashdata('msg','<div class="alert alert-success">Material Updated Successfully!</div>');

		redirect('SubCatagories/viewSubCatagory');
	}
//===========================End update collection function============Gihan Kaveendra========

//===========================Start delete collection function==========Gihan Kaveendra========
	public function deleteSubcategory(){
		$id = $this->uri->segment(3);
		$this->setting_model->delete('product_sub_catagory','sub_catagory_id',$id);
		redirect('SubCatagories/viewSubCatagory');
	}
// ==========================End delete collection function=============Gihan Kaveendra========
}
