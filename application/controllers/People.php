<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class People extends CI_Controller{



    function __construct(){

        parent::__construct();

        $this->check_isvalidated();

    }



    public function template($headerData, $sidebarData, $page, $mainData, $footerData) {

        $this->load->view('admin/components/header', $headerData);

        $this->load->view('admin/components/sidebar', $sidebarData);

        $this->load->view($page, $mainData);

        $this->load->view('admin/components/footer', $footerData);

    }



//=========================validate session start======================ishara sewwandi

    private function check_isvalidated(){

        if(!$this->session->userdata('validated')){

            redirect('login');

        }

    }

//validate session end



//=========================add people function start=========================ishara sewwandi

    public function addpeople() {

        $headerData = null;

        $sidebarData = null;

        $page = 'admin/people/addpeople';

        $mainData = array(

            'pagetitle' => 'Add People');

        $footerData = null;



        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }

//add main banner function end



//================upload images and details to database for 'people' ============ishara sewwandi

    public function insertpeople(){

        $data = array(

            'people_image' =>$this->setting_model->uploadNew('people_image'),
            'people_first_name' =>$this->input->post('people_fname'),
            'people_last_name' =>$this->input->post('people_lname'),
            // 'people_age' =>$this->input->post('people_age'),
            // 'people_address' =>$this->input->post('people_address'),
            // 'people_contact_number' =>$this->input->post('telephone'),
            // 'people_email' =>$this->input->post('people_email'),
            'people_description' =>$this->input->post('people_description'),
            'people_category' =>$this->input->post('people_category'),

        );

        $this->setting_model->insert($data, 'people');

        $this->session->set_flashdata('msg','<div class="alert alert-success">Person Added Successfully!</div>');

        redirect('people/addpeople');

    }

//upload images and details to database for 'people' end



//===============view people==============ishara sewwandi

    public function viewpeople() {

        $headerData = null;

        $sidebarData = null;

        $page = 'admin/people/viewpeople';

        $mainData = array(

            'people' => $this->setting_model->Get_All('people'),

        );

        $footerData = null;



        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);

    }

//view people end



//===========================edit people==================================

    public function editpeople() {
        $id = $this->uri->segment(3);

        $headerData = null;
        $sidebarData = null;

        $page = 'admin/people/editpeople';

        $mainData = array(
            'pagetitle' => 'Edit People',
            'people'=> $this->setting_model->Get_Single('people','people_id', $id),
        );

        $footerData = null;

        $this->template($headerData, $sidebarData, $page, $mainData, $footerData);
    }

//edit dining end

//================update people function start===========================================ishara sewwandi

    public function updatepeople() {

        //============image upload start===============================================

        if(isset($_FILES['fileinput']) && empty($_FILES['fileinput']['tmp_name'])) {

            $image = $this->input->post('people_image');

        } else {

            $image = $this->setting_model->uploadNew('fileinput');

        }

        //image upload end

        $id = $this->uri->segment(3);

        $data = array(

            'people_image' =>$image,
            'people_first_name' =>$this->input->post('people_fname'),
            'people_last_name' =>$this->input->post('people_lname'),
            // 'people_age' =>$this->input->post('people_age'),
            // 'people_address' =>$this->input->post('people_address'),
            // 'people_contact_number' =>$this->input->post('telephone'),
            // 'people_email' =>$this->input->post('people_email'),
            'people_description' =>$this->input->post('people_description'),
            'people_category' =>$this->input->post('people_category'),

        );
    
        $this->setting_model->update($data,'people','people_id',$id);
        $this->session->set_flashdata('msg','<div class="alert alert-success">People Updated Successfully!</div>');
        redirect('people/viewpeople');

    }

    //update people function end

//==================delete  people function===========================ishara sewwandi

    public function deletepeople(){

        $people_id = $this->uri->segment(3);

        $this->setting_model->delete('people','people_id',$people_id);

        redirect('people/viewpeople');

    }



}



