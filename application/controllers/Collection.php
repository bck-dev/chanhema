<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collection extends CI_Controller{

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

//============== start add collcetion function =====================Gihan Kaveendra==============

	public function addcollection() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/collection/addcollection';
		$mainData = array(
			'pagetitle' => 'Add New Collection'
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
//=========================== end add collection function ==========================

//============================Start insert collection function ======Gihan Kaveendra=====

	public function insertcollection() {
	    $first_image = $this->setting_model->uploadNew('main_image');
		$second_image = $this->setting_model->uploadNew('second_image');
		$banner_image = $this->setting_model->uploadNew('banner_image');
		
		$data = array(
			'collection_name' => $this->input->post('collection_name'),
			'banner_image' => $banner_image,
			'first_image' => $first_image,
			'main_description' => $this->input->post('main_description'),
			'second_image' => $second_image,
			'second_description' => $this->input->post('second_description')
		);
	    
		$this->setting_model->insert($data, 'collection');

		$this->session->set_flashdata('msg','<div class="alert alert-success">Collection Added Successfully!</div>');

		redirect('collection/addcollection');
	}
//==============================End insertcollection function ========Gihan kaveendra=====

//==============================Start View collection function ========Gihan Kaveendra====

	public function viewcollection() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/collection/viewcollection';
		$mainData = array(
			'collections' => $this->setting_model->Get_All('collection'),
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}
//=============================End View collection function ===========Gihan Kaveendra======

//=============================Start edit collection function =========Gihan Kaveendra======
	public function editcollection() {
		$id = $this->uri->segment(4);
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/collection/editcollections';
		$mainData = array(
			'pagetitle' => 'Edit Page Collection',
			'collection' => $this->setting_model->Get_Single('collection','	collection_id',$id)
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}
//============================End edit collectin function ============= Gihan kaveeendra====

//============================Start Update Collection function ======== Gihan kaveendra=====

	public function updatecollection(){
		$banner_image = $this->setting_model->uploadNew('banner_image');
		$first_image = $this->setting_model->uploadNew('first_image');
		$second_image = $this->setting_model->uploadNew('second_image');
		
		$id = $this->uri->segment(3);

		$data = array(
			'collection_name' => $this->input->post('collection_name'),
			'main_description' => $this->input->post('main_description'),
			'second_description' => $this->input->post('second_description')
			);

		if($banner_image!=false){
			$data['banner_image'] = $banner_image;
		}

		if($first_image!=false){
			$data['first_image'] = $first_image;
		}

		if($second_image!=false){
			$data['second_image'] = $second_image;
		}
		
		$this->setting_model->update($data,'collection','collection_id',$id);
		// echo $this->db->last_query();
		$this->session->set_flashdata('msg','<div class="alert alert-success">Collection Updated Successfully!</div>');

		redirect('collection/viewcollection');
	}
//===========================End update collection function============Gihan Kaveendra========

//===========================Start delete collection function==========Gihan Kaveendra========
	public function deletecollection($collection_id){
		
		$this->setting_model->delete('collection','collection_id',$collection_id);
		redirect('collection/viewcollection');
	}
// ==========================End delete collection function=============Gihan Kaveendra========
}
