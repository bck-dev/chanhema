<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller{

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

	//Start add place
	public function addnews(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/news/addnews';
		$mainData = array(
			'pagetitle' => 'Add New News'
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	//End add place
//============================Start Insert store function=========== Gihan Kaveendra==========
	public function insertnews(){
		$data = array(
			'news_title' =>$this->input->post('news_title'),
			'date' =>$this->input->post('date'),
			'news_description' =>$this->input->post('news_description'),
			'news_images' =>$this->setting_model->upload('news_images', 'upload/news'),
			
			);

		$data1 = $this->setting_model->insert($data, 'news');
		
		 $files = $_FILES;
                        $path = "news";
                        $fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
                        $this->multiple_image_uplaod->upload_image_news($fileName,$data1);


		$this->session->set_flashdata('msg','<div class="alert alert-success">Store Added Successfully!</div>');

		redirect('News/addnews');
	}
//===========================End insert store function============== Gihan Kaveendra=============

//===========================Start View store function============== Gihan Kaveendra=============
	public function viewnews() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/news/viewnews';
		$mainData = array(
			'news' => $this->setting_model->Get_All('news'),
			'news_image' => $this->setting_model->Get_All('news_images')
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}
//==========================End View store function================ Gihan Kaveendra==============

//==========================Start edit store function============== Gihan Kaveendra==============
	public function editnews() {
		$id = $this->uri->segment(3);
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/news/editnews';
		$mainData = array(
			'pagetitle' => 'Edit News',
			'news' => $this->setting_model->Get_Single('news','news_id',$id),
			'news_images' => $this->multiple_image_uplaod->edit_data_image_news($id),
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}
//========================End edit store function ================== Gihan Kaveendra============

//========================Start update store function ============== Gihan Kaveendra============
	public function updatenews(){
		$id = $this->uri->segment(3);
		$data = array(
			'news_title' =>$this->input->post('news_title'),
			'date' =>$this->input->post('date'),
			'news_description' =>$this->input->post('news_description'),
			'news_images' =>$this->setting_model->upload('news_images', 'upload/news'),
			);
			
			//edit multiple image upload
        if(count($_FILES['userfile']['name'])>1){
        $files = $_FILES;
        $path = "news";
        $fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
        $this->multiple_image_uplaod->edit_upload_image_news($fileName,$id);
        //end multiple image upload edit
        }
		
		$this->setting_model->update($data,'news','news_id',$id);
		$this->session->set_flashdata('msg','<div class="alert alert-success">News Updated Successfully!</div>');

		redirect('News/viewnews');
	}
//======================End update store function =================== Gihan Kaveeendra===========

//======================Start delete store function ================= Gihan Kaveendra============

	public function deletenews(){
		$news_id = $this->uri->segment(3);
		$this->setting_model->delete('news','news_id',$news_id);
		$this->setting_model->delete('news_images','news_news_id',$news_id);
		redirect('News/viewnews');
	}
//=======================End delete store function ================== Gihan Kaveendra============

	public function deleteImage(){
		$deleteid  = $this->input->post('id');
		$this->db->delete('news_images', array('news_image_id' => $deleteid)); 
		$verify = $this->db->affected_rows();
		echo $verify;
	}
}