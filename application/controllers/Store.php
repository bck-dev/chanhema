<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller{

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

	//Start add place

	public function addstore(){

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/stores/addstore';

		$mainData = array(
			'pagetitle' => 'Add New Store'
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	//End add place

//============================Start Insert store function=========== Gihan Kaveendra==========

	public function insertstore(){

		$data = array(
			'stores_title' =>$this->input->post('placetitle'),
			'stores_address' =>$this->input->post('address'),
			'stores_telephone_number' =>$this->input->post('telephone'),
			'latitude' =>$this->input->post('placelatitude'),
			'longtuide' =>$this->input->post('placelongitude'),
			);

		$this->setting_model->insert($data, 'stores');

		$this->session->set_flashdata('msg','<div class="alert alert-success">Store Added Successfully!</div>');
		redirect('Store/addstore');
	}

//===========================End insert store function============== Gihan Kaveendra=============
//===========================Start View store function============== Gihan Kaveendra=============

	public function viewstore() {

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/stores/viewstore';

		$mainData = array(
			'stores' => $this->setting_model->Get_All('stores')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}

//==========================End View store function================ Gihan Kaveendra==============
//==========================Start edit store function============== Gihan Kaveendra==============

	public function editstore() {
		$id = $this->uri->segment(3);

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/stores/editstore';

		$mainData = array(
			'pagetitle' => 'Edit Store',
			'store' => $this->setting_model->Get_Single('stores','stores_id',$id)
			);

		$footerData = null;
		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}

//========================End edit store function ================== Gihan Kaveendra============
//========================Start update store function ============== Gihan Kaveendra============
	public function updatestore(){
		$id = $this->uri->segment(3);

		$data = array(
			'stores_title' =>$this->input->post('stores_title'),
			'stores_address' =>$this->input->post('stores_address'),
			'stores_telephone_number' =>$this->input->post('stores_telephone_number'),
			'latitude' =>$this->input->post('latitude'),
			'longtuide' =>$this->input->post('longtuide'),
			);

		$this->setting_model->update($data,'stores','stores_id',$id);

		$this->session->set_flashdata('msg','<div class="alert alert-success">Store Updated Successfully!</div>');
		redirect('Store/viewstore');
	}

//======================End update store function =================== Gihan Kaveeendra===========
//======================Start delete store function ================= Gihan Kaveendra============

	public function deletestore(){
		$stores_id = $this->uri->segment(3);
		$this->setting_model->delete('stores','stores_id',$stores_id);
		redirect('Store/viewstore');
	}
//=======================End delete store function ================== Gihan Kaveendra============

}