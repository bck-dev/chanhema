<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	//Start Template
	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}
	//End Template

	//============================= Start Add Category ======= charith umeda ==========================//

	
	public function addcategory(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/category/addcategory';
		$mainData = array(
			'pagetitle' => 'Add New Category'
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	
	public function insertcategory(){
		$data = array(
			'category_name '=>$this->input->post('category_name'),
			'status' => 1
		);
		$this->setting_model->insert($data,'product_category');
		$this->session->set_flashdata('msg','<div class="alert alert-success">Category Added Successfully!</div>');
		redirect('category/addcategory');
	}

	//Start View Category
	public function viewcategory(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/category/viewcategory';
		$mainData = array(
			'pagetitle' => 'View Category',
			'category' => $this->setting_model->Get_All('product_category')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	//End View category

	//Start Edit Category

	public function editcategory(){
		$id = $this->uri->segment(3);
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/category/editcategory';
		$mainData = array(
			'pagetitle' => 'Edit Category',
			'category' => $this->setting_model->Get_Single('product_category','category_id',$id)
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	//End Edit category

	//start update category
	public function updatecategory(){
		$id =$this->uri->segment(3);
		$data = array(
			'category_name' => $this->input->post('categoryname'),

		);
		$this->setting_model->update($data,'product_category','category_id',$id);
		$this->session->set_flashdata('msg','<div class="alert alert-success">Feature Updated Successfully!</div>');
		redirect('category/viewcategory');
	}
	//end update category
	
	
// 	change catagory status (availability) ----------------------------------------------------------------------------------------------- coded by Gihan
// 	public function changeStat(){
	    
// 	    $cat_id = $this->input->post('id');
// 	    $status = $this->input->post('stat');
	    
// 	    $data = array(
// 	           'status' => $status 
// 	       );
// 	    $this->AdminModel->update('category','category_id',$cat_id,$data);
// 	    redirect('category/viewcategory');
	    
// 	}
// 	end change status-------------------------------------------------------------------------------------------------------------------------------------

	//Start Delete Category
	public function deletecategory(){
		$category_id = $this->uri->segment(3);
		$this->setting_model->delete_data('product_category','category_id',$category_id);
		redirect('category/viewcategory');
	}
	//End Delete Category

	//============================= End Add Category ======= charith umeda ==========================//
}