<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->check_isvalidated();
	}

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

	//validate session start
	private function check_isvalidated(){
		if(!$this->session->userdata('validated')){
			redirect('login');
		}
	}
	//validate session end
	public function index() {

			$headerData = null;
			$sidebarData = null;
			$page = 'admin/index';
			$mainData = array(
				
				);
			$footerData = null;

			$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
		}

	public function login(){ 
			$this->load->view('admin/login');
		}


	//delete function start
	public function delete() {
		$id = $this->input->post('id');
		$field = $this->input->post('field');
		$table = $this->input->post('table');

		if($this->setting_model->deleteData($table, $field, $id)) {
			return true;
		} else {
			return false;
		}

	}
	//delete function end	

	//edit change password function start --yasas vidanage
	public function settings() {
		$id = $this->uri->segment(3);
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/changeuser';
		$mainData = array(
			'pagetitle' => 'Edit User',
			'user_details' => $this->setting_model->Get_Single('login','user_id',$id)
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}
	//edit change password function end --yasas vidanage

	//update Setting function start --yasas vidanage
	public function updatesetting() {
		$id = $this->uri->segment(3);
		$password_tick = $this->input->post('user_password_checkbox');
		$password = $this->input->post('user_password');
		$confirm_password = $this->input->post('user_confirm_password');

		//check password is correct
		if($password!=$confirm_password){
			$this->session->set_flashdata('msg','<div class="alert alert-danger">Password Fields Mismatch</div>');
			redirect('admin/settings'.'/'.$id);
		}
		else{
			//check password is change or not
			if($password_tick){
				$data = array(
					'user_name'=>$this->input->post('user_name'),
					'email'=>$this->input->post('user_email'),
					'user_type'=>$this->input->post('user_category'),
					'password' => md5($password),
					'verification_status'=>1
				);
				$this->setting_model->update($data,'login','user_id',$id);
				$this->session->set_flashdata('msg','<div class="alert alert-success">User Updated Successfully</div>');
				redirect('admin');
			}
			else{
				$data = array(
					'user_name'=>$this->input->post('user_name'),
					'email'=>$this->input->post('user_email'),
					'user_type'=>$this->input->post('user_category'),
					'verification_status'=>1
				);
				$this->setting_model->update($data,'login','user_id',$id);
				$this->session->set_flashdata('msg','<div class="alert alert-success">User Updated Successfully</div>');
				redirect('admin');
			}
		}
	}
	//update Setting function end --yasas vidanage

}
