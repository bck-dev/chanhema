<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Mainbanner extends CI_Controller {



	function __construct(){

		parent::__construct();

	}



	//Start Template

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {

		$this->load->view('admin/components/header', $headerData);

		$this->load->view('admin/components/sidebar', $sidebarData);

		$this->load->view($page, $mainData);

		$this->load->view('admin/components/footer', $footerData);

	}

	//End Template



	//validate session function start -- charith umeda

	private function check_isvalidated(){

		if(!$this->session->userdata('validated')){

			redirect('login');

		}

	}

	//validate session function end ---- charith umeda

	//add main banner function start --Charith umeda

	public function addmainbanner(){

		$headerData = null;

		$sidebarData = null;

		$page = 'admin/mainbanner/addmainbanner';

		$mainData = array(

			'pagetitle' => 'Add New MainBanner');

		$footerData = null;



		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);

	}

	//add main banner function end --Charith umeda



	//insert mainbanner function start -- charith umeda

	public function insertmainbanner() {

		$data = array(

			'main_banner_image' =>$this->setting_model->upload('main_banner_image', 'upload/mainbanner'),

			'main_banner_text' =>$this->input->post('main_banner_text'),

			'main_banner_link' =>$this->input->post('main_banner_link'),

			'main_banner_status' =>$this->input->post('main_banner_status'),

			);



		$data1 = $this->setting_model->insert($data, 'main_banner');



		$this->session->set_flashdata('msg','<div class="alert alert-success">Main Banner Added Successfully!</div>');



		redirect('mainbanner/addmainbanner');

	}

	//insert mainbanner function end -- charith umeda



	//View mainbanner function start -- charith umeda

	public function viewmainbanner() {

		$headerData = null;

		$sidebarData = null;

		$page = 'admin/mainbanner/viewmainbanner';

		$mainData = array(

			'pagetitle' => 'View Banner',

			'main_banner' => $this->setting_model->Get_All('main_banner')

			);

		$footerData = null;



		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	

	}

	//View mainbanner function end --charith umeda



	//edit mainbanner function start --charith umeda

	public function editmainbanner() {
		$id = $this->uri->segment(3);
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/mainbanner/editmainbanner';

		$mainData = array(
			'pagetitle' => 'Edit Main Banner',
			'main_banner' => $this->setting_model->Get_Single('main_banner','main_banner_id',$id)
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);	
	}
	//edit mainbanner function end --charith umeda

	//update mainbanner function start --charith umeda
	public function updatemainbanner() {
		//image upload start
		if(isset($_FILES['fileinput']) && empty($_FILES['fileinput']['tmp_name'])) {
			$image = $this->input->post('main_banner_image');
		} else {
			$image = $this->setting_model->upload('fileinput', 'upload/mainbanner');
		}

		//image upload end --charith umeda
		$id = $this->uri->segment(3);

		$data = array(
			'main_banner_image' => $image,
			'main_banner_text' =>$this->input->post('main_banner_text'),
			'main_banner_link' =>$this->input->post('main_banner_link'),
		);

		$this->setting_model->update($data,'main_banner','main_banner_id',$id);
		$this->session->set_flashdata('msg','<div class="alert alert-success">Main Banner Updated Successfully!</div>');
		redirect('mainbanner/viewmainbanner');

	}
	//update mainbanner function end --charith umeda

	//Start Active and deactive features in navigation bar (Ajax)
	public function ajaxactivation(){
		$option_id = $this->input->post('id'); //id of the table
		$data = array(
			'main_banner_status'=>$this->input->post('value') //value of the checkbox (1-checked/0-unchecked)
		);
		$this->setting_model->update($data,'main_banner','main_banner_id',$option_id);
	}

	//Stop Active and deactive features in navigation bar (Ajax)
	public function deletemainbanner(){
        $main_banner_id = $this->uri->segment(3);
        $this->setting_model->delete('main_banner','main_banner_id',$main_banner_id);
        redirect('mainbanner/viewmainbanner');
    }

}