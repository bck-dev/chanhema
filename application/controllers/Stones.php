<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stones extends CI_Controller{

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

	//Start add stones ---- yasas vidanage
	public function addstones(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/stones/addstones';
		$mainData = array(
			'pagetitle' => 'Add New Stones'
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	//end stones add ------ yasas vidanage
	
	public function insertstones(){		
			$files = $_FILES;
			$path = "stones";
			$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
			$this->multiple_image_uplaod->upload_image_stones($this->input->post(),$fileName);
			$this->session->set_flashdata('msg','<div class="alert alert-success">Stones Added Successfully!</div>');
			redirect('stones/addstones');
	}
	//upload images and details to database for 'stones' end ---yasas vidanage

	//view pracices-- yasas vidanage
	public function viewstones() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/stones/viewstones';
		$mainData = array(
			'stones' => $this->setting_model->Get_All('stones'),
			'stones_images' => $this->setting_model->Get_All('stones_images'),
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);		
	}
	//view pracices end--yasasvidanage

	//edit pracices--yasas vidanage
	public function editstones() {
		$id = $this->uri->segment(3);

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/stones/editstones';

		$mainData = array(
			'pagetitle' => 'Edit Practices',
			'stones'=> $this->setting_model->Get_Single('stones','stones_id', $id),
			'stones_images' => $this->multiple_image_uplaod->edit_data_image_stones($id)		
			);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);		

	}
	//edit practices end--yasas vidanage

	//delete multiple images from javascript
	public function deleteimagestones(){
		$deleteid  = $this->input->post('id');
		$this->db->delete('stones_images', array('stones_images_id' => $deleteid)); 
		$verify = $this->db->affected_rows();
		echo $verify;
	}

	//edit images amd details to database for practices --yasasvidanage
	public function updatestones(){

			$stones_id = $this->uri->segment(3);
			$files = $_FILES;

			$path = "stones";

			if(!empty($files['userfile']['name'][0])){
				$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
				$this->multiple_image_uplaod->edit_upload_image_stones($stones_id,$this->input->post(),$fileName);
			}
			else{
				$service_id = $this->input->post('stones_id');
				$this->multiple_image_uplaod->edit_upload_image_stones($stones_id,$this->input->post());
			}
			
			$this->session->set_flashdata('msg','<div class="alert alert-success">Stones Updated Successfully!</div>');
			redirect('stones/viewstones');


	}	

	//delete entire practices
	public function deletestones(){
		$stones_id = $this->uri->segment(3);
		
		$this->setting_model->delete_data('stones','stones_id',$stones_id);
		$this->setting_model->delete_data('stones_images','stones_stones_id',$stones_id);
		redirect('stones/viewstones');
	}
}