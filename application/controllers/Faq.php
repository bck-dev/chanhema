<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	//Start Template
	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}
	//End Template

	//============================= Start Add Category =======ishara sewwandi  ==========================//

	//Start Category page
	public function addfaqcategory(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/faq/addfaq_category';
		$mainData = array(
			'pagetitle' => 'Add New Faq Category'
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	//End Add feature page

	//Start Insert Category======================ishara sewwandi  ==========================//
	public function insertcategory(){
		$data = array(
			'faq_category_name '=>$this->input->post('faqcategory')
		);
		$this->setting_model->insert($data,'faq_category');
		$this->session->set_flashdata('msg','<div class="alert alert-success">Category Added Successfully!</div>');
		redirect('faq/addfaqcategory');
	}
	//End insert Category

	//Start View Category
	public function viewfaqcategory(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/faq/viewfaq_category';
		$mainData = array(
			'pagetitle' => 'View Faq Category',
			'category' => $this->setting_model->Get_All('faq_category')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	//End View category============ishara sewwandi  ==========================//

	//Start Edit Category==========ishara sewwandi  ==========================//

	public function editcategory(){
		$id = $this->uri->segment(3);
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/faq/editfaq_category';
		$mainData = array(
			'pagetitle' => 'Edit Faq Category',
			'category' => $this->setting_model->Get_Single('faq_category','faq_category_id',$id)
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	//End Edit category============ishara sewwandi  ==========================//

	//start update category
	public function updatecategory(){
		$id =$this->uri->segment(3);
		$data = array(
			'faq_category_name' => $this->input->post('faqcategoryname'),

		);
		$this->setting_model->update($data,'faq_category','faq_category_id',$id);
		$this->session->set_flashdata('msg','<div class="alert alert-success">Feature Updated Successfully!</div>');
		redirect('faq/viewfaqcategory');
	}
	//end update category

	//Start Delete Category
	public function deletecategory(){
		$faq_category_id = $this->uri->segment(3);
		$this->setting_model->delete_data('faq_category','faq_category_id',$faq_category_id);
		redirect('faq/viewfaqcategory');
	}
	//End Delete Category=========ishara sewwandi  ==========================//

	//============================= End Add Category ======= =======ishara sewwandi  ==========================//
	
	public function addfaq(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/faq/addfaq';
		$mainData = array(
			'pagetitle' => 'Add New Faq Category',
			'category' => $this->setting_model->Get_All('faq_category')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	
	//Start Insert faq
	public function insertfaq(){
		$data = array(
			'faq_category_id '=>$this->input->post('faqcategory'),
			'faq'=>$this->input->post('faq'),
			'answer'=>$this->input->post('answer'),
		);
		$this->setting_model->insert($data,'faq');
		$this->session->set_flashdata('msg','<div class="alert alert-success">Faq Added Successfully!</div>');
		redirect('faq/addfaq');
	}
	//End insert faq==========ishara sewwandi  ==========================//
	
		//Start View faq==========ishara sewwandi  ==========================//
	public function viewfaq(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/faq/viewfaq';
		$mainData = array(
			'pagetitle' => 'View Faq',
			'category' => $this->setting_model->Get_All('faq_category'),
			'faq' => $this->setting_model->Get_All('faq')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	//End View faq==========ishara sewwandi  ==========================//
	
	
		public function editfaq(){
		$id = $this->uri->segment(3);
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/faq/editfaq';
		$mainData = array(
			'pagetitle' => 'Edit Faq',
				'category' => $this->setting_model->Get_All('faq_category'),
			'faq' => $this->setting_model->Get_Single('faq','faq_id',$id)
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	//End Edit faq==========ishara sewwandi  ==========================//

	//start update faq=========ishara sewwandi  ==========================//
	public function updatefaq(){
		$id =$this->uri->segment(3);
		$data = array(
			'faq_category_id '=>$this->input->post('faqcategory'),
			'faq'=>$this->input->post('faq'),
			'answer'=>$this->input->post('answer')
		);
		$this->setting_model->update($data,'faq','faq_id',$id);
		$this->session->set_flashdata('msg','<div class="alert alert-success">Faq Updated Successfully!</div>');
		redirect('faq/viewfaq');
	}
	//end update faq===============ishara sewwandi  ==========================//

	// delete faq //
	public function deletefaq(){
		$faq_id = $this->uri->segment(3);
		$this->setting_model->delete_data('faq','faq_id',$faq_id);
		redirect('faq/viewfaq');
	}

	// delete faq //
}