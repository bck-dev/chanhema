<?php

class Reset extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
    }

    public function index() {
   
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('admin/pss_reset');
        } else {

            $to_email = $this->input->post('email');
            $user = $this->login_model->get_user($to_email);


            $this->sendResetEmail($to_email, $user->security_code, $user->user_id, $user->email);
        }
    }

    public function email_check($str) {

        if ($this->login_model->reset_user_exist($str)) {

            $this->scode();
            return TRUE;
        } else {
           
            $this->form_validation->set_message('email_check', 'Cannot find a account associated with this email. Please check again');
            return FALSE;
        }
    }

    function send_email($data, $subject, $view_name, $to_email) {

        $from_email = $this->config->item('domain');
        $company_name = $this->config->item('company_name');
        $site = base_url();

        $data["company name"] = $company_name;
        $data["site"] = $site;
        $data["from_email"] = $from_email;
        $data["title"] = 'title';
        $data["subject"] = $subject;

        $content = $this->load->view($view_name, $data, TRUE);


        $config['protocol'] = 'mail';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n";

        $this->email->initialize($config);
        $this->email->from($from_email);
        $this->email->to($to_email);
        $this->email->subject($subject);

        $this->email->message($content);

        return $this->email->send();
    }

    function sendResetEmail($to_email, $secrete_code, $uid, $name) {
        $data = array();
        $subject = 'Reset Your Password';
        $data["title"] = "title";
        $data["action"] = "Account Recovery Wizard";

        $data["action_url"] = site_url("reset/verify") . "?email=$to_email&scode=$secrete_code";
        echo "<br>" . $data["action_url"];
        $data["name"] = $name;

        return $this->send_email($data, $subject, "admin/reset", $to_email);
    }

    public function scode() {

        $key = random_string('sha1', 40);
        $data = array(
            'security_code' => $key
            );

        $this->login_model->update_scode($data);
    }

    public function verify() {
        // otodo:please validate get data
        $email = $this->input->get("email");
        $secret_code = $this->input->get("scode");
        //echo $email ." ".$secret_code;
        $valid = $this->login_model->is_verified($email, $secret_code);
        if ($valid) {
            $this->load->view('admin/new_password', array("secret_code" => $secret_code, "email" => $email));
        } else {
            $data = array(
                'message_type' => 'danger',
                'message_content' => 'oops! either activation link is expired or invalid!'
                );

            $this->load->view('admin/basic_message', $data);
            //errorc token
        }
    }

    public function update() {

            $password = $this->input->post("password");
            $confpassword = $this->input->post("passwordconfirm");
            $secret_code = $this->input->post("scode");
            $email = $this->input->post("email");

        if($password==$confpassword){

            $valid = $this->login_model->is_verified($email, $secret_code);

            if($valid)
             {

                $pass_update = $this->login_model->update(array("password" => md5($password)), "login", "email", $email);

                if ($pass_update)
                {

                    $this->session->set_flashdata('msg', '<p id="success" class="alert alert-success">Password Update successfully!</p>');  
                    $this->load->view('admin/new_password', array("secret_code" => $secret_code, "email" => $email));
                    
                } 
                else
                 {
                    $this->session->set_flashdata('msg', '<p class="alert alert-danger">Password update Failed!</p>');
                     $this->load->view('admin/new_password', array("secret_code" => $secret_code, "email" => $email));
                 }
            }
            //user manually change password using setting
            elseif ($_SESSION['validated']==true && !empty($_SESSION['id'])) {



                $pass_update = $this->login_model->update(array("password" => md5($password)), "login", "user_id", $_SESSION['id']);

                if ($pass_update)
                {

                    $this->session->set_flashdata('msg', '<p id="success" class="alert alert-success">Password Update successfully!</p>');  
                    $this->load->view('admin/new_password');
                    
                } 
                else
                 {
                    $this->session->set_flashdata('msg', '<p class="alert alert-danger">Password update Failed!</p>');
                     $this->load->view('admin/new_password');
                 }
                 
             }

            else 
            {
                $this->session->set_flashdata('msg', '<p class="alert alert-danger">Invalid password!</p>');
                $this->load->view('admin/new_password', array("secret_code" => $secret_code, "email" => $email));
            }
        }

        else{
            $this->session->set_flashdata('msg', '<p class="alert alert-danger">Sorry! The Password will be Not Match!</p>');
            $this->load->view('admin/new_password', array("secret_code" => $secret_code, "email" => $email));

        }     
    }

    public function settings(){

        $data = $this->setting_model->Get_All('login');

        foreach($data as $result) {
            $email = $result['email'];
            $secret_code = $result['security_code'];
        }

        //echo $email ." ".$secret_code;
        $valid = $this->login_model->is_verified($email, $secret_code);

        if ($valid) {
            $this->load->view('admin/new_password', array("secret_code" => $secret_code, "email" => $email));
        }
        
        else {
            $data = array(
                'message_type' => 'danger',
                'message_content' => 'oops! Something wrong!'
                );

            

            $this->load->view('admin/basic_message', $data);
            //errorc token
        }
    }

}
