<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function template($headerData, $page, $mainData, $footerData)
    {
        $this->load->view('components/header', $headerData);
        $this->load->view($page, $mainData);
        $this->load->view('components/footer', $footerData);
    }

    public function template1($page, $mainData)
    {
        $collections = $this->setting_model->Get_All('collection');
        $categories = $this->setting_model->Get_All('product_category');

        $mainData['collections'] = $collections;
        $mainData['categories'] = $categories;
        $this->load->view($page, $mainData);
    }

    public function index()
    {
        //$headerData = null;
        $page = 'frontend/index';
        $products = $this->setting_model->Get_All('products');

        foreach ($products as $p) {
            $image = $this->setting_model->Get_All_Where('product_images', 'product_product_id', $p->product_id);
            $p->image = $image[0]['product_images_name'];
        }

        $mainData = array(
            'pageName' => "Home",
            'main_banner' => $this->setting_model->Get_All('main_banner'),
            'products' => $products,
            'product_images' => $this->setting_model->Get_All('product_images')
        );
        //$footerData = null;
        $this->template1($page, $mainData);
        //        $this->template(null, $page, $mainData, null);
    }

    public function gemstone()
    {
        // $headerData = null;
        $page = 'frontend/gemstone';
        $mainData = array(
            'pageName' => "Gemstone",
            'parent' => "gemstone",
            'stones' => $this->setting_model->Get_All('stones'),
            'stones_images' => $this->setting_model->Get_All('stones_images'),

        );
        $footerData = null;

        // $this->template($headerData, $page, $mainData, $footerData);
        $this->template1($page, $mainData);

        // 		$this->template1($page, $mainData);
    }

    public function guarantee()
    {
        // $headerData = null;
        $page = 'frontend/Guarantee';
        $mainData = array(
            'pageName' => "Guarantee",
            'parent' => "guarantee",

        );
        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function service()
    {
        // $headerData = null;
        $page = 'frontend/service';
        $mainData = array(
            'pageName' => "Service",
            'parent' => "service",
            'category' => $this->setting_model->Get_All('faq_category'),
            'faq' => $this->setting_model->Get_All('faq'),

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function return_policy()
    {
        // $headerData = null;
        $page = 'frontend/return_policy';
        $mainData = array(
            'pageName' => "Return Policy",
            'parent' => "shipping",

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function sitemap()
    {
        // $headerData = null;
        $page = 'frontend/sitemap';
        $mainData = array(
            'pageName' => "Sitemap"
        );

        $this->template1($page, $mainData);
    }

    public function shippingDetails()
    {
        // $headerData = null;
        $page = 'frontend/shipping_inner';
        $mainData = array(
            'pageName' => "Shipping Details",
            'parent' => "shipping",

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function store()
    {
        // $headerData = null;
        $page = 'frontend/store';
        $mainData = array(
            'pageName' => "Store",
            'parent' => "store",

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function collections($collection)
    {

        $collectionData = $this->setting_model->Get_Single_Row('collection', 'collection_name', urldecode($collection));
        $products = $this->setting_model->Get_All_Where_Like('products', 'collection_page', urldecode($collection));

        foreach ($products as $p) {
            $image = $this->setting_model->Get_All_Where('product_images', 'product_product_id', $p->product_id);
            $p->image = $image[0]['product_images_name'];
        }

        $page = 'frontend/collection';
        $mainData = array(
            'pageName' => urldecode($collection),
            'collection' => $collectionData,
            'products' => $products,
            'parent' => 'collection', #chamikara
            'active_collection' => urldecode($collection), #chamikara
        );

        $this->template1($page, $mainData);
    }

    public function history()
    {
        // $headerData = null;
        $page = 'frontend/history';
        $mainData = array(
            'pageName' => "History",
            'parent' => "about",
            'gallery' => $this->setting_model->Get_All_Where_Object_Array_Reverse('gallery', 'section', "Beginning", 'image_id')

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function sustainability()
    {
        // $headerData = null;
        $page = 'frontend/Sustainability';
        $mainData = array(
            'pageName' => "Sustainability",
            'parent' => "about",

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function sustainability_inner()
    {
        // $headerData = null;
        $page = 'frontend/Sustanability-inner';
        $mainData = array(
            'pageName' => "Sustainability",
            'parent' => "about",

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function peoples()
    {
        // $headerData = null;
        $page = 'frontend/peoples';
        $mainData = array(
            'pageName' => "People",
            'parent' => "about",

            'people' => $this->setting_model->Get_All('people')
        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function news()
    {
        // $headerData = null;
        $page = 'frontend/news';
        $mainData = array(
            'pageName' => "News",
            'parent' => "about",
            'news' => $this->setting_model->Get_All('news'),
            'news_image' => $this->setting_model->Get_All('news_images'),

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function contact()
    {
        // $headerData = null;
        $page = 'frontend/contactUs';
        $mainData = array(
            'pageName' => "Contact Us",

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function faq()
    {
        // $headerData = null;
        $faqByCategories = $this->setting_model->Get_All_New('faq_category', 'faq_category_name');
        foreach ($faqByCategories as $faqCategory) {
            $faqCategory->faqs = $this->setting_model->Get_All_Where_Object_Array('faq', 'faq_category_id', $faqCategory->faq_category_id, 'faq');
        }

        $faqs = $this->setting_model->Get_All_New('faq', 'faq_id');
        $page = 'frontend/faq';
        $mainData = array(
            'pageName' => "FAQ",
            'parent' => "contact",
            'faqByCategories' => $faqByCategories,
            'faqs' => $faqs
        );

        $this->template1($page, $mainData);
    }

    public function faqSingle($faqId)
    {
        // $headerData = null;

        $faqByCategories = $this->setting_model->Get_All_New('faq_category', 'faq_category_name');
        foreach ($faqByCategories as $faqCategory) {
            $faqCategory->faqs = $this->setting_model->Get_All_Where_Object_Array('faq', 'faq_category_id', $faqCategory->faq_category_id, 'faq');
        }

        $faqs = $this->setting_model->Get_All_Where_Object_Array('faq', 'faq_id', $faqId, 'faq_id');
        $page = 'frontend/faq';
        $mainData = array(
            'pageName' => "FAQ",
            'parent' => "contact",
            'faqByCategories' => $faqByCategories,
            'faqs' => $faqs
        );

        $this->template1($page, $mainData);
    }

    public function shipping()
    {
        // $headerData = null;
        $page = 'frontend/shipping';
        $mainData = array(
            'pageName' => "Shipping",

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function legalnotice()
    {
        // 		$headerData = null;    
        $page = 'frontend/legalnotice';
        $mainData = array(
            'pageName' => "Legal Notice",

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function privacy()
    {
        // 		$headerData = null;        
        $page = 'frontend/privacy';
        $mainData = array(
            'pageName' => "Privacy",

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function termsandcondition()
    {
        // $headerData = null;    
        $page = 'frontend/termsandcondition';
        $mainData = array(
            'pageName' => "Terms & Conditions",

        );


        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function newsinner()
    {
        // $headerData = null;
        $id = $this->uri->segment(3);
        $page = 'frontend/whats-news';
        $mainData = array(
            'pageName' => "News Detail",
            'newsinner' => $this->setting_model->Get_Single('news', 'news_id', $id),
            'news_image' => $this->setting_model->Get_All('news_images'),
            //  'newsinner' => $this->setting_model->newsjoin($id),

        );

        //print_r ($mainData);
        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function guaranteeinner()
    {
        // $headerData = null;
        $page = 'frontend/guaranteeinner';
        $mainData = array(
            'pageName' => "Guarantee Detail",

        );

        //print_r ($mainData);
        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function afterSale()
    {
        // $headerData = null;
        $page = 'frontend/afterSale';
        $mainData = array(
            'pageName' => "After Sale",

        );

        //print_r ($mainData);
        $footerData = null;

        $this->template1($page, $mainData);
    }

    public function products($category)
    {
        $category =  urldecode($category);
        $categoryData = $this->setting_model->Get_Single_Row('product_category', 'category_name', $category);

        if ($category == "All") {
            $category = "All Jewelery";
            $products = $this->setting_model->Get_All_New('products', 'product_id');
        } else {
            $products = $this->setting_model->Get_All_Where_Object_Array('products', 'product_catagory', $categoryData->category_id, 'product_id');
        }


        foreach ($products as $p) {
            $image = $this->setting_model->Get_All_Where('product_images', 'product_product_id', $p->product_id);
            $p->image = $image[0]['product_images_name'];
        }

        $collections = [];
        $allProducts = $this->setting_model->Get_All_New('products', 'product_id');
        foreach ($allProducts as $p) {
            if (count($collections) == 0) {
                $collections = explode(', ', $p->collection_page);
            } elseif (count($collections) == 1) {
                $i = 1;
                $c_temp = explode(', ', $p->collection_page);
                foreach ($c_temp as $c) {
                    if ($c != $collections[0]) {
                        $collections[$i] = $c;
                        $i++;
                    }
                }
            }
        }

        $subCatagories = $this->setting_model->Get_sub_categories_with_products($categoryData->category_id);
        $allCollections = $this->setting_model->Get_All_New('collection', 'collection_name');
        $page = 'frontend/products';
        $mainData = array(
            'pageName' => "Products",
            'products' => $products,
            'collection1' => $collections[0],
            'collection2' => $collections[1],
            'parent' => 'collection', # chamikara,
            'active_category' => $category, #chamikara
            'subCatagories' => $subCatagories,
            'allCollections' => $allCollections
        );

        $this->template1($page, $mainData);
    }

    public function ajaxProducts()
    {
        if ($_POST['category'] == "All") {
            $category = null;
        } else {
            $category = $this->setting_model->Get_Single_Row('product_category', 'category_name', $_POST['category']);
        }
        $sub_catagory = $this->setting_model->Get_Single_Row('product_sub_catagory', 'sub_catagory', $_POST['sub_category']);
        $collection = $_POST['collection'];

        $products = $this->setting_model->productFilter($category->category_id, $sub_catagory->sub_catagory_id, $collection);
        foreach ($products as $p) {
            $image = $this->setting_model->Get_All_Where('product_images', 'product_product_id', $p->product_id);
            $p->image = $image[0]['product_images_name'];
        }

        $output = json_encode($products);
        echo $output;
    }

    public function changeGallery()
    {
        $gallery = $this->setting_model->Get_All_Where_Object_Array_Reverse('gallery', 'section', $_POST['sec'], 'image_id');

        $output = json_encode($gallery);
        echo $output;
    }

    public function send_contact_email()
    {

        $subject1 = $this->input->post('subject1');
        $profile = $this->input->post('profile');
        $title = $this->input->post('title');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $email = $this->input->post('email');
        $message1 = $this->input->post('message1');
        $checkbox = $this->input->post('checkbox');

        if ($this->validteContactEmailForm()) {
            $this->_send_contact_email($subject1, $profile, $title, $firstname, $lastname, $message1, $email);
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger">Ops! Something went wrong! Try again!</div>');
            redirect('home/contact');
        }

        $this->session->set_flashdata('msg', '<div class="alert alert-success">Thanks for contacting us! We\'ll get back to you soon.</div>');
        redirect('home/contact');
    }

    private function validteContactEmailForm()
    {
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('message1', 'Message', 'trim|required');
        $this->form_validation->set_rules('checkbox', 'Checkbox', 'trim|required');

        return $this->form_validation->run();
    }

    private function _send_contact_email($subject1, $profile, $title, $firstname, $lastname, $message1, $email)
    {
        $log_path = base_url('assets/frontend/tironContact/img/logo_03.jpg');

        $message = "";

        $message .= "<table style=\"width: 60%; min-width: 500px;\">";
        $message .= "<tbody style=\"border: 1px solid #a8a8a8;\">";
        $message .= "<tr style=\"background-color: white;\">";
        $message .= "<td style=\"padding: 15px; text-align: center;\" colspan=2>";
        $message .= "<img style=\"padding: 10px; height: auto; width: 30%;\" src=\"$log_path\"/>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Subject:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$subject1</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Profile:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$profile</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Title:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$title</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">First Name:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$firstname</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Last Name:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$lastname</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Email:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$email</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "<tr style=\"\">";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; font-weight: bold; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">Message:</p>";
        $message .= "</td>";
        $message .= "<td style=\"padding-left: 10px; border: 1px solid #a8a8a8;\">";
        $message .= "<p style=\"font-size: 1.2em; padding: 5px; padding-bottom: 0px; margin-bottom: 0;\">$message1</p>";
        $message .= "</td>";
        $message .= "</tr>";

        $message .= "</tbody>";
        $message .= "</table>";

        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'bckl.dev@gmail.com',
            'smtp_pass' => 'bcklimited',
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $emailReceivers = array(
            'anojan.t@bckonnect.com'
        );

        foreach ($emailReceivers as $emailReceiver) {

            $from = 'Hemachandra';
            $to = $emailReceiver;
            $subject = "Contact Form";

            $this->email->from($from, 'Hemachandra Contact Form');
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
        }

        return true;
    }

    // public function afterSale(){

    // 	$this->load->view('frontend/afterSale') ;


    // }

    // 	public function gemstone() {
    // 		$headerData = null;
    // 		$page = 'gemstone';
    // 		$mainData = array(
    // 			);
    // 		$footerData = null;
    // 		$this->template($headerData, $page, $mainData, $footerData);
    // 	}
    // 	public function gurantee() {
    // // 		$headerData = null;
    // // 		$page = 'gurantee';
    // 		$mainData = array(
    // 			);
    // // 		$footerData = null;
    // 		$this->load->view('gurantee', $mainData);
    // 	}
    // 		public function services() {
    // // 		$headerData = null;
    // // 		$page = 'services';
    // 		$mainData = array(
    // 			);
    // // 		$footerData = null;
    // 		$this->load->view('services',$mainData);
    // 	}
    // 		public function store() {
    // // 		$headerData = null;
    // // 		$page = 'services';
    // 		$mainData = array(
    // 			);
    // // 		$footerData = null;
    // 		$this->load->view('store',$mainData);
    // 	}
    // 		public function peoples() {
    // // 		$headerData = null;
    // // 		$page = 'services';
    // 		$mainData = array(
    // 			);
    // // 		$footerData = null;
    // 		$this->load->view('peoples',$mainData);
    // 	} 


    /**
     * Get individual page images for Collection sub-category pages
     * 
     * @param int $categoryId
     * @param int $subCategoryId
     * 
     * @return string $image
     * Static banner image path for collection sub category
     */
    public function getSubCategoryImage($categoryId, $subCategoryId)
    {
        // list of static images
        $images = [
            'cat_1' => [
                'sub_2' => 'assets/frontend/collection_sub_category_images/2_1banner-nw2.jpg',
                'sub_5' => 'assets/frontend/collection_sub_category_images/5_1_banner.jpg',
                'sub_6' => 'assets/frontend/collection_sub_category_images/6_1_banner.jpg',
            ],
        ];

        // return the default image if category of sub category is not found
        if (!array_key_exists('cat_' . $categoryId, $images) || !array_key_exists('sub_' . $subCategoryId, $images['cat_' . $categoryId])) {
            return null;
        }

        // return image
        return $images['cat_' . $categoryId]['sub_' . $subCategoryId];
    }
}