<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	//Start Template
	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}
	//End Template

	//============================= Start addgallery =======ishara sewwandi  ==========================//

	//Start addgallery page
	public function addgallery(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/gallery/addgallery';
		$mainData = array(
			'pagetitle' => 'Add New Image'
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	//End Add feature page

	//Start Insert addgallery======================ishara sewwandi  ==========================//
	public function insertgallery(){
		$data = array(
			'image' => $this->setting_model->uploadNew('gallery_image'),
			'description' => $_POST['description'],
			'section' => $_POST['section']
		);
		
		
		$this->setting_model->insert($data, 'gallery');

		$this->session->set_flashdata('msg','<div class="alert alert-success">Image Added Successfully!</div>');
		redirect('gallery/addgallery');
	}
	//End insert Category

	//Start View Category
	public function viewgallery(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/gallery/viewgallery';
		$mainData = array(
			'pagetitle' => 'View Gallery',
			'gallery' => $this->setting_model->Get_All('gallery')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	//End View category============ishara sewwandi  ==========================//

	//Start Edit Category==========ishara sewwandi  ==========================//

	public function editgallery(){
		$id = $this->uri->segment(3);
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/gallery/editgallery';
		$mainData = array(
			'pagetitle' => 'Edit Image',
			'gallery' => $this->setting_model->Get_Single('gallery','image_id',$id)
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	//End Edit category============ishara sewwandi  ==========================//

	//start update category
	public function updategallery(){
		$id =$this->uri->segment(3);
		
		if(isset($_FILES['gallery_image']) && empty($_FILES['gallery_image']['tmp_name'])) {
            $image = $this->input->post('image');
        } else {
            $image = $this->setting_model->uploadNew('gallery_image');
        }
		$data = array(
			'image' => $image,
			'description' => $_POST['description'],
			'section' => $_POST['section']
		);
		$this->setting_model->update($data,'gallery','image_id',$id);
		$this->session->set_flashdata('msg','<div class="alert alert-success">Image Updated Successfully!</div>');
		redirect('gallery/viewgallery');
	}
	//end update category

	//Start Delete Image
	public function deletegallery(){
		$image_id = $this->uri->segment(3);
		$this->setting_model->delete('gallery','image_id', $image_id);
        
		redirect('gallery/viewgallery');
	}

}