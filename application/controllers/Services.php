<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->check_isvalidated();
	}

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

	//validate session start
	private function check_isvalidated(){
		if(!$this->session->userdata('validated')){
			redirect('login');
		}
	}
	//validate session end
	//add services function start --yasas vidanage
	public function addservices() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/services/addservices';
		$mainData = array(
			'pagetitle' => 'Add Services');
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	//add services function end --yasas vidanage

	//upload images and details to database for 'services' ---yasas vidanage
	public function insertservices(){		
			$files = $_FILES;
			$path = "services";
			$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
			$this->multiple_image_uplaod->upload_image_services($this->input->post(),$fileName);
			$this->session->set_flashdata('msg','<div class="alert alert-success">Services Added Successfully!</div>');
			redirect('services/addservices');
	}
	//upload images and details to database for 'services' end ---yasas vidanage

	//view services-- yasas vidanage
	public function viewservices() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/services/viewservices';
		$mainData = array(
			'pagetitle' => 'View Services',
			'services' => $this->setting_model->Get_All('services'),
			'services_images' => $this->setting_model->Get_All('services_images'),
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);		
	}
	//view services end--yasasvidanage

	//edit services--yasas vidanage
	public function editservices() {
		$id = $this->uri->segment(3);

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/services/editservices';

		$mainData = array(
			'pagetitle' => 'Edit Services',
			'services'=> $this->setting_model->Get_Single('services','services_id', $id),
			'services_image' => $this->multiple_image_uplaod->edit_data_image_services($id)		
			);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);		

	}
	//edit services end--yasas vidanage

	//delete multiple images from javascript
	public function deleteimageservices(){
		$deleteid  = $this->input->post('id');
		$this->db->delete(' services_images', array('services_images_id' => $deleteid)); 
		$verify = $this->db->affected_rows();
		echo $verify;
	}

	//edit images amd details to database for services --yasasvidanage
	public function updateservices(){

			$services_id = $this->uri->segment(3);
			$files = $_FILES;

			$path = "services";

			if(!empty($files['userfile']['name'][0])){
				$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
				$this->multiple_image_uplaod->edit_upload_image_services($services_id,$this->input->post(),$fileName);
			}
			else{
				$service_id = $this->input->post('service_id');
				$this->multiple_image_uplaod->edit_upload_image_services($services_id,$this->input->post());
			}
			
			$this->session->set_flashdata('msg','<div class="alert alert-success">Services Updated Successfully!</div>');
			redirect('services/viewservices');


	}	

	//delete entire services
	public function deleteservices(){
		//$service_id = $this->uri->segment(3);
		$services_id = $this->input->post('id');
		$limit_services=count($this->setting_model->Get_Single('services','services_id',$services_id));
		$limit_images=count($this->setting_model->Get_Single(' services_images','services_services_id',$services_id));
		$this->setting_model->delete_data('services','services_id',$services_id,$limit_services);
		$this->setting_model->delete_data('services_images','services_services_id',$services_id,$limit_images);
		redirect('services/viewservices');
	}

	

}
