<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller{

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

	//Start add product ---- yasas vidanage
	public function addproduct(){
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/product/addproduct';
		$mainData = array(
			'pagetitle' => 'Add New Product',
			'category' => $this->setting_model->Get_All('product_category'),
			'collections' => $this->setting_model->Get_All('collection'),
			'sub_catagory' => $this->setting_model->Get_All('product_sub_catagory')
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}

	//end product add ------ yasas vidanage
	
	public function insertproduct(){		
			$files = $_FILES;
			$path = "product";
			$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
			$this->multiple_image_uplaod->upload_image_product($this->input->post(),$fileName);
			$this->session->set_flashdata('msg','<div class="alert alert-success">Product Added Successfully!</div>');
			redirect('product/addproduct');
	}
	//upload images and details to database for 'product' end ---yasas vidanage

	//view product-- yasas vidanage
	public function viewproduct() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/product/viewproduct';
		$mainData = array(
			'products' => $this->setting_model->Get_All('products'),
			'product_images' => $this->setting_model->Get_All('product_images'),
			'collection' => $this->setting_model->Get_All('collection'),
			'category' => $this->setting_model->Get_All('product_category'),
			'sub_catagory' => $this->setting_model->Get_All('product_sub_catagory'),
		);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);		
	}

	// public function searchproduct() {
	// 	$headerData = null;
	// 	$sidebarData = null;
	// 	$page = 'admin/product/viewproduct';
	// 	$mainData = array(
	// 		'products' => $this->setting_model->Get_All('products'),
	// 		'product_images' => $this->setting_model->Get_All('product_images'),
	// 		'collection' => $this->setting_model->Get_All('collection'),
	// 		'category' => $this->setting_model->Get_All('product_category'),
	// 		'sub_catagory' => $this->setting_model->Get_All('product_sub_catagory'),
	// 	);
	// 	$footerData = null;

	// 	$this->template($headerData, $sidebarData, $page, $mainData, $footerData);		
	// }
	//view product end--yasasvidanage

	//edit product--yasas vidanage
	public function editproduct() {
		$id = $this->uri->segment(3);

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/product/editproduct';


		$product= $this->setting_model->Get_Single('products','product_id', $id);
		$product_images = $this->multiple_image_uplaod->edit_data_image_product($id);
		$categories = $this->setting_model->Get_All('product_category');
		$sub_catagories = $this->setting_model->Get_All('product_sub_catagory');
		$collections = $this->setting_model->Get_All_New('collection', 'collection_name');

		$product_collection=explode(", ",$product[0]['collection_page']);
		$count=count($product_collection);
		for($i=0; $i<$count; $i++){
			foreach($collections as $c){
				if($c->collection_name==$product_collection[$i]){
					$c->selected="yes";
				}
			}
		}

		$mainData = array(
			'pagetitle' => 'Edit Product',
			'product' => $product,
			'product_images' => $product_images,
			'categories' => $categories,
			'sub_categories' => $sub_catagories,
			'collections' => $collections
		);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);		

	}
	//edit product end--yasas vidanage

	//delete multiple images from javascript
	public function deleteImage(){
		$deleteid  = $this->input->post('id');
		$this->db->delete('product_images', array('product_images_id' => $deleteid)); 
		$verify = $this->db->affected_rows();
		echo $verify;
	}

	//edit images amd details to database for product --yasasvidanage
	public function updateproduct(){

			$product_id = $this->uri->segment(3);
			$files = $_FILES;

			$path = "product";

			if(!empty($files['userfile']['name'][0])){
				$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
				$this->multiple_image_uplaod->edit_upload_image_product($product_id,$this->input->post(),$fileName);
			}
			else{
				$service_id = $this->input->post('product_id');
				$this->multiple_image_uplaod->edit_upload_image_product($product_id,$this->input->post());
			}
			
			$this->session->set_flashdata('msg','<div class="alert alert-success">Product Updated Successfully!</div>');
			redirect('product/viewproduct');


	}	

	//delete entire product
	public function deleteproduct(){
		$product_id = $this->uri->segment(3);
		$limit_product=count($this->setting_model->Get_Single('products','product_id',$product_id));
		$limit_images=count($this->setting_model->Get_Single('product_images','product_product_id',$product_id));
		$this->setting_model->delete_data('products','product_id',$product_id, $limit_product);
		$this->setting_model->delete_data('product_images','product_product_id',$product_id, $limit_images);
		redirect('product/viewproduct');
	}
	
	//product search
	public function search(){
		$category = $this->input->post('category_search');
		$collection = $this->input->post('collection_search');

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/product/viewproduct';
		$mainData = array(
			'product_images' => $this->setting_model->Get_All('product_images'),
			'collection' => $this->setting_model->Get_All('collection'),
			'category' => $this->setting_model->Get_All('product_category'),
			'sub_catagory' => $this->setting_model->Get_All('product_sub_catagory'),
			'products' => $this->setting_model->Get_Ajax_Search_New('products', $category, $collection)			
		);
		
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
}