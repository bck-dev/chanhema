<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Practices extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->check_isvalidated();
	}

	public function template($headerData, $sidebarData, $page, $mainData, $footerData) {
		$this->load->view('admin/components/header', $headerData);
		$this->load->view('admin/components/sidebar', $sidebarData);
		$this->load->view($page, $mainData);
		$this->load->view('admin/components/footer', $footerData);
	}

	//validate session start
	private function check_isvalidated(){
		if(!$this->session->userdata('validated')){
			redirect('login');
		}
	}
	//validate session end
	//add pracices function start --yasas vidanage
	public function addpractices() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/practices/addpractices';
		$mainData = array(
			'pagetitle' => 'Add Practices');
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);
	}
	//add pracices function end --yasas vidanage

	//upload images and details to database for 'pracices' ---yasas vidanage
	public function insertpractices(){		
			$files = $_FILES;
			$path = "practices";
			$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
			$this->multiple_image_uplaod->upload_image_practices($this->input->post(),$fileName);
			$this->session->set_flashdata('msg','<div class="alert alert-success">Parctices Added Successfully!</div>');
			redirect('practices/addpractices');
	}
	//upload images and details to database for 'pracices' end ---yasas vidanage

	//view pracices-- yasas vidanage
	public function viewpractices() {
		$headerData = null;
		$sidebarData = null;
		$page = 'admin/practices/viewpractices';
		$mainData = array(
			'best_practices' => $this->setting_model->Get_All('best_practices'),
			'best_practices_images' => $this->setting_model->Get_All('best_practices_images'),
			);
		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);		
	}
	//view pracices end--yasasvidanage

	//edit pracices--yasas vidanage
	public function editpractices() {
		$id = $this->uri->segment(3);

		$headerData = null;
		$sidebarData = null;
		$page = 'admin/practices/editpractices';

		$mainData = array(
			'pagetitle' => 'Edit Practices',
			'best_practices'=> $this->setting_model->Get_Single('best_practices','best_practices_id', $id),
			'best_practices_images' => $this->multiple_image_uplaod->edit_data_image_practices($id)		
			);

		$footerData = null;

		$this->template($headerData, $sidebarData, $page, $mainData, $footerData);		

	}
	//edit practices end--yasas vidanage

	//delete multiple images from javascript
	public function deleteimagepractices(){
		$deleteid  = $this->input->post('id');
		$this->db->delete(' best_practices_images', array('best_practices_images_id' => $deleteid)); 
		$verify = $this->db->affected_rows();
		echo $verify;
	}

	//edit images amd details to database for practices --yasasvidanage
	public function updatepractices(){

			$best_practice_id = $this->uri->segment(3);
			$files = $_FILES;

			$path = "practices";

			if(!empty($files['userfile']['name'][0])){
				$fileName = $this->multiple_image_uplaod->multi_image_upload($files,$path);
				$this->multiple_image_uplaod->edit_upload_image_practices($best_practice_id,$this->input->post(),$fileName);
			}
			else{
				$service_id = $this->input->post('best_practices_id');
				$this->multiple_image_uplaod->edit_upload_image_practices($best_practice_id,$this->input->post());
			}
			
			$this->session->set_flashdata('msg','<div class="alert alert-success">Practice Updated Successfully!</div>');
			redirect('practices/viewpractices');


	}	

	//delete entire practices
	public function deletepractices(){
		//$service_id = $this->uri->segment(3);
		$best_practices_id = $this->input->post('id');
		$limit_practices=count($this->setting_model->Get_Single('best_practices','best_practices_id',$best_practices_id));
		$limit_images=count($this->setting_model->Get_Single('best_practices_images','best_practices_best_id',$best_practices_id));
		$this->setting_model->delete_data('best_practices','best_practices_id',$best_practices_id,$limit_practices);
		$this->setting_model->delete_data('best_practices_images','best_practices_best_id',$services_id,$limit_images);
		redirect('services/viewservices');
	}

}
