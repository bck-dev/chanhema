<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="retpolicyinner-img">
    <!-- <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div> -->
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>
                On the rare occasion a client chooses to part with a product, the following procedure will be adhered to
            </p>
        </div>
    </div>
</section>

<div class="container bottom-juwelery pb-5">
    <div class="row">
        <div class="col-md-8 pt-5 new-text">
            <div class="below-text mt-5">
                <p class="care-for-jewellary innerpages-content">
                    Products may be refunded or exchanged within 14 days of purchase, provided the items are in their
                    original packaging, and not worn. Any certificates, documentation provided with the item and
                    original packaging must also be returned.
                </p>
                <!-- Please get in touch with us through the Inquiries link below (provided in the grey bar section) to initiate a Return/Exchange process. -->

                <img src="<?php echo base_url('assets/frontend/'); ?>/img/Return_Policy_Inner.jpg"
                    class="d-block d-md-none w-100 mt-5" alt="">

                <h3 class="bell-heading mt-5">Refunds</h3>
                <p class="care-for-jewellary innerpages-content">
                    We agree to process the refund in the same mode of payment used for the original purchase (excluding
                    any delivery charges) within 14 business days of receiving your returned item. Cash refunds for cash
                    purchases can only be offered in store; a bank transfer will be arranged for customers who are
                    unable to visit us. Gift recipients are entitled to non-refundable merchandise credit. 
                    <br /><br />
                    All returns are subject to inspection by our quality assurance team before a refund is processed.
                </p>

                <h3 class="bell-heading mt-5">Customizations and Special Orders</h3>
                <p class="care-for-jewellary innerpages-content">
                    We regret we cannot offer exchanges or refunds on jewellery that has been altered or resized to the
                    customer’s requirements. 
                    <br /><br />
                    Neither can we offer exchanges or refunds for customized products/special orders.
                </p>

                <img src="<?php echo base_url('assets/frontend/'); ?>/img/Return_Policy_Inner_02.jpg"
                    class="d-block d-md-none w-100 mt-5" alt="">

                <h3 class="bell-heading mt-5">Terms and Conditions</h3>
                <p class="care-for-jewellary innerpages-content">
                    By using this website you are agreeing to the terms of our Returns Policy. We reserve the right to
                    modify our Returns Policy as our business needs require. We will post such changes on our website,
                    after which, your continued use of the website shall be deemed as your agreement to the modified
                    terms. 
                </p>
                <!-- Please get in touch with us through the Inquiries link below (provided in the grey bar section) to initiate a Refund/Exchange process. -->

                <!-- <h3 class="bell-heading mt-5">Gifting</h3>
                <p class="care-for-jewellary innerpages-content">
                    Bring some magic to a loved one by gifting them one of our exquisite handcrafted pieces. Contact us
                    for more information on our personalised gift-wrapping service.
                </p> -->
            </div>
        </div>
        <div class="col-md-4 pt-5 d-none d-md-block">
            <img src="<?php echo base_url('assets/frontend/'); ?>/img/Return_Policy_Inner.jpg" class="w-100 mt-5"
                alt="">
            <br /><br /><br />
            <img src="<?php echo base_url('assets/frontend/'); ?>/img/Return_Policy_Inner_02.jpg" class="w-100" alt="">
        </div>
    </div>
</div>
<section id="quality">
    <div class="container">
        <h1 class="topic mt-5">Get in Touch with Us</h1>
        <p class="para-quality">We greatly value our customers and are always willing to help with your queries. If you
            have query about your timepiece and would like to get in touch with us </p>
        <div class="phone-number">
            <div class="custom-inquire-area"><a class="custom-inquire-text" href="./contact">Click here for
                    inquiries</a></div>
            <!-- <a class="inquire-num" href="tel:0112874893">+94 11 287 48 93</a> -->
            <div class="custom-inquire-area"><a class="inquire-email"
                    href="mailto:mail@hemachandras.com">mail@hemachandras.com</a></div>
        </div>

    </div>
</section>

<!--end edit sectio tiorn-->

<script>
new Glider(document.querySelector('.glider'), {
    slidesToScroll: 1,
    slidesToShow: 4,
    draggable: true,
    dots: '.dots',
    arrows: {
        prev: '.glider-prev',
        next: '.glider-next'
    }
});




$('[data-fancybox="preview"]').fancybox({
    thumbs: {
        autoStart: true
    }
});
</script>


<?php $this->load->view('components/footer'); ?>