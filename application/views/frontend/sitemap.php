<?php $this->load->view('components/headerNew'); ?>   
<?php $this->load->view('components/menu'); ?>


<div class="container bottom-juwelery">
    <div class="row">
        <div class="col-md-12 p-5">
            <div class="below-text px-5">
              <h2>Site Map</h2>
              <ul>
                <li><a href="#">About us</a></li>
                <ul>
                  <li><a href="<?php echo base_url('story'); ?>">Story</a></li>
                  <li><a href="<?php echo base_url('sustainability'); ?>">Sustainability</a></li>
                  <li><a href="<?php echo base_url('people'); ?>">People</a></li>
                  <li><a href="<?php echo base_url('news'); ?>">News</a></li>
                </ul>

                <li><a href="<?php echo base_url('gemstone'); ?>">Gemstones </a></li>

                <li><a href="#">Collections</a></li>
                <ul class="collections_submenu">
                    <?php foreach($collections as $collection): ?>
                        <li><a href="<?php echo base_url(); ?>collections/<?php echo $collection['collection_name']; ?>"><?php echo $collection['collection_name']; ?> Collections</a></li>
                    <?php endforeach; ?>
                </ul>

                <li><a href="#">Jewelry</a></li>
                <ul>
                  <?php foreach ($categories as $category) : ?>
                      <li><a href="<?php echo base_url(); ?>products/<?php echo $category['category_name']; ?>"><?php echo $category['category_name']; ?></a></li>
                  <?php endforeach; ?>
                  <li><a href="<?php echo base_url(); ?>products/All">View All</a></li>
                </ul>

                <li><a href="<?php echo base_url('guarantee'); ?>">Guarantee </a></li>
                <li><a href="<?php echo base_url('service'); ?>">Services</a></li>
                <li><a href="<?php echo base_url('store'); ?>">Stores</a></li>
                <li><a href="<?php echo base_url('news'); ?>">Media </a></li>
                <li><a href="<?php echo base_url('contact'); ?>">Contact</a> </li>
                <li><a href="<?php echo base_url('shipping'); ?>"> Shipping</a> </li>
              </ul>
            </div>
        </div>
    </div>
</div>
    
<?php $this->load->view('components/footer'); ?>
