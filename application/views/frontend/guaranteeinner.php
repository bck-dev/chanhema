<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-img" class="care-img">
    <!-- <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div> -->
    <h1 class="text-capitalize">
        <!-- Care For Your Jewellery -->
    </h1>
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p style="font-size: 20px;">Time tested home-care tips and tricks to keep your jewellery alluringly
                beautiful, always.</p>
        </div>
    </div>
</section>

<div class="container bottom-juwelery guarantee-inner-top-space">
    <div class="row">
        <div class="col-md-8 new-text">
            <div class="below-text">

                <span class="care-for-jewellary bell-heading mt-5">Daily Care Tips</span><br /><br />
                <span class="care-for-jewellary"></span>
                <ul class="dailytips-list">
                    <li>
                        <p class="care-for-jewellary">
                            <!-- <b>Tip 1 :</b> --> Over time, light and heat can bleach or damage some gemstones. Avoid
                            this by storing your jewellery in a dry place away from humidity, moisture and extreme
                            temperatures.
                        </p>
                    </li>

                    <li>
                        <p class="care-for-jewellary">
                            <!-- <b>Tip 2 :</b> --> Avoid exposing your jewels to chemicals, both everyday substances
                            such as perfume and hairspray, but also household cleaning products, and chlorinated water.
                        </p>
                    </li>

                    <li>
                        <p class="care-for-jewellary">
                            <!-- <b>Tip 3 :</b> --> Remove your rings when washing up or showering as soap particles may
                            become lodged in stone settings and crevices. Soaps can also leave a film over metal and
                            stones, creating a dull appearance.
                        </p>
                    </li>

                    <li>
                        <p class="care-for-jewellary">
                            <!-- <b>Tip 4 :</b> --> Be mindful not to knock or leave your jewellery on hard surfaces,
                            which could scratch the surface of the metal and stone.
                        </p>
                    </li>

                    <li>
                        <p class="care-for-jewellary">
                            <!-- <b>Tip 5 :</b> --> When dressing, put on jewellery last as make-up, cosmetics and
                            perfume can cause damage to delicate items.
                        </p>
                    </li>
                </ul>

                <img src="<?php echo base_url('assets/frontend/'); ?>/img/Guarantee_Care_Inner.jpg"
                    class="d-block d-md-none w-100 mt-5" alt="">

                <p class="care-for-jewellary mt-5"><span class="bell-heading mt-5">Safe Storage</span><br /><br />
                    Store your jewellery in its own box, pouch or wrapped in cotton/tissue to prevent scratching,
                    chipping and entanglement.
                    <br /><br />
                    When travelling, protect your items from scratches or other impact damage by packing them in a
                    suitable box or case.
                </p>

                <img src="<?php echo base_url('assets/frontend/'); ?>/img/Guarantee_Care_Inner02.jpg"
                    class="d-block d-md-none w-100 mt-5" alt="">

                <p class="care-for-jewellary mt-5"><span class="bell-heading mt-5">Cleaning Methods</span><br /><br />
                    Most jewellery with coloured gems can be cleaned with warm water, mild dish soap (no detergents) and
                    a soft brush. Alternatively, a lint-free cloth may be used.
                    <br /><br />
                    We recommend cleaning your jewellery in a bowl of water, to prevent loss and damage if the item were
                    to fall. We advise using ultrasonic cleaners with caution as they can sometimes cause the gemstones
                    to chip, be shaken loose or possibly break.

                    <!-- <img src="<?php echo base_url('assets/frontend/'); ?>/img/caring-inner-3.jpg"
                        class="d-block d-md-none w-100 mt-5" alt=""> -->

            </div>
        </div>
        <div class="col-md-4 d-none d-md-block">

            <img src="<?php echo base_url('assets/frontend/'); ?>/img/Guarantee_Care_Inner.jpg" class="w-100" alt="">
            <br /><br /><br />
            <img src="<?php echo base_url('assets/frontend/'); ?>/img/Guarantee_Care_Inner02.jpg" class="w-100" alt="">
            <br /><br /><br />
            <!-- <img src="<?php echo base_url('assets/frontend/'); ?>/img/caring-inner-3.jpg" class="w-100" alt=""> -->
        </div>
    </div>
</div>


<!--end edit sectio tiorn-->

<script>
new Glider(document.querySelector('.glider'), {
    slidesToScroll: 1,
    slidesToShow: 4,
    draggable: true,
    dots: '.dots',
    arrows: {
        prev: '.glider-prev',
        next: '.glider-next'
    }
});




$('[data-fancybox="preview"]').fancybox({
    thumbs: {
        autoStart: true
    }
});
</script>


<?php $this->load->view('components/footer'); ?>