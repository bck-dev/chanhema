<?php $this->load->view('components/headerNew'); ?>   
<?php $this->load->view('components/menu'); ?>
    
<section id="service-img-people">
    <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div>
    <!-- <h1 class="text-capitalize">Dedicated to Perfection</h1> -->
    <h1 class="text-capitalize"><!-- Dedicated To Excellence --></h1>
  </section>

  <section class="blue-box">
    <div class="container">
          <div class="box-content">
            <p>
            Let us introduce you to our family, the heart and soul of our operation.
            </p>
          </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 people-main-intro-area">
          <p class="crafter-text crafter-text-new">Our employee longevity reflects the closeness of our team. The longest-serving members have tested gems, greeted visitors and crafted fine gold jewellery at Hemachandra for almost four decades. Some even worked alongside Ananda Hemachandra, our visionary founder, reflecting their degree of commitment. A strong work ethic, expertise and humility are also what define our talented team. With a firm belief in creating a supportive environment, we strive to enable each individual to reach his or her full creative potential.</p>
        </div>
      <div>
    </div>
  </section>

  <section>
    <div class="container people-top-section">
      <div class="row">
        <div class="col-md-6 crafter-img-area">
          <img src="<?php echo base_url('assets/frontend/'); ?>img/craftmen-n.jpg" class="w-100" alt="">
        </div>
        <div class="col-md-6 crafter-content-area pl-lg-5 mt-lg-4">
          <h6 id="feed-gem" class="crafter-sub-heading mt-lg-5">MASTER CRAFTSMEN</h6>
          <h3 class="crafter-heading">DIAMONDS IN THE ROUGH</h3>
          <p class="crafter-text mt-4">
          Within our team of master craftsmen lies an exceptional level of talent. For many of the jewellers it is an ancestral skill that has been passed down to them by their fathers and grandfathers. Over the decades, this accumulation of experience means their expertise in the intricate processes of gold and silver casting, delicate filigree work and gem setting is unparalleled. Each craftsmen, drive by dedication and passion for the craft, has a unique specialty whether it is setting cluster designs or creating elaborate gem-studded necklaces.

          </p>
        </div>
      </div>
    </div>
  </section>

  <!-- <section>
    <div class="container diamond-content-area">
      <div class="row">
        <div class="col-md-12">
          <h6 id="feed-gem" class="crafter-sub-heading">CRAFTMANSHIP</h6>
          <h3 class="crafter-heading">DIAMONDS IN THE ROUGH</h3>
          <p class="crafter-text diamond-crafter-text mt-4">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
          </p>
        </div>
      </div>
    </div>
  </section> -->

  <section id="feed_people">
    <div class="container">
      

<?php foreach($people as $peo):  ?>
<?php $first_name = $peo['people_first_name'];
        $second_name = $peo['people_last_name'];
        ?>
      <div class="row feed_people-wrap">
        <div class="col-md-6">
            <img src="<?php echo base_url(); ?><?php echo $peo['people_image']; ?>" class="people-image-area" alt="">
          
          <p class="feed_people-sec-text">
            
          </p>

         
        </div>
       
        <div class="col-md-6">
            
            <h3 class="people-name"><font color="black"><?php echo $first_name; ?></font></h3>
            <h6 class="people-title"><?php echo $peo['people_category'];?></h6>
            <p class="ffound">
            <?php echo $peo['people_description'];?>
          </p>
        </div>
      </div>
      <?php endforeach; ?>







    </div>
  </section>

<!-- ///////////////////////////foootererrrr///////////////////////// -->

<?php $this->load->view('components/footer'); ?>