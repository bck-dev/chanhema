<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<button onclick="topFunction()" id="top-buttton" title="Go to top"><i class="fa fa-arrow-up topbutton-arrow" aria-hidden="true" style=""></i></button>

<section id="service-img">
    <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
        <span class="policy-close">x</span></div>
    <!-- <h1 class="text-capitalize">Pledge <span class="mid-ft"> of </span>Service</h1> -->
    <h1 class="text-capitalize"><!-- Continuing The Tradition --></h1>
</section>

<section id="blue-box">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <p class="head-his history-timeline-heading">Three generations of...</p>
                    <div></div>
                    <div id="timeline" class="row time-three">
                        <div class="col-md-4 timeline-border">
                        <p class="history-button history-humble-begining histort-hb active_new active_new-hb" id="hbtn1">Humble Beginnings</p>
                        <!-- <hr class="his-hover-hr-hb"> -->
                        </div>
                        <div class="col-md-4 timeline-border">
                        <p class="history-button histort-eow" id="hbtn2">Expanding Our Wings</p>
                        <!-- <hr class="his-hover-hr-eow"> -->
                        </div>
                        <div class="col-md-4 timeline-text-last">
                        <p class="history-button histort-tc" id="hbtn3">Traditional Contemporary</p>
                        <!-- <hr class="his-hover-hr-tc"> -->
                        </div>
                    </div>
                    <!-- <hr class="timeline-hr">
                    <div class="timeline-vr"></div>
                    <div class="timeline-vr2"></div> -->
                    <!-- <div class="row">
                        <div class="col-md-12">
                            <ul class="timeline">
                                <li><a href="#hissec">1942</a></li>
                                <li><a href="#hissec02"> 1956</a> </li>
                                <span class="divde-sec"></span>

                                <li></li>
                                <li></li>
                                <span class="divde-therd"></span>

                                <li><a href="#feed2"> 1980</a> </li>
                                <li><a href="#feed3"> 2010</a> </li>
                            </ul>
                        </div>
                    </div> -->
                </div>

            </div>
        </div>
    </div>
</section>



<section id="hissec">
    <div class="container-fluid p-0">
        <div class="w-100">
            <div class="d-flex" id="first_content">
                <div class="left_image_section d-none d-md-block">
                    <img src="<?php echo base_url('assets/frontend/'); ?>/img/shop-front.png" class="w-100 left_img1" alt="">
                </div>

                <div class="content_section">
                    <img src="<?php echo base_url('assets/frontend/'); ?>/img/family.jpg" class="w-100 content_img1" alt="">
                    
                    <div>
                        
                        <h6 class="date-year text-right">1942</h6>
                        <h6 class="text-right">Humble Beginnings</h6>
                        <p class="text-justify">Ananda Hemachandra, our visionary founder who established the first store in 1942, had a passion for fine jewellery, which stemmed from a childhood watching his father craft gold pieces in their family home in southern Sri Lanka. Independent and entrepreneurial, Ananda moved to Kandy, which at that time was home to a large community of British tea planters and their families. Charismatic and determined, he proudly held his own alongside the Kandyan elite, and Hemachandras soon became one of the most respected jewellery stores in the former hill kingdom.</p>
                        <!-- <h6 class="date-year text-right">1942</h6> -->
                        <p class="text-justify hishigh">Ananda Hemachandra, a master goldsmith himself, founded Hemachandras and opened our first shop on Trincomalee Street in Kandy.</p>
                        <img src="<?php echo base_url('assets/frontend/'); ?>/img/comma.png" class="comma-img">
                        <!-- <h6 class="date-year text-right">1956</h6>
                        <p class="text-justify">Our second shop was opened in The Queen’s Hotel, Kandy, famous for its style and grandeur.</p> -->
                    </div>
                </div>

                <div class="right_image_section d-none d-md-block">
                    <img src="<?php echo base_url('assets/frontend/'); ?>/img/person2n.png" class="w-100 right_img1" alt="">
                </div>
            </div>

            <div class="d-none" id="second_content">
                <div class="left_image_section d-none d-md-block">
                    <img src="<?php echo base_url('assets/frontend/'); ?>/img/poster.png" class="w-100 left_img2" alt="">
                </div>

                <div class="content_section">
                    <div>
                        <div class="staff-image" style="margin-bottom: 20px !important;">
                            <img src="<?php echo base_url('assets/frontend/'); ?>/img/staff.png">
                        </div>
                        <h6 class="date-year-left">1980 </h6>
                        <h6 class="text-left">Evolution into one of Sri Lanka’s finest luxury jewellery brands</h6>
                        <p class="text-justify history-evolution-content">From humble beginnings in the 1940s when Ananda adorned the Kandyan glitterati with intricate, jewel-encrusted pieces, our Company evolved under the creativity of his children. George and Thusitha alongside their brothers Upali and Keerthi, undertook the mammoth task of stepping into their father’s shoes. The brothers ingeniously steered the company to grow steadily, despite the unfavourable economic climate of war-torn Sri Lanka. With a move to Dalada Veediya in Kandy, they expanded the workshop and gave our master craftsmen the freedom to create timeless pieces of exceptional quality.</p>
                        
                        <p class="text-justify hishigh">As the business grew, Hemachandras relocated to Dalada Veediya where we expanded the workshop and employed a team of master craftsmen.</p>
                        <img src="<?php echo base_url('assets/frontend/'); ?>/img/comma.png" class="comma-img">
                    </div>
                </div>

                <div class="right_image_section d-none d-md-block">
                    <img src="<?php echo base_url('assets/frontend/'); ?>/img/family-ch.jpg" class="w-100 right_img2" alt="">
                </div>
            </div>

            <div class="d-none" id="third_content">
                <div class="left_image_section d-none d-md-block">
                    <!-- <img src="<?php echo base_url('assets/frontend/'); ?>/img/shop-front.png" class="w-100 left_img1" alt=""> -->
                </div>

                <div class="content_section">
                    <div>
                        
                        <h6 class="date-year text-right">2010</h6>
                        <h6 class="text-right">Embracing the future through contemporary innovation</h6>
                        <p class="text-justify">Remaining faithful to the family legacy, today Hemachandras is managed by four of Ananda’s grandchildren under the expert guidance of Thusitha Hemachandra. Over generations, Hemachandras has evolved stylistically while still maintaining its deep cultural roots. Inspired to innovate and fuse the company’s rich Sri Lankan heritage with contemporary style, they hope to take Sri Lankan jewellery craftsmanship to new heights of style and luxury.</p>
                        
                        <p class="text-justify hishigh">The third generation, grandchildren of Ananda Hemachandra, are passionate about combining the company’s unique Sri Lankan heritage with exciting new jewellery innovations.</p>
                        <img src="<?php echo base_url('assets/frontend/'); ?>/img/comma.png" class="comma-img">
                    </div>
                </div>

                <div class="right_image_section d-none d-md-block">
                    <!-- <img src="<?php echo base_url('assets/frontend/'); ?>/img/person2n.png" class="w-100 right_img1" alt=""> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div id="gal-slider" class="slider">
        <?php foreach($gallery as $g): ?>
            <div class="history-gallery">
                <div class="image-thumb" style="background: url('<?php echo base_url(''); ?><?php echo $g->image; ?>') 50% 50% no-repeat; height: 300px; "></div>
                <p class="his-gal-text"><?php echo $g->description; ?></p>
            </div>
        <?php endforeach; ?>            
    </div>
</section>

<script>
    $(function() {
        // Smooth Scrolling
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });



    // window.onscroll = function() {
    //     scrollFunction()
    // };

    function galleryChange(section) {
        $.ajax({
        url: '<?php echo base_url('changeGallery') ?>', 
        type:'post',
        data: {sec: section},
        dataType: 'json',
        success: function(results){ 
                      
          var galleries =""; 
          jQuery.each(results, function( key, val ) {
            var gallery = "";

            gallery = gallery + '<div class="history-gallery">';
            gallery = gallery + '<div class="image-thumb" style="background: url(' + val['image'] +') 50% 50% no-repeat; height: 300px; "></div>';
            gallery = gallery + '<p class="his-gal-text">' + val['description'] + '</p>';
            gallery = gallery + '</div>';

            galleries = galleries + gallery;
          });
          
          galleries = '<div id="new-slick" class="slider">' +galleries +'</div>'
        //   $('.slider').slick('refresh');
        //   $('#gal-slider').html('');
        $('#gal-slider').removeClass('slider');
          $('#gal-slider').html(galleries);
          $('#new-slick').slick({
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        arrows: true,
        responsive: [{
          breakpoint: 720,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
           breakpoint: 480,
           settings: {
              arrows: true,
              slidesToShow: 1,
              slidesToScroll: 1
           }
        }]
    });
        },
    
        error:function(){
          console.log('error');
        }
      });
    }


    $(document).ready(function() {
        $(".scroll-top").click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            return false;
        });
    });

    $(document).ready(function() {
        $("#hbtn1").click(function() {
            galleryChange("Beginning");
            $('#hbtn2').removeClass('active_new active_new-eow');
            $('#hbtn3').removeClass('active_new active_new-tc');            
            $('#hbtn1').addClass('active_new active_new-hb');

            $('#aboutMenu').addClass('active');
            $('#first_content').removeClass('d-none');
            $('#first_content').addClass('d-flex');
            $('#second_content').removeClass('d-flex');
            $('#second_content').addClass('d-none');
            $('#third_content').removeClass('d-flex');
            $('#third_content').addClass('d-none');
            
            if($('body').width()<=480){
                $('html, body').animate({
                    scrollTop: $("#first_content").offset().top -300
                }, 0);
            }

            else{
                if ($(document).scrollTop() > 20) {
                    $('html, body').animate({
                        scrollTop: $("#first_content").offset().top -280
                    }, 0);
                } else {
                    $('html, body').animate({
                        scrollTop: $("#first_content").offset().top -300
                    }, 2000);
                }
            }
            
        });

        $("#hbtn2").click(function() {
            galleryChange("Expanding");
            $('#hbtn1').removeClass('active_new active_new-hb');
            $('#hbtn3').removeClass('active_new active_new-tc');
            $('#hbtn2').addClass('active_new active_new-eow');

            $('#aboutMenu').addClass('active');
            $('#first_content').removeClass('d-flex');
            $('#first_content').addClass('d-none');
            $('#second_content').removeClass('d-none');
            $('#second_content').addClass('d-flex');
            $('#third_content').removeClass('d-flex');
            $('#third_content').addClass('d-none');

            if($('body').width()<=480){
                $('html, body').animate({
                    scrollTop: $("#second_content").offset().top -300
                }, 0);
            }

            else{
                if ($(document).scrollTop() > 20) {
                    $('html, body').animate({
                        scrollTop: $("#second_content").offset().top -280
                    }, 0);
                } else {
                    $('html, body').animate({
                        scrollTop: $("#second_content").offset().top -300
                    }, 2000);
                }
            }

            
        });

        $("#hbtn3").click(function() {            
            galleryChange("Contemporary");
            $('#hbtn1').removeClass('active_new active_new-hb');
            $('#hbtn2').removeClass('active_new active_new-eow');
            $('#hbtn3').addClass('active_new active_new-tc');

            $('#aboutMenu').addClass('active');            
            $('#first_content').removeClass('d-flex');
            $('#first_content').addClass('d-none');
            $('#second_content').removeClass('d-flex');
            $('#second_content').addClass('d-none');
            $('#third_content').removeClass('d-none');
            $('#third_content').addClass('d-flex');

            if($('body').width()<=480){
                $('html, body').animate({
                    scrollTop: $("#third_content").offset().top -300
                }, 0);
            }

            else{
                if ($(document).scrollTop() > 20) {
                    $('html, body').animate({
                        scrollTop: $("#third_content").offset().top -265
                    }, 0);
                } else {
                    $('html, body').animate({
                        scrollTop: $("#third_content").offset().top -285
                    }, 2000);
                }
            }
            
        });
    });
</script>

<script>
        $(document).ready(function(){
      $("#hbtn2").click(function(){
        $(".first_content_section .third_content_section").hide();
      });
      $("#hbtn1").click(function(){
        $(".second_content_section").show();
      });
    });
</script>


<?php $this->load->view('components/footer'); ?>