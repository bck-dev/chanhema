<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-img">
    <!-- <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div> -->
    <!-- <h1 class="text-capitalize product-banner-heading">Care For Your Jewellery</h1> -->
</section>

<section class="product-heading-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h2 class="product-heading">
                    <?php echo $active_category; ?>
                    <span id="product-page-category-dropdown-btn" style="cursor: pointer;">
                        <img src="<?php echo base_url('assets/frontend/'); ?>/img/product-arrow.png" class="img-fluid "
                            alt="">
                    </span>
                </h2>
                <div class="product-page-category-dropdown-box">
                    <?php if ($active_category != "All Jewelery") : ?>
                    <a class="product-page-category-dropdown my-3" href="<?php echo base_url(); ?>products/All">
                        All Jewelery
                    </a>
                    <?php endif; ?>
                    <?php foreach ($categories as $category) : ?>
                    <?php if ($category['category_name'] != $active_category) : ?>
                    <a class="product-page-category-dropdown my-3"
                        href="<?php echo base_url(); ?>products/<?php echo $category['category_name']; ?>">
                        <?php echo $category['category_name']; ?>
                    </a>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div>
</section>

<section>
    <div class="container pt-5">
        <div class="row">
            <input type="hidden" value="<?php echo $active_category; ?>" id="category" />
            <input type="hidden" value="" id="sub-category" />
            <input type="hidden" value="" id="collection" />
            <div class="col-lg-5 col-md-6 col-xs-12">


                <div class="dropdown">
                    <div class="product-page-dropdown" type="button" data-toggle="dropdown"><span
                            id="sub-category-placeholder">FILTER BY STONE</span>
                    </div>
                    <ul class="dropdown-menu product-filtr-dropdown">
                        <?php foreach ($subCatagories as $sc) : ?>
                        <li onclick="filter(this)" class="products-options" data-type="Sub"
                            data-value="<?php echo $sc->sub_catagory ?>"><?php echo $sc->sub_catagory ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <span class="product-filter-icon"><img
                        src="<?php echo base_url('assets/frontend/'); ?>/img/filter-icon.png"
                        class="img-fluid product-filter-icon" alt=""></span>
            </div>

            <div class="col-lg-5 col-md-6 col-xs-12">
                <div class="dropdown">
                    <div class="product-page-dropdown" type="button" data-toggle="dropdown"><span
                            id="collection-placeholder">FILTER BY COLLECTION</span>
                    </div>
                    <ul class="dropdown-menu product-filtr-dropdown">
                        <?php foreach ($allCollections as $c) : ?>
                        <li onclick="filter(this)" class="products-options" data-type="Collect"
                            data-value="<?php echo $c->collection_name ?>"><?php echo $c->collection_name ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <span class="product-filter-icon"><img
                        src="<?php echo base_url('assets/frontend/'); ?>/img/filter-icon.png"
                        class="img-fluid product-filter-icon" alt=""></span>
            </div>
        </div>
    </div>
    </div>
</section>
<hr>

<section>
    <div class="container">
        <div class="row" id="products-section">
            <?php foreach ($products as $p) : ?>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 hover_div px-3 px-lg-4">
                <div class="product-item-area">
                    <div class="">
                        <img src="<?php echo base_url(); ?><?php echo $p->image; ?>" class="w-100" alt="">
                    </div>
                    <div class="caption">
                        <h4 class="product-item-heading"><?php echo $p->product_name; ?></h4>
                        <p class="product-item-description"><?php echo $p->product_description; ?></p>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<section>
    <div class="container product-inquire">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <h2 class="product-custamisation-heading">Interested in our exquisite designs</h2>
                <p class="product-custamisation-content mt-4">Find out more and visit one of our stores for expert
                    guidance on our product range and assistance selecting the Jewellery.</p>

                <div class="row mt-5">
                    <div class="col-xs-3 col-lg-2 col-md-1"></div>
                    <div class="col-xs-6 col-lg-8 col-md-12 collection-bottom-btn-area">
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <div class="product-custamisation-icon-text text-md-right d-flex m-auto">
                                    <div class="m-auto d-flex">
                                        <div>
                                            <a class="product-custamisation-icon-text"
                                                href="<?php echo base_url('store'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/'); ?>/img/location-icon.png"
                                                    class="img-fluid " alt="">
                                            </a>
                                        </div>
                                        <div class="v-align-fix">
                                            <a class="product-custamisation-icon-text"
                                                href="<?php echo base_url('store'); ?>">
                                                FIND A STORE
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="product-custamisation-icon-text text-center d-flex m-auto justify-center">
                                    <div class="m-auto d-flex">
                                        <div>
                                            <a class="product-custamisation-icon-text"
                                                href="<?php echo base_url('shipping'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/'); ?>/img/gift-box.png"
                                                    class="img-fluid " alt="">
                                            </a>
                                        </div>
                                        <div class="v-align-fix">
                                            <a class="product-custamisation-icon-text"
                                                href="<?php echo base_url('shipping'); ?>">
                                                INTERNATIONAL SHIPPING
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="product-custamisation-icon-text text-md-left d-flex m-auto">
                                    <div class="m-auto d-flex">
                                        <div>
                                            <a class="product-custamisation-icon-text"
                                                href="<?php echo base_url('contact'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/'); ?>/img/bell.png"
                                                    class="img-fluid " alt="">
                                            </a>
                                        </div>
                                        <div class="v-align-fix">
                                            <a class="product-custamisation-icon-text"
                                                href="<?php echo base_url('contact'); ?>">
                                                GET IN TOUCH
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3 col-lg-2 col-md-1"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container product-bottom-sections">
        <div class="row">
            <div class="col-md-6">
                <div class="row product-collection-area">
                    <div class="col-6 product-collection-img-area">
                        <img src="<?php echo base_url('assets/frontend/') . '/img/' . $collection1 . '.png'; ?>"
                            class="img-fluid " alt="">
                    </div>
                    <div class="col-6 product-collection-content-area">
                        <a href="<?php echo base_url('collections/' . $collection1); ?>">
                            <div class="">
                                <p class="product-collection-sub-heading">COLLECTION</p>
                                <P class="product-collection-heading"><?php echo $collection1 ?></P>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row product-collection-area">
                    <div class="col-6 product-collection-img-area">
                        <img src="<?php echo base_url('assets/frontend/') . '/img/' . $collection2 . '.png'; ?>"
                            class="img-fluid " alt="">
                    </div>
                    <div class="col-6 product-collection-content-area">
                        <a href="<?php echo base_url('collections/' . $collection2); ?>">
                            <div class="">
                                <p class="product-collection-sub-heading">COLLECTION</p>
                                <P class="product-collection-heading"><?php echo $collection2 ?></P>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<script>
$(document).ready(function() {
    $("#product-page-category-dropdown-btn").click(function() {
        if ($('.product-page-category-dropdown-box').is(":visible")) {
            $('.product-page-category-dropdown-box').hide();
        } else {
            $('.product-page-category-dropdown-box').show();
        }
    });
    $("#filter-btn").click(function() {
        if ($('.filter-box').is(":visible")) {
            $('.filter-box').hide();
        } else {
            $('.filter-box').show();
        }
    });


});

function filter(input) {
    var inputType = input.getAttribute("data-type");
    var inputValue = input.getAttribute("data-value");

    if (inputType == "Sub") {
        $('#sub-category').val(inputValue);
        $('#sub-category-placeholder').html(inputValue);
    } else {
        $('#collection').val(inputValue);
        $('#collection-placeholder').html(inputValue);
    }

    var category = $('#category').val();
    var sub_category = $('#sub-category').val();
    var collection = $('#collection').val();

    $.ajax({
        url: '<?php echo base_url('ajaxProducts') ?>',
        type: 'post',
        data: {
            category: category,
            sub_category: sub_category,
            collection: collection
        },
        dataType: 'json',
        success: function(results) {
            console.log(results);

            var products = "";
            jQuery.each(results, function(key, val) {
                var product = "";
                product = product +
                    '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 hover_div px-3 px-lg-4">';
                product = product + '<div class="product-item-area">';
                product = product + '<div class="">';
                product = product + '<img src="<?php echo base_url(); ?>' + val['image'] +
                    '" class="w-100" alt="">';
                product = product + '</div>';
                product = product + '<div class="caption">';
                product = product + '<h4 class="product-item-heading">' + val['product_name'] +
                    '</h4>';
                product = product + '<p class="product-item-description">' + val[
                    'product_description'] + '</p>';
                product = product + '</div>';
                product = product + '</div>';
                product = product + '</div>';
                products = products + product;
            });
            console.log(products);
            $('#products-section').html(products);
        },

        error: function() {
            console.log('error');
        }
    });
}
</script>

<?php $this->load->view('components/footer'); ?>