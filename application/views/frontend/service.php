<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-imgs">
    <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return
                Policy</a></span>
        <span class="policy-close">x</span>
    </div>
    <h1 class="text-capitalize">
        <!-- Pledge Of Service -->
    </h1>
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>
                We wish to offer you the ultimate ease of service whether it be customizing a gift for a loved one or
                placing a special order. Our expert team looks forward to giving you the best possible shopping and
                after-care experience.
            </p>
        </div>
    </div>
</section>

<section class="pt-5 mb-5 mt-1">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="card card1" style="width: 100%;">
                    <img class="card-img-top" src="<?php echo base_url('assets/frontend/'); ?>img/custamise.jpg"
                        alt="Card image cap">
                    <div class="title-section-card">
                        <h2 class="">Customise</h2>
                        <div class="patarn-img">

                        </div>
                    </div>
                    <div class="card-body">

                        <p class="card-text same-size-box-service">For a truly unique gift, any of our creations can be
                            customised using the precious metal and stone of your choice. With the personalised
                            assistance of our in-house team, it is also possible for you to create your own bespoke
                            design. Please contact us for further information on how we may help you bring your ideas to
                            life.</p>
                        <a href="./contact">Inquire</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card card1" style="width: 100%;">
                    <img class="card-img-top"
                        src="<?php echo base_url('assets/frontend/'); ?>/img/services-order-online.jpg"
                        alt="Card image cap">
                    <div class="title-section-card">
                        <h2 class="serv-title tit2">Order Online</h2>
                        <div class="patarn-img">

                        </div>
                    </div>
                    <div class="card-body">

                        <p class="card-text same-size-box-service">All of our products featured on the website are
                            available for purchase with prices provided on request. Please forward your query through
                            our inquiries page and our team will be in touch to assist you with the process. Please
                            refer to our Shipping Page for further relevant information.</p>
                        <a href="./contact">Inquire</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card card1" style="width: 100%;">
                    <img class="card-img-top"
                        src="<?php echo base_url('assets/frontend/'); ?>/img/Services_After_sales.jpg"
                        alt="Card image cap">
                    <div class="title-section-card">
                        <h2 class="serv-title tit3">After-Sales</h2>
                        <div class="patarn-img">

                        </div>
                    </div>
                    <div class="card-body">
                        <p class="card-text same-size-box-service">We are committed to providing you with a complete
                            range of jewellery care services. Our team is well equipped to offer you expert advice on
                            all your jewellery concerns, whether it be on how to personalize your jewellery, restore
                            them or simply preserve their beauty and longevity.</p>
                        <a href="<?php echo base_url('aftersale'); ?>">Read More</a>


                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<section id="quality">
    <div class="container">
        <h1 class="topic mt-5">Seeking Help?</h1>
        <p class="para-quality">For a truly unique gift, any of our creations can be customised using the precious metal
            and stone of your choice. With the personalised assistance of our in-house team, You can also create your
            own bespoke design.</p>
        <div class="phone-number">
            <div class="custom-inquire-area"><a class="custom-inquire-text" href="./contact">Click here for
                    inquiries</a></div>
            <!-- <a class="inquire-num" href="tel:0112874893">+94 11 287 48 93</a> -->
            <div class="custom-inquire-area"><a class="inquire-email"
                    href="mailto:mail@hemachandras.com">mail@hemachandras.com</a></div>
        </div>

    </div>
</section>

<?php $this->load->view('components/footer'); ?>