<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Landing Page</title>
  <link rel="stylesheet" href="landingpage/css/flexboxgrid.css">
  <link rel="stylesheet" href="landingpage/css/font-awesome.css">
  <link rel="stylesheet" href="landingpage/css/style.css">
  <!-- favicons for browsers -->
    <link rel="apple-touch-icon" sizes="60x60" href="landingpage/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="landingpage/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="landingpage/images/favicon-16x16.png">
    <link rel="manifest" href="landingpage/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="landingpage/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="landingpage/images/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="landingpage/images/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
</head>

<body>
    <div class="img-up">
<img src="landingpage/images/Vector%20Smart%20Object%20copy%206.png">
    </div>
    
    <div class="up-logo">
    <img src="landingpage/images/Vector%20Smart%20Object.png">
    
    </div>
  
  <!-- SHOWCASE -->
  <section id="showcase">
    <div class="container">
      <div class="row center-xs center-sm center-md center-lg middle-xs middle-sm middle-md middle-lg">
        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-7 showcase-content">

        </div>
      </div>
    </div>
  </section>

<!--blue card-->

<section id="blue-card">
    <div class="container">
      <div class="row center-xs center-sm center-md center-lg middle-xs middle-sm middle-md middle-lg">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 showcase-content">
            <div class="blue-line">
<h2 class="heading-up">We are currently crafting our new digital experience</h2>
                <p id="blue-para">We apologize for the inconvenience, until we are up and running you can walk into our store located in<br><div class="address"><b>No. 939, Peradeniya Road Kandy,</b></div><div class="normal">you can reach us for further assistance </div><div class="tel"><b>+94 (0) 812 387 387</b></div> 
               <p>or</p>  <h2 class="italy">mail@hemachandras.com</h2>  
                </p>
          </div>
            </div>
      </div>
    </div>
    
  </section>
    
    <img src="landingpage/images/Vector%20Smart%20Object%20copy%206.png">


<!-- blue card ends -->
<!--    Blue card ends-->
  <!-- two column grid -->
    


 
  



</body>

</html>