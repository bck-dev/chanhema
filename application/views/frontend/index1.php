<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Hemchandra | Home </title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
       
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>vendors/css/grid.css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>vendors/css/ionicons.min.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>vendors/css/normalize.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>vendors/css/animate.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>resources/css/stylesheet.css"/>
        <!-- open sans font used -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i" rel="stylesheet">
         <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/assets/owl.carousel.min.css">
        <!-- hover effect style sheet-->

        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>vendors/css/hovereffects/set1.css">
        <!-- click product stylesheet-->
        <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <!-- Hemachandra Responsive designing -->
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>resources/css/queries.css"/>
        <!-- Product slide effect stylehseet-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/'); ?>vendors/css/flickityproductslide.css"/>
        <!---- Slick cdn slider-->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

        
        <!-- favicons for browsers -->
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('assets/frontend/'); ?>resources/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/frontend/'); ?>resources/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/frontend/'); ?>resources/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo base_url('assets/frontend/'); ?>resources/img/favicon/site.webmanifest">
        <link rel="mask-icon" href="<?php echo base_url('assets/frontend/'); ?>resources/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="<?php echo base_url('assets/frontend/'); ?>resources/img/favicon/favicon.ico">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-config" content="<?php echo base_url('assets/frontend/'); ?>resources/img/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
    
    </head>
    <body>


         <header class="navig" id="main-menu-back" >
                <div class="main-menu">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">



                                <div id="top-head" class="navColor">
                                        <ul class="language-list">
                                                 <li>
                                                     <i class="fas fa-chevron-right"></i>
                                                    <select class="langselectbar" name="droplangsel">

                                                        <option>EN</option>
                                                        <option>SL</option>
                                                    </select>

                                                    <input type="text" name="text" id="text-serch" class="search">
                                                    <img src="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/iconfinder_basics-19_296814.png" class="search-ico" alt="">


                                                </li>
                                            </ul>
                                    <ul class="shiping-nav">
                                        <li><a href="news.html">Media</a></li>
                                        <li><a href="contactUs.html">Contact</a> </li>
                                        <li> <a href="shippin.html">Shipping</a> </li>
                                    </ul>
                                </div>



                            </div>
                        </div>
                        <div class="row">


                            <div id="second-nav" class="navcol" >
                                <ul>
                                    <li class="about-panel dow-panel-sec"><a href="#">About us </a></li>
                                    <li><a href="gemstone.html">Gemstones  </a></li>
                                    <li class="panel-section-sellection"><a href="#">Collection </a></li>
                                    <li>
                                        <a href=""> <img src="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/logo_03.jpg" class="logo" alt=""></a>
                                    </li>
                                    <li><a href="Guarantee.html">Guarantee </a></li>
                                    <li class="active"><a href="service.html" class="text-active">Services</a></li>
                                    <li><a href="store.html">Stores</a></li>
                                </ul>

                            </div>


                        </div>
                    </div>
                </div>




            </header>
            
            <section class="dropdown-menu" style="display: none;">
            <div class="section group">
            	<div class="col span_1_of_4">
            	    <p>Collections</p>
            	    <ul>
            	        <li><a href="collection.html">Love & Life</a></li>
            	        <li><a hre="#">Ceylone Doll</a></li>
            	        <li><a hre="#">Heritage</a></li>
            	       
            	    </ul>
            	
            	</div>
            	<!--	<div class="col span_1_of_4">-->
            	<!--    <p>Collections</p>-->
            	<!--    <ul>-->
            	<!--        <li><a href="collection.html">Love & Life</a></li>-->
            	<!--        <li><a hre="#">Ceylone Doll</a></li>-->
            	<!--        <li><a hre="#">Heritage</a></li>-->
            	       
            	<!--    </ul>-->
            	
            	<!--</div>-->
            	
            	<div class="col span_1_of_4 color-box">
            <p>Fine JEWELRY</p>
            	    <ul>
            	        <li><a hre="#">Adamantine</a></li>
            	        <li><a hre="#">Celestial Art</a></li>
            	        <li><a hre="#">Contemporary</a></li>
            	        <li><a hre="#">Moon Stone</a></li>
            	        <li><a hre="#">Island Life</a></li>
            	         <li class="vall-btn"><a hre="#">view All</a></li>
            	       
            	    </ul>
            	</div>
            	<div class="col span_1_of_4 color-box">
                        <p>JEWELRY</p>
            	    <ul>
            	        <li><a hre="#">Accessories</a></li>
            	        <li><a hre="#">Bracelets</a></li>
            	        <li><a hre="#">Charms</a></li>
            	        <li><a hre="#">Earrings</a></li>
            	        <li><a hre="#">Engagement Rings</a></li>
            	        <li><a hre="#">Men's Jewelry</a></li>
            	        <li><a hre="#">Necklaces</a></li>
            	        <li class="vall-btn"><a hre="#">view All</a></li>
            	       
            	    </ul>
            	</div>
            	
            </div>
            </section>
            
          
<section id="story-panel" class="dropdown-menu1" style="display: none;">
            <div class="section group">
            	<div class="col span_1_of_4">
            	    <p>Collections</p>
            	    <ul>
            	        <li style="font-family:sans-serif; padding-bottom:40px;font-weight:bold;"><a hre="#" style="color:black; padding-bottom:30px;padding-left:36px;" class="sub-tit">Company</a></li>
            	        <li><a href="history.html" style="font-family: 'Open Sans', sans-serif; color:black; font-size:14px;padding-left:36px;cursor:pointer;">Story</a></li>
            	         <li style="font-family: 'Open Sans', sans-serif; font-size:14px; cursor:pointer;"><a href="Sustainability.html" style="font-family: 'Open Sans', sans-serif; color:black; font-size:14px;padding-left:36px;">Sustainability</a></li>
            	         <li style="font-family: 'Open Sans', sans-serif; font-size:14px; cursor:pointer;"><a href="peoples.html" style="font-family: 'Open Sans', sans-serif; color:black; font-size:14px;padding-left:36px;">People</a></li>
            	          <li style="font-family: 'Open Sans', sans-serif; font-size:14px;cursor:pointer;"><a href="news.html" style="font-family: 'Open Sans', sans-serif; color:black; font-size:14px;padding-left:36px;">News</a></li>
            	       
            	       
            	    </ul>
            	
            	</div>
            	<div class="col span_1_of_4 color-box">
            <p hidden>Fine JEWELRY</p>
            
            	    <ul>
            	        <!--<li><a hre="#">Adamantine</a></li>-->
            	        <!--<li><a hre="#">Celestial Art</a></li>-->
            	       <li><a hre="#"><image style="padding-bottom:5px;" src="<?php echo base_url('assets/frontend/'); ?>img/Story1.jpg" width="250px" height="130px"></a></li>
            	        <li><a hre="#"><image src="<?php echo base_url('assets/frontend/'); ?>img/People1.jpg" width="250px" height="130px"></a></li>
            	        <!--<li><a hre="#">Island Life</a></li>-->
            	        <!--<li><a hre="#">view All</a></li>-->
            	       
            	    </ul>
            	</div>
            	<div class="col span_1_of_4 color-box">
                        <p hidden>JEWELRY</p>
            	    <ul>
            	        <li style="font-family:sans-serif; padding-bottom:40px;font-weight:bold;"><a hre="#" style="color:black; padding-bottom:30px;" class="sub-tit">Reach Us</a></li>
            	        <!--<li class="padding-down"><a hre="#">Bracelets</a></li>-->
            	        <li style="font-family:sans-serif; font-size:15px; font-weight:bold; padding-bottom:20px;color:black;"><a hre="#"  style="font-family:sans-serif; color:black; padding-bottom:30px;">COLOMBO 07- Srilanka </a></li>
            	        <li style="font-family:sans-serif; font-size:14px;"><a hre="#"  style="font-family:sans-serif; color:black; padding-bottom:30px;">NO 74 Flower Rd, colombo 00700</a></li>
            	        <li style="font-family:sans-serif; font-size:14px;"><a hre="#"  style="font-family:sans-serif; color:black; padding-bottom:30px;">0094 11 2874893</a></li>
            	        <li style="font-family:sans-serif; font-size:14px; padding-bottom:30px;"><a hre="#"  style="font-family:sans-serif; color:black; padding-bottom:30px;">9am-5.30pm</a></li>
            	        <!--<li><a hre="#">Necklaces</a></li>-->
            	        <!--<li><a hre="#">view All</a></li>-->
            	        <li class="icons">
                    <i style="font-size:15px; cursor:pointer; padding-right:5px;" class="fab fa-facebook-f"></i>
                    <i style="font-size:15px; cursor:pointer; padding-right:5px;"class="fab fa-twitter"></i>
                    <i style="font-size:15px; cursor:pointer; padding-right:5px;"class="fab fa-instagram"></i>
                    <i style="font-size:15px; cursor:pointer; padding-right:5px;"class="fab fa-youtube"></i>
                    </li>
            	       
            	    </ul>
            	</div>
            	
            </section>
     
            
            
            
            <section class="sticky">
            <div class="container">
                <div class="col-md-12">
                    <div class="stickynav">
                        <div class="nanigation-stick">
                                <div  class="stik-nav">
                                        <ul>
                                            <li><a href="history.html">About us </a></li>
                                            <li><a href="gemstone.html">Gemstones  </a></li>
                                            <li><a href="collection.html">Collection </a></li>
                                            <li class="border-img">
                                                <a href=""> <img src="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/stick_navigation_bar.png" class="logoStick" alt=""></a>
                                            </li>
                                            <li><a href="Guarantee.html">Guarantee </a></li>
                                            <li><a href="service.html" class="text-active">Services</a></li>
                                            <li><a href="store.html">Stores</a></li>
                                        </ul>

                                    </div>
                        </div>
                    </div>
                </div>
            </div>
       </section>


    
    
    
    
    
<div id="getFixed">
     <!-- section banner-->
        <section  class="banners-carosul"  >
             
            <div class="return-policy-notice" id="policy-div" >
                <p class="policy-paragraph">We Just Update our <a href="#" class="policy-link">Return policy</a></p>
                <div class="policy-close-divider">
                    <a style="color: white" class="close-ionic-icon" id="return-policy-close"><i class="ion-ios-close-empty"></i></a>
                </div>
            </div>
            
          
            
        
          <div id="slider-container" >
            <ul class="images-container">
            <li>
               <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/banner/bannerimage1.png">
              </li>
              <li>
               <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/banner/bannerimage2.png">
              </li>
              <li>
                <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/banner/bannerimage3.png">
              </li>
              <li>
                <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/banner/bannerimage4.png">
              </li>
              <li>
                <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/banner/bannerimage5.png">
              </li>
            </ul>
            <span class="arrow a-left banner-bullet-rows"></span>
            <span class="arrow a-right banner-bullet-rows"></span>
            <div class="bullets-container banner-bullet-rows"></div>
          </div>
        </section>
    </div>
        
    <div class="divider-paralax ">
        <div class="js--section-feature"></div>
        <!-- section description of story-->
        <section class="first-section paralax-effect " id="container"  >            
            <!-- Box with contents-->
            <div class="row box-grid-style" >
                
                    <div class="grid">
                        <div class="content-banner-grid">
                            <div class="col span-1-of-3">
                            <figure class="effect-sarah responsive-view">
                                <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/gridbox/1.png" alt="img13"/>
                                <figcaption>
                                    <h2 class="grid-main-ban-class">Gurantee</h2>
                                    <p class="grid-paragraph-class">THE QUEST to find the glittering fire</p>
                                    <a href="#">View more</a>
                                </figcaption>			
                            </figure>
                        </div>
                        
                        <div class="col span-1-of-3">
                            <figure class="effect-sarah">
                                <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/gridbox/2.png" alt="img20"/>
                                <figcaption>
                                    <h2 class="grid-main-ban-class">visionary spirit</h2>
                                    <p class="grid-paragraph-class">THE QUEST to find the glittering fire</p>
                                    <a href="#">View more</a>
                                </figcaption>			
                            </figure>
                        </div>
                        
                        <div class="col span-1-of-3">
                            <figure class="effect-sarah">
                                <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/gridbox/3.png" alt="img13"/>
                                <figcaption>
                                    <h2 class="grid-main-ban-class">international Shipping</h2>
                                    <p class="grid-paragraph-class">THE QUEST to find the glittering fire</p>
                                    <a href="#">View more</a>
                                </figcaption>			
                            </figure>
                        </div>
                            
                            
                        </div>
                    
                    </div>
            </div>
            
        
        </section>   
        

        

         <section class="section_story_paragraph_hemachandra " id="storysection"  style="width: 100% !important;">

             <div class="row ">
                <div class="content-desc1 ">
                    <p class="long-copy">
                        The Kingdom of Kandy lasted till 1815 and during this period it influenced it’s 
                        own original style of decorative items worn for personal adornment that consisted of local 
                        gemstones. Crafted by genorations of designated caste of jewellery makers served specially for royal families at the time using highest grade precious metal and gemstones with great level of craftsmanship. HEMACHANDRA has continued this bespoke designs and 
                        craftsmanship of unique Kandian jewellery to date through generations.
                    </p>
                    <div class="more">
                         <a href="#" class="story">The Story</a>
                    </div>
                   
                </div>
                
            </div>
            
        </section>
        
         <!---=========================== ========================================-->
        <!---=========================== Mohters day special New ================-->
          <section class="pick-motherday">
            <div class="row ">
                <div class="main-box">
                    <img id="mothersdaypatternImage" src="<?php echo base_url('assets/frontend/'); ?>resources/img/pattern/pattern.png" class="patternmaker" alt="pattern"/>
                    <div >
                        <div class="col span-1-of-2">
                            <div class="mother-day-content-grid" >
                                <h3 class="motherday-pics-title" >Our Pick for mother's day</h3>
                                <p class="mothersday-pic-paragraph">Hemachandra was fascinated by gemstones: he established contacts all over the world to find the most precious, the rarest and the most exceptional stones. </p>
                                <div class="discover">
                                     <a href="#" class="story">DISCOVER</a>
                                </div>
                            </div>
                        </div>
                        <div class="col span-1-of-2">
                            <div class="mothers-day-right-section-content" >
                                <img  src="<?php echo base_url('assets/frontend/'); ?>resources/img/static_image/image1.png" alt="content image 1"     class="mothers-day-special-img">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
              
        </section>
         <!---=========================== Mohters day special Modification ================-->
         <!---============================================================================-->
        
        

        <!--Product slider-->
         <section class="product-slide">
             
             <div class="left-opacity"></div>
            <div class="right-opacity"></div>
            <div class="row">
                <div class="content-area">  
                    
                        <div class="carousel-wrapper">
                          <div class="carousel" data-flickity='{ "autoPlay": 3000 , "prevNextButtons": false, "initialIndex": 1, "pageDots": false, "wrapAround": true }'>
                                                            
                            <div class="carousel-cell">
                              <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/product/product3.png" /> <!--http://www.pngmart.com/files/1/Gold-Rings-PNG-Photos.png-->
                              <div class="price">
                               <h4>CLUSTERS</h4>
                                <p>Anatometal threaded and threadless Gem Clusters allow a personal look to your piercings</p>
                                <a class="more" href="#">Explore </a>  
                              </div>
                            </div>
                            <div class="carousel-cell">
                              <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/product/product4.png" />
                              <div class="price">
                                 <h4>CLUSTERS</h4>
                                <p>Anatometal threaded and threadless Gem Clusters allow a personal look to your piercings</p>
                                <a class="more" href="#">Explore</a>  
                              </div>
                            </div>
                            <div class="carousel-cell ">
                              <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/product/product4.png" />
                              <div class="price">
                                 <h4>CLUSTERS</h4>
                                <p>Anatometal threaded and threadless Gem Clusters allow a personal look to your piercings</p>
                                <a class="more" href="#">Explore</a>  
                              </div>
                            </div>
                            <div class="carousel-cell ">
                              <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/product/product4.png" />
                              <div class="price">
                                 <h4>CLUSTERS</h4>
                                <p>Anatometal threaded and threadless Gem Clusters allow a personal look to your piercings</p>
                                <a class="more" href="#">Explore</a>  
                              </div>
                            </div>
                            <div class="carousel-cell ">
                              <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/product/product4.png" />
                              <div class="price">
                                <h4>CLUSTERS</h4>
                                <p>Anatometal threaded and threadless Gem Clusters allow a personal look to your piercings</p>
                                <a class="more" href="#">Explore</a>  
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
        </section>
        <!--Product slider end-->
        
        
        
        
        <!-- =================================== Craftman ship customize ==========================-->
         <!--Craftman Ship-->
        <section class="craftman-ship">
            <div class="row motherday-divider">
                <div class="main-box">
                    <img id="craftmanImage" class="craftman-box patternmaker" src="<?php echo base_url('assets/frontend/'); ?>resources/img/pattern/pattern.png"  alt="pattern"/>
                    <div>
                        <div class="col span-1-of-2 craft-left">
                            <div class="craftgrid-left">
                                <img  class="craft-man-left-img" src="<?php echo base_url('assets/frontend/'); ?>resources/img/static_image/image2.png" alt="content image 1" >
                            </div>
                        </div>
                        <div class="col span-1-of-2 craft-content-class">
                            <div class="craftman-content-grid">
                                <h3 class="craftman-section-title">The Craftmanship</h3>
                                <p class="craftman-section-paragraph">Hemachandra was fascinated by gemstones: he established contacts all over the world to find the most precious, the rarest and the most exceptional stones. </p>
                                <div class="discover">
                                     <a href="#" class="story">GEMSTONES</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </section>
        
        <!-- =================================== Craftman ship Customize==========================-->
        
        
      
    
    
        <!-- section grid box -->
        <section class="hemachandra-grid-desc">
            <div class="row hemachandra-widthadjuster">
                <div class="col span-1-of-3 box">
                    <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/hemachandra_speacial/image11.png" alt="image1"/>
                    <h3 class="hemachandra-special-title">Design</h3>
                    <div class="hemachandra-special-desc">
                        <p>Whether in geometric perfection of design or in the purity of its Art Deco lines, has always displayed a passion for abstraction. For nearly 160 years, inclination for correct proportions, noble structures and majestic forms has influenced the artistic creation of its jewelry.</p>
                    </div>
                </div>
                <div class="col span-1-of-3 box">
                    <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/hemachandra_speacial/image2.png" alt="image2"/>
                    <h3 class="hemachandra-special-title">Metal</h3>
                    <div class="hemachandra-special-desc">
                        <p>They must therefore provide maximum protection in case of impacts, and each detail of their construction has been developed and tested to withstand intensive use. </p>
                    </div>
                </div>
                <div class="col span-1-of-3 box">
                    <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/hemachandra_speacial/image3.png" alt="image3"/>
                    <h3 class="hemachandra-special-title">Gemestones</h3>
                    <div class="hemachandra-special-desc">
                        <p>To us, a stone was deemed exceptional if it had a special quality; the capacity to project an inner beauty and strength. This is why Boucheron creations haveof the most precious and the most original stones.</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- section grid box end -->
        
         <!-- section hemachandra history of craft start here -->
        <section class="hemachandra-history-craft">
            <div class="row">
                <div class="historical-background">
                    <div class="history-description-box">
                        <div class="hemachandra-history-contents">
                            <h4 class="hemachandra-history-title">74 Year of Craftsmanship</h4>
                            <p class="heamchandra-paragraph">Hemachandra was fascinated by gemstones: he established contacts all over the world to find the most precious, the rarest and the most exceptional stones. </p>
                            <div class="more-info-history">
                                <a href="#" class="our-story">Our Story</a>
                            </div>
                        </div>
                    </div>
                </div>
                
               
            </div>
             <div class="bottom-divider-breaker">
            </div>
        </section>
        <!-- section hemachandra history of craft start here -->
        
        <!-- footer section start here-->
        <footer>
            <div class="row">
                <div class="footer-nav-links">
                    <div class="hemachandra-footer-logo">
                        <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/footerlogo/footerlogo.png" alt="footer logo"/>
                    </div>
                    <ul class="footer-nav">
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Customer Care</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Shipping</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Gurantee</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Contact</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Stores</a></li>
                    </ul>
                    <div class="sociallinks">
                        <ul class="social-link">
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-twitter"></i></a></li>
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-googleplus"></i></a></li>
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-instagram"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <section class="bottom-footer-container">
            <div class="secondrow">
                <div class="col span-1-of-3">
                    <p class="copywrite">
                       &copy; 2019 Hemachandra &dash; kandy
                    </p>
                   
                </div>
                <div class="col span-1-of-3">
                    <ul class="bottom-footer">
                        <li> <a href="termsandcondition.html">Terms & Condition</a> </li>
                         <li> <a href="legalnotice.html">Legal Notice</a> </li>
                         <li> <a href="privacy.html">Privacy Policy</a> </li>
                         <li> <a href="#">Site Map</a> </li>
                    </ul>
                </div>
                <div class="col span-1-of-3">
                    <div class="projectdone">
                        <a href="http://bckonnect.com/"><p class="projectby"> BcKonnect</p></a>
                        <img src="<?php echo base_url('assets/frontend/'); ?>resources/img/bcklogo.png" class="companylogoimage" alt="company logo">
                    </div>
                </div>
                
            </div>
        </section>
        

        
    </div>
               
        
        <!-- jquery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <!-- script file linked here-->
        <!-- Jquery Migration start-->
        <script src="<?php echo base_url('assets/frontend/'); ?>resources/js/browser.js"></script>
        <!-- Jquery Migration end-->
        <script src="<?php echo base_url('assets/frontend/'); ?>resources/js/script.js"></script>
        <script src="http://bckonnect.com/medex/assets/frontend/js/owl.carousel.min.js"></script>
          <!-- jquery waypoint reference link http://imakewebthings.com/waypoints/-->
        <script src="<?php echo base_url('assets/frontend/'); ?>vendors/js/jquery.waypoints.min.js"></script>
            
        <!-- click product slider-->
        <script type="text/javascript" src="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <!-- flip product grid slide js-->
        <script type="text/javascript" src="<?php echo base_url('assets/frontend/'); ?>vendors/js/flickity_product.min.js"></script>
        <!-- Steller paralax -->
        <script type="text/javascript" src="<?php echo base_url('assets/frontend/'); ?>vendors/js/steller.js"></script>
        <!-- jquery waypoint reference link http://imakewebthings.com/waypoints/-->
        <script src="<?php echo base_url('assets/frontend/'); ?>vendors/js/jquery.waypoints.min.js"></script>
        <!-- slick CDN javascript-->
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        
       
        
        
    </body>
</html>