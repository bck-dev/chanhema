<?php $this->load->view('components/headerNew'); ?>   
<?php $this->load->view('components/menu'); ?>

    <!-- header end -->
    <section id="service-img" style="background:url('<?php echo base_url($collection->banner_image); ?>') no-repeat; background-size:cover; background-position:center;">
        <h1 class="text-capitalize"></h1>
    </section>

    <section class="collection-product-images">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img class="w-100" src="<?php echo base_url(); ?><?php echo $collection->first_image; ?>" style="width:100%;" alt="">
                </div>
                <div class="col-md-6 d-none d-md-block">
                  <div class="collection-content-middle">
                    <span class="overview">OVERVIEW</span>
                    <h1 class="obeserve"><?php echo $collection->collection_name;?> COLLECTION</h1>
                    <p class="product-dis mb-0">
                        <?php echo $collection->main_description; ?>
                    </p>
                  </div>
                </div>
                <div class="col-md-6 d-block d-md-none">
                  <div class="text-center">
                    <span class="overview">OVERVIEW</span>
                    <h1 class="obeserve"><?php echo $collection->collection_name;?> COLLECTION</h1>
                    <p class="product-dis mb-0 text-center">
                        <?php echo $collection->main_description; ?>
                    </p>
                  </div>
                </div>
            </div>

            <?php if($collection->second_image): ?>
            <div class="row mt-5">
                <div class="col-md-6 d-none d-md-block">
                  <div class="w-100 collection-content-middle collection-blockquote text-center">
                      <i class="fas fa-quote-left" ></i>
                      <p class="m-0 my-4"><?php echo $collection->second_description;?></p>
                      <i class="fas fa-quote-right"></i>
                  </div>
                </div>

                <div class="col-md-6 d-block d-md-none ">
                  <div class="w-100 collection-blockquote text-center">
                      <i class="fas fa-quote-left" ></i>
                      <p class="m-0 my-4"><?php echo $collection->second_description;?></p>
                      <i class="fas fa-quote-right"></i>
                  </div>
                </div>
                <div class="col-md-6">
                    <img class="w-100" src="<?php echo base_url(); ?><?php echo $collection->second_image; ?>" alt="">
                </div>
            </div>
            <?php endif; ?>
        </div>
    </section>


    <section id="slid-product">
        <div class="owl-carousel owl-theme">
            <?php foreach ($products as $p) : ?>
                <div class="item">
                    <div class="pro-img">
                        
                        <img src="<?php echo base_url(); ?><?php echo $p->image; ?>" class="proone" alt="">
                                
                        <div class="pro-details text-center">
                            <h4 class="collection-slider-heading"><?php echo $p->product_name; ?></h4>
                            <p><?php echo $p->product_description; ?></p>
                            <!--<a href="#">Explore</a>-->
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>


        </div>
    </section>
    
    
    <section>
      <div class="container collection-inquire">
        <div class="row">
          <div class="col-lg-1"></div>
          <div class="col-lg-10">
            <h2 class="product-custamisation-heading">Interested in our exquisite designs</h2>
            <p class="product-custamisation-content mt-4">Find out more and visit one of our stores for expert guidance on our product range and assistance selecting the Jewellery.</p>

            <div class="row mt-5">
              <div class="col-xs-3 col-lg-2 col-md-1"></div>
              <div class="col-xs-6 col-lg-8 col-md-12 collection-bottom-btn-area">
                <div class="row">
                  <div class="col-md-3 text-center">
                    <div class="product-custamisation-icon-text text-md-right d-flex m-auto">
                      <div class="m-auto d-flex">
                        <div>
                          <a class="product-custamisation-icon-text" href="<?php echo base_url('store'); ?>">
                            <img src="<?php echo base_url('assets/frontend/'); ?>/img/location-icon.png" class="img-fluid " alt=""> 
                          </a>
                        </div>
                        <div class="v-align-fix">
                          <a class="product-custamisation-icon-text" href="<?php echo base_url('store'); ?>">
                             FIND A STORE
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 text-center">
                    <div class="product-custamisation-icon-text text-center d-flex m-auto justify-center">
                      <div class="m-auto d-flex">
                        <div>
                          <a class="product-custamisation-icon-text" href="<?php echo base_url('shipping'); ?>">
                            <img src="<?php echo base_url('assets/frontend/'); ?>/img/gift-box.png" class="img-fluid " alt="">
                          </a>
                        </div>
                        <div class="v-align-fix">
                          <a class="product-custamisation-icon-text" href="<?php echo base_url('shipping'); ?>">
                             INTERNATIONAL SHIPPING
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3 text-center">
                    <div class="product-custamisation-icon-text text-md-left d-flex m-auto">
                      <div class="m-auto d-flex">
                        <div>
                          <a class="product-custamisation-icon-text" href="<?php echo base_url('contact'); ?>">
                            <img src="<?php echo base_url('assets/frontend/'); ?>/img/bell.png" class="img-fluid " alt="">
                          </a>
                        </div>
                        <div class="v-align-fix">
                          <a class="product-custamisation-icon-text" href="<?php echo base_url('contact'); ?>">
                             GET IN TOUCH
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-3 col-lg-2 col-md-1"></div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section id="seek-help">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="contant-service-text">Customisation Service</h2>
                    <div class="row">
                        <div class="col align-self-center">
                          <div class="service-sentnce">
                            <p>For a truly unique  gift, any of our creations can be customised using the precious metal and stone  of your choice. With the personalised assistance of our in-house team, You can also create your own bespoke design.</p>
                          </div>
                        </div>
                    </div>
                    <div class="phone-number">
                      <!-- <a href="<?php echo base_url('contact'); ?>"><h1 class="inq-tel">For Inquiries</h1></a>
                      <span class="inq-tel-num">+94 81 238 73 87</span> -->
                      <div class="custom-inquire-area"><a class="custom-inquire-text" href="./contact">Click here for inquiries</a> </div>
                      <!-- <a class="inquire-num" href="tel:0112874893">+94 11 287 48 93</a> -->
                      <div class="custom-inquire-area"><a class="inquire-email" href="mailto:mail@hemachandras.com">mail@hemachandras.com</a></div>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <!-- footer strart -->

<?php $this->load->view('components/footer'); ?>
