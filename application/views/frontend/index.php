<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="main-head">
    <?php foreach ($main_banner as $data) : ?>
    <img class="d-block w-100 img-fluid"
        src="<?php echo base_url() ?>upload/mainbanner/<?php echo $data['main_banner_image']; ?>" alt="First slide">
    </div>
    <?php endforeach; ?>
</section>

<section id="middle-three">
    <div class="container">
        <div class="wrapper-middle d-flex justify-content-center main-slider-img-area">
            <div class="main-three-sec">
                <a style="color:#fff;" href="<?php echo base_url('story'); ?>"><img
                        src="<?php echo base_url('assets/frontend/'); ?>img/home-story-n.jpg" alt="">
                    <div class="three-sentence home-banner-bellow-content">
                        <h4>Story</h4>
                        <p class="home-banner-bottom-content">A passion for fine jewellery craftsmanship, a family
                            legacy</p>
                    </div>
                </a>
            </div>


            <div class="main-three-sec">
                <a style="color:#fff;" href="<?php echo base_url('news'); ?>"><img
                        src="<?php echo base_url('assets/frontend/'); ?>img/home-news-nw.jpg" alt="">
                    <div class="three-sentence home-banner-bellow-content">
                        <h4>News</h4>
                        <p class="home-banner-bottom-content">Keep up-to-date with our <br />latest developments</p>
                    </div>
                </a>
            </div>


            <div class="main-three-sec">
                <a style="color:#fff;" href="<?php echo base_url('guarantee'); ?>"><img
                        src="<?php echo base_url('assets/frontend/'); ?>img/home-guarantee.jpg" alt="">
                    <div class="three-sentence home-banner-bellow-content">
                        <h4>Guarantee</h4>
                        <p class="home-banner-bottom-content">Exceptional quality and trust, <br />the heart of our
                            ethos</p>
                    </div>
                </a>
            </div>

        </div>
    </div>
</section>

<section id="middle-discription">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="disc-main home-main-content">
                    <!-- <h4 class="home-history-heading">Origins – A rich cultural heritage </h4>     -->
                    <p class="home-main-content">
                        The story of our family-run jewellery company has unfolded over seven glittering decades.
                        Specialising in handcrafting elegant artisan gold and silver jewellery set with exceptional Sri
                        Lankan gemstones, we ensure each piece is perfectly unique. Quality and intricacy remain at the
                        forefront of our designs, which have been favoured by clients who make up an extraordinary
                        portfolio of heads of state, royalty and celebritites.
                    </p>
                    <p class="home-main-content">Descending from a long line of jewellers, a passion for the craft runs
                        in the family. For centuries the Vishvakula Caste, to which we owe our heritage, created
                        priceless treasures for Sri Lankan kings and queens.</p>
                    <p class="text-center"><a href="<?php echo base_url('story'); ?>"
                            class="disc-link home-story-btn">the Story</a></p>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="special-card">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6 no-gut">
                        <div>
                            <div class="border-image">
                                <img src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/gridbox/pattern.png"
                                    alt="">
                            </div>
                            <div class="text-wrapper">
                                <div class="mt-4 pt-1 home-featured-box">
                                    <h3 class="home-special-card">Kandyan Collection</h3>
                                    <div class="disc card-text  ellipsis">
                                        <!-- <p>Hemachandra was fascinated by gemstones: he established contacts all over the world to find the most precious, the rarest and the most exceptional stones. </p> -->
                                        <p>Characterised by stunning coloured gems and elegant motifs inspired by the
                                            former hill kingdom of Sri Lanka, this is one of our most opulent
                                            collections. Antique Sri Lankan beauty is fused with contemporary flair to
                                            create these intricate designs.</p>
                                    </div>
                                    <div class="discover">
                                        <a class="mothers-day-button"
                                            href="<?php echo base_url('collections/Kandyan%20Collection'); ?>">DISCOVER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 no-gut">
                        <div class="caid-image">
                            <img src="<?php echo base_url('assets/frontend/'); ?>img/homepage_kandyan.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="slid-product">

    <div class="owl-carousel owl-theme" id="home-carousel">
        <?php foreach ($products as $product) : ?>
        <div class="item">
            <div class="pro-img">
                <?php foreach ($product_images as $image) { ?>
                <?php if ($image['product_product_id'] == $product['product_id']) { ?>
                <img src="<?php echo base_url($image['product_images_name']); ?>" class="proone" alt="" />
                <?php
                            break;
                        }
                        ?>
                <?php } ?>
                <div class="pro-details text-center">
                    <h4 class="collection-slider-heading"><?php echo $product['product_name']; ?></h4>
                    <p><?php echo $product['product_description']; ?></p>
                    <!--<a href="#">Explore</a>-->
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</section>

<section id="special-ship">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6 no-gut">
                        <div class="caid-image">
                            <img src="<?php echo base_url('assets/frontend/'); ?>/img/home-gemstone.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 no-gut">


                        <div class="border-image">
                            <img src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/gridbox/pattern.png"
                                alt="">
                        </div>
                        <div class="text-wrapper">
                            <h3 class="home-special-card mt-3 home-featured-box2">Gemstones</h3>
                            <div class="disc">
                                <!-- <p>Hemachandra was fascinated by gemstones: he established contacts all over the world to find the most precious, the rarest and the most exceptional stones. </p> -->
                                <p>We take pride in ensuring all the gemstones used in our jewellery are carefully
                                    hand-selected by our experienced gemmologists. Exceptional quality sapphires of all
                                    hues, aquamarines, amethysts andperidots are among a variety of coloured gemstones
                                    found naturally in Sri Lanka’s mines.</p>
                            </div>
                            <div class="discover">
                                <a class="mothers-day-button" href="<?php echo base_url('gemstone'); ?>">DISCOVER</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="card-main-page">
    <div class="container px-4">
        <div class="row">
            <div class="col-md-4 px-4">
                <div class="card">
                    <img class="card-img-top w-100" src="<?php echo base_url('assets/frontend/'); ?>img/design-n.jpg"
                        alt="Card image cap">
                    <div class="card-body">
                        <h5 class="main-card-h home-card-heading">Design</h5>
                        <p calss="main-card-body">Emblematising timeless elegance, our jewellery is intended to be with
                            you a lifetime. Over the years, we have become renowned for the quality and intricacy of our
                            designs. Many of which are influenced by Sri Lanka’s rich history, and inspired by beautiful
                            motifs from architecture, nature and art.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4 px-4">
                <div class="card">
                    <img class="card-img-top w-100" src="<?php echo base_url('assets/frontend/'); ?>img/metal-n.jpg"
                        alt="Card image cap">
                    <div class="card-body">
                        <h5 class="main-card-h home-card-heading">Metal</h5>
                        <p calss="main-card-body">Selecting the finest most pure metals - gold and silver – is an
                            integral part of our creation process. Each item is quality marked in accordance with
                            international regulations. The fine jewellery we craft can be custom-made in a metal of your
                            choice.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4 px-4">
                <div class="card">
                    <img class="card-img-top w-100"
                        src="<?php echo base_url('assets/frontend/'); ?>/img/home-gemstones-nw.png"
                        alt="Card image cap">
                    <div class="card-body">
                        <h5 class="main-card-h home-card-heading">Gemstones</h5>
                        <p calss="main-card-body">We take great care in selecting gems that exude great beauty. Many
                            come from Sri Lanka’s mines, which have been famous for millenia, particularly for the
                            sapphires they hold. The originality of each Hemachandra creation comes in part from the
                            ethereality of the stones we choose.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="main-bot-card" class="mb-5">
    <div class="container home-craftmanship-img-container">
        <div class="row mobile-craftmanship">
            <div class="col-md-12 home-craftmanship-img-area">
                <img src="<?php echo base_url('assets/frontend/'); ?>/img/craft.png" class="img-fluid" alt="">
            </div>


            <div class="wrap-final-card">
                <h4 class="hemachandra-history-title home-special-card">75 Years of Craftsmanship</h4>

                <p class="heamchandra-paragraph home-craftman-text">Established in 1942 in Kandy, Sri Lanka, we remain a
                    family-run fine jeweller. Hemachandra continues to build on the family legacy begun by our visionary
                    founder, Ananda Cyril Hemachandra, who was an entrepreneur and master goldsmith. From the inception,
                    we have handcrafted exquisite gold and silver jewellery using precious gemstones. This focus on
                    preserving and celebrating the artisan skill of fine jewellery making has been central to every
                    generation of our family enterprise.</p>

                <div class="more-info-history pt-4">
                    <a class="home-more-info orange-hover" href="<?php echo base_url('story'); ?>">Our Story</a>
                </div>



            </div>
        </div>
    </div>
</section>
<?php $this->load->view('components/footer'); ?>