<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<!-- header end -->
<section id="service-img">
    <!-- <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div> -->
    <h1 class="text-capitalize">
        <!-- Find Us  -->
    </h1>
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>
                We strive to build meaningful and personal relationships with our customers.They are the foundation of
                our trusted reputation.
            </p>
        </div>
    </div>
</section>

<section class="pt-5 mb-5 mt-1">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top"
                        src="<?php echo base_url('assets/frontend/'); ?>/img/Store_ContactUS_Colombo.jpg"
                        alt="Card image cap">
                    <div class="title-section-card title-section-card-contact">
                        <h2>Colombo</h2>
                        <div class="patarn-img">

                        </div>
                    </div>

                    <div class="card-body bd-body">
                        <p class="craft-h">FLAGSHIP STORE - Colombo 07, Sri Lanka</p>
                        <p class="colombo-box-1"><b>No. 59 Flower Rd, Colombo 07</b> <br /> <span><a
                                    class="contact-link" href="tel:+94112874893">+94 11 287 48 93</a></span>
                            <br /><span>
                        </p>
                        <p class="colombo-box-2">9AM–5:30PM</p>
                    </div>
                    <div class="card-body">
                        <br />
                        <p class="colombo-box">CRAFT HOUSE - Kandy, Sri Lanka</p>
                        <p class="craft-address"><span><a class="contact-link"
                                    href="https://goo.gl/maps/jtiCgXd7CSG5Eiem8" target="blank">No. 939 Peradeniya Rd,
                                    Kandy 20000
                                </a></span><br>
                            <span><a class="contact-link" href="tel:+94812387387">+94 81 238 73 87</a></span>
                        </p>
                        <p class="colombo-box-2">9AM–5:30PM</p>
                    </div>
                </div>
            </div>

            <div class="col-md-8 contact-form-area">
                <div class="contact-top-text">
                    <p class="contact-top-text">We greatly value our customers and are always willing to help with your
                        queries, address your concerns and listen to your valuable feedback. If you would like to get in
                        touch with us, please fill out the form below and make sure all fields marked with an * are
                        completed.</p>
                </div>

                <form action="<?php echo site_url('home/send_contact_email'); ?>" method="POST"
                    class="form-section needs-validation" novalidate>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">SUBJECT </label>
                        <select class="form-control" name="subject1" id="exampleFormControlSelect1" required>
                            <option value="Group/Identify">Nature of Inquiry</option>
                            <option value="Product Information">Product Information</option>
                            <option value="Sale Policy">Sale Policy</option>
                            <option value="Boutique Information">Boutique Information</option>
                            <option value="After Sale Service">After Sale Service</option>
                            <option value="General Feedback">General Feedback</option>
                            <option value="Proposal For Collaboration">Proposal For Collaboration</option>
                            <option value="Other">Other</option>
                        </select>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="form-group box-sec-from">
                        <label for="exampleFormControlSelect1">PROFILE </label>
                        <select class="form-control" name="profile" id="exampleFormControlSelect1" required>
                            <option value="Which profile?">Customer Group/ Profile</option>
                            <option value="Individual">Individual</option>
                            <option value="Professional / Company">Professional / Company</option>
                            <option value="Media">Media</option>
                            <option value="Candidates">Candidates</option>
                        </select>
                        <div class="valid-feedback">
                            Looks good!
                        </div>

                    </div>

                    <div class="radio-button-sec box-sec-from">
                        <label for="exampleFormControlSelect1">NAME <span class="mandatory">*</span></label>
                    </div>
                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">

                                <select class="form-control" name="title" id="exampleFormControlSelect1" required>
                                    <option value="">Title</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                </select>
                                <div class="invalid-feedback">Required</div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="firstname" class="form-control" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="First Name" required />
                                <div class="invalid-feedback">Required</div>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="lastname" class="form-control" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="Last name" required />
                                <div class="invalid-feedback">Required</div>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                        </div>


                    </div>

                    <div class="radio-button-sec box-sec-from">
                        <label for="exampleFormControlSelect1">EMAIL <span class="mandatory">*</span></label>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="Email" required>
                                <div class="invalid-feedback">Needs a valid email</div>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                        </div>
                    </div>

                    <div class="radio-button-sec box-sec-from">
                        <label for="exampleFormControlSelect1">MESSAGE <span class="mandatory">*</span></label>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">

                                <textarea class="form-control" name="message1" id="exampleFormControlTextarea1"
                                    rows="10" required></textarea>
                                <div class="invalid-feedback">Your message to us is required</div>
                                <div class="valid-feedback">Looks good!</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-check box-sec-from">
                                <input type="checkbox" name="checkbox" class="form-check-input" id="exampleCheck1"
                                    required>
                                <label class="condition-text form-check-label">
                                    By checking this box, you agree that your data will be processed by Hemachandra as
                                    data controller,
                                    in conformity with its privacy policy, which describes all details surrounding the
                                    processing
                                    (including use, transfers and retention) of your data, as well as your rights and
                                    choices regarding such processing.
                                </label>
                                <div class="invalid-feedback">You must agree before submitting.</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 ">
                            <!-- <div class="g-recaptcha" data-sitekey="6LdLXqIUAAAAAO0ycFZwdx1oud5BsC66Nf9KCJZW"></div> -->
                        </div>

                        <div class="col-md-4 ">

                            <button type="submit" class="btn btn-link contact-send-button">send</button>
                        </div>


                    </div>
            </div>



            </form>

            <!-- Modal -->
            <div class="modal fade" id="contactUsSubmittedModal" tabindex="-1" role="dialog"
                aria-labelledby="contactUsSubmittedModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">

                            <img src="<?php echo base_url('assets/frontend/tironserviceassets/img/logo_03.jpg') ?>"
                                alt="" style="display: block;width: 150px;margin: 0px auto;">

                            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->
                        </div>
                        <div class="modal-body">
                            <p class="text-center"> Submitting, Please Wait... </p>
                        </div>
                        <!-- <div class="modal-footer">
                <button type="button" class="btn btn-link send-button" data-dismiss="modal">close</button>
            </div> -->
                    </div>
                </div>
            </div>
        </div>


    </div>
    </div>

</section>

<section id="quality">
    <div class="container p-5">
        <p class="para-quality">Browse through our FAQs to find answers to the most commonly raised questions.
        </p>
        <a href="<?php echo base_url('faq'); ?>">
            <div class="faq-button">
                FAQ
            </div>
        </a>
    </div>
</section>

<!-- footer strart -->

<?php $this->load->view('components/footer'); ?>