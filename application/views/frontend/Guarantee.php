<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-imgs" class="guarantee-banner">
    <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return
                Policy</a></span>
        <span class="policy-close">x</span>
    </div>
    <h1 class="text-capitalize">
        <!-- Unbroken Trust -->
    </h1>
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>Client satisfaction is a guiding principle at Hemachandra - your experience is of the utmost importance
                to us.</p>
        </div>

    </div>
</section>

<section class="pt-5 mb-5 mt-1">
    <div class="container">
        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="tile-1">
                    <div class="card">
                        <img class="card-img-top" src="<?php echo base_url('assets/frontend/'); ?>/img/quality-n2.jpg"
                            alt="Card image cap">
                        <div class="font-img-sus g-blue-button">
                            <h4>The Craft</h4>
                            <div class="patarn-img"></div>
                        </div>
                        <div class="card-body sustainablity-card-body">
                            <p class="mb-0 same-size-box-guarantee">Each item is hand crafted in our workshops in Kandy
                                and Colombo by master Craftsmen. Every item is individually and meticulously checked for
                                excellence in workmanship by an experienced quality control team before being presented
                                to our clients to view. We believe our unrivaled quality is the result of the care and
                                attention to detail demonstrated at every step of the production process.</p>

                            <!-- <div id="demo1" class="collapse pb-2 ">
                    <p class="">experienced quality control team before being presented to our clients to view. We believe our unrivaled quality is the result of the care and attention to detail demonstrated at every step of the production process.</p>
                    </div> -->

                            <!-- <a href="#" class="read-more abc" data-toggle="collapse" data-target="#demo1">Discover</a> -->
                            <!-- <a href="#" class="read-more abc discover-toggle" data-toggle="collapse" data-target="#demo1">Discover</a> -->
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4">
                <div class="tile-1">
                    <div class="card">
                        <img class="card-img-top"
                            src="<?php echo base_url('assets/frontend/'); ?>/img/Guarantee_Testing.jpg"
                            alt="Card image cap">
                        <div class="font-img-sus g-blue-button">
                            <h4>Testing</h4>
                            <div class="patarn-img"></div>
                        </div>
                        <div class="card-body sustainablity-card-body">
                            <p class="mb-0 same-size-box-guarantee">All gemstones set in our jewellery are checked by
                                our GIA (USA) and FGA (UK) qualified Gemmologists, to ensure only the finest stones make
                                their way into the hands of our craftsmen. All our products come with a jeweller’s
                                guarantee, which is our assurance to clients of the genuineness of their purchase.</p>

                            <!-- <div id="demo2" class="collapse pb-2">
                    <p class="">of our craftsmen. All our products come with a jeweller’s guarantee, which is our assurance to clients of the genuineness of their purchase.</p> 
                    </div> -->

                            <!-- <a href="#" class="read-more abc" data-toggle="collapse" data-target="#demo2">Discover</a> -->
                            <!-- <a href="#" class="read-more abc discover-toggle" data-toggle="collapse" data-target="#demo2">Discover</a> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 ">
                <div class="tile-1">
                    <div class="card">
                        <img class="card-img-top"
                            src="<?php echo base_url('assets/frontend/'); ?>/img/Guarantee_careTile.jpg"
                            alt="Card image cap">
                        <div class="font-img-sus g-blue-button">
                            <h4>Care</h4>
                            <div class="patarn-img"></div>
                        </div>
                        <div class="card-body sustainablity-card-body">
                            <p class="mb-0 same-size-box-guarantee">We are happy to share a few time tested home-care
                                tips and tricks to help you keep your jewellery in the finest possible condition for a
                                lifetime of enjoyment.</p>
                            <!-- <a id="scroll_to_care" target="care" style="color: #05205f; text-decoration: underline">Discover</a> -->
                            <a href="<?php echo base_url(); ?>home/guaranteeinner/"
                                style="color: #05205f; text-decoration: underline">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end row -->
    </div>
</section> <!-- title section -->

<!--   QUality- section -->

<section id="quality">
    <div class="container">
        <h1 class="topic mt-5">Our Guarantee</h1>
        <p class="para-quality">Every item of jewellery we sell comes with a certificate of authenticity, as we stand by
            the genuineness of the raw materials we use. If you have any queries or concerns we ask you to get in touch
            with our team and we will be happy to help.</p>
        <div class="phone-number">
            <div class="custom-inquire-area"><a class="custom-inquire-text" href="./contact">Click here for
                    inquiries</a></div>
            <!-- <a class="inquire-num" href="tel:0112874893">+94 11 287 48 93</a> -->
            <div class="custom-inquire-area"><a class="inquire-email"
                    href="mailto:mail@hemachandras.com">mail@hemachandras.com</a></div>
        </div>

    </div>
</section>





<!--Quality section ends -->

<!-- <section id="feed-gua">
    <div class="container" id="care">
      <div class="row feed-gua-wrap">
        <div class="col-md-8">
          <h3>Care For Your Jewellery </h3>
                    
          <p><span style="font-size:20px;"><strong>Safe Storage</strong></span><br />Store your jewellery in its own box, pouch or wrapped in cotton/tissue to prevent scratching, chipping and entanglement.</p>
          <p class="feed-gua-sec-text">
            When travelling, protect your items from scratches or other impact damage by packing them in a suitable box or case.
          </p>
          <p class="read-left"><a href="<?php echo base_url(); ?>home/guaranteeinner/"><strong>Read more</strong></a></p>
        </div>

        <div class="col-md-4 col-sm-12">
          <img src="<?php echo base_url('assets/frontend/'); ?>/img/safe-for-jewellary.png" class="img-fluid" alt="">
        </div>
      </div>

    </div>
  </section> -->

<script>
$('.discover-toggle').click(function() {
    $(this).text(function(i, old) {
        return old == 'Discover' ? 'Collapse' : 'Discover';
    });
});
</script>

<!-- ///////////////////////////foootererrrr///////////////////////// -->

<?php $this->load->view('components/footer'); ?>