<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-img">
    <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return
                Policy</a></span>
        <span class="policy-close">x</span>
    </div>
    <h1 class="text-capitalize">
        <!-- To Your Doorstep  -->
    </h1>
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>
                We offer international shipping so no matter where you are in the world, you are never too far away to
                own one of our unique creations.
            </p>
        </div>
    </div>
</section>

  <section id="tiles">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
          <div class="tile-1">
            <div class="card">
                <img class="card-img-top" src="<?php echo base_url('assets/frontend/'); ?>/img/shipping.jpg" alt="Card image cap">
                <!-- <div class="font-img">
                  <h4>Shipping</h4>
                </div> -->
                <div class="title-section-card">
                    <h2 class="serv-title tit3">Shipping</h2>
                      <div class="patarn-img">

                            </div>
                        </div>
                        <div class="card-body">
                            <p class="card-text description same-size-box-shipping">We undertake worldwide delivery
                                through trusted partners.</p>
                            <a href="./shippingDetails" style="color: #05205f; text-decoration: underline">Read More</a>
                        </div>
                    </div>
                </div>
            </div>

         <div class="col-lg-4 col-md-4">
           <div class="tile-1">
             <div class="card">
                 <img class="card-img-top" src="<?php echo base_url('assets/frontend/'); ?>lahiruguranteeassets/img/return-policy-n.jpg" alt="Card image cap">
                 <!-- <div class="font-img">
                   <h4 class="life">Return Policy</h4>
                 </div> -->
                 <div class="title-section-card">
                    <h2 class="serv-title tit3">Return Policy</h2>
                      <div class="patarn-img">

                            </div>
                        </div>
                        <div class="card-body">
                            <p class="card-text description same-size-box-shipping">Products may be refunded or
                                exchanged within 14 days of purchase, provided the items are in their original
                                packaging, and not worn. Any certificates, documentation provided with the item and
                                original packaging must also be returned.</p>
                            <a href="./return_policy" style="color: #05205f; text-decoration: underline">Read More</a>

                            </p>
                            <!-- <div id="demo3" class="collapse pb-2 ">
                        <p class="discover-content-area">item and original packaging must also be returned.</p>
                        <a href="#return" style="color: #05205f; text-decoration: underline">Read More</a>
                      </div>
                      <a href="#" class="read-more abc" data-toggle="collapse" data-target="#demo3">Discover</a>-->
                        </div>
                    </div>
                </div>
            </div>

          <div class="col-lg-4 col-md-4 ">
            <div class="tile-1">
              <div class="card">
                  <img class="card-img-top" src="<?php echo base_url('assets/frontend/'); ?>/img/gift-n.jpg" alt="Card image cap">
                  <!-- <div class="font-img">
                    <h4>Gifting</h4>
                  </div> -->
                  <div class="title-section-card">
                    <h2 class="serv-title tit3">Gifting</h2>
                      <div class="patarn-img">

                            </div>
                        </div>
                        <div class="card-body">
                            <p class="card-text description shipping-gift-description same-size-box-shipping">Bring some
                                magic to a loved one by gifting them one of our exquisite handcrafted pieces. Contact us
                                for more information on our personalised gift-wrapping service.</p>
                            <a href="./contact">inquire</a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end row -->
    </div>
</section> <!-- title section -->

<!--   QUality- section -->

<section id="quality">
    <div class="container">
        <h1 class="topic mt-5">Get in Touch with Us</h1>
        <p class="para-quality">We greatly value our customers and are always willing to help with your queries. If you
            have query about your timepiece and would like to get in touch with us </p>
        <div class="phone-number">
            <div class="custom-inquire-area"><a class="custom-inquire-text" href="./contact">Click here for
                    inquiries</a> </div>
            <!-- <a class="inquire-num" href="tel:0112874893">+94 11 287 48 93</a> -->
            <div class="custom-inquire-area"><a class="inquire-email"
                    href="mailto:mail@hemachandras.com">mail@hemachandras.com</a></div>
        </div>

    </div>
</section>





<?php $this->load->view('components/footer'); ?>