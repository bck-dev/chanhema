<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-img" class="faq-banner">
    <!-- <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div> -->
    <h1 class="banner-heading text-capitalize">
        <!-- Frequently Asked Questions -->
    </h1>
    <!-- <h1 class="text-capitalize">Care For Your Jewellery</h1> -->

</section>


<div class="container bottom-juwelery">
    <div class="row pt-5 pb-5">
        <div class="col-md-4 pt-5 d-block d-md-none">
            <!-- Accordion Section Start-->
            <div class="accordion" id="maccordionExample">
                <?php foreach ($faqByCategories as $faqCategory) : ?>
                <div class="card">
                    <div class="card-header" id="mheading<?php echo $faqCategory->faq_category_id; ?>">
                        <h2 class="mb-0">
                            <button class="btn btn-link faq-accordion-btn" type="button" data-toggle="collapse"
                                data-target="#mcollapse<?php echo $faqCategory->faq_category_id; ?>"
                                aria-expanded="true"
                                aria-controls="mcollapse<?php echo $faqCategory->faq_category_id; ?>">
                                <span class="faq-bullet"><img
                                        src="<?php echo base_url('assets/frontend/'); ?>/img/faq-arrow.png" width="7"
                                        height="7" class="mr-2" /> <?php echo $faqCategory->faq_category_name; ?></span>
                            </button>
                        </h2>
                    </div>

                    <div id="mcollapse<?php echo $faqCategory->faq_category_id; ?>" class="collapse"
                        aria-labelledby="mheading<?php echo $faqCategory->faq_category_id; ?>"
                        data-parent="#maccordionExample">
                        <div class="card-body">
                            <ul>
                                <?php foreach ($faqCategory->faqs as $faq) : ?>
                                <li class="faq-accordion-content-area"><a
                                        href="<?php echo base_url('faq/'); ?><?php echo $faq->faq_id; ?>"><?php echo $faq->faq; ?></a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <!-- Accordion Section End -->
        </div>

        <div class="col-md-8 pt-5 faq-highlight">
            <div class="below-text">
                <?php foreach ($faqs as $faq) : ?>
                <h3 class="bell-heading">
                    <?php echo $faq->faq; ?>
                </h3>
                <p class="care-for-jewellary mb-5">
                    <?php echo $faq->answer; ?>
                </p>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="col-md-4 pt-5 d-none d-md-block">
            <!-- Accordion Section Start-->
            <div class="accordion faq-sticky" id="accordionExample">
                <?php foreach ($faqByCategories as $faqCategory) : ?>
                <div class="card">
                    <div class="card-header faq-category-box" id="heading<?php echo $faqCategory->faq_category_id; ?>">
                        <h2 class="mb-0">
                            <button class="btn btn-link faq-accordion-btn" type="button" data-toggle="collapse"
                                data-target="#collapse<?php echo $faqCategory->faq_category_id; ?>" aria-expanded="true"
                                aria-controls="collapse<?php echo $faqCategory->faq_category_id; ?>">
                                <span class="faq-bullet"><img
                                        src="<?php echo base_url('assets/frontend/'); ?>/img/faq-arrow.png" width="7"
                                        height="7" class="mr-2" /> <?php echo $faqCategory->faq_category_name; ?></span>
                            </button>
                        </h2>
                    </div>

                    <div id="collapse<?php echo $faqCategory->faq_category_id; ?>" class="collapse"
                        aria-labelledby="heading<?php echo $faqCategory->faq_category_id; ?>"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            <ul>
                                <?php foreach ($faqCategory->faqs as $faq) : ?>
                                <li class="faq-accordion-content-area"><a
                                        href="<?php echo base_url('faq/'); ?><?php echo $faq->faq_id; ?>"><?php echo $faq->faq; ?></a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <!-- Accordion Section End -->
        </div>
    </div>
</div>


<!--end edit sectio tiorn-->

<script>
new Glider(document.querySelector('.glider'), {
    slidesToScroll: 1,
    slidesToShow: 4,
    draggable: true,
    dots: '.dots',
    arrows: {
        prev: '.glider-prev',
        next: '.glider-next'
    }
});




$('[data-fancybox="preview"]').fancybox({
    thumbs: {
        autoStart: true
    }
});
</script>


<?php $this->load->view('components/footer'); ?>