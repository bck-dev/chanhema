<!DOCTYPE HTML>
<html>
    <head>
    	<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Hemchandra | Collection Inquries inner </title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <link rel="stylesheet" href="vendors/css/grid.css"/>
        <link rel="stylesheet" href="vendors/css/ionicons.min.css"/>
        <link rel="stylesheet" href="vendors/css/normalize.css"/>
        <link rel="stylesheet" href="vendors/css/animate.css"/>
        <link rel="stylesheet" href="resources/css/stylesheet.css"/>
        <!-- open sans font used -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i" rel="stylesheet">
         <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/assets/owl.carousel.min.css">
        <!-- hover effect style sheet-->

        <link rel="stylesheet" href="vendors/css/hovereffects/set1.css">
        <!-- click product stylesheet-->
        <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <!-- Hemachandra Responsive designing -->
        <link rel="stylesheet" href="resources/css/queries.css"/>
        <!-- Product slide effect stylehseet-->
        <link rel="stylesheet" type="text/css" href="vendors/css/flickityproductslide.css"/>
        
        <!--Google font playfair -->
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
        <!--belfair google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Bellefair" rel="stylesheet">
        <!--maven google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Maven+Pro" rel="stylesheet">
        
        <!-- Font Awesome-->
        <link href="vendors/css/fontawesome.css" rel="stylesheet">
        
        <!-- collection page css-->
        <link href="resources/css/collection.css" rel="stylesheet">
        
        <!-- Inquiries page css-->
        <link href="resources/css/inquiries.css" rel="stylesheet">
        
        <!-- favicons for browsers -->
        <link rel="apple-touch-icon" sizes="60x60" href="resources/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="resources/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="resources/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="resources/img/favicon/site.webmanifest">
        <link rel="mask-icon" href="resources/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="resources/img/favicon/favicon.ico">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-config" content="resources/img/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        
        
        <!--Owl carousel -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
        
         <!---- Slick cdn slider-->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css"/>    
    </head>
    <body>

        <header>
            <!--naviation Menu -->
            <nav class="windows-mode-nav main-navigator-menu" >
                <div class="navirow">
                    <div class="container">
                        
                        <div class="navigation-top">
                            
                        </div>
                    
                        <ul class="main-nav" id="main-navigation">
                            
                            <ul class="rightsitemenu-top main-nav nav" id="navigationmobile">
                            <li class="list-right">
                                   <a href="#" class="toprightul">SHIPPING</a>
                            </li>
                             <li class="list-right">
                               <a href="#" class="toprightul">CONTACT</a>
                            </li>
                             <li class="list-right">
                               <a href="#" class="toprightul">MEDIA</a>
                            </li>
                        </ul>
                            
                            
                            <!--top naviagtion right corner search bar-->
                            <ul class="language-list" >
                                <li><i class="ion-ios-arrow-right"></i> 
                                    <select class="langselectbar" name="droplangsel">
                                        <option>EN</option>
                                        <option>SL</option>
                                    </select>
                                        <input type="text" name="text" class="search"/>
                                      <a class="js-searchbar"><i class="ion-ios-search-strong icon-small"></i></a>
                                      
                                </li>
                            </ul>
                            <!-- top navigation right bar corner search bar ende-->
                            
                            <!-- naviagtion main menu verticle middle -->
                            <li class="left"><a href="#">Story </a></li>
                            <li class="left"><a href="gemstone.html">Gemstones</a></li>
                            <li class="left drop-down"><a href="collection.html">Collection</a>
                            
                                <div class="navigation-submenu-row" style="float: left;" >
                                    <div>
                                        <div class="col First-sub-menu">
                                            <div class="collection-section">
                                                <h3 class="navigation-sub-menu-title">COLLECTION</h3>
                                                <ul class="hemachndra-sub-menu">
                                                    <li><a href="#">Love & Life</a></li>
                                                    <li><a href="#">Ceylon doll</a></li>
                                                    <li><a href="#">Heritage</a></li>
                                                </ul>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col second-sub-row" style="display: block; float: left;" >
                                            <div class="fine-jewelery-section">
                                                
                                                <div class="col span-1-of-2">
                                                    <div class="" style="margin-left: -22px;">
                                                        <h3 class="navigation-sub-menu-jewelery">Fine jewelery</h3>
                                                        <div class="list-of-view-menu">
                                                            <ul class="hemachandra-second-sub-menu">
                                                                <li><a href="#">Adamantine</a></li>
                                                                <li><a href="#">Celestial Art</a></li>
                                                                <li><a href="#">Contemporary</a></li>
                                                                <li><a href="#">Moon Stone</a></li>
                                                                <li><a href="#">Island Life</a></li>
                                                                <li><a href="#">View All</a></li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col span-1-of-2">
                                                     <div class="" style="margin-left: -22px;">
                                                        <h3 class="navigation-sub-menu-jewelery">Fine jewelery</h3>
                                                        <div class="list-of-view-menu">
                                                            <ul class="hemachandra-second-sub-menu">
                                                                <li><a href="#">Adamantine</a></li>
                                                                <li><a href="#">Celestial Art</a></li>
                                                                <li><a href="#">Contemporary</a></li>
                                                                <li><a href="#">Moon Stone</a></li>
                                                                <li><a href="#">Island Life</a></li>
                                                                <li><a href="#">View All</a></li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                </div>
                                                

                                            </div>
                                            
                                        </div>

                                    </div>
                                    
                                </div>
                            
                            
                            
                            </li>
                            <li>
                                <a href="index.html">
                                    <div class="logocen">
                                        <img src="resources/img/hemalogopng.png" class="logo" alt="hemachandra logo"/>
                                    </div>
                                </a>
                            </li>
                            <li class="right first-gurante-child"><a href="#">Gurantee</a></li>
                            <li class="right"><a href="#">Services</a></li>
                            <li class="right"><a href="#">Stores</a></li>
                        </ul>
                    
                    </div>
                
                </div>
            </nav>
            <!--naviation Menu ended -->
            
            <!- ================================================================================->
            <!-- ===============sticky navigation menu start here===============================-->
            <!- ================================================================================->
            <nav id="sticky-navi"  class="windows-mode-nav naviagtion-background noti" >
                <div class="stickynavirow">
                    <div class="stickycontainer">
                        
                        <div class="navigation-top">
                            
                        </div>
                    
                        <ul class="main-nav sticky-main-nav" id="main-navigation">

                            
                            <!-- naviagtion main menu verticle middle -->
                            <li class="left"><a href="#">Story </a></li>
                            <li class="left"><a href="gemstone.html">Gemstones</a></li>
                            <li class="left drop-down"><a href="collection.html">Collection</a>
                            
                                <div class="navigation-submenu-row" style="float: left;" >
                                    <div>
                                        <div class="col First-sub-menu">
                                            <div class="collection-section">
                                                <h3 class="navigation-sub-menu-title">COLLECTION</h3>
                                                <ul class="hemachndra-sub-menu">
                                                    <li><a href="#">Love & Life</a></li>
                                                    <li><a href="#">Ceylon doll</a></li>
                                                    <li><a href="#">Heritage</a></li>
                                                </ul>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col second-sub-row" style="display: block; float: left;" >
                                            <div class="fine-jewelery-section">
                                                
                                                <div class="col span-1-of-2">
                                                    <div class="" style="margin-left: -22px;">
                                                        <h3 class="navigation-sub-menu-jewelery">Fine jewelery</h3>
                                                        <div class="list-of-view-menu">
                                                            <ul class="hemachandra-second-sub-menu">
                                                                <li><a href="#">Adamantine</a></li>
                                                                <li><a href="#">Celestial Art</a></li>
                                                                <li><a href="#">Contemporary</a></li>
                                                                <li><a href="#">Moon Stone</a></li>
                                                                <li><a href="#">Island Life</a></li>
                                                                <li><a href="#">View All</a></li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col span-1-of-2">
                                                     <div class="" style="margin-left: -22px;">
                                                        <h3 class="navigation-sub-menu-jewelery">Fine jewelery</h3>
                                                        <div class="list-of-view-menu">
                                                            <ul class="hemachandra-second-sub-menu">
                                                                <li><a href="#">Adamantine</a></li>
                                                                <li><a href="#">Celestial Art</a></li>
                                                                <li><a href="#">Contemporary</a></li>
                                                                <li><a href="#">Moon Stone</a></li>
                                                                <li><a href="#">Island Life</a></li>
                                                                <li><a href="#">View All</a></li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                </div>
                                                

                                            </div>
                                            
                                        </div>

                                    </div>
                                    
                                </div>
                            
                            
                            
                            </li>
                            <li>
                                <a href="index.html">
                                    <div class="stickylogocen">
                                        <img src="resources/img/stick_navigation_bar.png" class="sticky_navi_logo" alt="hemachandra logo"/>
                                    </div>
                                </a>
                            </li>
                            <li class="right first-gurante-child"><a href="#">Gurantee</a></li>
                            <li class="right"><a href="#">Services</a></li>
                            <li class="right"><a href="#">Stores</a></li>
                        </ul>
                    
                    </div>
                
                </div>
            </nav>
            <!- ================================================================================->
            <!-- ==================sticku navigation menu end here==============================-->
            <!- ================================================================================->
            
            
            <!-- mobile View navigation menu -->
            <nav id="global-nav" class="nav scrolled-nav fixed" role="full-horizontal">
                 <input type="checkbox" id="button" style="opacity:0">
                
               
                <label class="onclicknav " for="button" onclick> Menu <i class="ion-chevron-down"></i></label>
                    
                
                <!-- display on mobile view logo -->
                <a href="index.html">
                <div class="navleft">
                 <img src="resources/img/hemalogopng.png" class="logomob" alt="hemachandra logo"/>
                </div>
                </a>
                    <ul class="main-nav navigation-unorderlist">
                        <li><i class="ion-ios-arrow-right"></i> 
                            <select class="langselectbar" name="droplangsel">
                                <option>EN</option>
                                <option>SL</option>
                            </select>
                            <a class="js-searchbar"><i class="ion-ios-search-strong icon-small"></i></a>
                            <input type="text" name="text" class="search"/>
                        </li>
                        <li class="left"><a href="#">Story </a></li>
                        <li class="left"><a href="gemstone.html">Gem Stone</a></li>
                        <li class="left mobile-navigation-menu-droplist"><a href="collection.html">Collection</a>
                            <!---- submenu starting here-->
                            <div class="mobile-menu-navigation-sub-menu">
                                 <ul class="mobile-submenu-class">
                                    <p class="paragraph-submenu-class"> Collection</p>
                                    <li><a href="#">Love & Life</a></li>
                                    <li><a href="#">Ceylon doll</a></li>
                                    <li><a href="#">Heritage</a></li>
                                </ul>
                                
                                
                                <ul class="mobile-submenu-class">
                                    <p style="color: black; float: left;  margin-bottom: 29px; font-weight: 600;"> Fine Jewelery</p>
                                    <li><a href="#">Love & Life</a></li>
                                    <li><a href="#">Ceylon doll</a></li>
                                    <li><a href="#">Heritage</a></li>
                                </ul>
                            </div>
                            <!-- Submenu divider end here-->
                               
                          
                        </li>
                        <li class="right"><a href="#">Gurantee</a></li>
                        <li class="right"><a href="#">Services</a></li>
                        <li class="right"><a href="#">Stores</a></li>
                        <li class="right"><a href="#">Shipping</a></li>
                        <li class="right"><a href="#">Contact</a></li>
                        <li class="right"><a href="#">media</a></li>
                    
                    </ul>
            </nav>
             <!-- mobile View navigation menu -->
        </header>
        
        <!---first banner section-->
        <div class="banner-img-first-inquiry-section"></div>
        
        <!-- collection first section -->
        <section class="collection-first-section">
            <div class="row">

                    <div class="col span-1-of-2">
                        <div class="collection-image-divider">
                             <img src="resources/img/collection/collection_ring_img.png" class="ring-image" alt="Ring Image"/>
                        </div>
                       
                    </div>
                    <div class="col span-1-of-2">
                        <div class="collection-image-divider-description">
                            <div class="collection-overview">
                                <p class="overview-small-content">Overview</p>
                                <h3 class="title-of-collection">An Obsession</h3>
                            </div>
                            <div class="description-of-collection">
                                  <p class="Paragraph-content-of-collection" >Breitling has its very own place among watch brands: the highly exclu-sive ‘stronghold’ of technical watches, mainly chronographs. A longstanding partner of aviation, a field where reliability and precision play a vital role, the brand has always devoted premier importance to the quality of its products, designed to withstand intensive use in the most trying conditions.</p>
                            </div>
                        </div>
                    </div>
                
                
            </div>
        </section>
        
        <!-- second section of collection page-->
        <section class="second-collection-section">
            <div class="row">
                <div class="col span-1-of-2">
                    <div class="second-description-of-collection">
                        <div class="double-quote"></div>
                        <p class="subtitle-paragraph-of-collection" >Breitling has its very own place among  watch brands: the highly exclusive ‘strong-hold’ of technical watches, mainly.</p>
                        <div class="double-quote-end"></div>
                    </div>
                </div>
                <div class="col span-1-of-2">
                    <div class="signature-collection-devider">
                             <img src="resources/img/collection/signature.png" alt="Signature"/>
                    </div>
                </div>
            </div>
        </section>
        

        
        
        <!============== customize section of product hemachandra =======================>
        <section class="product-list-section">
            <div class="row product-rows">
              <div class="product-container-box">
                
                <div class="collection-product">
                    <div class="collection-photo-image">
                        <img src="resources/img/collection/diamond.png" class="" alt="product image 1"/>
                    </div>
                    
                    <div class="collection-paragraph">
                        <h3 class="product-title">AURA EARRINGS</h3>
                        <p class="product-collection-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                        <a class="collection-inquire" href="#">Inquire</a>  
                        
                    </div>
                </div>
                
                 <div class="collection-product">
                    <div class="collection-photo-image">
                        <img src="resources/img/collection/diamond.png" class="" alt="product image 1"/>
                    </div>
                    
                    <div class="collection-paragraph">
                        <h3 class="product-title">AURA EARRINGS</h3>
                        <p class="product-collection-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                        <a class="collection-inquire" href="#">Inquire</a>  
                        
                    </div>
                </div>
                
                 <div class="collection-product">
                    <div class="collection-photo-image">
                        <img src="resources/img/collection/diamond.png" class="" alt="product image 1"/>
                    </div>
                    
                    <div class="collection-paragraph">
                        <h3 class="product-title">AURA EARRINGS</h3>
                        <p class="product-collection-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                        <a class="collection-inquire" href="#">Inquire</a>  
                        
                    </div>
                </div>
                
                <div class="collection-product">
                    <div class="collection-photo-image">
                        <img src="resources/img/collection/diamond.png" class="" alt="product image 1"/>
                    </div>
                    
                    <div class="collection-paragraph">
                         <h3 class="product-title">AURA EARRINGS</h3>
                        <p class="product-collection-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                        <a class="collection-inquire" href="#">Inquire</a>  
                        
                    </div>
                </div>
                
                <div class="collection-product">
                    <div class="collection-photo-image">
                        <img src="resources/img/collection/diamond.png" class="" alt="product image 1"/>
                    </div>
                    
                    <div class="collection-paragraph">
                         <h3 class="product-title">AURA EARRINGS</h3>
                        <p class="product-collection-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                        <a class="collection-inquire" href="#">Inquire</a>  
                        
                    </div>
                </div>
                
                <div class="collection-product">
                    <div class="collection-photo-image">
                        <img src="resources/img/collection/diamond.png" class="" alt="product image 1"/>
                    </div>
                    
                    <div class="collection-paragraph">
                         <h3 class="product-title">AURA EARRINGS</h3>
                        <p class="product-collection-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                        <a  class="collection-inquire" href="#">Inquire</a>  
                        
                    </div>
                </div>
                
            </div>
            </div>
        </section>
        
        
        
        
               
<!-- Comment By Raheem for future enhancement purpose
        <section class="product-collection-section">
            <div class="container">
                <div class="row">
                    <div class="SlickCarousel">
                    <div class="ProductBlock">
                      <div class="Content seccontetnsec">
                          <div class="product-contents">
                             <img src="resources/img/collection/diamond.png" class="collection-product-images" alt="product image 1"/>
                             <div class="product-info">
                                 <h3 class="product-title">AURA EARRINGS</h3>
                                <p class="product-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                                <a class="inquire" href="inquiries_inner.html">Inquire</a>  
                              </div>
                          </div>
                      </div>
                    </div> 
                    
                    <div class="ProductBlock">
                      <div class="Content seccontetnsec">
                          <div class="product-contents">
                             <img src="resources/img/collection/diamond.png" class="collection-product-images" alt="product image 1"/>
                             <div class="product-info">
                                 <h3 class="product-title">AURA EARRINGS</h3>
                                <p class="product-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                                <a class="inquire" href="inquiries_inner.html">Inquire</a>  
                              </div>
                          </div>
                      </div>
                    </div> 
                        
                        <div class="ProductBlock">
                      <div class="Content seccontetnsec">
                          <div class="product-contents">
                             <img src="resources/img/collection/diamond.png" class="collection-product-images" alt="product image 1"/>
                             <div class="product-info">
                                 <h3 class="product-title">AURA EARRINGS</h3>
                                <p class="product-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                                <a class="inquire" href="inquiries_inner.html">Inquire</a>  
                              </div>
                          </div>
                      </div>
                    </div> 
                        
                        <div class="ProductBlock">
                      <div class="Content seccontetnsec">
                          <div class="product-contents">
                             <img src="resources/img/collection/diamond.png" class="collection-product-images" alt="product image 1"/>
                             <div class="product-info">
                                 <h3 class="product-title">AURA EARRINGS</h3>
                                <p class="product-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                                <a class="inquire" href="inquiries_inner.html">Inquire</a>  
                              </div>
                          </div>
                      </div>
                    </div> 
                        <div class="ProductBlock">
                      <div class="Content seccontetnsec">
                          <div class="product-contents">
                             <img src="resources/img/collection/diamond.png" class="collection-product-images" alt="product image 1"/>
                             <div class="product-info">
                                 <h3 class="product-title">AURA EARRINGS</h3>
                                <p class="product-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                                <a class="inquire" href="inquiries_inner.html">Inquire</a>  
                              </div>
                          </div>
                      </div>
                    </div> 
                        
                        <div class="ProductBlock">
                      <div class="Content seccontetnsec">
                          <div class="product-contents">
                             <img src="resources/img/collection/diamond.png" class="collection-product-images" alt="product image 1"/>
                             <div class="product-info">
                                 <h3 class="product-title">AURA EARRINGS</h3>
                                <p class="product-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                                <a class="inquire" href="inquiries_inner.html">Inquire</a>  
                              </div>
                          </div>
                      </div>
                    </div> 
                        
                    <div class="ProductBlock">
                      <div class="Content seccontetnsec">
                          <div class="product-contents">
                             <img src="resources/img/collection/diamond.png" class="collection-product-images" alt="product image 1"/>
                             <div class="product-info">
                                 <h3 class="product-title">AURA EARRINGS</h3>
                                <p class="product-paragraph">18kt white gold, set with a Light Topaz gemstone and pave diamonds.</p>
                                <a class="inquire" href="inquiries_inner.html">Inquire</a>  
                              </div>
                          </div>
                      </div>
                    </div> 
                    
                </div>
            </div>
            </div>
        </section>
-->
        
        
        
      

         <!-- footer section start here-->
        <footer>
            <div class="row">
                <div class="footer-nav-links">
                    <div class="hemachandra-footer-logo">
                        <img src="resources/img/footerlogo/footerlogo.png" alt="footer logo"/>
                    </div>
                    <ul class="footer-nav">
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Customer Care</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Shipping</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Guarantee</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Contact</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Stores</a></li>
                    </ul>
                    <div class="sociallinks">
                        <ul class="social-link">
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-twitter"></i></a></li>
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-googleplus"></i></a></li>
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-instagram"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <section class="bottom-footer-container">
            <div class="secondrow">
                <div class="col span-1-of-3">
                    <p class="copywrite">
                       &copy; 2018 Hemachandra &dash; kandy
                    </p>
                </div>
                <div class="col span-1-of-3">
                    <ul class="bottom-footer">
                         <li> <a href="termsandcondition.html">Terms & Condition</a> </li>
                         <li> <a href="legalnotice.html">Legal Notice</a> </li>
                         <li> <a href="privacy.html">Privacy Policy</a> </li>
                         <li> <a href="#">Site Map</a> </li>
                    </ul>
                </div>
                <div class="col span-1-of-3">
                    <div class="projectdone">
                        <a href="http://bckonnect.com/"><p class="projectby"> BcKonnect</p></a>
                        <img src="resources/img/bcklogo.png" class="companylogoimage" alt="company logo">
                    </div>
                </div>
                
            </div>
        </section>
        
        
        <!-- jquery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <!-- script file linked here-->
        <!-- Jquery Migration start-->
        <script src="resources/js/browser.js"></script>
        <!-- Jquery Migration end-->
        <script src="resources/js/script.js"></script>
        <script src="http://bckonnect.com/medex/assets/frontend/js/owl.carousel.min.js"></script>
          <!-- jquery waypoint reference link http://imakewebthings.com/waypoints/-->
        <script src="vendors/js/jquery.waypoints.min.js"></script>
            
        <!-- click product slider-->
        <script type="text/javascript" src="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <!-- flip product grid slide js-->
        <script type="text/javascript" src="vendors/js/flickity_product.min.js"></script>
        <!-- Steller paralax -->
        <script type="text/javascript" src="vendors/js/steller.js"></script>
        <!-- jquery waypoint reference link http://imakewebthings.com/waypoints/-->
        <script src="vendors/js/jquery.waypoints.min.js"></script>
        <!-- slick CDN javascript-->
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        
    </body>
</html>