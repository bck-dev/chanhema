<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-img">
    <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return
                Policy</a></span>
        <span class="policy-close">x</span>
    </div>
    <h1 class="text-capitalize">
        <!-- Here <span class="mid-ft"> To </span> Help -->
    </h1>
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>
                Walk into the harmonious spaces of our Colombo or Kandy stores where we offer a unique personalised
                service.
                Browse our latest collections or order a bespoke piece to treasure for years to come.
            </p>
        </div>
    </div>
</section>

<section class="pt-5 mb-5 mt-1">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top"
                        src="<?php echo base_url('assets/frontend/'); ?>/img/Store_ContactUS_Colombo.jpg"
                        alt="Card image cap">
                    <div class="title-section-card">
                        <h2>Colombo</h2>
                        <div class="patarn-img">

                        </div>
                    </div>
                    <div class="card-body">
                        <p class="colombo-box">FLAGSHIP STORE</p>
                        <p class="colombo-box-1">59, Flower Road, Colombo 07<br />+94 11 287 48 93</p>
                        <p class="colombo-box-2">9AM–5:30PM</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top" src="<?php echo base_url('assets/frontend/'); ?>/img/kandy.jpg"
                        alt="Card image cap">
                    <div class="title-section-card">
                        <h2>Kandy</h2>
                        <div class="patarn-img">

                        </div>
                    </div>
                    <div class="card-body">
                        <p class="colombo-box">HERITAGE STORE</p>
                        <p class="colombo-box-1">Queens Hotel, Kandy<br />+94 81 238 73 87</p>
                        <p class="colombo-box-2">9AM–5:30PM</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top"
                        src="<?php echo base_url('assets/frontend/'); ?>/img/Stores_privatesuite.jpg"
                        alt="Card image cap">
                    <div class="title-section-card">
                        <h2 class="store-ps-box">Private Suite</h2>
                        <div class="patarn-img">

                        </div>
                    </div>
                    <div class="card-body">

                        <p class="card-text">Make an appointment with one of our specialists to enjoy a truly
                            personalised experience.</p>
                        <a href="./contact">inquire</a>


                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- title section -->


<section id="quality">
    <div class="container">
        <h1 class="topic mt-5">Customisation Service</h1>
        <p class="para-quality">For a truly unique gift, any of our creations can be customised using the precious metal
            and stone of your choice. With the personalised assistance of our in-house team, You can also create your
            own bespoke design.</p>
        <div class="phone-number">
            <div class="custom-inquire-area"><a class="custom-inquire-text" href="./contact">Click here for
                    inquiries</a> </div>
            <!-- <a class="inquire-num" href="tel:0112874893">+94 11 287 48 93</a> -->
            <div class="custom-inquire-area"><a class="inquire-email"
                    href="mailto:mail@hemachandras.com">mail@hemachandras.com</a></div>
        </div>

    </div>
</section>




<?php $this->load->view('components/footer'); ?>