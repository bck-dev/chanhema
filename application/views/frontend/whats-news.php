<?php $this->load->view('components/headerNew'); ?>   
<?php $this->load->view('components/menu'); ?>

<section id="service-img">
        <!-- <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div> -->
    <h1 class="text-capitalize"><!-- Find us --></h1>
</section>

<?php foreach ($newsinner as $data): ?>
    <section class="news-header">
        <div class="news-title news-inner-heading">
            <h1 class="bell-heading"><?php echo $data['news_title']; ?> </h1>
            <p class="date"><?php echo date('d M Y', strtotime($data['date'])); ?>
        </div>

    </section>

    <!-- end -->

    <section class="news-slider">
        <div class="container">
            <div class="news-slider-mobile">
                        <div class="glider-contain">
                                <div class="glider">
                                
                                <?php foreach ($news_image as $fr): ?>
                    <?php if ($fr['news_news_id'] == $data['news_id'] ): ?>
                                  <div class="profile">
                                      <a href="<?php echo base_url(); ?><?php echo $fr['image_name']; ?>" data-fancybox="preview" data-width="1500" data-height="1000">
                                        <img src="<?php echo base_url(); ?><?php echo $fr['image_name']; ?>" />
                                        </a>
                                        
                                  </div>
                                   <?php endif ?>
                           <?php endforeach ?>
                                   
                          
                                 




                                  </div>
                                  <button role="button" aria-label="Previous" class="glider-prev">«</button>
					  <button role="button" aria-label="Next" class="glider-next">»</button>
					  <div role="tablist" class="dots"></div>
                                  </div>
                </div>
        </div>
    </section>



       <div class="container bottom-juwelery">
           <div class="row">
               <div class="col-md-12">
                    <div class="below-text text-center">
                            <p class="carre-txt news-inner-content"><?php echo $data['news_description']; ?></p>

                        </div>
                        <p class="text-center carre-txt1">
                        </p>
               </div>
           </div>
       </div>
       
        <?php endforeach ?>

<!--end edit sectio tiorn-->
    <?php $this->load->view('components/footer'); ?>
