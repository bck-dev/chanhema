<?php $this->load->view('components/headerNew'); ?>   
<?php $this->load->view('components/menu'); ?>

    <section id="service-img-news">
        <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div>
        <h1 class="text-capitalize"><!-- What’s New --></h1>
    </section>

    <section class="blue-box">
        <div class="container ">
                    <div class="box-content">
                        <p>
                        Keep up-to-date with our events and news highlights.
                        </p>
                    </div>
        </div>
    </section>


    <!-- title section -->



    <section id="feed-news">
        <div class="container">
            <?php foreach($news as $data) {?>
            <?php $limited_word = word_limiter($data['news_description'],30);?>

           <div class="row m-0 feed-news-wrap">
                <div class="col-md-8 news-box">
                    <h3><?php echo $data['news_title']; ?></h3>
                    <span class="date"><strong><?php echo date('d M Y', strtotime($data['date'])); ?></strong></span>
                    <p class="first-ptap">
                       <?php echo $limited_word; ?> </p>
                    <p class="read-left"><a href="<?php echo base_url(); ?>home/newsinner/<?php echo $data['news_id']; ?>"><strong>Read more</strong></a></p>
                    <!--<p class="read-right"><a href="#">Gallery</a></p>-->
                </div>

                <div class="col-md-4 col-sm-12">
                    <?php foreach ($news_image as $fr): ?>
                    <?php if ($fr['news_news_id'] == $data['news_id'] ): ?>
                    <img src="<?php echo base_url()?><?php echo $fr['image_name']; ?>" class="img-fluid w-100 pt-3 mt-5" alt="">
                    <?php break; ?>
                          <?php endif ?>
                           <?php endforeach ?>
                </div>
            </div>
            <?php } ?>
<!--              <div class="row feed-news-wrap">-->
<!--                  <div class="col-md-8">-->
<!--                      <h3>HEMACHANDRA unveils new TALI collection with-->
<!--  interactive astrology app-->
<!--  </h3>-->
<!--  <span class="date">03 MAY 2019</span>-->
<!--                      <p class="first-ptap">-->
<!--                          Hemachandra first presented its new TALI collection during the Fall/ Winter 2019 show. Inspired by a spiritual world, the luck-bringing models in the range were revealed in a virtual constellation via a mobile app created specially for the occasion.-->
<!--                      </p>-->
<!--                      <p class="feed-news-sec-text">-->
<!--                          This service is always undertaken with respect to the original technical and aesthetic specifications of the piece.-->
<!--                      </p>-->
<!--                  </div>-->

<!--                  <div class="col-md-4 col-sm-12">-->
<!--                      <img src="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/box4.png" class="img-fluid photo-cust" alt="">-->
<!--                  </div>-->
<!--              </div>-->



        </div>
    </section>

    <script>
        $(document).ready(function() {
            var showChar = 100;
            var ellipsestext = "...";
            var moretext = "more";
            var lesstext = "less";
            $('.more').each(function() {
                var content = $(this).html();

                if(content.length > showChar) {

                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar-1, content.length - showChar);

                    var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                    $(this).html(html);
                }

            });
        });
    </script>
<?php $this->load->view('components/footer'); ?>
