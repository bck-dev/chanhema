<?php $this->load->view('components/headerNew'); ?>   
<?php $this->load->view('components/menu'); ?>

    <section id="service-imgs">
        <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div>
                <h1 class="text-capitalize">After Sale</h1>
    </section>

    <section class="blue-box">
        <div class="container">
            <div class="row  ">
                <div class="col-md-12">
                    <div class="box-content">
                        <p>
                        We wish to offer you the ultimate ease of service whether it be customizing a gift for a loved one or ordering a special piece. Our expert team look forward to giving you the best possible shopping and after-care experience.
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </section>
    

    <section id="feed">
        <div class="container" id="aftersale">
            <div class="row feed-wrap">
                <div class="col-md-8">
                    <!-- <h3>After-Sales</h3> -->
                    <p class="thin-font">
                        <b>Cleaning and Polishing: </b>
                        <br/>Depending on the piece and signs of wear it shows, we may recommend 
                        simple cleaning, ultrasonic cleaning or polishing to remove scratches and preserve the shine. 
                        For jewels in white gold, the polishing service includes rhodium plating to enhance the brilliance of the metal. 
                        For each service performed, a professional quality inspection ensures that each stone is safely set and that functional 
                        parts, such as clasps and safety locks, work properly.
                        <br /><br />
                        <b>Engraving: </b>
                        <br/>Jewellery can be personalized by engraving a name, a date or a message on the precious 
                        metal (service subject to technical constraints and space availability). Engraving is a discretionary 
                        service offered by our store if requested at the time of purchase.
                        <br /><br />
                        <b>Re-sizing: </b>
                        <br/>Performed on a bracelet, necklace, chain or ring, this service consists of increasing 
                        or reducing the size of the item within limitations. Some jewels, due to their unique design, cannot be 
                        re-sized. Please get in touch with us for advice and resizing options.
                        <br /><br />
                        <b>Earring Services: </b>
                        <br/>Your earring clips can be adjusted for better comfort. The adjustment increases 
                        or decreases the clip tension and gap, and therefore the hold on the earlobe. 
                        <br /><br />
                        <b>Plating Services: </b>
                        <br/>For plated jewellery which is showing signs of wear, we offer plating services 
                        to restore the original finish. You can also to opt to have your jewellery pieces plated to alter the 
                        appearance. 
                        <br /><br />
                        <b>Repair Services: </b>
                        <br/>Proper care and handling will protect the appearance of your jewellery over time. 
                        If your jewellery becomes damaged, please avoid wearing it until examined by one of our staff in store. 
                        After a careful assessment, a quotation will be given, and if accepted repairs and replacements will be made. 

                    </p>
                </div>

                <div class="col-md-4">
                    <img src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/serv01.png" class="photo-cust" alt="">
                </div>
            </div>


            

        </div>
    </section>


    <footer id="footer-secs" class="p-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                            <img class="" src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/footerlogo.png" alt="">
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12">

                        <div class="foot-link">
                            <ul>
                                <li> <a href="#">Customer Care</a> </li>
                                <li> <a href="<?php echo base_url('shipping')?>">Shipping</a> </li>
                                <li> <a href="<?php echo base_url('guarantee')?>"> Gurantee</a> </li>
                                <li> <a href="<?php echo base_url('contact')?>"> Contact</a> </li>
                                <li> <a href="<?php echo base_url('store')?>">Stores</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row pt-5">
                    <div class="wrap-icon-menu">
                        <ul>
                            <li><i class="fab fa-facebook-f"></i></li>
                            <li><i class="fab fa-twitter"></i></li>
                            <li><i class="fab fa-google-plus-g"></i></li>
                            <li><i class="fab fa-instagram"></i></li>
                        </ul>
                    </div>
                </div>
            </div>

        </footer>

        <section id="bottom-copyright" class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <span class="date-hema">&COPY; 2019 Hemachandra ‐ kandy</span>
                    </div>
                    <div class="col-md-6">
                        <ul>
                         <li> <a href="<?php echo base_url('termsandcondition')?>">Terms & Condition</a> </li>
                         <li> <a href="<?php echo base_url('legalnotice')?>">Legal Notice</a> </li>
                         <li> <a href="<?php echo base_url('privacy')?>">Privacy Policy</a> </li>
                         <li> <a href="#">Site Map</a> </li>
                        </ul>
                    </div>
                    <div class="col-md-3 bottom-foot-text date-hemas">
                        <div class="bc-contect">
                                <a href="http://bckonnect.com">BcKonnect</a> <img src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/bcklogo.png" alt="">
                        </div>

                    </div>
                </div>
            </div>
        </section>



    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script
  src="https://code.jquery.com/jquery-3.4.0.min.js"
  integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
  crossorigin="anonymous"></script>
    <script src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/js/servis-func.js"></script>
    <script src="<?php echo base_url('assets/frontend/'); ?>menuFixed/menu.js"></script>

</body>

</html>
