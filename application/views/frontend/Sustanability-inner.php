
<?php $this->load->view('components/headerNew'); ?>   
<?php $this->load->view('components/menu'); ?>
     
     <div class="container" style="padding-top: 50px;">   
    <section id="service-img-susin">
        <!-- <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div> -->
        <!-- <h1 class="text-capitalize">Find us</h1> -->
    </section>

    
    <section class="news-header">
        <div class="news-title">
            <h1>HEMACHANDRA unveils new TALI collection with <br> interactive astrology app </h1>
            <p class="date">03 MAY 2019</p>
        </div>

    </section>

    <!-- end -->

    <section class="news-slider">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                        <div class="glider-contain">
                                <div class="glider">
                                  <div class="profile">
                                        <a href="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" data-fancybox="images" data-width="2048" data-height="1365">
                                                <img src="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" />
                                        </a>
                                  </div>
                                  <div class="profile">
                                    <a href="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" data-fancybox="images" data-width="2048" data-height="1365">
                                        <img src="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" />
                                </a>
                                  </div>
                                  <div class="profile">
                                    <a href="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" data-fancybox="images" data-width="2048" data-height="1365">
                                        <img src="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" />
                                </a>
                                  </div>
                                  <div class="profile">
                                    <a href="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" data-fancybox="images" data-width="2048" data-height="1365">
                                        <img src="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" />
                                </a>
                                  </div>
                                  <div class="profile">
                                    <a href="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" data-fancybox="images" data-width="2048" data-height="1365">
                                        <img src="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" />
                                </a>
                                  </div>
                                  <div class="profile">
                                    <a href="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" data-fancybox="images" data-width="2048" data-height="1365">
                                        <img src="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" />
                                </a>
                                  </div>
                                  <div class="profile">
                                    <a href="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" data-fancybox="images" data-width="2048" data-height="1365">
                                        <img src="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/jew1.jpg" />
                                </a>
                                  </div>




                                  </div>
                                  <button role="button" aria-label="Previous" class="glider-prev">«</button>
					  <button role="button" aria-label="Next" class="glider-next">»</button>
					  <div role="tablist" class="dots"></div>
                                  </div>
                </div>
            </div>
        </div>
    </section>
    </div>


       <div class="container bottom-juwelery">
           <div class="row">
               <div class="col-md-12">
                    <div class="below-text text-center">
                            <p class="carre-txt">A careful adjustment to your creation ensures that it fits perfectly and with the greatest of comfort. The Mains d’Or of the Maison apply their technical expertise to finely adjust your chosen piece of jewelry. They skilfully shape the metal of a ring, modify the length of a chain, adapt the post or clip mechanism of an earring.</p>

                        </div>
                        <p class="text-center carre-txt1">This service is always undertaken with respect to the original technical and aesthetic specifications of the piece.
                        </p>
               </div>
           </div>
       </div>

<!--end edit sectio tiorn-->

    <footer id="footer-secs" class="p-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                            <img class="" src="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/footerlogo.png" alt="">
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12">

                        <div class="foot-link">
                            <ul>
                                <li> <a href="<?php echo base_url('contact'); ?>">Customer Care</a> </li>
                                <li> <a href="<?php echo base_url('shipping'); ?>">Shipping</a> </li>
                                <li class="btactive"> <a href="<?php echo base_url('guarantee'); ?>"> Guarantee</a> </li>
                                <li> <a href="<?php echo base_url('contact'); ?>"> Contact</a> </li>
                                <li> <a href="<?php echo base_url('store'); ?>">Stores</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row pt-5">
                    <div class="wrap-icon-menu">
                        <ul>
                            <li><i class="fab fa-facebook-f"></i></li>
                            <li><i class="fab fa-twitter"></i></li>
                            <li><i class="fab fa-google-plus-g"></i></li>
                            <li><i class="fab fa-instagram"></i></li>
                        </ul>
                    </div>
                </div>
            </div>

        </footer>

        <section id="bottom-copyright" class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <span class="date-hema">&COPY; 2019 Hemachandra ‐ kandy</span>
                    </div>
                    <div class="col-md-6">
                        <ul>
                         <li> <a href="<?php echo base_url('termsandcondition'); ?>">Terms & Condition</a> </li>
                         <li> <a href="<?php echo base_url('legalnotice'); ?>">Legal Notice</a> </li>
                         <li> <a href="<?php echo base_url('privacy'); ?>">Privacy Policy</a> </li>
                         <li> <a href="#">Site Map</a> </li>
                        </ul>
                    </div>
                    <div class="col-md-3 bottom-foot-text date-hemas">
                        <div class="bc-contect">
                                <a href="bckonnect.com">BcKonnect</a> <img src="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/img/bcklogo.png" alt="">
                        </div>

                    </div>
                </div>
            </div>
        </section>




    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.0.min.js" integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="crossorigin="anonymous"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"async defer></script>
    <script src="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/js/glider.min.js"></script>
    <script src="<?php echo base_url('assets/frontend/'); ?>menuFixed/menu.js"></script>


       <script>
    	new Glider(document.querySelector('.glider'), {
  slidesToScroll: 1,
  slidesToShow: 4,
  draggable: true,
  dots: '.dots',
  arrows: {
    prev: '.glider-prev',
    next: '.glider-next'
  }
});
    </script>
    


</body>

</html>
