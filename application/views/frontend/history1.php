<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans|EB+Garamond" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tironHistory/css/history.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tironHistory/css/time.css>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url('assets/frontend/'); ?>img/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url('assets/frontend/'); ?>img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="<?php echo base_url('assets/frontend/'); ?>img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/favicon/browserconfig.xml">
    <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
    <title>Hemchandra | History </title>
</head>

<body>
        <header class="navig" id="main-menu-back" >
                <div class="main-menu">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">



                                <div id="top-head" class="navColor">
                                        <ul class="language-list">
                                                 <li>
                                                     <i class="fas fa-chevron-right"></i>
                                                    <select class="langselectbar" name="droplangsel">

                                                        <option>EN</option>
                                                        <option>SL</option>
                                                    </select>

                                                    <input type="text" name="text" id="text-serch" class="search">
                                                    <img src="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/iconfinder_basics-19_296814.png" class="search-ico" alt="">


                                                </li>
                                            </ul>
                                    <ul class="shiping-nav">
                                        <li class="hicolor"><a href="<?php echo base_url('news'); ?>">Media </a></li>
                                        <li><a href="<?php echo base_url('contact'); ?>">Contact</a> </li>
                                        <li> <a href="<?php echo base_url('shipping'); ?>"> Shipping</a> </li>
                                    </ul>
                                </div>



                            </div>
                        </div>
                        <div class="row">


                            <div id="second-nav" class="navcol" >
                                <ul>
                                    <li class=" about panel-about"><a href="#">About Us</a></li>
                                    <li><a href="<?php echo base_url('gemstone'); ?>">Gemstones  </a></li>
                                    <li class="dow-panel-sec"><a href="#">Collection </a></li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>"> <img src="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/logo_03.jpg" class="logo" alt=""></a>
                                    </li>
                                    <li><a href="<?php echo base_url('guarantee'); ?>">Guarantee </a></li>
                                    <li><a href="<?php echo base_url('service'); ?>" class="text-active">Services</a></li>
                                    <li><a href="<?php echo base_url('store'); ?>">Stores</a></li>
                                </ul>

                            </div>


                        </div>
                    </div>
                </div>


  <!--dorp sec1-->
                    
                    <section id="drop-one" style="display: none;">
                <div class="container wrap-dorop">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 offset-md-1 offset-lg-1">
                        <?php foreach($collection_main1 as $col):?>
                            <h4><?php echo $col['celebrate_name'];?></h4>
                            
                            <ul>
                            <?php foreach($collection_sub1 as $subCat):?>
                                <?php $sub_cat_id = $subCat['page_sub_catagory_id'];
                                      $cat_id = $subCat['page_catagory_id'];
                                ?>
            	                <li><a href="<?php echo base_url();?>home/collection/<?php echo $sub_cat_id ?>/<?php echo $cat_id;?>"><?php echo $subCat['page_catagory_name']; ?></a></li>
            	                    <?php endforeach;?>
                                <!--<li><a href="#">Love & Life</a></li>-->
                                <!--<li><a href="#">Ceylone Doll</a></li>-->
                                <!--<li><a href="#">Heritage</a></li>-->
                               
                            </ul>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-md-3 col-lg-3 clt-image">
                        <?php foreach($collection_main2 as $col):?>
                            <h4><?php echo $col['celebrate_name'];?></h4>
                            
                            <ul>
                            <?php foreach($collection_sub2 as $subCat):?>
            	                <li><a href="<?php echo base_url();?>home/collection/<?php echo $sub_cat_id ?>/<?php echo $cat_id;?>"><?php echo $subCat['page_catagory_name']; ?></a></li>
            	             <?php endforeach; ?>
            	             
                                <!--<li><a href="#">Adamantine</a></li>-->
                                <!--<li><a href="#">Celestial Art</a></li>-->
                                <!--<li><a href="#">Contemporary</a></li>-->
                                <!--<li><a href="#">Moon Stone</a></li>-->
                                <!--<li><a href="#">Island Life</a></li>-->
                                <li class="vall-btn"><a href="#">View All</a></li>
                               
                            </ul>
                            <?php endforeach; ?>
                        </div>
                        
                        <div class="col-md-3 col-lg-3 clt-image">
                        <?php foreach($collection_main3 as $col):?>
                            <h4><?php echo $col['celebrate_name'];?></h4>
                            
                            <ul>
                             <?php foreach($collection_sub3 as $subCat):?>
            	                <li><a href="<?php echo base_url();?>home/collection/<?php echo $sub_cat_id ?>/<?php echo $cat_id;?>"><?php echo $subCat['page_catagory_name']; ?></a></li>
            	             <?php endforeach; ?>
                                
                                <li class="vall-btn"><a href="#">View All</a></li>
                               
                               
                            </ul>
                            <?php endforeach; ?>
                        </div>
                        
                        <div class="col-md-3 col-lg-3 clt-image">
                        <?php foreach($collection_main4 as $col):?>
                            <h4><?php echo $col['celebrate_name'];?></h4>
                            
                            <ul>
                             <?php foreach($collection_sub5 as $subCat):?>
            	                <li><a href="<?php echo base_url();?>home/collection/<?php echo $sub_cat_id ?>/<?php echo $cat_id;?>"><?php echo $subCat['page_catagory_name']; ?></a></li>
            	             <?php endforeach;?>
                                
                                <li class="vall-btn"><a href="#">View All</a></li>
                               
                               
                            </ul>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </section>
                    
            <section id="drop-two" style="display: none;">
                <div class="container wrap-dorop">
                    <div class="row">
                        <div class="col-md-2 col-lg-2 offset-md-1 offset-lg-1">
                            <h4>Company</h4>
                            
                            <ul>
                                <li><a href="<?php echo base_url('story'); ?>">Story</a></li>
                                <li><a href="<?php echo base_url('sustainability'); ?>">Sustainability</a></li>
                                <li><a href="<?php echo base_url('people'); ?>">People</a></li>
                                <li><a href="<?php echo base_url('news'); ?>">News</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-lg-4 clt-image">
                            
                            <img src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/collection-img.png" class="auth-image">
                            <img src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/collection-img.png" class="auth-image2">
                        </div>
                        <div class="col-md-3 col-lg-4 clt-image">
                            <h4>hellow</h4>
                            <p class="colomb">COLOMBO 07 - Sri Lanka</p>
                            <p class="flower-rd">No. 74 Flower Rd, Colombo 00700<br>
                                0094 11 2874 893</p>
                                
                            
                                <ul class="drow-ico">
                                    <li><i class="fab fa-facebook-f"></i></li>
                                    <li><i class="fab fa-twitter"></i></li>
                                    <li><i class="fab fa-instagram"></i></li>
                                    <li><i class="fab fa-youtube"></i></li>
                                </ul>
                                    
                        </div>
                        <div class="col-md-2 col-lg-2"></div>
                    </div>
                </div>
            </section>


            </header>

       <section class="sticky">
            <div class="container">
                <div class="col-md-12">
                    <div class="stickynav">
                        <div class="nanigation-stick">
                                <div  class="stik-nav" >
                                        <ul>
                                            <liclass="active" ><a href="#">About us </a></li>
                                             <li><a href="<?php echo base_url('gemstone'); ?>">Gemstones  </a></li>
                                            <li><a href="#">Collection </a></li>
                                            <li class="border-img">
                                                <a href="<?php echo base_url(); ?>"> <img src="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/stick_navigation_bar.png" class="logoStick" alt=""></a>
                                            </li>
                                            <li><a href="<?php echo base_url('guarantee'); ?>">Guarantee </a></li>
                                            <li ><a href="<?php echo base_url('service'); ?>" class="text-active">Services</a></li>
                                            <li><a href="<?php echo base_url('store'); ?>">Stores</a></li>
                                        </ul>

                                    </div>
                        </div>
                    </div>
                </div>
            </div>
       </section>
    <section class="mobile-sec">
      <div class="container">
        <div class="row">
            <div class="mobile-nav-sec">
                <div class="img-tag">
                    <img src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/logo_03.jpg" alt="">
                    <h4 id="open-menu">menu <i class="fas fa-chevron-down"></i></h4>


                </div>

              </div>

        </div>
        <nav id="overlay">
            <ul>
              <li class="left"><a href="#">About </a></li>
              <li class="left"><a href="<?php echo base_url('gemstone'); ?>">Gemstones</a></li>
              <li class="left drop-down"><a href="#">Collection</a>
                <li class="right first-gurante-child"><a href="<?php echo base_url('gurantee'); ?>">Gurantee</a></li>
                <li class="right "><a href="<?php echo base_url('service'); ?>" class="text-active">Services</a></li>
                <li class="right"><a href="<?php echo base_url('store'); ?>">Stores</a></li>
                </li>
            </ul>
          </nav>


      </div>

    </section>

    <section id="service-img">
        <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div>
        <h1 class="text-capitalize">What’s New</h1>
    </section>

    <section id="blue-box">
        <div class="container ">
            <div class="row  ">
                <div class="col-md-12">
                    <div class="box-content">
                        <p>continuing the tradition..</p>

                        <div class="row time-three">
                            <div class="col-md-4">
                                <p>Humble begening</p>
                            </div>
                            <div class="col-md-4">
                                <p>Expanding our wings</p>
                            </div>
                            <div class="col-md-4">
                                <p>Traditional contemporary</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="timeline">
                                    <li><a href="#feed">1942</a></li>
                                    <li><a href="#feed1">  1946</a> </li>
                                    <li><a href="#feed2">  1948</a> </li>
                                    <li><a href="#feed3">  1952</a> </li>
                                    <span class="divde-sec"></span>
                                    <span class="divde-therd"></span>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>


    



    <section id="feed">
        <div class="container-fluid">
            <div class="row feed-wrap">
            
            <div class="col-md-4 his-img">
            <img src="<?php echo base_url('assets/frontend/'); ?>tironHistory/img/History-v2_02.jpg" alt="">
            </div>  
            <div class="col-md-4 his-content">
                <div class="row hiswrap">
                    
                    <div class="col-md-12">
                        <h6 class="date-year text-right">1942</h6>
                        <h6 class="text-right">the origine story</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="historyl">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>   
                    </div>
                    <div class="col-md-6">
                        <div class="historyr">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="historyS">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>
                    </div>
                
                </div>
            </div>  
            <div class="col-md-4 his-imgsec">
                    <img src="<?php echo base_url('assets/frontend/'); ?>tironHistory/img/History-v2_04.jpg" alt="">
            </div>  


</div>
        </div>
    </section>

    <section id="feed1">
        <div class="container-fluid">
            <div class="row feed-wrap">
            
            <div class="col-md-4 his-img">
            <img src="<?php echo base_url('assets/frontend/'); ?>tironHistory/img/History-v2_02.jpg" alt="">
            </div>  
            <div class="col-md-4 his-content">
                <div class="row hiswrap ">
                    
                    <div class="col-md-12">
                        <h6 class="date-year text-right">1946</h6>
                        <h6 class="text-right">the origine story</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="historyl">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>   
                    </div>
                    <div class="col-md-6">
                        <div class="historyr">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="historyS">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>
                    </div>
                
                </div>
            </div>  
            <div class="col-md-4 his-imgsec">
                    <img src="<?php echo base_url('assets/frontend/'); ?>tironHistory/img/History-v2_04.jpg" alt="">
            </div>  

</div>

        </div>
    </section>


    <section id="feed2">
        <div class="container-fluid">
            <div class="row feed-wrap">
            
            <div class="col-md-4 his-img">
            <img src="<?php echo base_url('assets/frontend/'); ?>tironHistory/img/History-v2_02.jpg" alt="">
            </div>  
            <div class="col-md-4 his-content">
                <div class="row hiswrap">
                    
                    <div class="col-md-12">
                        <h6 class="date-year text-right">1948</h6>
                        <h6 class="text-right">the origine story</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="historyl">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>   
                    </div>
                    <div class="col-md-6">
                        <div class="historyr">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="historyS">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>
                    </div>
                
                </div>
            </div>  
            <div class="col-md-4  his-imgsec">
                    <img src="<?php echo base_url('assets/frontend/'); ?>tironHistory/img/History-v2_04.jpg" alt="">
            </div>  


</div>
        </div>
    </section>

    <section id="feed3">
        <div class="container-fluid">
            <div class="row feed-wrap">
            
            <div class="col-md-4 his-img">
            <img src="<?php echo base_url('assets/frontend/'); ?>tironHistory/img/History-v2_02.jpg" alt="">
            </div>  
            <div class="col-md-4 his-content">
                <div class="row hiswrap">
                    
                    <div class="col-md-12">
                        <h6 class="date-year text-right">1952</h6>
                        <h6 class="text-right">the origine story</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="historyl">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>   
                    </div>
                    <div class="col-md-6">
                        <div class="historyr">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>
                    </div>
                    <div class="col-md-12 ">
                        <div class="historyS">
                            <p class="text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. A voluptates ab temporibus adipisci odio dolorum nobis fugit enim cum nostrum.</p>
                        </div>
                    </div>
                
                </div>
            </div>  
            <div class="col-md-4 his-imgsec">
                    <img src="<?php echo base_url('assets/frontend/'); ?>tironHistory/img/History-v2_04.jpg" alt="">
            </div>  


</div>
        </div>
    </section>


    <footer id="footer-secs" class="p-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                            <img class="" src="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/footerlogo.png" alt="">
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12">

                        <div class="foot-link">
                            <ul>
                                <li> <a href="#">Customer Care</a> </li>
                                <li> <a href="<?php echo base_url('shipping'); ?>">Shipping</a> </li>
                                <li> <a href="<?php echo base_url('guarantee'); ?>"> Gurantee</a> </li>
                                <li> <a href="<?php echo base_url('contact'); ?>"> Contact</a> </li>
                                <li> <a href="<?php echo base_url('store'); ?>">Store</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row pt-5">
                    <div class="wrap-icon-menu">
                        <ul>
                            <li><i class="fab fa-facebook-f"></i></li>
                            <li><i class="fab fa-twitter"></i></li>
                            <li><i class="fab fa-google-plus-g"></i></li>
                            <li><i class="fab fa-instagram"></i></li>
                        </ul>
                    </div>
                </div>
            </div>

        </footer>

        <section id="bottom-copyright" class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <span class="date-hema">&COPY; 2019 Hemachandra ‐ kandy</span>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li> <a href="<?php echo base_url('ternandcondition'); ?>">Terms & Condition</a> </li>
                            <li> <a href="<?php echo base_url('legalnotice'); ?>">Legal Notice</a> </li>
                            <li> <a href="<?php echo base_url('privacy'); ?>">Privacy Policy</a> </li>
                         <li> <a href="#">Site Map</a> </li>
                        </ul>
                    </div>
                    <div class="col-md-3 bottom-foot-text date-hemas">
                        <div class="bc-contect">
                                <a href="#">BcKonnect</a> <img src="<?php echo base_url('assets/frontend/'); ?>tiornNews/img/bcklogo.png" alt="">
                        </div>

                    </div>
                </div>
            </div>
        </section>



    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script
  src="https://code.jquery.com/jquery-3.4.0.min.js"
  integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
  crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="<?php echo base_url('assets/frontend/'); ?>tironHistory/js/servis-func.js"></script>

    <script src="<?php echo base_url('assets/frontend/'); ?>tironHistory/js/time.js"></script>
    
    <script>
          
    window.sr = ScrollReveal();
	sr.reveal('.his-img',{
		duration: 2000,
		origin:'left',
		distance:'400px'
	});
	sr.reveal('.his-content',{
		duration: 2000,
		origin:'bottom',
		distance:'400px'
	});
	sr.reveal('.his-imgsec',{
		duration: 2000,
		origin:'right',
		 distance:'200px'
	});

    </script>
    <script>
        $(function() {
          // Smooth Scrolling
          $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html, body').animate({
                  scrollTop: target.offset().top
                }, 1000);
                return false;
              }
            }
          });
        });
        </script>

    
</body>

</html>
