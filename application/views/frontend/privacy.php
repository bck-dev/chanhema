<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-img">
    <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return
                Policy</a></span>
        <span class="policy-close">x</span>
    </div>
    <h1 class="text-capitalize">
        <!-- Privacy Policy  -->
    </h1>
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>
                We value your privacy and are committed to protecting your personal information
            </p>
            <!--<a href="#">-->
            <!--<div class="faq-button">-->
            <!--<a href="service.html#seek-help" >FAQ</a>-->
            <!--</div></a>-->
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- <div>
                        <h3 class="bell-heading">Information Collection</h3>
                    </div> -->
            </div>
        </div>
        <div class="row">
            <ul>
                <h3 class="bell-heading">Information Collection</h3>
                <li>
                    <h5 class="privacy-subheading">Personal identification information</h5>
                    <p class="privacy-content">We may collect personal identification information from users in a
                        variety of ways, including, but not limited to, when users visit our site, fill out a form, and
                        in connection with other activities, services, features or resources we make available on our
                        site. Users may be asked for, as appropriate, name, email address, mailing address, phone
                        number. Users may, however, visit our site anonymously. We will collect personal identification
                        information from users only if they voluntarily submit such information to us. Users can always
                        refuse to supply personally identification information, except that it may prevent them from
                        engaging in certain site-related activities.</p>
                </li>
                <li>
                    <h5 class="privacy-subheading">Non-personal identification information</h5>
                    <p class="privacy-content">We may collect non-personal identification information about users
                        whenever they interact with our site. Non-personal identification information may include the
                        browser name, the type of computer and technical information about users means of connection to
                        our site, such as the operating system and the Internet service providers utilized and other
                        similar information.</p>
                </li>
                <li>
                    <h5 class="privacy-subheading">Usage</h5>
                    <p class="privacy-content">We may collect and use your personal information for the following
                        purposes:</p>

                <li>
                    <p class="privacy-content">To improve the quality of our customer service - the information you
                        provide helps us respond to your customer service requests and support your needs more
                        efficiently.</p>
                </li>
                <li>
                    <p class="privacy-content">To personalise user experience we may use information to understand how
                        the site is used.</p>
                </li>
                <li>
                    <p class="privacy-content">To improve our site we may use your feedback to improve our products and
                        services.</p>
                </li>
                <li>
                    <p class="privacy-content">To run a promotion, contest, survey or other site feature, we may use
                        information to send you details you agreed to receive about topics we think will be of interest
                        to you.</p>
                </li>
                <li>
                    <p class="privacy-content">To send periodic emails, updates and order information we may use the
                        email address provided. It may also be used to respond to your inquiries, questions, and/or
                        other requests. If a user decides to opt-in to our mailing list, they will receive emails that
                        may include company news, updates, related product or service information. If at any time you
                        would like to unsubscribe from receiving future emails, we include detailed unsubscribe
                        instructions at the bottom of each email or you may contact us via our site.</p>
                </li>

                </li>
                <li>
                    <h3 class="bell-heading">Cookies</h3>

                <li>
                    <p class="privacy-content"><strong>Definition –</strong> Cookies are small files made up of letters
                        and numbers that are automatically stored on your computer’s browser when you visit certain
                        websites.</p>
                </li>
                <li>
                    <p class="privacy-content"><strong>Purpose -</strong> Cookies are used to keep track of your
                        movements on a site, to help you resume where you left off, and remember your preferences. The
                        website saves a corresponding file to the one that is in your browser. In this file the website
                        stores your site movements and any information you have given voluntarily such as your name,
                        address and email address.</p>
                </li>
                <li>
                    <p class="privacy-content"><strong>We use non-</strong>essential cookies on the basis of your
                        consent. You have the right to withdraw this at any point. You can set your cookie preferences
                        by changing your browser settings.</p>
                </li>

                </li>
                <li>
                    <h3 class="bell-heading">Information sharing</h3>
                    <p class="privacy-content">We do not sell, trade, or rent users personal identification information
                        to others except under the following circumstances:</p>

                <li>
                    <p class="privacy-content">we will disclose your personal information to third party providers for
                        the purposes of providing online and other payment services, handling credit checks and fraud
                        prevention;</p>
                </li>
                <li>
                    <p class="privacy-content">we will disclose your personal information to any law enforcement agency,
                        court, regulator, government authority or other third party where we believe this is necessary
                        to comply with a legal or regulatory obligation, or otherwise to protect our rights or the
                        rights of any third party; </p>
                </li>
                <li>
                    <p class="privacy-content">we will disclose your personal information to any third party that
                        purchases, or to which we transfer, all or substantially all of our assets and business. Should
                        such a sale or transfer occur, we will use reasonable efforts to try to ensure that the entity
                        to which we transfer your personal information uses it in a manner that is consistent with this
                        Privacy Policy.</p>
                </li>

                </li>
                <li>
                    <h3 class="bell-heading">Information Protection</h3>
                    <p class="privacy-content"><strong>Security –</strong> At Hemachandra, we want you to feel confident
                        about using our platform and we are committed to protecting the personal information we collect.
                        Your information is only shared with employees who need access to it in order to do their jobs
                        e.g. to process a payment or send you an order update. We have appropriate technical and
                        organizational physical, electronic, and procedural safeguards to protect the personal
                        information that you provide to us against unauthorized or unlawful processing and against
                        accidental loss, damage or destruction. </p>
                </li>
                <li>
                    <h3 class="bell-heading">Changes to policy</h3>
                    <p class="privacy-content">We reserve the right, at our sole discretion, to modify or replace these
                        Terms and Conditions at any time. If we do make changes, we will post an updated version of them
                        on this page with an indication of their last updated date.</p>
                </li>
            </ul>
        </div>
    </div>
</section>

<section id="seek-help">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="contant-service-text">Customisation Service</h2>
                <div class="row">
                    <div class="col align-self-center">
                        <div class="service-sentnce customisation-text">
                            <p>For a truly unique gift, any of our creations can be customised using the precious metal
                                and stone of your choice. With the personalised assistance of our in-house team, ou can
                                also create your own bespoke design.</p>
                        </div>
                    </div>

                </div>
                <div class="phone-number">
                    <!-- <h1 class="inq-tel">For Inquiries</h1> -->
                    <div class="custom-inquire-area"><a class="custom-inquire-text" href="./contact">Click here for
                            inquiries</a></div>
                    <!-- <a class="inq-tel-num" href="tel:+94812387387"><p class="inq-tel-num">+94 81 238 73 87</p></a> -->
                    <div class="custom-inquire-area"><a class="inquire-email"
                            href="mailto:mail@hemachandras.com">mail@hemachandras.com</a></div>
                </div>

            </div>

        </div>

    </div>

</section>

<!-- footer strart -->

<?php $this->load->view('components/footer'); ?>