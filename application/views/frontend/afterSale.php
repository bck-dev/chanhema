<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-imgs" class="after_sales_banner">
    <!-- <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div> -->
    <h1 class="after-sale-banner-text">
        <!-- Care For Your Jewellery -->
    </h1>
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>
                We are committed to providing you with the highest level of jewellery care services
            </p>
        </div>
    </div>
</section>

<!-- <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 aftesale-main-intro-area">
          <p class="crafter-text aftersale-intro">We are pleased to offer the following services in store to our clients.For each service performed, a quality inspection follows ensuring that each setting is secure and all functional parts, such as clasps and safety locks, work well.Should you have any questions regarding the services we offer or require specific information, we invite you to <a href="<?php echo base_url('contact'); ?>">get in touch</a> or visit us in store.</p>
        </div>
      <div>
    </div>
  </section> -->

<div class="container bottom-juwelery pb-5">
    <div class="row">
        <div class="col-md-8 pt-5 new-text">
            <div class=" below-text">
                <p class="care-for-jewellary innerpages-content">
                    We are pleased to offer the following services in store to our clients.For each service performed, a
                    quality inspection follows ensuring that each setting is secure and all functional parts, such as
                    clasps and safety locks, work well.Should you have any questions regarding the services we offer or
                    require specific information, we invite you to <a href="<?php echo base_url('contact'); ?>"
                        class="orange-highlight">get in touch</a> or visit us in store.</p>

                <h3 class="bell-heading mt-5">Cleaning and Polishing Services</h3>
                <p class="care-for-jewellary innerpages-content">
                    Depending on the item and signs of wear it shows, we may recommend simple cleaning, ultrasonic
                    cleaning or polishing to remove scratches and preserve the shine. For white gold jewellery, the
                    polishing service includes rhodium plating to enhance the brilliance of the metal. </p>

                <h3 class="bell-heading mt-5">Engraving Services</h3>
                <p class="care-for-jewellary innerpages-content">
                    Jewellery can be personalized by engraving a name, a date or a message on the metal. This service
                    however is subject to technical constraints and the availability of space on the item in question
                    and must be requested at the time of purchase.</p>

                <img src="<?php echo base_url('assets/frontend/'); ?>/img/After_Sales_Inner01.jpg"
                    class="d-block d-md-none my-3 w-100" alt="">

                <h3 class="bell-heading mt-5">Re-sizing Services</h3>
                <p class="care-for-jewellary innerpages-content">
                    We are able to increase or reduce the size of an item within limitations, to improve fit and
                    comfort. Bracelets, necklaces, chains or rings may be re-sized, however certain designs may not
                    allow for it.</p>

                <h3 class="bell-heading mt-5">Earring Services</h3>
                <p class="care-for-jewellary innerpages-content">
                    Earring clips can be adjusted for better comfort. An adjustment can be made to increase or decrease
                    the clip tension and gap, and thereby improve the level comfort of the hold on the earlobe. </p>

                <img src="<?php echo base_url('assets/frontend/'); ?>/img/After_Sales_Inner02.jpg"
                    class="d-block d-md-none my-3 w-100" alt="">

                <h3 class="bell-heading mt-5">Plating Services</h3>
                <p class="care-for-jewellary innerpages-content">
                    For plated jewellery showing signs of wear, we offer plating services to restore the original finish
                    of the product. You may also choose to have your jewellery plated to alter its appearance. </p>

                <h3 class="bell-heading mt-5">Repair Services</h3>
                <p class="care-for-jewellary innerpages-content">
                    Proper care and handling will protect your jewellery over time. If however your jewellery is
                    damaged, please avoid further use until examined by our staff. After a careful assessment, a
                    quotation will be provided, and if the charges are accepted you repairs and replacements will be
                    made.</p>

                <img src="<?php echo base_url('assets/frontend/'); ?>/img/After_Sales_Inner03.jpg"
                    class="d-block d-md-none my-3 w-100" alt="">

            </div>
        </div>

        <div class="col-md-4 pt-5 d-none d-md-block">
            <img src="<?php echo base_url('assets/frontend/'); ?>/img/After_Sales_Inner01.jpg" class="w-100" alt="">
            <br /><br />
            <img src="<?php echo base_url('assets/frontend/'); ?>/img/After_Sales_Inner02.jpg" class="w-100" alt="">
            <br /><br />
            <img src="<?php echo base_url('assets/frontend/'); ?>/img/After_Sales_Inner03.jpg" class="w-100" alt="">
        </div>
    </div>
</div>


<!--end edit sectio tiorn-->
<script>
new Glider(document.querySelector('.glider'), {
    slidesToScroll: 1,
    slidesToShow: 4,
    draggable: true,
    dots: '.dots',
    arrows: {
        prev: '.glider-prev',
        next: '.glider-next'
    }
});




$('[data-fancybox="preview"]').fancybox({
    thumbs: {
        autoStart: true
    }
});
</script>

<?php $this->load->view('components/footer'); ?>