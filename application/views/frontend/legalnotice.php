<?php $this->load->view('components/headerNew'); ?>   
<?php $this->load->view('components/menu'); ?>

    <section id="service-img">
        <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div>
        <h1 class="text-capitalize"><!-- Legal Notice  --></h1>
    </section>

    <section class="blue-box">
        <div class="container">
                    <div class="box-content">
                        <p>
                            We follow Sri Lankan law 
                        </p>
                        <!--<a href="#">-->
                        <!--<div class="faq-button">-->
                        <!--    <a href="service.html#seek-help" >FAQ</a>-->
                        <!--</div></a>-->
                        
                        
                    </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- <div>
                        <h3 class="bell-heading">Legal Notice (applicable to Sri Lankan Laws)</h3>
                    </div> -->
                </div>
            </div>
            <div class="row">
                <div>
                    <ul>
                        <h3 class="bell-heading">Legal Notice (applicable to Sri Lankan Laws)</h3>
                        <li>
                            <h5 class="privacy-subheading">Applicable Law and Competent Court</h5>

                            <p class="privacy-content">Except for any different rights according to applicable law – both national and international – these Terms and Conditions are regulated by Sri Lankan legislation and shall be interpreted according to it, included any dispute on Terms and Conditions existence, validity and effectiveness. Related to it, the competent court shall be the Colombo Law Court. By virtue of application of Sri Lankan legislation, in case of potential interpretative contrasts between the Sinhala version and other language versions of these Terms and Conditions, the Sri Lankan ones shall be applied.</p>
                            <br><br/>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="seek-help">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="contant-service-text">Customisation Service</h2>
                    <div class="row">
                        <div class="col align-self-center">
                                <div class="service-sentnce customisation-text">
                                        <p>For a truly unique  gift, any of our creations can be customised using the precious metal and stone  of your choice. With the personalised assistance of our in-house team, ou can also create your own bespoke design.</p>
                                        </div>
                        </div>
                        
                    </div>
                    <div class="phone-number">
                            <!-- <h1 class="inq-tel">For Inquiries</h1> -->
                            <div class="custom-inquire-area"><a class="custom-inquire-text" href="./contact">Click here for inquiries</a></div>
                            <!-- <a class="inq-tel-num" href="tel:+94812387387"><p class="inq-tel-num">+94 81 238 73 87</p></a> -->
                            <div class="custom-inquire-area"><a class="inquire-email" href="mailto:mail@hemachandras.com">mail@hemachandras.com</a></div>
                        </div>
                    
                </div>
            
            </div>

        </div>

    </section>

    <!-- footer strart -->

    <?php $this->load->view('components/footer'); ?>
