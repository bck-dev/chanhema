<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="shipinner-img">
    <!-- <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return Policy</a></span>
                <span class="policy-close">x</span></div> -->
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>
                No matter where you are in the world, enjoy receiving one of our hand-crafted pieces of jewellery
                straight to your door
            </p>
        </div>
    </div>
</section>

<div class="container bottom-juwelery pb-5">
    <div class="row">
        <div class="col-md-8 new-text">
            <div class="below-text pt-5">
                <h3 class="bell-heading mt-5">Charges & Fees</h3>
                <p class="care-for-jewellary innerpages-content">
                    Quotations for delivery will be obtained through reputed courier services and will be provided, and
                    acceptance of the charges by you will be required prior to your order being confirmed. The courier
                    company catering to your region will determine shipping charges based on the package weight,
                    delivery location and various other factors. A clear consice description of the charges applicable
                    will be provided to you.
                </p>
                <img src="<?php echo base_url('assets/frontend/'); ?>/img/Shipping_Inner_Page01.jpg"
                    class="d-block d-md-none w-100 mt-5" alt="">

                <h3 class="bell-heading mt-5">Duties & Taxes</h3>
                <p class="care-for-jewellary innerpages-content">
                    Duties and taxes will be applicable as per your country's regulations and are to be borne by the
                    customer. These charges will be billed to you by the courier company at the time of delivery. 
                </p>
                <img src="<?php echo base_url('assets/frontend/'); ?>/img/Shipping_Inner_Page02.jpg"
                    class="d-block d-md-none w-100 mt-5" alt="">

                <h3 class="bell-heading mt-5">Insurance</h3>
                <p class="care-for-jewellary innerpages-content">
                    All shipments are fully insured and require a signature and proof of ID at the time of delivery. At
                    this point, responsibility for your purchased goods passes to you. If you have specified a recipient
                    other than yourself for delivery purposes (for example a gift), then the customer agrees to accept a
                    signature by them as well as their photo ID as evidence of delivery. 
                </p>

                <br />
                <p class="care-for-jewellary innerpages-content">
                    Please <a class="orange-highlight" href="<?php echo base_url('contact'); ?>">get in touch</a> for
                    further information
                    including rates and charges that may apply to your order.
                </p>
            </div>
        </div>
        <div class="col-md-4 pt-5 d-none d-md-block">
            <img src="<?php echo base_url('assets/frontend/'); ?>/img/Shipping_Inner_Page01.jpg" class="w-100 mt-5"
                alt="">
            <br /><br /><br />
            <img src="<?php echo base_url('assets/frontend/'); ?>/img/Shipping_Inner_Page02.jpg" class="w-100" alt="">
        </div>
    </div>
</div>


<!--end edit sectio tiorn-->

<script>
new Glider(document.querySelector('.glider'), {
    slidesToScroll: 1,
    slidesToShow: 4,
    draggable: true,
    dots: '.dots',
    arrows: {
        prev: '.glider-prev',
        next: '.glider-next'
    }
});




$('[data-fancybox="preview"]').fancybox({
    thumbs: {
        autoStart: true
    }
});
</script>


<?php $this->load->view('components/footer'); ?>