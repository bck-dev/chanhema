<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="sustain-img">
    <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return
                Policy</a></span>
        <span class="policy-close">x</span>
    </div>
    <h1 class="">
        <!-- Making a Lasting Impact  -->
    </h1>
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>
                We believe it is our duty to preserve the sustainable art of fine jewellery craftsmanship in Sri Lanka.
                In doing so, we aim to minimise our impact as a jeweller while positively influencing our community
                through the preservation of a traditional art form.
            </p>
        </div>
    </div>
</section>

<section class="py-5 mb-5 mt-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 px-4">
                <div class="tile-1">
                    <div class="card">
                        <img class="card-img-top w-100"
                            src="<?php echo base_url('assets/frontend/'); ?>img/employees-nw.png" alt="Card image cap">
                        <div class="font-img-sus sustainability-blue-button">
                            <h4>The Craft</h4>
                            <div class="patarn-img"></div>
                        </div>
                        <div class="card-body sustainablity-card-body">
                            <div>

                                <p>
                                    Our team are integral to our success. Some come from families who have worked with
                                    us for two or three generations. The connection we share with the local community,
                                    built on respect and trust, is as important to us as the craft we have pledged to
                                    preserve.
                                </p>
                                <div id="craftScroll" style="cursor: pointer;"><a
                                        style="color: #05205f; text-decoration: underline;">Read More</a></div>
                            </div>
                            <!-- <div id="demo" class="collapse pb-2">
                    We believe passionately that this expert skill is one that needs to be celebrated and preserved as part of Sri Lanka’s unique heritage. 
                      <br/>
                    <a href="#shipping" style="color: #05205f; text-decoration: underline">Read More</a>
                    </div>
                    <a class="discover" href="#" data-toggle="collapse" data-target="#demo">Discover</a> -->

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 px-4">
                <div class="tile-1">
                    <div class="card">
                        <img class="card-img-top w-100"
                            src="<?php echo base_url('assets/frontend/'); ?>img/sourcing-n.jpg" alt="Card image cap">
                        <div class="font-img-sus sustainability-blue-button">
                            <h4 class="life">The Source</h4>
                            <div class="patarn-img"></div>
                        </div>
                        <div class="card-body sustainablity-card-body">
                            <div>

                                <p>
                                    Fine craftsmanship begins with responsibly sourcing high-quality raw materials. Over
                                    the past seven decades we have been sourcing gems and precious metals from trusted
                                    partners. The majority of our handpicked gemstones, which include sapphires,
                                    tourmalines, aquamarines and amethysts, come from small Sri Lankan mines.
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 px-4">
                <div class="tile-1">
                    <div class="card">
                        <img class="card-img-top w-100"
                            src="<?php echo base_url('assets/frontend/'); ?>img/packaging.jpg" alt="Card image cap">
                        <div class="font-img-sus sustainability-blue-button">
                            <h4>The Packaging</h4>
                            <div class="patarn-img"></div>
                        </div>
                        <div class="card-body sustainablity-card-body">
                            <div>

                                <p>
                                    In every action we strive to remain respectful and concious of our environment,
                                    while staying true to our Sri Lankan heritage. In upholding these values we were
                                    presented with a unique opportunity to support a local cottage industry. The weaving
                                    of palmyrah leaves to create various decorative and houshold items; is a centuries
                                    old handicraft, practiced in rural Sri Lanka. We use woven palmyrah pouches (known
                                    locally as ‘hambili’) to package your valuable purchases, an entirely natural
                                    product.
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end row -->
    </div>
</section> <!-- title section -->

<!--   QUality- section -->

<section id="quality">

    <div class="video-overlay-sus">
        <div class="container">
            <div class="text-center video-overlay-content">
                <h1 class="topic orange-highlight">Tribute to the Craft</h1>
                <span class="play-button"><i class="far fa-play-circle orange-highlight"></i></span>
            </div>
        </div>

        <div class="sustainability-video-box d-none">
            <video class="sustainability-video" controls>
                <source src="assets/frontend/video/srilanka-fire-sapphire.mp4" type="video/mp4">
            </video>
        </div>
    </div>
</section>
<script>
$(document).ready(function() {
    $('.play-button').click(function() {
        $('.video-overlay-content').addClass('d-none');
        $('.sustainability-video-box').removeClass('d-none');
        $('.sustainability-video-box').addClass('d-block');
        $('.sustainability-video')[0].play();
    });
});
</script>

<section id="feed-sus">
    <div class="container" id="shipping">
        <div class="row feed-sus-wrap">
            <div class="col-md-8 new-text">
                <h3 class="bell-heading">The Craft</h3>
                <p class="first-ptap">
                    The skill of our craftsmen is fundamental to our success as a jeweller. We are proud that in many
                    instances two, sometimes three generations of families have chosen to be part of our company. This
                    legacy, in terms of the expertise that is passed down from one generation to another, is one that
                    has a wider impact on the local community.
                </p>

                <p class="first-ptap">
                    We are dedicated to ensuring our master craftsmen have stable, fulfilling careers in which their
                    skills are celebrated and valued rather than overlooked, in favour of more efficient and
                    technologically advanced methods of production. High quality jewellery craftsmanship is a dying art
                    in Sri Lanka and one we believe must be protected for future generations to appreciate. By providing
                    lifelong positions we remain committed to the fulfilment of our craftsmen and the protection of this
                    heritage skill.

                </p>

                <p class="first-ptap">
                    Handmaking jewellery is a labour of love, requiring great skill and patience. The process is
                    incredibly creative and each craftsman’s personal flair means that every creation is entirely
                    unique.
                </p>

                <p class="first-ptap">We believe passionately that this expert skill is one that needs to be celebrated
                    and preserved as part of Sri Lanka’s unique heritage.</p>
            </div>

            <div class="col-md-4">
                <img class="w-100" src="<?php echo base_url('assets/frontend'); ?>/img/Sustainability_thecraft.jpg"
                    alt="">
                <br /><br />
                <img class="w-100" src="<?php echo base_url('assets/frontend'); ?>/img/Sustainability_thecraft2.jpg"
                    alt="">
            </div>
        </div>

    </div>
</section>

<script>
$('.discover').on('click', function() {
    if ($('.discover').html() == "Discover") {
        $('.discover').html('Show Less');
    } else {
        $('.discover').html('Discover');
    }
});

$("#craftScroll").click(function() {
    $('html, body').animate({
        scrollTop: $("#feed-sus").offset().top - 80
    }, 1000);
});
</script>

<?php $this->load->view('components/footer'); ?>