<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-img">
    <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return
                Policy</a></span>
        <span class="policy-close">x</span>
    </div>
    <h1 class="text-capitalize">
        <!-- Terms & Condition  -->
    </h1>
</section>

<section class="blue-box">
    <div class="container">
        <div class="box-content">
            <p>
                By using our platforms, you agree to be bound by the following Terms and Conditions
            </p>
            <!--<a href="#">-->
            <!--<div class="faq-button">-->
            <!--    <a href="service.html#seek-help" >FAQ</a>-->
            <!--</div></a>-->


        </div>

    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div>
                <ul>
                    <li>
                        <h3 class="bell-heading">Intellectual Property</h3>
                        <p class="privacy-content"><strong>Trademark -</strong> The trademark “Hemachandra”© and all
                            trademarks, logos and service marks (the “Trademarks”) which appear on this website are
                            registered and/or unregistered Hemachandra trademarks and are the exclusive property of
                            Hemachandra. The website, and all material, elements and content included on the website
                            (including drawings, designs, illustrations, photographs, text, computer code, sound tracks,
                            and graphics) which appear on this website as well as the look and feel of and know-how
                            related to the website, are the exclusive property of Hemachandra and the use by you of
                            Hemachandra’s Trademarks or any such material, look and feel and know how, in any manner, is
                            strictly prohibited. Nothing contained in the website should be construed as granting by
                            implication or otherwise, any license or right to use any Trademarks or other material
                            displayed on this website without the written permission of Hemachandra. You have permission
                            to electronically copy and print hard copies of pages from this website solely in connection
                            with non-commercial purposes related to placing an order or shopping with the website.
                            Unless we give you specific permission in advance, any other use of this website, its
                            content and its information, are strictly prohibited.</p>
                        <ul>
                            <li class="privacy-content">Copyright – © <?php echo date('Y'); ?> Hemachandra. All rights
                                reserved.</p>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <h3 class="bell-heading">Acceptance of terms</h3>
                        <p class="privacy-content">By accessing or using any part of this website or services provided
                            by/through the website, you agree to accept and comply with the Terms and Conditions
                            outlined here. These Terms and Conditions constitute a binding contract between you and us.
                            You are responsible for regularly reviewing the Terms of Use. You can review the most
                            current version of the Terms and Conditions on this page. If you do not wish to be bound by
                            these, please do not access or use any part of the website or any of the services provided
                            by or through the website.</p>

                    </li>
                    <li>
                        <h3 class="bell-heading">Changing of terms</h3>
                        <p class="privacy-content">We reserve the right, at our sole discretion, to modify or replace
                            these Terms and Conditions at any time. If we do make changes, we will post an updated
                            version of them on this page with an indication of their last updated date.</p>

                    </li>
                    <!-- <li>
                                <h class="bell-heading">Third-party functionality</h3>
                                <p class="privacy-content">We reserve the right, at our sole discretion, to modify or replace these Terms and Conditions at any time. If we do make changes, we will post an updated version of them on this page with an indication of their last updated date.</p> <br/><br/><br/>
                            </li> -->
                    <li>
                        <h3 class="bell-heading">Disclaimer/Limitation of liability</h3>
                        <p class="privacy-content">To the fullest extent permitted by law, under no circumstance
                            whatsoever shall Hemachandra be liable for any losses arising out of or in connection with
                            the use of the website unless caused by Hemachandra’s gross negligence or wilful misconduct.
                        </p>

                        <p class="privacy-content">Other than as required by applicable consumer protection law, in no
                            event will Hemachandra be liable for errors or omissions in the information contained on the
                            website or for any losses or damages, direct, indirect, incidental, special or
                            consequential, resulting from your use or misuse of, or reliance on, any such information.
                        </p>

                        <p class="privacy-content">The website, including, without limitation, all services, content,
                            functions and materials, is provided “as is”, “as available”, without warranty of any kind,
                            either express or implied, including, without limitation, any warranty for information,
                            data, data processing services, uptime or uninterrupted access, any warranties concerning
                            the availability, accuracy, usefulness, or content of information, and any warranties of
                            title, offer to sell, availability, non-infringement, merchantability or fitness of products
                            or information for a particular purpose, and we hereby disclaim any and all such warranties,
                            express and implied.</p>

                        <p class="privacy-content">We do not warrant that the website or the services, content,
                            functions or materials contained therein will be timely, secure, uninterrupted or error
                            free, or that defects will be corrected. We make no warranty that the website will meet
                            user’s requirements.</p>

                        <p class="privacy-content">Hemachandra also assumes no responsibility, and shall not be liable
                            for, any damages to, or viruses, worms, trojan horses or other harmful material that may
                            infect your computer equipment or other property on account of your access to, use of, or
                            browsing in the website or your downloading of any materials, data, text, images, video, or
                            audio from the website. If you are dissatisfied with the website, your sole remedy is to
                            discontinue using the website.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<section>

</section>

<section id="seek-help">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="contant-service-text">Customisation Service</h2>
                <div class="row">
                    <div class="col align-self-center">
                        <div class="service-sentnce customisation-text">
                            <p>For a truly unique gift, any of our creations can be customised using the precious metal
                                and stone of your choice. With the personalised assistance of our in-house team, ou can
                                also create your own bespoke design.</p>
                        </div>
                    </div>

                </div>
                <div class="phone-number">
                    <!-- <h1 class="inq-tel">For Inquiries</h1> -->
                    <div class="custom-inquire-area"><a class="custom-inquire-text" href="./contact">Click here for
                            inquiries</a></div>
                    <!-- <a class="inq-tel-num" href="tel:+94812387387"><p class="inq-tel-num">+94 81 238 73 87</p></a> -->
                    <div class="custom-inquire-area"><a class="inquire-email"
                            href="mailto:mail@hemachandras.com">mail@hemachandras.com</a></div>
                </div>

            </div>

        </div>

    </div>

</section>

<!-- footer strart -->

<?php $this->load->view('components/footer'); ?>