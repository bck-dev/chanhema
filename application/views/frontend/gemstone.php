<?php $this->load->view('components/headerNew'); ?>
<?php $this->load->view('components/menu'); ?>

<section id="service-img">
    <div class="policy"><span class="policy-text">We Just Update our <a href="#" style="color:#ff6b00; ">Return
                Policy</a></span>
        <span class="policy-close">x</span>
    </div>
    <!-- <h1 class="text-capitalize">Pledge <span class="mid-ft"> of </span>Service</h1> -->
</section>

<!-- <section id="blue-box">
        <div class="container">
            <div class="row  ">
                <div class="col-md-12">
                    <div class="box-content">
                        <p>
                            Our commitment is also expressed by the care with which we select the most talented specialists. As a family-owned enterprise, we strive to make them full-fledged members of our company as regards its values, it heritage, and of course its future.
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </section> -->


<!-- title section -->



<section id="feed-gem">
    <div class="container mt-5">
        <div class="row feed-gem-wrap mt-5">
            <div class="col-md-6 col-sm-12 text-center">
                <img src="<?php echo base_url('assets/frontend/'); ?>img/GemstonePassion.png"
                    style="width: 80% !important; margin: -80px auto;" alt="">
            </div>
            <div class="col-md-6">

                <h3>THE PASSION</h3>
                <p class="gemstone-passion-content">
                    Creating fine jewellery of exceptional quality using precious Sri Lankan gems and expert
                    craftsmanship is intrinsic to our family business.
                </p>
                <h5><a class="our-story-button f400" href="<?php echo base_url('story') ?>">Our Story</a></h5>
            </div>


        </div>
    </div>
</section>


<!-- second section of collection page-->

<div class="gemstone-product-slider-new">
    <div class="gemstone-slider">
        <?php foreach ($stones as $data) : ?>
        <div class="col gemstone-single">
            <?php foreach ($stones_images as $key) : ?>
            <?php if ($key['stones_stones_id'] == $data['stones_id']) : ?>
            <div class="gemstone-product-image">
                <img src="<?php echo base_url(); ?><?php echo $key['stones_images_name']; ?>"
                    class="collection-product-images" alt="product image 1" />
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
            <div class="product-info">
                <h3 class="gemstone-title"><?php echo $data['stones_name']; ?></h3>
                <p class="product-subheading"><?php echo $data['stones_status']; ?></p>
                <p class="product-paragraph-gemstones-page"><?php echo $data['stones_description']; ?> </p>

            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<!--Product slider second section-->

<section id="feed-gem">
    <div class="container mt-lg-5">

        <div class="row feed-gem-wrap">
            <div class="col-md-6 pt-lg-5 gemstone-journey-textarea">
                <h3 class="blu mt-5">THE JOURNEY</h3>
                <p class="tiffy gemstone-journey-content">
                    Our experienced gemmologists select cut stones of the highest lustre and purity, many of which come
                    from the mines of Sri Lanka’s plentiful gem deposits.
                </p>


            </div>

            <div class="col-md-6">
                <img class="neck" src="<?php echo base_url('assets/frontend/'); ?>img/Gemstone_The_Journey.jpg"
                    class="photo-cust" alt="">
            </div>
        </div>

        <div class="row feed-gem-wrap gem-feed-2">
            <div class="col-md-6">
                <img class="gemstone-design-img"
                    src="<?php echo base_url('assets/frontend/gemstone/'); ?>img/gemstone-thedesign-n.jpg"
                    class="photo-cust" alt="">
            </div>
            <div class="col-md-6">
                <h3 class="bluu">THE DESIGN</h3>
                <p class="tiff gemstone-design-content">
                    Inspired by Sri Lanka’s rich heritage while maintaining the signature Hemachandra style of
                    exquisitely detailed handcrafted pieces, our talented in-house team create exclusive designs.
                </p>


            </div>


        </div>

        <div class="row feed-gem-wrap gem-feed-3">
            <div class="col-md-6 gemstone-journey-textarea">
                <h3 class="blu">THE CREATION</h3>
                <p class="tiffy gemstone-craft-content">
                    In the expert hands of some of the country’s finest craftsmen, the stones are set in silver or gold,
                    and metamorphosed into timelessly sophisticated pieces.
                </p>
            </div>

            <div class="col-md-6">
                <img class="neck" src="<?php echo base_url('assets/frontend/'); ?>img/Gemstone_TheCreation.jpg"
                    class="photo-cust" alt="">
            </div>
        </div>

    </div>


</section>

<div class="footer-pattern"></div>
<footer id="footer-secs" class="p-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img class="" src="<?php echo base_url('assets/frontend/gemstone/'); ?>img/footerlogo.png" alt="">
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="foot-link">
                    <ul class="footer-quick-links">
                        <li> <a href="<?php echo base_url('contact'); ?>">Customer Care</a> </li>
                        <li> <a href="<?php echo base_url('shipping'); ?>">Shipping</a> </li>
                        <li> <a href="<?php echo base_url('faq'); ?>"> FAQ</a> </li>
                        <li> <a href="<?php echo base_url('contact'); ?>"> Contact</a> </li>
                        <li> <a href="<?php echo base_url('store'); ?>">Stores</a> </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row pt-5">
            <div class="wrap-icon-menu">
                <ul class="p-0">
                    <li><i class="fab fa-facebook-f"></i></li>
                    <li><i class="fab fa-youtube"></i></li>
                    <li><i class="fab fa-instagram"></i></li>
                </ul>
            </div>
        </div>
    </div>

</footer>

<section id="bottom-copyright" class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-3 col-sm-12">
                <span class="date-hema">&COPY; <?php echo date('Y'); ?> Hemachandra ‐ kandy</span>
            </div>
            <div class="col-md-6 col-lg-6 m-0 p-0">
                <ul>
                    <li> <a href="<?php echo base_url('termsandcondition'); ?>">Terms & Condition</a> </li>
                    <li> <a href="<?php echo base_url('legalnotice'); ?>">Legal Notice</a> </li>
                    <li> <a href="<?php echo base_url('privacy'); ?>">Privacy Policy</a> </li>
                    <li> <a href="<?php echo base_url('sitemap'); ?>">Site Map</a> </li>
                </ul>
            </div>
            <div class="col-md-2 col-lg-3 bottom-foot-text footer-agency">
                <div class="bc-contect">
                    <a href="http://bckonnect.com">BCKonnect</a> <img
                        src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/bcklogo.png" alt="">
                </div>

            </div>
        </div>
    </div>
</section>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>

<!-- jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- script file linked here-->
<!-- Jquery Migration start-->
<script src="<?php echo base_url('assets/frontend/gemstone/'); ?>resources/js/browser.js"></script>

<!-- <script src="<?php echo base_url('assets/frontend/gemstone/'); ?>resources/js/script.js"></script> -->
<script src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/js/owl.carousel.js"></script>
<!-- <script src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/js/owl.carousel.min.js"></script> -->
<!-- <script src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/js/owl.js"></script>  -->
<!-- click product slider-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<!-- flip product grid slide js-->
<!-- <script type="text/javascript" src="<?php echo base_url('assets/frontend/gemstone/'); ?>vendors/js/flickity_product.min.js"></script> -->

<!-- slick CDN javascript-->
<script src="<?php echo base_url('assets/frontend/'); ?>menuFixed/menu.js"></script>

<script>
$(document).ready(function() {
    $('.gemstone-slider').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
});
</script>
</body>