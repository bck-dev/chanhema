<div class="footer-pattern"></div>

<footer id="footer-secs" class="p-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img class="" src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/footerlogo.png"
                    alt="">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="foot-link">
                    <ul class="footer-quick-links m-0 p-0">
                        <li class=""> <a href="<?php echo base_url('contact'); ?>">Customer Care</a> </li>
                        <li class=""> <a href="<?php echo base_url('shipping'); ?>">Shipping</a> </li>
                        <li class=""> <a href="<?php echo base_url('faq'); ?>">FAQ</a> </li>
                        <li class=""> <a href="<?php echo base_url('contact'); ?>">Contact</a> </li>
                        <li class=""> <a href="<?php echo base_url('store'); ?>">Stores</a> </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row pt-5">
            <div class="wrap-icon-menu">
                <ul class="p-0">
                    <li><i class="fab fa-facebook-f"></i></li>
                    <li><i class="fab fa-youtube"></i></li>
                    <li><i class="fab fa-instagram"></i></li>
                </ul>
            </div>
        </div>
    </div>

</footer>

<section id="bottom-copyright" class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-3 col-sm-12">
                <span class="date-hema">&COPY; <?php echo date('Y'); ?> Hemachandra ‐ kandy</span>
            </div>
            <div class="col-md-6 col-lg-6 m-0 p-0">
                <ul class="m-0 p-0">
                    <li> <a href="<?php echo base_url('termsandcondition'); ?>">Terms & Condition</a> </li>
                    <li> <a href="<?php echo base_url('legalnotice'); ?>">Legal Notice</a> </li>
                    <li> <a href="<?php echo base_url('privacy'); ?>">Privacy Policy</a> </li>
                    <li> <a href="<?php echo base_url('sitemap'); ?>">Site Map</a> </li>
                </ul>
            </div>
            <div class="col-md-2 col-lg-3 bottom-foot-text footer-agency">
                <div class="bc-contect">
                    <a href="http://bckonnect.com">BCKonnect</a> <img
                        src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/bcklogo.png" alt="">
                </div>

            </div>
        </div>
    </div>
</section>

<!-- swiper -->
<script src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/js/owl.carousel.js"></script>
<!-- <script src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/js/owl.carousel.min.js"></script> -->
<script src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/js/owl.js"></script>
<script src="<?php echo base_url('assets/frontend/'); ?>menuFixed/menu.js"></script>
<script src="<?php echo base_url('assets/frontend/sust-inner-assest/js/glider.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/frontend/sust-inner-assest/js/jquery.fancybox.min.js');?>"></script>
<script>
if (document.querySelector('.glider') !== null) {
    new Glider(document.querySelector('.glider'), {
        slidesToScroll: 1,
        slidesToShow: 4,
        draggable: true,
        dots: '.dots',
        arrows: {
            prev: '.glider-prev',
            next: '.glider-next'
        }
    });

    $('[data-fancybox="preview"]').fancybox({
        thumbs: {
            autoStart: true
        }
    });
}



//  contact us page form submission validation control
window.addEventListener('load', function() {
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            } else {
                $('#contactUsSubmittedModal').modal('show');
            }
            form.classList.add('was-validated');
        }, false);
    });
}, false);
</script>


</body>

</html>