<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Hemchandra | Home </title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/')?>vendors/css/grid.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/')?>vendors/css/ionicons.min.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/')?>vendors/css/normalize.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/')?>vendors/css/animate.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/')?>resources/css/stylesheet.css"/>
        <!-- open sans font used -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i" rel="stylesheet">
         <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/assets/owl.carousel.min.css">
        <!-- hover effect style sheet-->

        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/')?>vendors/css/hovereffects/set1.css">
        <!-- click product stylesheet-->
        <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <!-- Hemachandra Responsive designing -->
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/')?>resources/css/queries.css"/>
        <!-- Product slide effect stylehseet-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/')?>vendors/css/flickityproductslide.css"/>
        <!---- Slick cdn slider-->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

        
        <!-- favicons for browsers -->
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('assets/frontend/')?>resources/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/frontend/')?>resources/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/frontend/')?>resources/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo base_url('assets/frontend/')?>resources/img/favicon/site.webmanifest">
        <link rel="mask-icon" href="<?php echo base_url('assets/frontend/')?>resources/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="<?php echo base_url('assets/frontend/')?>resources/img/favicon/favicon.ico">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-config" content="<?php echo base_url('assets/frontend/')?>resources/img/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        
        
           <!--Google font playfair -->
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
        <!--belfair google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Bellefair" rel="stylesheet">
        <!--maven google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Maven+Pro" rel="stylesheet">
        
        <!-- Font Awesome-->
        <link href="<?php echo base_url('assets/frontend/')?>vendors/css/fontawesome.css" rel="stylesheet">
        
        <!-- collection page css-->
        <link href="<?php echo base_url('assets/frontend/')?>resources/css/collection.css" rel="stylesheet">
        
        <!-- Inquiries page css-->
        <link href="<?php echo base_url('assets/frontend/')?>resources/css/inquiries.css" rel="stylesheet">
        
         <!--Owl carousel -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
        
         <!---- Slick cdn slider-->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css"/>    
        
        <!-- gemstone css file-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/')?>resources/css/gemstone.css"/>
        
      
   
    
    </head>
    <body>

        <header>
            <!--naviation Menu -->
            <nav class="windows-mode-nav main-navigator-menu" >
                <div class="navirow">
                    <div class="container">
                        
                        <div class="navigation-top">
                            
                        </div>
                    
                        <ul class="main-nav" id="main-navigation">
                            
                        
                        
                        <ul class="rightsitemenu-top main-nav nav" id="navigationmobile">
                            <li class="list-right">
                                   <a href="#" class="toprightul">SHIPPING</a>
                            </li>
                             <li class="list-right">
                               <a href="#" class="toprightul">CONTACT</a>
                            </li>
                             <li class="list-right">
                               <a href="#" class="toprightul">MEDIA</a>
                            </li>
                        </ul>
                      
                            
                            
                            <!--top naviagtion right corner search bar-->
                            <ul class="language-list" >
                                <li><i class="ion-ios-arrow-right"></i> 
                                    <select class="langselectbar" name="droplangsel">
                                        <option>EN</option>
                                        <option>SL</option>
                                    </select>
                                        <input type="text" name="text" class="search"/>
                                      <a class="js-searchbar"><i class="ion-ios-search-strong icon-small"></i></a>
                                      
                                </li>
                            </ul>
                            <!-- top navigation right bar corner search bar ende-->
                            
                            <!-- naviagtion main menu verticle middle -->
                            <li class="left"><a href="#">Story </a></li>
                            <li class="left"><a href="<?php echo base_url('home/gemstone')?>">Gemstones</a></li>
                            <li class="left drop-down"><a href="<?php echo base_url('home/collection')?>">Collection</a>
                            
                                <div class="navigation-submenu-row" style="float: left;" >
                                    <div>
                                        <div class="col First-sub-menu">
                                            <div class="collection-section">
                                                <h3 class="navigation-sub-menu-title">COLLECTION</h3>
                                                <ul class="hemachndra-sub-menu">
                                                    <li><a href="#">Love & Life</a></li>
                                                    <li><a href="#">Ceylon doll</a></li>
                                                    <li><a href="#">Heritage</a></li>
                                                </ul>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col second-sub-row" style="display: block; float: left;" >
                                            <div class="fine-jewelery-section">
                                                
                                                <div class="col span-1-of-2">
                                                    <div class="" style="margin-left: -22px;">
                                                        <h3 class="navigation-sub-menu-jewelery">Fine jewelery</h3>
                                                        <div class="list-of-view-menu">
                                                            <ul class="hemachandra-second-sub-menu">
                                                                <li><a href="#">Adamantine</a></li>
                                                                <li><a href="#">Celestial Art</a></li>
                                                                <li><a href="#">Contemporary</a></li>
                                                                <li><a href="#">Moon Stone</a></li>
                                                                <li><a href="#">Island Life</a></li>
                                                                <li><a href="#">View All</a></li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col span-1-of-2">
                                                     <div class="" style="margin-left: -22px;">
                                                        <h3 class="navigation-sub-menu-jewelery">Fine jewelery</h3>
                                                        <div class="list-of-view-menu">
                                                            <ul class="hemachandra-second-sub-menu">
                                                                <li><a href="#">Adamantine</a></li>
                                                                <li><a href="#">Celestial Art</a></li>
                                                                <li><a href="#">Contemporary</a></li>
                                                                <li><a href="#">Moon Stone</a></li>
                                                                <li><a href="#">Island Life</a></li>
                                                                <li><a href="#">View All</a></li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                </div>
                                                

                                            </div>
                                            
                                        </div>

                                    </div>
                                    
                                </div>
                            
                            
                            
                            </li>
                            <li>
                                <a href="<?php echo base_url('home')?>">
                                <div class="logocen">
                                    <img src="<?php echo base_url('assets/frontend/')?>resources/img/hemalogopng.png" class="logo" alt="hemachandra logo"/>
                                </div>
                                 </a>
                            </li>
                            <li class="right first-gurante-child"><a href="<?php echo base_url('home/gurantee')?>">Gurantee</a></li>
                            <li class="right"><a href="<?php echo base_url('home/services')?>">Services</a></li>
                            <li class="right"><a href="<?php echo base_url('home/store')?>">Stores</a></li>
                        </ul>
                    
                    </div>
                
                </div>
            </nav>
            <!--naviation Menu ended -->
            
            <!- ================================================================================->
            <!-- ===============sticky navigation menu start here===============================-->
            <!- ================================================================================->
            <nav id="sticky-navi"  class="windows-mode-nav naviagtion-background noti" >
                <div class="stickynavirow">
                    <div class="stickycontainer">
                        
                        <div class="navigation-top">
                            
                        </div>
                    
                        <ul class="main-nav sticky-main-nav" id="main-navigation">

                            
                            <!-- naviagtion main menu verticle middle -->
                            <li class="left"><a href="#">Story </a></li>
                            <li class="left"><a href="<?php echo base_url('home/gemstone')?>">Gemstones</a></li>
                            <li class="left drop-down"><a href="<?php echo base_url('home/collection')?>">Collection</a>
                            
                                <div class="navigation-submenu-row" style="float: left;" >
                                    <div>
                                        <div class="col First-sub-menu">
                                            <div class="collection-section">
                                                <h3 class="navigation-sub-menu-title">COLLECTION</h3>
                                                <ul class="hemachndra-sub-menu">
                                                    <li><a href="#">Love & Life</a></li>
                                                    <li><a href="#">Ceylon doll</a></li>
                                                    <li><a href="#">Heritage</a></li>
                                                </ul>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col second-sub-row" style="display: block; float: left;" >
                                            <div class="fine-jewelery-section">
                                                
                                                <div class="col span-1-of-2">
                                                    <div class="" style="margin-left: -22px;">
                                                        <h3 class="navigation-sub-menu-jewelery">Fine jewelery</h3>
                                                        <div class="list-of-view-menu">
                                                            <ul class="hemachandra-second-sub-menu">
                                                                <li><a href="#">Adamantine</a></li>
                                                                <li><a href="#">Celestial Art</a></li>
                                                                <li><a href="#">Contemporary</a></li>
                                                                <li><a href="#">Moon Stone</a></li>
                                                                <li><a href="#">Island Life</a></li>
                                                                <li><a href="#">View All</a></li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col span-1-of-2">
                                                     <div class="" style="margin-left: -22px;">
                                                        <h3 class="navigation-sub-menu-jewelery">Fine jewelery</h3>
                                                        <div class="list-of-view-menu">
                                                            <ul class="hemachandra-second-sub-menu">
                                                                <li><a href="#">Adamantine</a></li>
                                                                <li><a href="#">Celestial Art</a></li>
                                                                <li><a href="#">Contemporary</a></li>
                                                                <li><a href="#">Moon Stone</a></li>
                                                                <li><a href="#">Island Life</a></li>
                                                                <li><a href="#">View All</a></li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                </div>
                                                

                                            </div>
                                            
                                        </div>

                                    </div>
                                    
                                </div>
                            
                            
                            
                            </li>
                            <li>
                                <div class="stickylogocen">
                                    <img src="<?php echo base_url('assets/frontend/')?>resources/img/stick_navigation_bar.png" class="sticky_navi_logo" alt="hemachandra logo"/>
                                </div>
                            </li>
                            <li class="right first-gurante-child"><a href="<?php echo base_url('home/gurantee')?>">Gurantee</a></li>
                            <li class="right"><a href="<?php echo base_url('home/services')?>">Services</a></li>
                            <li class="right"><a href="<?php echo base_url('home/store')?>">Stores</a></li>
                        </ul>
                    
                    </div>
                
                </div>
            </nav>
            <!- ================================================================================->
            <!-- ==================sticku navigation menu end here==============================-->
            <!- ================================================================================->
            
            
            <!-- mobile View navigation menu -->
            <nav id="global-nav" class="nav scrolled-nav fixed" role="full-horizontal">
                 <input type="checkbox" id="button" style="opacity:0">
                
               
                <label class="onclicknav " for="button" onclick> Menu <i class="ion-chevron-down"></i></label>
                    
                
                <!-- display on mobile view logo -->
                <div class="navleft">
                 <img src="<?php echo base_url('assets/frontend/')?>resources/img/hemalogopng.png" class="logomob" alt="hemachandra logo"/>
                </div>
                    <ul class="main-nav navigation-unorderlist">
                        <li><i class="ion-ios-arrow-right"></i> 
                            <select class="langselectbar" name="droplangsel">
                                <option>EN</option>
                                <option>SL</option>
                            </select>
                            <a class="js-searchbar"><i class="ion-ios-search-strong icon-small"></i></a>
                            <input type="text" name="text" class="search"/>
                        </li>
                        <li class="left"><a href="#">Story </a></li>
                        <li class="left"><a href="<?php echo base_url('home/gemstone')?>">Gem Stone</a></li>
                        <li class="left mobile-navigation-menu-droplist"><a href="<?php echo base_url('home/collection')?>">">Collection</a>
                            <!---- submenu starting here-->
                            <div class="mobile-menu-navigation-sub-menu">
                                 <ul class="mobile-submenu-class">
                                    <p class="paragraph-submenu-class"> Collection</p>
                                    <li><a href="#">Love & Life</a></li>
                                    <li><a href="#">Ceylon doll</a></li>
                                    <li><a href="#">Heritage</a></li>
                                </ul>
                                
                                
                                <ul class="mobile-submenu-class">
                                    <p style="color: black; float: left;  margin-bottom: 29px; font-weight: 600;"> Fine Jewelery</p>
                                    <li><a href="#">Love & Life</a></li>
                                    <li><a href="#">Ceylon doll</a></li>
                                    <li><a href="#">Heritage</a></li>
                                </ul>
                            </div>
                            <!-- Submenu divider end here-->
                               
                          
                        </li>
                        <li class="right"><a href="<?php echo base_url('home/gurantee')?>">">Gurantee</a></li>
                        <li class="right"><a href="<?php echo base_url('home/services')?>">">Services</a></li>
                        <li class="right"><a href="<?php echo base_url('home/store')?>">">Stores</a></li>
                        <li class="right"><a href="#">Shipping</a></li>
                        <li class="right"><a href="#">Contact</a></li>
                        <li class="right"><a href="#">media</a></li>
                    
                    </ul>
            </nav>
             <!-- mobile View navigation menu -->
        </header>