  <!-- footer section start here-->
        <footer>
            <div class="row">
                <div class="footer-nav-links">
                    <div class="hemachandra-footer-logo">
                        <img src="<?php echo base_url('assets/frontendone/')?>resources/img/footerlogo/footerlogo.png" alt="footer logo"/>
                    </div>
                    <ul class="footer-nav">
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Customer Care</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Shipping</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Guarantee</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Contact</a></li>
                        <li class="hemahcnadra-custom-footer-links"><a href="#">Stores</a></li>
                    </ul>
                    <div class="sociallinks">
                        <ul class="social-link">
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-twitter"></i></a></li>
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-googleplus"></i></a></li>
                            <li class="list-of-social-icon"><a href="#"><i class="ion-social-instagram"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <section class="bottom-footer-container">
            <div class="secondrow">
                <div class="col span-1-of-3">
                    <p class="copywrite">
                       &copy; 2018 Hemachandra &dash; kandy
                    </p>
                </div>
                <div class="col span-1-of-3">
                    <ul class="bottom-footer">
                        <li><a href="#">Terms & condition</a></li>
                        <li><a href="#">Legal Notice</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>
                <div class="col span-1-of-3">
                    <div class="projectdone">
                        <a href="http://bckonnect.com/"><p class="projectby"> BcKonnect</p></a>
                        <img src="<?php echo base_url('assets/frontendone/')?>resources/img/bcklogo.png" class="companylogoimage" alt="company logo">
                    </div>
                </div>
                
            </div>
        </section>
        

        
    </div>
               
        
        <!-- jquery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <!-- script file linked here-->
        <!-- Jquery Migration start-->
        <script src="<?php echo base_url('assets/frontendone/')?>resources/js/browser.js"></script>
        <!-- Jquery Migration end-->
        <script src="<?php echo base_url('assets/frontendone/')?>resources/js/script.js"></script>
        <script src="http://bckonnect.com/medex/assets/frontend/js/owl.carousel.min.js"></script>
          <!-- jquery waypoint reference link http://imakewebthings.com/waypoints/-->
        <script src="<?php echo base_url('assets/frontendone/')?>vendors/js/jquery.waypoints.min.js"></script>
            
        <!-- click product slider-->
        <script type="text/javascript" src="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <!-- flip product grid slide js-->
        <script type="text/javascript" src="<?php echo base_url('assets/frontendone/')?>vendors/js/flickity_product.min.js"></script>
        <!-- Steller paralax -->
        <script type="text/javascript" src="<?php echo base_url('assets/frontendone/')?>vendors/js/steller.js"></script>
        <!-- jquery waypoint reference link http://imakewebthings.com/waypoints/-->
        <script src="<?php echo base_url('assets/frontendone/')?>vendors/js/jquery.waypoints.min.js"></script>
        <!-- slick CDN javascript-->
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
         
        
        
    </body>
</html>