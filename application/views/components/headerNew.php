<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">

        <!-- menu fix is in service css -->
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/css/service.css">
        <?php if($pageName=="History"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tironhistryassets/css/history.css">
        <?php endif; ?>
        
        <?php if($pageName=="Sustainability"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>css/gurantee.css">
        <?php endif; ?>
        
        <?php if($pageName=="People"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>resources/css/peoples.css">
        <?php endif; ?>
        
        <?php if($pageName=="Contact Us" || $pageName=="Store"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/css/contact.css">
        <?php endif; ?>
        <?php if($pageName=="News"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tiornNews/css/news.css">
        <?php endif; ?>
        <?php if($pageName=="Gemstone"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/gemstone/'); ?>css/gem.css">    
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>headercss/header.css">
            <link href="<?php echo base_url('assets/frontend/gemstone/'); ?>resources/css/collection.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/gemstone/'); ?>resources/css/gemstone.css"/>
        <?php endif; ?>
        <?php if($pageName=="Collection"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tironCollectionassests/css/collection.css">
        <?php endif; ?>
        <?php if($pageName=="Guarantee"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>css/gurantee.css">
        <?php endif; ?>
        <?php if($pageName=="Store"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tironstoreassets/css/service.css">
        <?php endif; ?>
        <?php if($pageName=="Shipping"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tironShipin/css/shipin.css">
        <?php endif; ?>
        <?php if($pageName=="Contact Us"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tironContact/css/contact.css">
        <?php endif; ?>
        <?php if($pageName=="News Detail" || $pageName=="Guarantee Detail"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>whatstiron/css/contact.css">
        <?php endif; ?>
        <?php if($pageName=="Terms & Conditions" || $pageName=="Privacy" || $pageName=="Legal Notice"): ?>
            <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>Lahirutermsassests/css/contact.css">
        <?php endif; ?>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/frontend/'); ?>tironCollectionassests/img/favicon-32x32.png">
        <link rel="icon" type="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/image/png" sizes="16x16" href="tironserviceassets/img/favicon-16x16.png">
        <link rel="manifest" href="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/favicon/site.webmanifest">
        <link rel="mask-icon" href="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/favicon/favicon.ico">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-config" content="<?php echo base_url('assets/frontend/'); ?>resources/img/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>sust-inner-assest/css/glider.min.css">
        
        <!-- swiper  -->
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/css/owl.theme.default.css">

        <!-- click product stylesheet-->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css"/>
        
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/sust-inner-assest/css/jquery.fancybox.min.css'); ?>">
        
        <!-- gemstone css file-->
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>css/new-menu.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>css/custom.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>css/alignments.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>css/responsive.css">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.4.0.min.js" integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="assets/frontend/resources/js/history-slider.js"></script>
        <title>Hemachandra | <?php echo $pageName; ?></title>

        <script>var base_url = '<?php echo base_url() ?>';</script>
    </head>
    <body>