<header class="new-nav display-ip11">
    <div class="new-main-menu">
        <div class="row m-0">
            <div id="top-head" class="new-top-bar">
                <ul class="language-list">
                    <li>
                        <i class="fas fa-chevron-right"></i>
                        <select class="langselectbar" name="droplangsel">

                            <option>EN</option>
                            <option>SL</option>
                        </select>
                        <!-- <input type="text" name="text" id="text-serch" class="search">
                            <img src="<?php echo base_url('assets/frontend/'); ?>tironserviceassets/img/iconfinder_basics-19_296814.png" class="search-ico" alt=""> -->
                    </li>
                </ul>
                <ul class="shiping-nav shipping-nav-new">
                    <li><a href="<?php echo base_url('news'); ?>">Media </a></li>
                    <li><a href="<?php echo base_url('contact'); ?>">Contact</a> </li>
                    <li><a href="<?php echo base_url('shipping'); ?>"> Shipping</a> </li>
                </ul>
            </div>
        </div>


        <div class="row m-0">
            <div id="second-nav" class="navcol new-second-menu">

                <ul class="m-auto">
                    <li id="aboutMenu" class="about panel-about act-line <?php if ($parent == "about") {
                                                                                echo "active";
                                                                            } ?>"><a href="#">About&nbsp;us </a></li>
                    <li class="<?php if ($parent == "gemstone") {
                                    echo "active";
                                } ?>"><a href="<?php echo base_url('gemstone'); ?>">Gemstones </a></li>
                    <li class="dow-panel-sec act-line <?php if ($parent == "collection") {
                                                            echo "active";
                                                        } ?>"><a href="#">Collection </a></li>
                    <!-- third one section -->
                    <li class="new-logo-li">
                        <a href="<?php echo base_url(); ?>"> <img
                                src="<?php echo base_url('assets/frontend/'); ?>img/logo.svg" class="new-logo"
                                alt=""></a>
                        <a href="<?php echo base_url(); ?>"> <img
                                src="<?php echo base_url('assets/frontend/'); ?>img/logo-small.svg"
                                class="new-small-logo" alt=""></a>
                    </li>
                    <li class="<?php if ($parent == "guarantee") {
                                    echo "active";
                                } ?>"><a href="<?php echo base_url('guarantee'); ?>">Guarantee </a></li>
                    <li class="<?php if ($parent == "service") {
                                    echo "active";
                                } ?>"><a href="<?php echo base_url('service'); ?>" class="text-active">Services</a>
                    </li>
                    <li class="<?php if ($parent == "store") {
                                    echo "active";
                                } ?>"><a href="<?php echo base_url('store'); ?>">Stores</a></li>
                </ul>
            </div>
        </div>
    </div>

    <section id="drop-one" style="display: none;">
        <div class="container wrap-dorop clt-height drop-d">
            <div class="row clt-height">
                <div class="col-md-3 col-lg-3 offset-md-1 offset-lg-1">
                    <h4>Collections</h4>
                    <ul class="collections_submenu">
                        <?php foreach ($collections as $collection) : ?>
                        <li class="<?php echo $collection['collection_name'] == $active_collection ? 'active' : '' ?>">
                            <a
                                href="<?php echo base_url(); ?>collections/<?php echo $collection['collection_name']; ?>"><?php echo $collection['collection_name']; ?></a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-8 col-lg-8 clt-image clt-height">
                    <h4>Jewelry</h4>
                    <ul class="collections_submenu">
                        <?php foreach ($categories as $category) : ?>
                        <li class="<?php echo ($category['category_name'] == $active_category) ? 'active' : '' ?>"><a
                                href="<?php echo base_url(); ?>products/<?php echo $category['category_name']; ?>"><?php echo $category['category_name']; ?></a>
                        </li>
                        <?php endforeach; ?>
                        <li class="vall-btn"><a href="<?php echo base_url(); ?>products/All">View All</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="drop-two" style="display: none;">
        <div class="container wrap-dorop">
            <div class="row">
                <div class="col-md-2 col-lg-2 offset-md-1 offset-lg-1">
                    <h4>Company</h4>

                    <ul>
                        <li><a href="<?php echo base_url('story'); ?>">Story</a></li>
                        <li><a href="<?php echo base_url('sustainability'); ?>">Sustainability</a></li>
                        <li><a href="<?php echo base_url('people'); ?>">People</a></li>
                        <li><a href="<?php echo base_url('news'); ?>">News</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-lg-4 clt-image">

                    <a href="<?php echo base_url('story'); ?>"><img
                            src="<?php echo base_url('assets/frontend/'); ?>img/Story_DropDown.jpg"
                            class="auth-image"></a>
                    <a href="<?php echo base_url('people'); ?>"><img
                            src="<?php echo base_url('assets/frontend/'); ?>img/people.png" class="auth-image2"></a>
                </div>
                <div class="col-md-3 col-lg-4 clt-image">
                    <h4>reach us</h4>
                    <p class="colomb" style="text-align:left;">CRAFT HOUSE - Kandy, Sri Lanka</p>
                    <p class="flower-rd pt-0" style="text-align:left;">
                        Queens Hotel, Kandy<br>
                        +94 81 238 73 87
                    </p>
                    <p class="panel-time" style="text-align:left">9AM–5:30PM</p>

                    <div class="wrap-icon-menu">
                        <ul>
                            <li class="ml-0 mr-3"><i class="fab fa-facebook-f"></i></li>
                            <li class="ml-0 mr-3"><i class="fab fa-youtube"></i></li>
                            <li class="ml-0 mr-3"><i class="fab fa-instagram"></i></li>
                        </ul>
                    </div>

                </div>
                <div class="col-md-2 col-lg-2"></div>
            </div>
        </div>
    </section>

</header>

<section class="mobile-sec-new">
    <div class="container">
        <div class="row">
            <div class="mobile-nav-sec">
                <div class="img-tag">
                    <a href="<?php echo base_url(); ?>"><img
                            src="<?php echo base_url('assets/frontend/'); ?>img/logo.svg" alt=""></a>
                    <h4 id="open-menu">Menu <i class="fas fa-chevron-down"></i></h4>


                </div>

            </div>

        </div>
        <nav id="overlay">
            <ul class="d-block d-lg-none">
                <li class="right" id="aboutExpand">
                    <button class="btn mobile-menu-dropdown dropdown-toggle">
                        About
                    </button>
                </li>
                <div class="w-100 mobile-dropdown-new d-none aboutMenuMobile pt-1 pb-3">
                    <a href="<?php echo base_url('story'); ?>">
                        <div class="pt-2 pb-1">Story</div>
                    </a>
                    <a href="<?php echo base_url('sustainability'); ?>">
                        <div class="py-1">Sustainability</div>
                    </a>
                    <a href="<?php echo base_url('people'); ?>">
                        <div class="py-1">People</div>
                    </a>
                    <a href="<?php echo base_url('news'); ?>">
                        <div class="pt-2 pb-2">News</div>
                    </a>
                </div>

                <li class="right"><a href="<?php echo base_url('gemstone'); ?>">Gemstones</a></li>

                <li class="right" id="collectionExpand">
                    <button class="btn mobile-menu-dropdown dropdown-toggle">
                        Collections
                    </button>
                </li>
                <div class="w-100 mobile-dropdown-new d-none collectionMenu pt-1 pb-3">
                    <div class="mt-3">
                        <b>Collections</b>
                    </div>
                    <!-- collections -->
                    <?php foreach ($collections as $collection) : ?>
                    <a class="<?php echo $collection['collection_name'] == $active_collection ? 'active' : '' ?>"
                        href="<?php echo base_url('collections/' . $collection['collection_name']); ?>">
                        <div class="py-1"><?php echo $collection['collection_name']; ?> Collections</div>
                    </a>
                    <?php endforeach; ?>

                    <div class="mt-2">
                        <b>Jewelry</b>
                    </div>
                    <!-- categories -->
                    <?php foreach ($categories as $category) : ?>
                    <a class="<?php echo ($category['category_name'] == $active_category) ? 'active' : '' ?>"
                        href="<?php echo base_url('products/' . $category['category_name']); ?>">
                        <div class="py-1"><?php echo $category['category_name']; ?></div>
                    </a>
                    <?php endforeach; ?>

                    <a class="mobile-view-all" href="<?php echo base_url('products/All'); ?>">
                        <div>View All</div>
                    </a>

                </div>

                <li class="right first-gurante-child"><a href="<?php echo base_url('guarantee'); ?>">Gurantee</a></li>
                <li class="right "><a href="<?php echo base_url('service'); ?>" class="text-active">Services</a></li>
                <li class="right"><a href="<?php echo base_url('store'); ?>">Stores</a></li>
                <li class="right"><a href="<?php echo base_url('shipping'); ?>">Shipping</a></li>
                <li class="right"><a href="<?php echo base_url('contact'); ?>">Contact</a></li>
                <li class="right"><a href="<?php echo base_url('news'); ?>">Media</a></li>
            </ul>
        </nav>


    </div>

</section>