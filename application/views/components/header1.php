<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title></title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="robots" content="index, follow">

  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend/');?>img/favicon.ico">
  <link rel="stylesheet" href="<?php echo base_url('assets/frontend/'); ?>css/main.css">
  <link rel="canonical" href="http://localhost:4000/">
  <link rel="alternate" type="application/rss+xml" title="Your awesome title" href="http://localhost:4000/feed.xml">
</head>
<body>
<nav>
	<div class="top-bar">
		<div class="container">
			<ul>
				<li><a href="<?php echo base_url('home/about'); ?>">about</a></li>
				<li><a href="<?php echo base_url('home/corporate'); ?>">corporate</a></li>
				<li><a href="<?php echo base_url('home/news'); ?>">news</a></li>
				<li><a href="<?php echo base_url('home/partners'); ?>">partners & suppliers</a></li>
				<li><a href="<?php echo base_url('home/career'); ?>">careers</a></li>
				<li class="social-icon facebook"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li class="social-icon linkedin"><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				<li class="social-icon youtube"><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
			</ul>			
		</div>
	</div>

	<div class="bottom-bar">
		<div class="container">
			<div class="brand">
				<a href="<?php echo base_url('home');?>"><img src="<?php echo base_url('assets/frontend/');?>img/logo.png" alt="logo.png"></a>
			</div>
			<ul class="nav-links">
				<li>
					<a href="<?php echo base_url('home/solutions'); ?>" class="dropdown-link">solutions for hospitals <span class="angle-down"><i class="fa fa-angle-double-down" aria-hidden="true"></i></span></a>
					<ul class="dropdown-menu">
						<li><a href="">infection control</a></li>
						<li><a href="">patient care</a></li>
						<li><a href="">skin & wound management</a></li>
						<li><a href="">medical equipment</a></li>
						<li><a href="">surgical equipment</a></li>
					</ul>
				</li>
				<li>
					<a href="<?php echo base_url('home/equipments'); ?>" class="dropdown-link">equipments for clinics <span class="angle-down"><i class="fa fa-angle-double-down" aria-hidden="true"></i></span></a>
					<ul class="dropdown-menu">
						<li><a href="">clinical assessment</a></li>
						<li><a href="">disinfectants</a></li>
						<li><a href="">disposables & single patient use</a></li>
						<li><a href="">detection devices</a></li>
					</ul>
				</li>
				<li>
					<a href="<?php echo base_url('home/products'); ?>" class="dropdown-link">products for home <span class="angle-down"><i class="fa fa-angle-double-down" aria-hidden="true"></i></span></a>
					<ul class="dropdown-menu">
						<li><a href="">blood pressure monitors</a></li>
						<li><a href="">glucose meters</a></li>
						<li><a href="">laser hair removers</a></li>
						<li><a href="">joint support</a></li>
					</ul>
				</li>												
			</ul>

			<div class="mobile-toggle-button">
				<span></span>
			</div>

		</div>
	</div>
</nav>