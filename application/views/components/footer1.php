<footer>
	<div class="container">
		<div class="top-footer">
			<div class="left-side-section">
				<ul>
					<li><a href="">company</a></li>
					<li><a href="<?php echo base_url('home/about'); ?>">about</a></li>
					<li><a href="<?php echo base_url('home/news'); ?>">media</a></li>
					<li><a href="<?php echo base_url('home/career'); ?>">careers</a></li>
					<li><a href="<?php echo base_url('home/corporate'); ?>">corporate</a></li>
				</ul>

				<ul>
					<li><a href="">business sectors</a></li>
					<li><a href="<?php echo base_url('home/solutions'); ?>">solutions for hospitals</a></li>
					<li><a href="<?php echo base_url('home/equipments'); ?>">equipments for clinics</a></li>
					<li><a href="<?php echo base_url('home/products'); ?>">products for home</a></li>
				</ul>

				<ul>
					<li><a href="">quick links</a></li>
					<li><a href="">products</a></li>
					<li><a href="">partners & suppliers</a></li>
					<li><a href="<?php echo base_url('home/contact'); ?>">contact</a></li>
					<!-- <li><a href="">facebook</a></li> -->
				</ul>

				<ul>
					<li><a href="">just blank</a></li>
					<li><a href="">7, dickmans road</a></li>
					<li><a href="">colombo 5</a></li>
					<li><a href="">srilanka</a></li>
				</ul>

				<ul>
					<li><a href="">just blank</a></li>
					<li><a href="mailto:info@medex.com" style="text-transform: initial;">info@medex.com</a></li>
					<li><a href="tel:+94112590557">+94 11 2590557</a></li>
				</ul>				
			</div>
			<div class="right-side-section">
				<div class="footer-logo">
					<img src="<?php echo base_url('assets/frontend/');?>img/footer-logo.png" alt="footer-logo.png">
				</div>				
			</div>
		</div>			
		<div class="bottom-footer">
			<p>&copy; 2017 All rights reserved. Medex Pvt Ltd</p>
		</div>
	</div>
</footer>

<div class="gallery-pop-up">
	<div class="gallery-pop-wrap">
		<div class="gallery-image">
			<div class="image-dismiss"></div>
		</div>		
	</div>
</div>

<div class="enquiry-form">
	<div class="enquiry-form-wrap">
		<form role="form" action="<?php echo base_url('home/send_email_inquery'); ?>" method="POST" enctype='multipart/form-data' class="enquiry">
			<div class="enquiry-dismiss"></div>

			<h3 class="page-title">
				enquiry form
			</h3>

			<div class="form-group">
				<label for="enquriry-name">name</label>
				<input type="text" id="enquriry_name" name="enquriry_name" required>
			</div>
			<div class="form-group">
				<label for="enquriry-email">email</label>
				<input type="email" id="enquriry_email" name="enquriry_email" required>
			</div>
			<div class="form-group">
				<label for="enquiry-phone">phone</label>
				<input type="text" id="enquiry_phone" name="enquiry_phone">
			</div>
			<div class="form-group subject-group">
				<label for="subject">subject</label>
				<!-- <p class="enquiry-subject"></p> -->
				<input type="text" name="enquiry-subject" id="enquiry_subject" class="enquiry_subject" value="" style="border: none; outline: none; pointer-events: none;">
			</div>
			<div class="form-group textarea-group">
				<label for="enquiry-message">message</label>
				<textarea name="enquiry_message" id="enquiry_message"></textarea>
			</div>
			<div class="form-group">
				<label for="" style="color: transparent;">nothing</label>
				<input type="submit" value="submit" class="cta-btn">
			</div>				
		</form>
	</div>
</div>

<!-- <div class="inquire-popup-wrap">
	<form action="" method="post">
		<div class="form-group">
			<img src="/" alt="">
		</div>
	</form>
</div> -->

<script src="<?php echo base_url('assets/frontend/'); ?>js/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url('assets/frontend/'); ?>js/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/frontend/'); ?>js/functions.js"></script>
</body>
</html>