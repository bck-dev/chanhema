      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" id="sidebar_list">

            <li class="header" id="mainnav_header">MAIN NAVIGATION</li>
            <br>

            <!-- clock widget start -->
            <div id="timewidget">
              <div class="clock">
                <a><strong><span class="cityname" id="cityName"></span></strong></a>
                <div class="align">
                  <a><span class="time zero"></span><span class="time hours"></span></a> :
                  <a><span class="time minutes"></span></a> 
                  <a><span class="time ampm"></span></a>
                  <!-- <a><span class="time seconds"></span></a>  -->
                </div>
                <a><strong><span class="day"></span></strong></a>
                <a><span class="Year month"></span></a>  
                <a><span class="Year date"></span></a> 
                <a><span class="Year year"></span></a>
              </div>
            </div>
            <!-- clock widget end -->

            <br>
            <!--wheather widget-->
            <div id="weatherWidget">
              <a class="weatherwidget-io" href="https://forecast7.com/en/6d9379d86/colombo/" data-label_1="" data-label_2="Today" data-font="Fira Sans" data-days="3" data-textcolor="#ffffff" disabled>WEATHER</a>
              <script>
                !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
              </script>
            </div>
            <!-- End weather widget -->


            <!-- Navigation Bar -->
            
            <!-- End Navigation Bar -->
            <!-- Start main banner -->
            <li class="treeview">
              <a href="#">
                <i class="far fa-image">&nbsp;&nbsp;</i><span>Main Banner</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
               <li>
                <a href="<?php echo base_url('Mainbanner/addmainbanner'); ?>">
                  <i class="far fa-circle"></i> <span>Add New</span>
                </a>
                <a href="<?php echo base_url('Mainbanner/viewmainbanner'); ?>">
                  <i class="far fa-circle"></i> <span>View All</span>
                </a>
              </li>
            </ul>
          </li> 
          <!--end main banner -->
          <!-- Start notification bar sidebar -->
            <li class="treeview">
              <a href="#">
                <i class="fas fa-edit">&nbsp;&nbsp;</i><span>Notification Bar</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
               <li>
                <a href="<?php echo base_url('Notificationbar/editnotification/1'); ?>">
                  <i class="far fa-circle"></i> <span>Edit</span>
                </a>
                <a href="<?php echo base_url('Notificationbar/viewnotification'); ?>">
                  <i class="far fa-circle"></i> <span>View</span>
                </a>
              </li>
            </ul>
          </li> 
          <!--end notification bar sidebar -->
          <!--start Services side bar-->
          <li class="treeview">
            <a href="#">
              <i class="fas fa-tags"></i>&nbsp;&nbsp;<span>Services</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <!--<li><a href="<?php echo base_url('Services/addservices');?>"><i class="far fa-circle"></i> <span>Add New</span></a></li>-->
              <li><a href="<?php echo base_url('Services/viewservices');?>"><i class="far fa-circle"></i> <span>View All</span></a></li>
            </ul>
          </li>
          <!--end Services  side bar-->

          <!--start best practices -->
          <!--<li class="treeview">-->
          <!--  <a href="#">-->
          <!--    <i class="far fa-check-square"></i>&nbsp;&nbsp;<span>Best Practices</span>-->
          <!--    <i class="fa fa-angle-left pull-right"></i>-->
          <!--  </a>-->
          <!--  <ul class="treeview-menu">-->
          <!--    <li><a href="<?php echo base_url('Practices/addpractices');?>"><i class="far fa-circle"></i> <span>Add New</span></a></li>-->
          <!--    <li><a href="<?php echo base_url('Practices/viewpractices');?>"><i class="far fa-circle"></i> <span>View All</span></a></li>-->
          <!--  </ul>-->
          <!--</li>-->

          <!--end best practices-->

          <!--start Category-->
          <li class="treeview">
            <a href="#">
              <i class="fas fa-list"></i>&nbsp;&nbsp;<span>Product Category</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('Category/addcategory'); ?>"><i class="far fa-circle"></i> Add New</a></li>
              <li><a href="<?php echo base_url('Category/viewcategory'); ?>"><i class="far fa-circle"></i> View All</a></li>
            </ul>
          </li>
          <!--end Category-->
          
          <!-- Start subcatagory section -->
           <li class="treeview">
            <a href="#">
              <i class="fas fa-list"></i>&nbsp;&nbsp;<span>Product Material</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('SubCatagories/addsubcatagory'); ?>"><i class="far fa-circle"></i> Add New</a></li>
              <li><a href="<?php echo base_url('SubCatagories/viewSubCatagory'); ?>"><i class="far fa-circle"></i> View All</a></li>
            </ul>
          </li>
          <!-- End Sub catagory section -->
          

          <li class="treeview">
            <a href="#">
              <i class="fas fa-sitemap"></i>&nbsp;&nbsp;<span>Collection</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
             <li><a href="<?php echo base_url('Collection/addcollection'); ?>"><i class="far fa-circle"></i> <span>Add New</span></a></li>
             <li><a href="<?php echo base_url('Collection/viewcollection'); ?>"><i class="far fa-circle"></i> <span>View All</span></a></li>
           </ul>
         </li>
         <!-- end Collection-->



       <!--start people -->
       <li class="treeview">
        <a href="#">
          <i class="fas fa-users"></i>&nbsp;&nbsp;<span>People</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('People/addpeople'); ?>"><i class="far fa-circle"></i> <span>Add New</span></a></li>
          <li><a href="<?php echo base_url('People/viewpeople'); ?>"><i class="far fa-circle"></i> <span>View All</span></a></li>
        </ul>
      </li>
      <!-- End people -->

      <!-- Location Map Page -->

      <!-- Stores Page -->
      <li class="treeview">
        <a href="#">
          <i class="fas fa-shopping-cart"></i>&nbsp;&nbsp;<span>Stores</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('Store/addstore'); ?>"><i class="far fa-circle"></i> <span>Add New</span></a></li>
          <li><a href="<?php echo base_url('Store/viewstore'); ?>"><i class="far fa-circle"></i> <span>View All</span></a></li>
        </ul>
      </li>
      <!-- End Location Map Page -->

      <!-- Stones Page -->
      <li class="treeview">
        <a href="#">
          <i class="fas fa-gem"></i>&nbsp;&nbsp;<span>Stones</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('Stones/addstones'); ?>"><i class="far fa-circle"></i> <span>Add New</span></a></li>
          <li><a href="<?php echo base_url('Stones/viewstones'); ?>"><i class="far fa-circle"></i> <span>View All</span></a></li>
        </ul>
      </li>
      <!-- End stones  Page -->

      <!-- Product Page -->
      <li class="treeview">
        <a href="#">
          <i class="fas fa-shopping-bag"></i>&nbsp;&nbsp;<span>Products</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('product/addproduct'); ?>"><i class="far fa-circle"></i> <span>Add New</span></a></li>
         <li><a href="<?php echo base_url('product/viewproduct'); ?>"><i class="far fa-circle"></i> <span>View All</span></a></li>
       </ul>
     </li>
     <!-- End Product Page -->
     
     <!-- faq cat Page -->
      <li class="treeview">
        <a href="#">
          <i class="fas fa-shopping-bag"></i>&nbsp;&nbsp;<span>Faq Category</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('faq/addfaqcategory'); ?>"><i class="far fa-circle"></i> <span>Add New</span></a></li>
         <li><a href="<?php echo base_url('faq/viewfaqcategory'); ?>"><i class="far fa-circle"></i> <span>View All</span></a></li>
       </ul>
     </li>
     <!-- End faq Page -->

<!-- faq cat Page -->
      <li class="treeview">
        <a href="#">
          <i class="fas fa-shopping-bag"></i>&nbsp;&nbsp;<span>Faq</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('faq/addfaq'); ?>"><i class="far fa-circle"></i> <span>Add New</span></a></li>
         <li><a href="<?php echo base_url('faq/viewfaq'); ?>"><i class="far fa-circle"></i> <span>View All</span></a></li>
       </ul>
     </li>
     <!-- End faq Page -->
     
     <!-- news Page -->
      <li class="treeview">
        <a href="#">
          <i class="far fa-newspaper"></i>&nbsp;&nbsp;<span>News</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('News/addnews'); ?>"><i class="far fa-circle"></i> <span>Add New</span></a></li>
         <li><a href="<?php echo base_url('News/viewnews'); ?>"><i class="far fa-circle"></i> <span>View All</span></a></li>
       </ul>
     </li>
     <!-- End news Page -->
     
     <!-- gallery Page -->
     
      <li class="treeview">
             <a href="#">
               <i class="far fa-image">&nbsp;&nbsp;</i><span>Gallery</span>
               <i class="fa fa-angle-left pull-right"></i>
             </a>
             <ul class="treeview-menu">
              <li>
               <a href="<?php echo base_url('Gallery/addgallery'); ?>">
                 <i class="far fa-circle"></i> <span>Add New</span>
               </a>
               <a href="<?php echo base_url('Gallery/viewgallery'); ?>">
                 <i class="far fa-circle"></i> <span>View All</span>
               </a>
             </li>
           </ul>
      </li>
          
   </ul>
   <!-- End gallery Page -->
   

 </section>
 <!-- /.sidebar -->
</aside>

