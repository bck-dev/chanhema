<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hemachandra | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Metro Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/'); ?>metro-bootstrap.min.css">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>dist/css/skins/_all-skins.min.css">
    <!-- jquery confirm css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/common/css/'); ?>jquery-confirm.min.css">
    <!-- bootstrap iconpicker -->
    <link rel="stylesheet" href="<?php echo base_url('assets/common/css/'); ?>fontawesome-iconpicker.min.css">
    <!-- custom css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/'); ?>custom.css">
    <!-- round toggle button css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/'); ?>toggleslider.css">
    <!-- dropdown css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/'); ?>animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/'); ?>bootstrap-dropdownhover.min.css">
    <!-- time css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/admin/dist/css/'); ?>time.css">
    <link href="<?php echo base_url('assets/admin/'); ?>plugins/summernote/summernote.css" rel="stylesheet">
  
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('assets/admin/'); ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo base_url('admin'); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels --> 
          <span class="logo-mini"><b></b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Hemachandra Dashboard</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a> -->
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown notifications-menu">
                <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning" id="label label-warning">0</span>
                </a> -->
                <ul class="dropdown-menu" >
                  <li class="header">You have <span id="header_notify">no</span> <span id="notification">notifications</span></li>
                  <li>
                    <!-- inner menu: contains the actual data -->

                    <!-- this list shows the notifications of products -->
                    <ul class="menu" id="product_notify">
                      <!-- <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                      </li> -->
                    </ul>
                  </li>
                  <li class="footer"><a href="#"></a></li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-bottom: 35px !important;">
                  <img src="<?php echo base_url('assets/admin/dist/img'); ?>/avatar.png" class="user-image" alt="admin">
                  <span class="hidden-xs"></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                      <img src="<?php echo base_url('assets/admin/dist/img'); ?>/avatar.png" class="user-image" alt="admin">
                    <?php if($_SESSION['usertype']==1): ?>
                        <label class="label label-danger">Admin</label>
                        <?php elseif($_SESSION['usertype']==2): ?>
                          <label class="label label-success">Editor</label>
                          <?php elseif($_SESSION['usertype']==3):?>
                            <label class="label label-warning">Feader</label>
                          <?php endif; ?>
                        </p>
                        <p>
                          <?php echo $_SESSION['username']; ?>
                        </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url('Admin/settings/'.$this->session->userdata('id'))?>" class="btn btn-default btn-flat">Settings</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url('login/logout')?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>

        </nav>
      </header>