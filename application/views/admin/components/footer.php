      <footer class="main-footer">

        <div class="pull-right hidden-xs">

          <b>Version</b> 1.0

        </div>

        <strong>Copyright &copy; <?php echo date('Y'); ?> </strong> BcKonnect All rights reserved.

      </footer>



      <!-- Control Sidebar -->

      <aside class="control-sidebar control-sidebar-dark">

        <!-- Create the tabs -->

        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">

          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>

        </ul>

        <!-- Tab panes -->

        <div class="tab-content">

          <!-- Home tab content -->

          <div class="tab-pane" id="control-sidebar-home-tab">

            <h3 class="control-sidebar-heading">Recent Activity</h3>

            <ul class="control-sidebar-menu">

              <li>

                <a href="javascript::;">

                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                  <div class="menu-info">

                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                    <p>Will be 23 on April 24th</p>

                  </div>

                </a>

              </li>

              <li>

                <a href="javascript::;">

                  <i class="menu-icon fa fa-user bg-yellow"></i>

                  <div class="menu-info">

                    <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                    <p>New phone +1(800)555-1234</p>

                  </div>

                </a>

              </li>

              <li>

                <a href="javascript::;">

                  <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                  <div class="menu-info">

                    <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                    <p>nora@example.com</p>

                  </div>

                </a>

              </li>

              <li>

                <a href="javascript::;">

                  <i class="menu-icon fa fa-file-code-o bg-green"></i>

                  <div class="menu-info">

                    <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                    <p>Execution time 5 seconds</p>

                  </div>

                </a>

              </li>

            </ul><!-- /.control-sidebar-menu -->



            <h3 class="control-sidebar-heading">Tasks Progress</h3>

            <ul class="control-sidebar-menu">

              <li>

                <a href="javascript::;">

                  <h4 class="control-sidebar-subheading">

                    Custom Template Design

                    <span class="label label-danger pull-right">70%</span>

                  </h4>

                  <div class="progress progress-xxs">

                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>

                  </div>

                </a>

              </li>

              <li>

                <a href="javascript::;">

                  <h4 class="control-sidebar-subheading">

                    Update Resume

                    <span class="label label-success pull-right">95%</span>

                  </h4>

                  <div class="progress progress-xxs">

                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>

                  </div>

                </a>

              </li>

              <li>

                <a href="javascript::;">

                  <h4 class="control-sidebar-subheading">

                    Laravel Integration

                    <span class="label label-warning pull-right">50%</span>

                  </h4>

                  <div class="progress progress-xxs">

                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>

                  </div>

                </a>

              </li>

              <li>

                <a href="javascript::;">

                  <h4 class="control-sidebar-subheading">

                    Back End Framework

                    <span class="label label-primary pull-right">68%</span>

                  </h4>

                  <div class="progress progress-xxs">

                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>

                  </div>

                </a>

              </li>

            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->



          <!-- Settings tab content -->

          <div class="tab-pane" id="control-sidebar-settings-tab">

            <form method="post">

              <h3 class="control-sidebar-heading">General Settings</h3>

              <div class="form-group">

                <label class="control-sidebar-subheading">

                  Report panel usage

                  <input type="checkbox" class="pull-right" checked>

                </label>

                <p>

                  Some information about this general settings option

                </p>

              </div><!-- /.form-group -->



              <div class="form-group">

                <label class="control-sidebar-subheading">

                  Allow mail redirect

                  <input type="checkbox" class="pull-right" checked>

                </label>

                <p>

                  Other sets of options are available

                </p>

              </div><!-- /.form-group -->



              <div class="form-group">

                <label class="control-sidebar-subheading">

                  Expose author name in posts

                  <input type="checkbox" class="pull-right" checked>

                </label>

                <p>

                  Allow the user to show his name in blog posts

                </p>

              </div><!-- /.form-group -->



              <h3 class="control-sidebar-heading">Chat Settings</h3>



              <div class="form-group">

                <label class="control-sidebar-subheading">

                  Show me as online

                  <input type="checkbox" class="pull-right" checked>

                </label>

              </div><!-- /.form-group -->



              <div class="form-group">

                <label class="control-sidebar-subheading">

                  Turn off notifications

                  <input type="checkbox" class="pull-right">

                </label>

              </div><!-- /.form-group -->



              <div class="form-group">

                <label class="control-sidebar-subheading">

                  Delete chat history

                  <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>

                </label>

              </div><!-- /.form-group -->

            </form>

          </div><!-- /.tab-pane -->

        </div>

      </aside><!-- /.control-sidebar -->

      <!-- Add the sidebar's background. This div must be placed

        immediately after the control sidebar -->

        <div class="control-sidebar-bg"></div>



      </div><!-- ./wrapper -->

      <!-- jQuery UI -->

      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

      <!-- Bootstrap 3.3.5 -->

      <script src="<?php echo base_url('assets/admin/'); ?>bootstrap/js/bootstrap.min.js"></script>

      <!-- geolocation js -->

      <script language="JavaScript" src="//www.geoplugin.net/javascript.gp" type="text/javascript"></script>

      <!-- FastClick -->

      <script src="<?php echo base_url('assets/admin/'); ?>plugins/fastclick/fastclick.min.js"></script>

      <!-- AdminLTE App -->

      <script src="<?php echo base_url('assets/admin/'); ?>dist/js/app.min.js"></script>

      <!-- Sparkline -->

      <script src="<?php echo base_url('assets/admin/'); ?>plugins/sparkline/jquery.sparkline.min.js"></script>

      <!-- jvectormap -->

      <script src="<?php echo base_url('assets/admin/'); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

      <script src="<?php echo base_url('assets/admin/'); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

      <!-- SlimScroll 1.3.0 -->

      <script src="<?php echo base_url('assets/admin/'); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>

      <!-- ChartJS 1.0.1 -->

      <script src="<?php echo base_url('assets/admin/'); ?>plugins/chartjs/Chart.min.js"></script>

      <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

      <!-- <script src="<?php //echo base_url('assets/admin/'); ?>dist/js/pages/dashboard2.js"></script> -->

      <!-- AdminLTE for demo purposes -->

      <script src="<?php echo base_url('assets/admin/'); ?>dist/js/demo.js"></script>

      <!-- jquery confirm js -->

      <script src="<?php echo base_url('assets/common/js/'); ?>jquery-confirm.min.js"></script>

      <!-- bootstrap dropdown hover js -->

      <script src="<?php echo base_url('assets/admin/'); ?>dist/js/bootstrap-dropdownhover.min.js"></script>

      <!-- font awesome icon picker -->

      <script src="<?php echo base_url('assets/admin/'); ?>dist/js/fontawesome-iconpicker.min.js"></script>

      <!-- JSColor color picker -->

      <script src="<?php echo base_url('assets/admin/'); ?>dist/js/jscolor.js"></script>

      <!-- simple clock js -->

      <!-- <script src="<?php echo base_url('assets/admin/'); ?>dist/js/simpleClock.min.js"></script> -->

      <!-- nic edit js -->

      <script src="https://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>

      <!-- time js -->

      <script src="<?php echo base_url('assets/admin/'); ?>dist/js/time.js"></script>
      <script src="<?php echo base_url('assets/admin/'); ?>plugins/summernote/summernote.min.js"></script>
      

      <!-- <script type="text/javascript">
        bkLib.onDomLoaded(function() {
          nicEditors.allTextAreas();
          jQuery('.ne-except').each(function(){
            jQuery(this).prev().prev().hide();
            jQuery(this).prev().hide();
            jQuery(this).on('keyup',function(){
              jQuery(this).prev()
              .find('.nicEdit-main')
              .html(jQuery(this).val());
            });
            jQuery(this).show();
          });
        });
      </script> -->

      <!-- url js -->

      <script src="<?php echo base_url('assets/admin/'); ?>dist/js/url.js"></script>

      <!-- custom js -->

      <script src="<?php echo base_url('assets/admin/'); ?>dist/js/custom.js"></script>


<!-- rich text editor -->
    <script>
      $('.note').summernote({
        tabsize: 2,
        height: 120,
        toolbar: [
          ['font', ['bold', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['insert', ['link']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ]
      });
    </script>

<!-- Script for delete images of case studies -->

<script type="text/javascript">

  function deleteimageservices(image_id)

  {

    var answer = confirm ("Are you sure you want to delete this picture?");

    if (answer){



      $.ajax({

        type: "POST",

        url: "<?php echo site_url('admin/deleteimageservices');?>",

        data: "image_id="+image_id,

        success: function (response) {

          if (response == 1) {

            $(".imagelocation"+image_id).remove(".imagelocation"+image_id);

          };



        }

      });

    }

  }

</script>



<!-- Script for delete images of news -->

<script type="text/javascript">

  function deleteimagenews(image_id)

  {

    var answer = confirm ("Are you sure you want to delete this picture?");

    if (answer){



      $.ajax({

        type: "POST",

        url: "<?php echo site_url('admin/deleteimagenews');?>",

        data: "image_id="+image_id,

        success: function (response) {

          if (response == 1) {

            $(".imagelocation"+image_id).remove(".imagelocation"+image_id);

          };



        }

      });

    }

  }

</script>



<!-- Script for font awsome icon picker -->

<script>

  $(function () {

    $('.action-destroy').on('click', function () {

      $.iconpicker.batch('.icp.iconpicker-element', 'destroy');

    });

        // Live binding of buttons

        $(document).on('click', '.action-placement', function (e) {

          $('.action-placement').removeClass('active');

          $(this).addClass('active');

          $('.icp-opts').data('iconpicker').updatePlacement($(this).text());

          e.preventDefault();

          return false;

        });

        $('.action-create').on('click', function () {

          $('.icp-auto').iconpicker();



          $('.icp-dd').iconpicker({

                //title: 'Dropdown with picker',

                //component:'.btn > i'

              });

          $('.icp-opts').iconpicker({

            title: 'With custom options',

            icons: [

            {

              title: "fab fa-github",

              searchTerms: ['repository', 'code']

            },

            {

              title: "fas fa-heart",

              searchTerms: ['love']

            },

            {

              title: "fab fa-html5",

              searchTerms: ['web']

            },

            {

              title: "fab fa-css3",

              searchTerms: ['style']

            }

            ],

            selectedCustomClass: 'label label-success',

            mustAccept: true,

            placement: 'bottomRight',

            showFooter: true,

                // note that this is ignored cause we have an accept button:

                hideOnSelect: true,

                // fontAwesome5: true,

                templates: {

                  footer: '<div class="popover-footer">' 

                  // '<div style="text-align:left; font-size:12px;">Placements: \n\ 

                  // <a href="#" class=" action-placement">inline</a>\n\

                  // <a href="#" class=" action-placement">topLeftCorner</a>\n\

                  // <a href="#" class=" action-placement">topLeft</a>\n\

                  // <a href="#" class=" action-placement">top</a>\n\

                  // <a href="#" class=" action-placement">topRight</a>\n\

                  // <a href="#" class=" action-placement">topRightCorner</a>\n\

                  // <a href="#" class=" action-placement">rightTop</a>\n\

                  // <a href="#" class=" action-placement">right</a>\n\

                  // <a href="#" class=" action-placement">rightBottom</a>\n\

                  // <a href="#" class=" action-placement">bottomRightCorner</a>\n\

                  // <a href="#" class=" active action-placement">bottomRight</a>\n\

                  // <a href="#" class=" action-placement">bottom</a>\n\

                  // <a href="#" class=" action-placement">bottomLeft</a>\n\

                  // <a href="#" class=" action-placement">bottomLeftCorner</a>\n\

                  // <a href="#" class=" action-placement">leftBottom</a>\n\

                  // <a href="#" class=" action-placement">left</a>\n\

                  // <a href="#" class=" action-placement">leftTop</a>\n\

                  // </div><hr></div>'

                }

              }).data('iconpicker').show();

        }).trigger('click');





        // Events sample:

        // This event is only triggered when the actual input value is changed

        // by user interaction

        $('.icp').on('iconpickerSelected', function (e) {

          $('.lead .picker-target').get(0).className = 'picker-target fa-3x ' +

          e.iconpickerInstance.options.iconBaseClass + ' ' +

          e.iconpickerInstance.options.fullClassFormatter(e.iconpickerValue);

        });

      });

    </script>

    

    <!-- delete confirmation dialogbox -->

  <!-- <script type="text/javascript">

    $('.ItemConfirm').on('click', function () {

        return confirm('Are you sure want to delete?');

    });

  </script>

-->

</body>

</html>