<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      View Stones
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">View Stones</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- table box -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
             Stones List
           </h3>
         </div><!-- /.box-header -->
         <div class="box-body no-padding">
          <?php echo $this->session->flashdata('msg'); ?>
          <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
          <table class="table table-condensed">
            <tr>
              <th style="width: 10px">#</th>
              <th>Image</th>
              <th>Name</th>
              <th>Status</th>
              <th>Options</th>
            </tr>
            <?php 
            $count = 0;
            foreach($stones as $data) {
              ?>

              <tr>
                <td><?php echo ++$count; ?>.</td>
                <td>
                  <?php
                  foreach ($stones_images as $key ) { 
                    ?>
                    <?php if($key['stones_stones_id']==$data['stones_id']){?>
                      <img src="<?php echo base_url(); ?><?php echo $key['stones_images_name']; ?>" style="vertical-align:text-top;height: 100px;width: 100px;">
                    <?php break; }?>
                  <?php } ?>
                </td>
                <td><?php echo $data['stones_name']; ?></td>
                <td><?php echo $data['stones_status']==1 ? 'Cut' : 'Uncut' ?></td>
                <td>
                 <a href="" class="label label-primary" data-toggle="modal" data-target="#exampleModal<?php echo $data['stones_id'];?>" data-whatever="@mdo">View</a>
                 <a href="<?php echo base_url(); ?>stones/editstones/<?php echo $data['stones_id']; ?>" class="label label-success">Edit</a>
                 <a href="<?php echo base_url(); ?>stones/deletestones/<?php echo $data['stones_id']; ?>" class="label label-danger">Delete</a>
               </td>
             </tr> 
           <?php } ?>
         </tr> 

       </table>

     </div><!-- /.box-body -->
   </div><!-- /.box -->

 </div><!--/.col (full) -->
</div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php foreach ($stones as $value) {;?>
  <?php
  $stones_id = $value['stones_id'];
  $stones_description = $value['stones_description'];
  $stones_name = $value['stones_name'];
  $stones_status = $value['stones_status'];
  ?>
  <div class="modal fade" id="exampleModal<?php echo $stones_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title" id="exampleModalLabel">Stone Details</h3>
        </div>
        <div class="modal-body" style="max-height: 500px; overflow-y: auto;">
         <div class="form-group">
          <label>Name :</label>&nbsp;
          <?php echo $stones_name;?>
        </div>
        <div class="form-group">
          <label>Status :</label>&nbsp;
          <?php echo $stones_status;?>
        </div>
        <div class="form-group">
          <label>Description :</label>&nbsp;
          <?php echo $stones_description;?>
        </div>
        <div class="form-group">
          <label>Images :</label>&nbsp;
          <?php
          foreach ($stones_images as $key ) { 
            ?>
            <?php if($key['stones_stones_id']==$stones_id){?>
              <img src="<?php echo base_url(); ?>upload/stones/<?php echo $key['stones_images_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
            <?php }?>
          <?php } ?>
        </div>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php }?>