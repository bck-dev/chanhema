<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->

          <?php foreach ($stones as $value) {
            $stones_id = $value['stones_id'];
            $stones_name = $value['stones_name'];
            $stones_status = $value['stones_status'];
            $stones_description = $value['stones_description'];


          } ?>
          <!-- form start -->
          <form role="form" action="<?php echo base_url('stones/updatestones/'.$stones_id); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">
                <label for="stones_name">Name</label>
                <input type="text" class="form-control" id="stones_name" placeholder="Enter Stone Name" name="stones_name" value="<?php echo $stones_name; ?>" required>
              </div>

              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control note" rows="5" id="stones_description" name="stones_description" placeholder="Enter Description for Stones" required><?php echo $stones_description;?></textarea>
              </div>

              <div class="form-group">
                <?php
                if(isset($stones_images) && is_array($stones) && count($stones)): $i=1;
                  foreach ($stones_images as $key => $data) { 
                    ?>
                    <div class="image<?php echo $data['stones_images_id'] ?>">
                      <br/>
                      <img src="<?php echo base_url(); ?><?php echo $data['stones_images_name']; ?>" style="vertical-align:text-top;"  width="200" height="200">
                      <a href="" style="cursor:pointer;"  id="close<?php echo $data['stones_images_id']; ?>" class="label label-danger">X</a>
                    </div>
                    <?php }endif; ?>
                  </div>
                  
                  <div class="form-group">
                    <label>Images</label>
                    <input type="file" name="userfile[]" id="image_file" accept=".png,.jpg,.jpeg,.gif" multiple>
                    <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                      Recomanded resolution for images is <strong>120 x 120</strong> pixels.
                    </p>
                  </div>
                  
                <div class="form-group">
                <label>Status</label>
                <input type="text" class="form-control" id="stones_status" placeholder="Enter Stone Status" name="stones_status" value="<?php echo $stones_status; ?>" required>
              </div>   
                                              
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update Stones Details</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
  if(isset($stones_images) && is_array($stones) && count($stones)): $i=1;
  foreach ($stones_images as $key => $data): ?>
  <script>
    $(document).ready(function() {
      $("#close<?php echo $data['stones_images_id']; ?>").click(function() {
          $('.image<?php echo $data['stones_images_id']; ?>').hide();
          var path = "<?php echo base_url(); ?>" + "stones/deleteimagestones";
          console.log(path);
          $.ajax({
            url: path, 
            type: 'post',
            data: {'id':<?php echo $data['stones_images_id']; ?>},
            success: function(msg) {
              console.log(msg);
            }
          });
      });
    });
  </script>
<?php endforeach; endif; ?>