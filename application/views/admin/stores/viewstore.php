<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      View Stores
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">View Stores</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- table box -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
              Store List
            </h3>
            <?php echo $this->session->flashdata('msg'); ?>
            <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>       
          </div><!-- /.box-header -->

          <div class="box-body no-padding">
            <table class="table table-condensed">
              <tr>
                <th style="width: 10px">#</th>
                <th>Stores Title</th>
                <th>Stores Address</th>
                <th>Stores Telephone</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Options</th>
              </tr>

             <?php
                $count = 0;
                foreach($stores as $data) {
              ?>
              <tr>
                <td><?php echo ++$count; ?>.</td>
                <td><?php echo $data['stores_title']; ?></td>
                <td><?php echo $data['stores_address']; ?></td>
                <td><?php echo $data['stores_telephone_number']; ?></td>
                <td><?php echo $data['latitude']; ?></td>
                <td><?php echo $data['longtuide']; ?></td>
                <td>
                  <a href="<?php echo base_url(); ?>store/editstore/<?php echo $data['stores_id']; ?>" class="label label-success">Edit</a>
                  <a href="<?php echo base_url(); ?>store/deletestore/<?php echo $data['stores_id']; ?>" class="label label-danger">Delete</a>
                </td>
              </tr> 

              <?php } ?>                                        

            </table>

          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->