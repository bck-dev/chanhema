<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
         <?php foreach ($store as $value) {

            $stores_id = $value['stores_id'];

            $stores_title = $value['stores_title'];

            $stores_address = $value['stores_address'];

            $stores_telephone_number = $value['stores_telephone_number'];

            $latitude = $value['latitude'];

            $longtuide = $value['longtuide'];

          } ?>

          <!-- form start -->

         <form role="form" action="<?php echo base_url('Store/updatestore/'.$stores_id); ?>" method="POST" enctype='multipart/form-data'>

            <div class="box-body">

              <?php echo $this->session->flashdata('msg'); ?>

              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">

                <label for="colecttion_name">Store title</label>

                <input type="text" class="form-control" id="stores_title" placeholder="Enter Banner Title" name="stores_title" value="<?php echo  $stores_title;?>" required>

              </div>



              <div class="form-group">                     

                <label for="collection_description">Store Address</label>

                <textarea class="form-control note" rows="3" id="stores_address" name="stores_address" placeholder="Enter Description"><?php echo $stores_address;?></textarea>

              </div> 

              <div class="form-group">

                <label for="placetitle">Store Telephone</label>

                <input type="text" class="form-control" id="stores_telephone_number" placeholder="Enter Telephone Number" name="stores_telephone_number" value="<?php echo $stores_telephone_number; ?>" required>

              </div>



              <div class="form-group">

                <label for="home_page_event_title">Coordinates</label>

                <div class="form-row">

                  <div class="form-group col-md-6" style="padding-left: 0px;">

                    <label for="placelatitude" class="help-block">Latitude</label>

                    <input type="text" class="form-control" id="latitude" name="latitude" placeholder="Enter Latitude Value" value="<?php echo $latitude; ?>"> 

                  </div>

                  <div class="form-group col-md-6" style="padding-right: 0px;">

                    <label for="placelongitude" class="help-block">Longitude</label>

                    <input type="text" class="form-control" id="longtuide" name="longtuide" placeholder="Enter Longitude Value" value="<?php echo $longtuide; ?>"> 

                  </div>

                </div>

              </div>



            </div><!-- /.box-body -->

            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Update Store</button>

              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>

            </div>

          </form>

        </div><!-- /.box -->



      </div><!--/.col (full) -->

    </div>   <!-- /.row -->

  </section><!-- /.content -->

</div><!-- /.content-wrapper -->

