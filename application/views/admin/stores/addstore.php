<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

      <?php echo $pagetitle; ?>

    </h1>

    <ol class="breadcrumb">

      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active"><?php echo $pagetitle; ?></li>

    </ol>

  </section>



  <!-- Main content -->

  <section class="content">

    <div class="row">

      <!-- left column -->

      <div class="col-md-12">

        <!-- general form elements -->

        <div class="box box-primary">

          <div class="box-header with-border">

            <h3 class="box-title"><?php echo $pagetitle; ?></h3>

          </div><!-- /.box-header -->

          <!-- form start -->

          <form role="form" action="<?php echo base_url('Store/insertstore'); ?>" method="POST" enctype='multipart/form-data'>

            <div class="box-body">

              <?php echo $this->session->flashdata('msg'); ?>

              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>



              <div class="form-group">

                <label for="placetitle">Store Title</label>

                <input type="text" class="form-control" id="placetitle" placeholder="Enter Place Title" name="placetitle" required>

              </div> 



              <div class="form-group">

                <label for="placedescription">Store Address</label>

                <textarea class="form-control note" rows="5" id="address" name="address" placeholder="Enter Address"></textarea>

              </div>



              <div class="form-group">

                <label for="placetitle">Store Telephone</label>

                <input type="text" class="form-control" id="telephone" placeholder="Enter Telephone Number" name="telephone" required>

              </div>



              <div class="form-group">
                <label for="home_page_event_title">Coordinates</label>
                <div class="form-row">
                  <div class="form-group col-md-6" style="padding-left: 0px;">
                    <label for="placelatitude" class="help-block">Latitude</label>
                    <input type="text" class="form-control" id="placelatitude" name="placelatitude" placeholder="Enter Latitude Value"> 
                  </div>

                  <div class="form-group col-md-6" style="padding-right: 0px;">
                    <label for="placelongitude" class="help-block">Longitude</label>
                    <input type="text" class="form-control" id="placelongitude" name="placelongitude" placeholder="Enter Longitude Value"> 
                  </div>
                </div>

              </div>



            </div><!-- /.box-body -->

            <div class="box-footer">

              <button type="submit" class="btn btn-primary">Create Store</button>

              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>

            </div>

          </form>

        </div><!-- /.box -->



      </div><!--/.col (full) -->

    </div>   <!-- /.row -->

  </section><!-- /.content -->

</div><!-- /.content-wrapper -->

