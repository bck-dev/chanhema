<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- table box -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
              Banner List
            </h3>
          </div><!-- /.box-header -->

          <div class="box-body no-padding" id="navTable">
            <?php echo $this->session->flashdata('msg'); ?>
            <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                        
            <table class="table table-condensed">
              <tr>
                <th style="width: 10px">#</th>
                <th>Banner Image</th>
                <th>Banner Text</th>
                <th>Link</th>
                <th>Status</th>
                <th>Activate/Deactivate</th>
                <th>Options</th>
              </tr>

              <?php
                $count = 0;
                foreach($main_banner as $data) {
              ?>
                <tr>
                  <td><?php echo ++$count; ?>.</td>
                  <td><img src="<?php echo base_url()?>upload/mainbanner/<?php echo $data['main_banner_image']; ?>" height="100px" width="200px" ></td>
                  <td><?php echo $data['main_banner_text']; ?></td>
                  <td><?php echo $data['main_banner_link']; ?></td>
                  <td>
                   <?php echo $data['main_banner_status']==1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Deactive</span>' ; ?>
                  </td>
                  <td>
                  <input class="mainbaroption" type="checkbox" name="main_banner_status" value="<?php echo $data['main_banner_id'] ?>" <?php echo $data ['main_banner_status']==1?'checked':''; ?>>&nbsp;Activate
                </td>
                <td>
                  <a href="<?php echo base_url(); ?>mainbanner/editmainbanner/<?php echo $data['main_banner_id']; ?>" class="label label-success">Edit</a>
                  <a href="<?php echo base_url(); ?>mainbanner/deletemainbanner/<?php echo $data['main_banner_id']; ?>" class="label label-danger">Delete</a>
                </td>
              </tr> 
            <?php } ?>                                        

          </table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (full) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->