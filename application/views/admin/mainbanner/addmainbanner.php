<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('mainbanner/insertmainbanner'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
              <div class="form-group">
                <label for="main_banner_image">Banner Image</label>
                <input type="file" id="main_banner_image" name="main_banner_image" required>
              </div>  
              <div class="form-group">
                <label for="main_banner_text">Banner Text</label>
                <input type="text" class="form-control" id="main_banner_text" placeholder="Enter Banner Text" name="main_banner_text" required>
              </div>
              <div class="form-group">
                <label for="main_banner_link">Add Link</label>
                <input type="text" class="form-control" id="main_banner_link" placeholder="Enter URL" name="main_banner_link" required>
              </div>   
              <div class="form-group">                     
                <label for="main_banner_status">Status</label>
                <select name="main_banner_status" class="form-control" id="main_banner_status">
                  <option value="1">Active</option>
                  <option value="2">Deactive</option>
                </select>
              </div>                                     
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Create Banner</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
