<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
         <?php foreach ($main_banner as $value) {
            $main_banner_id = $value['main_banner_id'];
            $main_banner_image= $value['main_banner_image'];
            $main_banner_text = $value['main_banner_text'];
            $main_banner_link = $value['main_banner_link'];

          } ?>
          <!-- form start -->
         <form role="form" action="<?php echo base_url('mainbanner/updatemainbanner/'.$main_banner_id); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                <div class="form-group">
                <label for="main_banner_image">Main Banner Image</label>
                <input type="hidden" id="main_banner_image" name="main_banner_image" value="<?php echo $main_banner_image; ?>">
                <div class="image-group" style="position: relative; margin-top: 20px;">
                  <img src="<?php echo base_url('upload/mainbanner/'); ?>/<?php echo $main_banner_image;?>" class="thumbnail" alt="<?php echo $main_banner_image; ?>" style="width: 20%"> 
                <span class="img-close">&#10006;</span>                
                </div>
                <input type="file" id="input-image" name="fileinput"> 

                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.</p>
              </div> 
              <div class="form-group">
                <label for="main_banner_text">Banner Text</label>
                <input type="text" class="form-control" id="main_banner_text" placeholder="Enter Banner Text" name="main_banner_text" value="<?php echo  $main_banner_text?>" required>
              </div> 

              <div class="form-group">
                <label for="main_banner_link">Add Link</label>
                <input type="text" class="form-control" id="main_banner_link" placeholder="Enter URL" name="main_banner_link" value="<?php echo  $main_banner_link?>" required>
              </div>                                       
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update Banner</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
