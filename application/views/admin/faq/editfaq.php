<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Get values from table -->
  <?php foreach ($faq as $data) {
      $faq_id = $data['faq_id'];
      $quest = $data['faq'];      
      $answer = $data['answer'];
      $faq_category_id = $data['faq_category_id'];
  } ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('faq/updatefaq/').$faq_id; ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
              
              <div class="form-group">
                <label for="people_category">Faq Category</label>
                <select class="form-control" id="faqcategory" name="faqcategory">
                    <option value="">-- Select Faq Category --</option>
                    <?php foreach($category as $type): ?>
                    <option value="<?php echo $type['faq_category_id']?>"<?php echo ($faq_category_id == $type['faq_category_id']) ? 'selected' : ''; ?>><?php echo $type['faq_category_name']; ?></option>
                    <?php endforeach; ?>
                </select>
              </div>

              <div class="form-group">
                <label>Faq</label>
                <input class="form-control" id="faq" name="faq" value="<?php echo $quest;?>" placeholder="Enter Question" />
              </div>

              <div class="form-group">
                <label>Answer</label>
                <textarea class="form-control note" rows="5" id="answer" name="answer" placeholder="Enter Answer"><?php echo $answer;?></textarea>
              </div>

            </div><!-- /.box-body -->
              <!-- Essential element for fontawsome font picker -->
              <button class="btn btn-danger action-destroy" style="display: none;">Destroy instances</button>
              <button class="btn btn-default action-create" style="display: none;">Create instances</button>  
              <!--End Essential element for fontawsome font picker -->                              
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update Faq</button>
              <a class="btn btn-default" href="<?php echo base_url('faq/viewfaq'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
