<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      View Stores
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">View News</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- table box -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
              News List
            </h3>
          <?php echo $this->session->flashdata('msg'); ?>
          <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
           
          </div><!-- /.box-header -->
            
          <div class="box-body no-padding">
            <table class="table table-condensed">
              <tr>
                <th style="width: 10px">#</th>
                <th>News Title</th>
                <th>Date</th>
                <th>News Description</th>
                <th>Image</th>
                <th>Options</th>
              </tr>
             <?php
                $count = 0;
                foreach($news as $data) {
              ?>
              <tr>
                <td><?php echo ++$count; ?>.</td>
                <td><?php echo $data['news_title']; ?></td>
                <td><?php echo $data['date']; ?></td>
                <td><?php echo $data['news_description']; ?></td>
                <td>

                <?php foreach ($news_image as $fr): ?>
                    <?php if ($fr['news_news_id'] == $data['news_id'] ): ?>
                        <img src="<?php echo base_url(); ?><?php echo $fr['image_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 0px;margin-left: 3px;" >
                            <?php break; ?>
                          <?php endif ?>

                    <?php endforeach ?>
                  </td>

                
                
                <td>
                  <a href="" data-inquiry-id="<?php echo  $data['news_id']; ?>" data-action-id="3" data-toggle="modal" data-target="#vehicleModal<?php echo $data['news_id'];?>" class="label label-primary">view</a>
                  <a href="<?php echo base_url(); ?>news/editnews/<?php echo $data['news_id']; ?>" class="label label-success">Edit</a>
                  <a href="<?php echo base_url(); ?>news/deletenews/<?php echo $data['news_id']; ?>" class="label label-danger">Delete</a>
                </td>
              </tr> 
              <?php } ?>                                        
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php foreach($news as $data):
    $collection_id = $data['news_id'];

?>
  <div class="modal fade" id="vehicleModal<?php echo $data['news_id'];?>" role="dialog">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="exampleModalLabel">News Images</h3>
      </div>
      <div class="modal-body" style="max-height: 500px; overflow-y: auto;">

        <div class="form-group">
          <label> Images :</label>&nbsp;
          <?php foreach ($news_image as $fr): ?>
          <?php if ($fr['news_news_id'] == $data['news_id'] ): ?>
          <img src="<?php echo base_url(); ?><?php echo $fr['image_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
          <?php endif ?>
          <?php endforeach ?>
        </div>
        
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  </div>
<?php 
endforeach;
?>

