<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
         <?php foreach ($news as $value) {
            $news_id = $value['news_id'];
            $news_title = $value['news_title'];
            $date = $value['date'];
            $news_description = $value['news_description'];
           
          } ?>
          <!-- form start -->
         <form role="form" action="<?php echo base_url('news/updatenews/'.$news_id); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
              
              <div class="form-group">

                <?php if (isset($news_images) && is_array($news) && count($news)): $i = 1;
                    foreach ($news_images as $key => $data) { ?>
                      <div class="row">
                        <div class="col-md-6">
                            <div class="image<?php echo $data['news_image_id'] ?>">
                              <br/>
                              <img src="<?php echo base_url(); ?><?php echo $data['image_name']; ?>" style="vertical-align:text-top;"  width="200" height="200">
                              <div style="cursor:pointer;" id="close<?php echo $data['news_image_id'] ?>" class="label label-danger">X</div>
                            </div>
                        </div>
                      </div>
                    <?php }endif; ?>

              </div>
                <div class="form-group">
                  <label> Images</label>
                    <input type="file" id="uploadFile" name="userfile[]" value=" " multiple/>
                    <p style="color: red">Minimum Upload 2 or more images</p>
                    <p class="help-block">Image size should be below then 3mb and JPG,JPEG,PNG, GIF can be uploaded.<br/>Recomanded resolution for images is <strong>120 x 120</strong>pixels.</p>
                </div>
                <br><br>       
              
              
              <div class="form-group">
                <label for="placetitle">News Title</label>
                <input type="text" class="form-control" id="newstitle" placeholder="Enter News Title" name="news_title" value="<?php echo $news_title; ?>" required/>
              </div>

              <div class="form-group">
                <label for="placetitle">Date</label>
                <input type='text' class="form-control"  name="date" id="datepicker" value="<?php echo $date; ?>" style="width: 350px;" required/>    
              </div>
        
        <script type="text/javascript">
    $(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});
</script>
    


              <div class="form-group">
                <label for="placedescription">News Description</label>
                <textarea class="form-control note" rows="5" name="news_description" placeholder="Enter Description" value="<?php echo $news_description; ?>" required><?php echo $news_description;?></textarea>
              </div>



             

            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update news</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<?php if (isset($news_images) && is_array($news) && count($news)): 
  foreach ($news_images as $key => $data) : ?>
  <script>
    $(document).ready(function() {
      $("#close<?php echo $data['news_image_id']; ?>").click(function() {
          $('.image<?php echo $data['news_image_id']; ?>').hide();
          var path = "<?php echo base_url(); ?>" + "news/deleteImage";
          console.log(path);
          $.ajax({
            url: path, 
            type: 'post',
            data: {'id':<?php echo $data['news_image_id']; ?>},
            success: function(msg) {
              console.log(msg);
            }
          });
      });
    });
  </script>
<?php endforeach; endif; ?>