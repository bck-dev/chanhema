<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('News/insertnews'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">
                <label for="placetitle">News Title</label>
                <input type="text" class="form-control" id="newstitle" placeholder="Enter News Title" name="news_title" required>
              </div> 

              
              <div class="form-group">
                <label for="placetitle">Date</label>
                <input type='text' class="form-control"  name="date" id="datepicker" style="width: 350px;" required/>      
              </div>
        
              <script type="text/javascript">
                  $(document).ready(function () {
                      $('#datepicker').datepicker({
                        uiLibrary: 'bootstrap'
                      });
                  });
              </script>


              <div class="form-group">
                <label for="placedescription">News Description</label>
                <textarea class="form-control note" rows="5" id="" name="news_description" placeholder="Enter Description" required></textarea>
              </div>

              <div class="form-group">
                <label>Image Upload</label>
                <input type="file" id="uploadFile" name="userfile[]" multiple required/>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div>
 

            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Create News</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
