<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->

          <?php foreach ($services as $value) {
            $services_id = $value['services_id'];
            $services_name = $value['services_name'];
            $services_description = $value['services_description'];


          } ?>
          <!-- form start -->
          <form role="form" action="<?php echo base_url('services/updateservices/'.$services_id); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">
                <label for="services_name">Name</label>
                <input type="text" class="form-control" id="services_name" placeholder="Enter Service Name" name="services_name" value="<?php echo $services_name; ?>" required>
              </div>

              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control note" rows="5" name="services_description" placeholder="Enter Description for Services"><?php echo $services_description;?></textarea>
              </div>

              <div class="form-group">
                <?php
                if(isset($services_image) && is_array($services) && count($services)): $i=1;
                  foreach ($services_image as $key => $data) { 
                    ?>
                    <div class="imagelocation<?php echo $data['services_images_id'] ?>">
                      <br/>
                      <img src="<?php echo base_url(); ?>upload/services/<?php echo $data['services_images_name']; ?>" style="vertical-align:text-top;"  width="200" height="200">
                      <a href="" style="cursor:pointer;" data-imagedin-id="<?php echo $data['services_images_id']; ?>" class="label label-danger imagedinConfirm">X</a>
                    </div>
                    <?php }endif; ?>
                  </div>
                  <div class="form-group">
                    <label>Images</label>
                    <input type="file" name="userfile[]" id="image_file" accept=".png,.jpg,.jpeg,.gif" multiple>
                    <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                      Recomanded resolution for images is <strong>120 x 120</strong> pixels.
                    </p>
                  </div>   
                                              
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update Services Details</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
