<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      View Services
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">View Services</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- table box -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
             Services List
           </h3>
         </div><!-- /.box-header -->
         <div class="box-body no-padding">
          <?php echo $this->session->flashdata('msg'); ?>
          <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
          <table class="table table-condensed">
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>Options</th>
            </tr>
            <?php 
            $count = 0;
            foreach($services as $data) {
              ?>

              <tr>
                <td><?php echo ++$count; ?>.</td>
                <td><?php echo $data['services_name']; ?></td>
                <td>
                 <a href="" class="label label-primary" data-toggle="modal" data-target="#exampleModal<?php echo $data['services_id'];?>" data-whatever="@mdo">View</a>
                 <a href="<?php echo base_url(); ?>services/editservices/<?php echo $data['services_id']; ?>" class="label label-success">Edit</a>
                 <!--<a href="" data-services-id="<?php echo $data['services_id']; ?>" class="label label-danger servicesConfirm">Delete</a>-->
               </td>
             </tr> 
             <?php } ?>
           </tr> 

         </table>

       </div><!-- /.box-body -->
     </div><!-- /.box -->

   </div><!--/.col (full) -->
 </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php foreach ($services as $value) {;?>
<?php
$services_id = $value['services_id'];
$services_description = $value['services_description'];
$services_name=$value['services_name'];
?>
<div class="modal fade" id="exampleModal<?php echo $services_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="exampleModalLabel">Service Details</h3>
      </div>
      <div class="modal-body" style="max-height: 500px; overflow-y: auto;">
        <div class="form-group">
          <label>Name :</label>&nbsp;
          <?php echo $services_name;?>
        </div>
        <div class="form-group">
          <label>Description :</label>&nbsp;
          <?php echo $services_description;?>
        </div>
        <div class="form-group">
          <label>Images :</label>&nbsp;
          <?php
          foreach ($services_images as $key ) { 
            ?>
            <?php if($key['services_services_id']==$services_id){?>
            <img src="<?php echo base_url(); ?>upload/services/<?php echo $key['services_images_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
            <?php }?>
            <?php } ?>
          </div>
          <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php }?>