<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      View Best Practices
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">View Best Practices</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- table box -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
             Best Practices List
           </h3>
         </div><!-- /.box-header -->
         <div class="box-body no-padding">
          <?php echo $this->session->flashdata('msg'); ?>
          <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
          <table class="table table-condensed">
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>Options</th>
            </tr>
            <?php 
            $count = 0;
            foreach($best_practices as $data) {
              ?>

              <tr>
                <td><?php echo ++$count; ?>.</td>
                <td><?php echo $data['best_practices_name']; ?></td>
                <td>
                 <a href="" class="label label-primary" data-toggle="modal" data-target="#exampleModal<?php echo $data['best_practices_id'];?>" data-whatever="@mdo">View</a>
                 <a href="<?php echo base_url(); ?>practices/editpractices/<?php echo $data['best_practices_id']; ?>" class="label label-success">Edit</a>
                 <a href="" data-practices-id="<?php echo $data['best_practices_id']; ?>" class="label label-danger practicesConfirm">Delete</a>
               </td>
             </tr> 
             <?php } ?>
           </tr> 

         </table>

       </div><!-- /.box-body -->
     </div><!-- /.box -->

   </div><!--/.col (full) -->
 </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php foreach ($best_practices as $value) {;?>
<?php
$best_practices_id = $value['best_practices_id'];
$best_practices_name = $value['best_practices_name'];
$best_practices_description = $value['best_practices_description'];
?>
<div class="modal fade" id="exampleModal<?php echo $best_practices_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="exampleModalLabel">Best Practice Details</h3>
      </div>
      <div class="modal-body" style="max-height: 500px; overflow-y: auto;">
        <div class="form-group">
          <label>Name :</label>&nbsp;
          <?php echo $best_practices_name;?>
        </div>
        <div class="form-group">
          <label>Description :</label>&nbsp;
          <?php echo $best_practices_description;?>
        </div>
        <div class="form-group">
          <label>Images :</label>&nbsp;
          <?php
          foreach ($best_practices_images as $key ) { 
            ?>
            <?php if($key['best_practices_best_id']==$best_practices_id){?>
            <img src="<?php echo base_url(); ?>upload/practices/<?php echo $key['best_practices_images_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
            <?php }?>
            <?php } ?>
          </div>
          <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php }?>