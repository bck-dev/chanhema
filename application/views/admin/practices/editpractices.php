<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->

          <?php foreach ($best_practices as $value) {
            $best_practices_id = $value['best_practices_id'];
            $best_practices_description = $value['best_practices_description'];
            $best_practices_name = $value['best_practices_name'];

          } ?>
          <!-- form start -->
          <form role="form" action="<?php echo base_url('practices/updatepractices/'.$best_practices_id); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">
                <label for="best_practices_name">Name</label>
                <input type="text" class="form-control" id="best_practices_name" placeholder="Enter Name" name="best_practices_name" value="<?php echo $best_practices_name; ?>" required>
              </div>

              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="5" id="best_practices_description" name="best_practices_description" placeholder="Enter Description for Practices"><?php echo $best_practices_description;?></textarea>
              </div>

              <div class="form-group">
                <?php
                if(isset($best_practices_images) && is_array($best_practices) && count($best_practices)): $i=1;
                  foreach ($best_practices_images as $key => $data) { 
                    ?>
                    <div class="imagelocation<?php echo $data['best_practices_images_id'] ?>">
                      <br/>
                      <img src="<?php echo base_url(); ?>upload/practices/<?php echo $data['best_practices_images_name']; ?>" style="vertical-align:text-top;"  width="200" height="200">
                      <a href="" style="cursor:pointer;" data-imagepractice-id="<?php echo $data['best_practices_images_id']; ?>" class="label label-danger imagepracticeConfirm">X</a>
                    </div>
                    <?php }endif; ?>
                  </div>
                  <div class="form-group">
                    <label>Images</label>
                    <input type="file" name="userfile[]" id="image_file" accept=".png,.jpg,.jpeg,.gif" multiple>
                    <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                      Recomanded resolution for images is <strong>120 x 120</strong> pixels.
                    </p>
                  </div>   
                                              
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update Practices Details</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
