<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- table box -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
              <?php echo $pagetitle; ?>
            </h3>
          </div><!-- /.box-header -->
          <div class="box-body no-padding">
            <!-- flashdata -->
            <?php echo $this->session->flashdata('msg'); ?>
            <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
            <!-- End flashdata -->
            <table class="table table-condensed">
              <tr>
                <th style="width: 10px">#</th>
                <th>Material</th>
                <th>Options</th>
                <!--<th>Status</th>-->
              </tr>
             <?php
                $count = 0;
                foreach($sub_cat as $data) {
              ?>
              <tr>
                <td><?php echo ++$count; ?>.</td>
                <td><?php echo $data['sub_catagory']?></td>
                <td>
                  <a href="<?php echo base_url(); ?>SubCatagories/editSubcategory/<?php echo $data['sub_catagory_id']; ?>" class="label label-success">Edit</a>
                  <a href="<?php echo base_url(); ?>SubCatagories/deleteSubcategory/<?php echo $data['sub_catagory_id']; ?>" class="label label-danger">Delete</a>
                </td>
              </tr> 
              <?php } ?>                                        
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
