<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Get values from table -->

  <?php 
    $image=$gallery[0];
  ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('gallery/updategallery/').$image['image_id']; ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
              
              <div class="form-group">
                <label for="categoryname">Category Name</label>
                <input type="hidden" id="image" name="image" value="<?php echo $image['image']; ?>">
                
                <div class="image-group" style="position: relative; margin-top: 20px;">
                    <img src="<?php echo base_url(''); ?><?php echo $image['image']; ?>" class="thumbnail" style="width: 20%">
                    <span class="img-close">&#10006;</span>
                </div>
                <input type="file" id="image" name="gallery_image">

                <p class="help-block">Image size should be below then 300kb and JPG, JPEG, PNG, GIF can be uploaded.</p>
              </div>
              
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="5" id="description" name="description" placeholder="Enter Description for Gallery" required><?php echo $image['description']; ?></textarea>
              </div>

              <div class="form-group">
                <label>Section</label>
                <select class="form-control" name="section" required>
                    <option selected="selected"><?php echo $image['section']; ?></option>
                    <?php if($image['section']!="Beginning"): ?><option>Beginning</option><?php endif; ?>
                    <?php if($image['section']!="Expanding"): ?><option>Expanding</option><?php endif; ?>
                    <?php if($image['section']!="Contemporary"): ?><option>Contemporary</option><?php endif; ?>
                </select>
              </div>                          
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update image</button>
              <a class="btn btn-default" href="<?php echo base_url('gallery/viewgallery'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
