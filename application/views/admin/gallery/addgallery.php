<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('gallery/insertgallery'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
              

              <div class="form-group">
                <label>Image Upload</label>
                <input type="file" id="uploadFile" name="gallery_image"/>
                <p class="help-block">Image size should be below then 300kb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div>

              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="5" id="description" name="description" placeholder="Enter Description for Gallery" required></textarea>
              </div>

              <div class="form-group">
                <label>Section</label>
                <select class="form-control" name="section" required>
                    <option disabled="disabled" selected="selected">Select Section</option>
                    <option>Beginning</option>
                    <option>Expanding</option>
                    <option>Contemporary</option>
                </select>
              </div>

            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Save Gallery</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
          
        </div><!-- /.box -->
<div id="image_preview"></div>
      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script type="text/javascript">
  
  $("#uploadFile").change(function(){
     $('#image_preview').html("");
    
     var total_file=document.getElementById("uploadFile").files.length;

     for(var i=0;i<total_file;i++)
     {
      $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
     }

  });

  

</script>
