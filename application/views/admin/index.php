<style>
  .tileset1{
    z-index: 5 !important;
  }
  .tileset2{
    z-index: 4 !important;
  }
  .tileset3{
    z-index: 3 !important;
  }
  .tileset4{
    z-index: 2 !important;
  }
  .tileset5{
    position: relative;
    z-index: 1 !important;
  }
  
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
    </h1>

    <!-- Slider button -->
    <div class="container">
      <div>
       Sidebar View
       <label class="switch">
        <input type="checkbox" id="sidebarDisplay">
        <span class="slider round"></span>
      </label>
    </div>
  </div>
  <!-- End Slider button -->


  <ol class="breadcrumb">
    <li><a href="<?php echo base_url('admin'); ?>"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Info boxes -->
  <div class="row">

    <!-- main banner dropdown -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="tileset1 dropdown thumbnail tile tile-wide tile-blue">
        <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
          <h1>Main Banner</h1>
          <i class="far fa-3x fa-image"></i>
        </a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url('Mainbanner/addmainbanner'); ?>">Add New</a></li>
          <li><a href="<?php echo base_url('Mainbanner/viewmainbanner'); ?>">View All</a></li>
        </ul>
      </div>
    </div><!-- /.col -->
    <!-- End main banner dropdown -->

    <!-- Services dropdown -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <!-- Dropdown 2 -->
      <div class="tileset1 dropdown thumbnail tile tile-wide tile-pomegranate">
        <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
          <h1>Services</h1>
          <i class="fas fa-3x fa-tags"></i>
        </a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url('Services/addservices');?>">Add New</a></li>
          <li><a href="<?php echo base_url('Services/viewservices');?>">View All</a></li>
        </ul>
      </div>
    </div><!-- /.col -->
    <!-- End Services dropdown -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <!-- best practices dropdown -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <!-- Dropdown 3 -->
      <div class="tileset1 dropdown thumbnail tile tile-wide tile-green">
        <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
          <h1>Notification Bar</h1>
          <i class="far fa-3x fa-check-square"></i>
        </a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url('Notificationbar/editnotification');?>">Edit</a></li>
          <li><a href="<?php echo base_url('Notificationbar/viewnotification');?>">View All</a></li>
        </ul>
      </div>
    </div><!-- /.col -->
    <!-- End best practices dropdown -->

  </div><!-- /.row -->

  <!-- fix for small devices only -->
  <div class="clearfix visible-sm-block"></div>

  <div class="row">

    <!-- best practices dropdown -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <!-- Dropdown 1 -->
      <div class="tileset2 dropdown thumbnail tile tile-wide tile-green">
        <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
          <h1>Product Category</h1>
          <i class="far fa-3x fa-check-square"></i>
        </a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url('Category/viewcategory');?>">View All</a></li>
          <!--<li><a href="<?php echo base_url('Practices/viewpractices');?>">View All</a></li>-->
        </ul>
      </div>
    </div><!-- /.col -->
    <!-- End best practices dropdown -->

    <!-- category page dropdown -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <!-- Dropdown 2 -->
      <div class="tileset2 dropdown thumbnail tile tile-wide tile-orange">
        <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
          <h1>Product Material</h1>
          <i class="fas fa-3x fa-list"></i>
        </a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url('SubCatagories/addsubcatagory'); ?>">Add New</a></li>
          <li><a href="<?php echo base_url('SubCatagories/viewSubCatagory'); ?>">View All</a></li>
        </ul>
      </div>
    </div><!-- /.col -->
    <!-- End category page dropdown -->

    <!-- collection dropdown -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <!-- Dropdown 3 -->
      <div class="tileset2 dropdown thumbnail tile tile-wide tile-wet-asphalt">
        <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
          <h1>Collections</h1>
          <i class="fas fa-3x fa-sitemap"></i>
        </a>
        <ul class="dropdown-menu">
          <!--<li><a href="<?php echo base_url('collection/addcollection'); ?>">Add New</a></li>-->
          <li><a href="<?php echo base_url('collection/viewcollection'); ?>">View All</a></li>
        </ul>
      </div>
    </div><!-- /.col -->
    <!-- End collection dropdown -->

  </div><!-- /.row -->

  <!-- fix for small devices only -->
  <div class="clearfix visible-sm-block"></div>

  <div class="row">
   <!-- people dropdown -->
   <div class="col-md-4 col-sm-6 col-xs-12">
    <!-- Dropdown 2 -->
    <div class="tileset3 dropdown thumbnail tile tile-wide tile-lime">
      <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
        <h1>People</h1>
        <i class="fas fa-3x fa-users"></i>
      </a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo base_url('People/addpeople'); ?>">Add New</a></li>
        <li><a href="<?php echo base_url('People/viewpeople'); ?>">View All</a></li>
      </ul>
    </div>
  </div><!-- /.col -->
  <!-- End people dropdown -->

  <!-- stores dropdown -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <!-- Dropdown 3 -->
    <div class="tileset3 dropdown thumbnail tile tile-wide tile-purple">
      <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
        <h1>Stores</h1>
        <i class="fas fa-3x fa-shopping-cart"></i>
      </a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo base_url('Store/addstore'); ?>">Add New</a></li>
        <li><a href="<?php echo base_url('Store/viewstore'); ?>">View All</a></li>
      </ul>
    </div>
  </div><!-- /.col -->
  <!-- End stores dropdown -->
  <!-- stones dropdown -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <!-- Dropdown 1 -->
    <div class="tileset3 dropdown thumbnail tile tile-wide tile-pumpkin">
      <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
        <h1>Stones</h1>
        <i class="fas fa-3x fa-gem"></i>
      </a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo base_url('Stones/addstones'); ?>">Add New</a></li>
        <li><a href="<?php echo base_url('Stones/viewstones'); ?>">View All</a></li>
      </ul>
    </div>
  </div><!-- /.col -->
  <!-- End stones dropdown -->
    <!-- Product dropdown -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <!-- Dropdown 2 -->
    <div class="tileset4 dropdown thumbnail tile tile-wide tile-asbestos">
      <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
        <h1>Products</h1>
        <i class="fas fa-3x fa-shopping-bag"></i>
      </a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo base_url('Product/addproduct'); ?>">Add New</a></li>
        <li><a href="<?php echo base_url('Product/viewproduct'); ?>">View All</a></li>
      </ul>
    </div>
  </div><!-- /.col -->
  <!-- End product dropdown -->

  <!-- Faq category Section -->
   <div class="col-md-4 col-sm-6 col-xs-12">
    <!-- Dropdown 2 -->
    <div class="tileset4 dropdown thumbnail tile tile-wide tile-asbestos">
      <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
        <h1>Faq Category</h1>
        <i class="fas fa-3x fa-shopping-bag"></i>
      </a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo base_url('faq/addfaqcategory'); ?>">Add New</a></li>
        <li><a href="<?php echo base_url('faq/viewfaqcategory'); ?>">View All</a></li>
      </ul>
    </div>
  </div>
  
  <!-- FAq Section -->
  
    <div class="col-md-4 col-sm-6 col-xs-12">
    <!-- Dropdown 2 -->
    <div class="tileset4 dropdown thumbnail tile tile-wide tile-blue">
      <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
        <h1>Faq</h1>
        <i class="fas fa-3x fa-shopping-bag"></i>
      </a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo base_url('faq/addfaq'); ?>">Add New</a></li>
        <li><a href="<?php echo base_url('faq/viewfaq'); ?>">View All</a></li>
      </ul>
    </div>
  </div>
  
  <!-- news section -->
  <div class="col-md-4 col-sm-6 col-xs-12">
    <!-- Dropdown 2 -->
    <div class="tileset5 dropdown thumbnail tile tile-wide tile-asbestos">
      <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
        <h1>News</h1>
        <i class="fas fa-3x fa-shopping-bag"></i>
      </a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo base_url('News/addnews'); ?>">Add New</a></li>
        <li><a href="<?php echo base_url('News/viewnews'); ?>">View All</a></li>
      </ul>
    </div>
  </div>

  <div class="col-md-4 col-sm-6 col-xs-12">
    <!-- Dropdown 2 -->
    <div class="tileset5 dropdown thumbnail tile tile-wide tile-blue">
      <a href="#" class="fa-links dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
        <h1>Gallery</h1>
        <i class="fas fa-3x fa-image"></i>
      </a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo base_url('Gallery/addgallery'); ?>">Add New</a></li>
        <li><a href="<?php echo base_url('Gallery/viewgallery'); ?>">View All</a></li>
      </ul>
    </div>
  </div>

</div><!-- /.row -->

<input type="hidden" id="home_page" value="1"> <!-- to identify homepage -->

</section><!-- /.content -->
</div><!-- /.content-wrapper -->
