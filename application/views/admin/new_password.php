<!DOCTYPE html>
<html>
    <head>
    <title>Hemachandras</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/dist/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/dist/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/dist/vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/dist/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/dist/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/dist/vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/dist/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/dist/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/dist/css/main.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin'); ?>/dist/css/custom.css">
<!--===============================================================================================-->
</head>

<body class="loginPage">
    
    <div class="limiter">
        <div class="container-login100" style="
    background-image:url('../back2.jpg');
    height: 100%; background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
">
            <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
                <form class="login100-form validate-form" action='<?php echo site_url('reset/update') ?>' method='post' name='process'>
                     <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                    <span class="login100-form-title p-b-33">
                        <strong style="font-weight: 9500">Hemachandras</strong>
                        <br>
                       Password Reset
                    </span>

                    <?php echo $this->session->flashdata('msg'); ?>

                    <div class="form-group wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input class="form-control input100" type="password" name="password" placeholder="new Password" required>
                        <span class="focus-input100-1"></span>
                        <span class="focus-input100-2"></span>
                    </div>
                    <div class="form-group wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input class="form-control input100" type="password" name="passwordconfirm" placeholder="Confirm Password" required>
                        <span class="focus-input100-1"></span>
                        <span class="focus-input100-2"></span>
                         <input type="hidden" value="<?php echo $secret_code ?>" name="scode"/>
                        <input type="hidden" value="<?php echo $email ?>" name="email"/>
                    </div>

                    <div class="form-group container-login100-form-btn m-t-20">
                        <button class="login100-form-btn" type="submit">
                           Reset Password
                        </button>
                    </div>

                

                    <!-- <div class="text-center">
                        <span class="txt1">
                            Create an account?
                        </span>

                        <a href="#" class="txt2 hov1">
                            Sign up
                        </a>
                    </div> -->
                </form>
            </div>
        </div>
    </div>
    

    
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/admin'); ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/admin'); ?>/dist/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/admin'); ?>/dist/vendor/bootstrap/js/popper.js"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/admin'); ?>/dist/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/admin'); ?>/dist/vendor/daterangepicker/moment.min.js"></script>
    <script src="<?php echo base_url('assets/admin'); ?>/dist/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/admin'); ?>/dist/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/admin'); ?>/dist/js/main.js"></script>

</body>
</html>

























   <!--  -->







    <!-- <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="../../index2.html"><b>Medex</b></a>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">Reset Your Password</p>
                <form action='<?php //echo site_url('reset/update') ?>' method='post' name='process'>
                    <?php //echo validation_errors('<div class="alert alert-danger>", "</div>"'); ?>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Confirm Password" name="passwordconfirm">
                        <input type="hidden" value="<?php //echo $secret_code ?>" name="scode"/>
                        <input type="hidden" value="<?php //echo $email ?>" name="email"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>



                   -->