<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- table box -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
             Notification List
           </h3>
         </div><!-- /.box-header -->
         <div class="box-body no-padding">
          <?php echo $this->session->flashdata('msg'); ?>
          <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
          <table class="table table-condensed">
            <tr>
              <th style="width: 10px">#</th>
              <th>Title</th>
              <th>Status</th>
              <th>Options</th>
            </tr>
            <?php 
            $count = 0;
            foreach($notification_bar as $data) {
            ?>

              <tr>
                <td><?php echo ++$count; ?>.</td>
                <td><?php echo nl2br(trim(strip_tags($data['notification_bar_description']))); ?></td>
                <th>
                  <?php if($data['notification_bar_status']=='1'): ?>
                    <label class="label label-success">Activate</label>
                  <?php else: ?>
                    <label class="label label-danger">Deactivate</label>
                  <?php endif; ?>
                </th>
                <td>
                 <a href="<?php echo base_url(); ?>notificationbar/editnotification/<?php echo $data['notification_bar_id']; ?>" class="label label-success">Edit</a>
                 <!--<a href="" data-notification-id="<?php echo $data['notification_bar_id']; ?>" class="label label-danger notificationConfirm">Delete</a>-->
               </td>
             </tr> 
             <?php } ?>
           </tr> 

         </table>

       </div><!-- /.box-body -->
     </div><!-- /.box -->

   </div><!--/.col (full) -->
 </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->