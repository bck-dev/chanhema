<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
         <?php foreach ($notification_bar as $value) {
            $notification_id = $value['notification_bar_id'];
            $notification_text = $value['notification_bar_description'];
            $notification_status = $value['notification_bar_status'];

          } ?>
          <!-- form start -->
         <form role="form" action="<?php echo base_url('notificationbar/updatnotification/'.$notification_id); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">
                <label>Notification Text</label>
                <textarea class="form-control note" rows="3" name="notofocation_description" placeholder="Enter Notification" required><?php echo $notification_text; ?></textarea>
              </div> 

              <div class="form-group">
                <label for="main_banner_link">Notification Status</label>
                <br/>
                <label class="radio-inline"><input type="radio" name="notification_radio" value="1" <? echo ($notification_status==1) ? 'checked' : '';?> >
                  Activate
                </label>
                <label class="radio-inline"><input type="radio" name="notification_radio" value="0" <? echo ($notification_status==0) ? 'checked' : '';?>>
                  Deactivate
                </label>
              </div>                                       
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update Notification</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
