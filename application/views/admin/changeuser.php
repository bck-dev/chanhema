<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <?php foreach($user_details as $user): ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('admin/updatesetting').'/'.$user['user_id']; ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
              
              <div class="form-group">
                <label>User Name</label>
                <input type="text" class="form-control" id="user_name" placeholder="Enter user's name" required  name="user_name" value="<?php echo $user['user_name'];?>">
              </div>

              <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" id="user_email" placeholder="Enter user's email address" required  name="user_email" value="<?php echo $user['email'];?>">
              </div>

              <div class="form-check">
                <input type="checkbox" class="form-check-input" id="passwordCheck" name="user_password_checkbox">
                <label class="form-check-label" for="passwordCheck"> Enable Change Password</label>
              </div>

              <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control userPassword" id="user_password" value="password" required  name="user_password" readonly >
              </div>

              <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="form-control userPassword" id="user_confirm_password" value="password" required  name="user_confirm_password" readonly >
              </div>

              <!-- <div class="form-group">
                <label for="placecategory">Category</label>
                <select class="form-control" id="user_category" name="user_category" required>
                  <option value="">-- Select User Category --</option>
                  <option value="1" <?php echo $user['user_type']==1 ? 'selected' : '' ;?>>Administrater (Can add/edit/delete all the features in the website)</option>
                  <option value="2" <?php echo $user['user_type']==2 ? 'selected' : '' ;?>>Editor (Can only add/edit the features in the website)</option>
                </select>
              </div> -->

              <input type="hidden" name="user_category" value="1"> <!-- user category 1-Admin -->

            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update User</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
  <?php endforeach; ?>
</div><!-- /.content-wrapper -->
