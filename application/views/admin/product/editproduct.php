<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          
          <?php foreach ($product as $value) {
            $product_id = $value['product_id'];
            $product_name = $value['product_name'];
            $product_description = $value['product_description'];
            $product_matterials = $value['product_matterials'];
            $product_journey = $value['product_journey'];
            $product_catagory = $value['product_catagory'];
            $product_sub_catagory = $value['product_sub_catagory'];

          } ?>
          <form role="form" action="<?php echo base_url('product/updateproduct/'.$product_id); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">
                <label for="product_name">Product Name</label>
                <input type="text" class="form-control" id="product_name" placeholder="Enter Product Name" name="product_name" required value="<?php echo $product_name;?>">
              </div> 
              
              <!-- <div class="form-group d-none">
                <label>Product Description</label>
                <textarea class="form-control note" rows="5" id="product_description" name="product_description" placeholder="Enter Description for Product"><?php echo $product_description; ?></textarea>
              </div>

              <div class="form-group d-none">
                <label>Product Materials</label>
                <textarea class="form-control note" rows="5" id="product_matterials" name="product_matterials" placeholder="Enter Description for Product Materials"><?php echo $product_matterials; ?></textarea>
              </div>

              <div class="form-group d-none">
                <label>Product Journey</label>
                <textarea class="form-control note" rows="5" id="product_journey" name="product_journey" placeholder="Enter Description for Product Journey"><?php echo $product_journey;?></textarea>
              </div> -->
              
              <div class="form-group">
                <?php
                if(isset($product_images) && is_array($product) && count($product)): $i=1;
                foreach ($product_images as $key => $data) { 
                  ?>
                  <div class="image<?php echo $data['product_images_id'] ?>">
                    <br/>
                    <img src="<?php echo base_url(); ?><?php echo $data['product_images_name']; ?>" style="vertical-align:text-top;"  width="200" height="200">
                    <div style="cursor:pointer;" id="close<?php echo $data['product_images_id'] ?>" class="label label-danger">X</div>
                  </div>
                <?php }endif; ?>
              </div>
              <div class="form-group">
                <label>Images</label>
                <input type="file" name="userfile[]" id="image_file" accept=".png,.jpg,.jpeg,.gif" multiple>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                  Recomanded resolution for images is <strong>120 x 120</strong> pixels.
                </p>
              </div>   

              <div class="form-group">
                <label>Product Category</label>
                <select class="form-control" name="product_catagory" required> 
                  <option value="">(None)</option>
                  <?php foreach($categories as $data):?>
                    <option value="<?php echo $data['category_id']?>" <?php echo $data['category_id']==$product_catagory ? 'selected' : '' ;?>><?php echo $data['category_name']; ?></option> 
                  <?php endforeach;?>
                </select>
              </div> 

              <div class="form-group">
                <label>Product Sub Category</label>
                <select class="form-control" id="sub_catagory1" name="product_sub_catagory" required > 
                  <option value="">(None)</option>
                  <?php foreach($sub_categories as $data):?>
                   <option value="<?php echo $data['sub_catagory_id']?>" class="<?php echo "aa".$data['catagory_id']."aa";?>" <?php echo $data['sub_catagory_id']==$product_sub_catagory ? 'selected' : '' ;?>><?php echo $data['sub_catagory']; ?></option>
                 <?php endforeach;?>
               </select>
             </div> 

             <div class="form-group">
              <label>Collections</label>
              <select class="form-control" id="collection" name="collection_page[]" multiple="multiple"> 
                <option value="">(None)</option>
                <?php foreach($collections as $c):?>
                  <option value="<?php echo $c->collection_name?>" <?php if($c->selected=="yes"): echo "selected"; endif; ?>>
                    <?php echo $c->collection_name;?>
                  </option> 
                <?php endforeach;?>
              </select>
            </div>
            
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Update Products</button>
            <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
          </div>
        </form>
      </div><!-- /.box -->

    </div><!--/.col (full) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->



<?php
  if(isset($product_images) && is_array($product) && count($product)): $i=1;
  foreach ($product_images as $key => $data): ?>
  <script>
    $(document).ready(function() {
      $("#close<?php echo $data['product_images_id']; ?>").click(function() {
          $('.image<?php echo $data['product_images_id']; ?>').hide();
          var path = "<?php echo base_url(); ?>" + "product/deleteImage";
          console.log(path);
          $.ajax({
            url: path, 
            type: 'post',
            data: {'id':<?php echo $data['product_images_id']; ?>},
            success: function(msg) {
              console.log(msg);
            }
          });
      });
    });
  </script>
<?php endforeach; endif; ?>
