<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('product/insertproduct'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>

              <div class="form-group">
                <label for="product_name">Product Name</label>
                <input type="text" class="form-control" id="product_name" placeholder="Enter Product Name" name="product_name" required>
              </div> 
              
              <!-- <div class="form-group d-none">
                <label>Product Description</label>
                <textarea class="form-control note" rows="5" id="product_description" name="product_description" placeholder="Enter Description for Product"></textarea>
              </div>

              <div class="form-group d-none">
                <label>Product Materials</label>
                <textarea class="form-control note" rows="5" id="product_matterials" name="product_matterials" placeholder="Enter Description for Product Materials"></textarea>
              </div>

              <div class="form-group d-none">
                <label>Product Journey</label>
                <textarea class="form-control note" rows="5" id="product_journey" name="product_journey" placeholder="Enter Description for Product Journey"></textarea>
              </div> -->
              
              <div class="form-group">
                <label>Product Images</label>
                <input type="file" name="userfile[]" required id="image_file" accept=".png,.jpg,.jpeg,.gif" multiple required>
                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be uploaded.<br/>
                  Recomanded resolution for images is <strong>400 x 600</strong> pixels.
                </p>
              </div>    

              <div class="form-group">
                <label>Product Category</label>
                <select class="form-control" name="product_catagory" required> 
                  <option value="">(None)</option>
                  <?php foreach($category as $data):?>
                    <option value="<?php echo $data['category_id']?>"><?php echo $data['category_name'];?></option> 
                  <?php endforeach;?>
                </select>
              </div> 

              <div class="form-group">
                <label>Product Sub Category</label>
                <select class="form-control" id="sub_catagory" name="product_sub_catagory" required> 
                  <option value="">(None)</option>
                  <?php foreach($sub_catagory as $data):?>
                    <option value="<?php echo $data['sub_catagory_id']?>" class="<?php echo "aa".$data['catagory_id']."aa";?>"><?php echo $data['sub_catagory'];?></option> 
                  <?php endforeach;?>
                </select>
              </div>

              <hr>

            <div class="form-group">
              <label>Collections</label>
              <select class="form-control" id="collection" name="collection_page[]" multiple="multiple"> 
                <option value="">(None)</option>
                <?php foreach($collections as $data):?>
                  <option value="<?php echo $data['collection_name']?>"><?php echo $data['collection_name'];?></option> 
                <?php endforeach;?>
              </select>
            </div>      
              
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Add Product</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
