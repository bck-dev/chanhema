<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      View Products
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">View Products</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- table box -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
             Product List
           </h3>
           <!-- search dropdown -->
           
           <div class="form-group">
            <form action="<?php echo base_url('product/search'); ?>" method="POST" >
              <div class="row">
                <div class="col-md-3">
                <label >Product Category</label>
                  <select class="form-control" id="category_search" name="category_search"> 
                    <option value="0">(All)</option>
                    <?php foreach($category as $cat_data):?>
                      <option value="<?php echo $cat_data['category_id']?>"><?php echo $cat_data['category_name'];?></option> 
                    <?php endforeach;?>
                  </select>
                </div>
                <div class="col-md-3">
                <label>Collection page</label>
                  <select class="form-control" id="collection_search" name="collection_search"> 
                    <option value="0">(All)</option>
                    <?php foreach($collection as $col_data):?>
                      <option value="<?php echo $col_data['collection_name']?>"><?php echo $col_data['collection_name'];?></option> 
                    <?php endforeach;?>
                  </select>
                </div>
                
                <div class="col-md-2">
                  <label>&nbsp;&nbsp;</label>
                  <button type="submit" class="form-control btn btn-warning" type="button">Search</button>
                </div>
              </div>
            </form>
           </div>
           <!-- End search dropdown -->

         </div><!-- /.box-header -->
         <div class="box-body no-padding">
          <?php echo $this->session->flashdata('msg'); ?>
          <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
          <table class="table table-condensed" id="navTable">
            <tr>
              <th style="width: 10px">#</th>
              <th>Image</th>
              <th>Name</th>
              <th>Product Category</th>
              <th>Product Sub Category</th>
              <th>Collection page</th>
              <th>Option</th>
            </tr>
            <!-- Defining variables -->
            <?php 
              $product_catagory='';
              $product_sub_catagory='';
              $collection_page_main='';
              $collection_page_sub='';
            ?>
            <!-- End defining variables -->
            <?php 
            $count = 0;
            foreach($products as $data) {
              //product_catagory
              foreach ($category as $cat) {
                if($data['product_catagory']==$cat['category_id']){
                  $product_catagory = $cat['category_name'];
                  break;
                }
                else{
                  $product_catagory = '_';
                }
              }
              //End product_catagory
              
              //start product sub category 
              
              foreach ($sub_catagory as $subcat) {
                if($data['product_sub_catagory']==$subcat['sub_catagory_id']){
                  $product_sub_catagory = $subcat['sub_catagory'];
                  break;
                }
                else{
                  $product_sub_catagory = '_';
                }
              }
              
              //end product sub category

            ?>

              <tr>
                <td><?php echo ++$count; ?>.</td>
                <td>
                 <?php foreach ($product_images as $key ) { ?>
                  <?php if($key['product_product_id']==$data['product_id']){?>
                    <img src="<?php echo base_url(); ?><?php echo $key['product_images_name']; ?>" style="vertical-align:text-top;height: 100px;width: 100px;">
                  <?php break; }?>
                <?php } ?>
              </td>
              <td><?php echo $data['product_name']; ?></td>
              <td><?php echo $product_catagory; ?></td>
              <td><?php echo $product_sub_catagory; ?></td>
              <td><?php echo $data['collection_page']; ?></td>
              <td>
               <a href="" class="label label-primary" data-toggle="modal" data-target="#exampleModal<?php echo $data['product_id'];?>" data-whatever="@mdo">View</a>
               <a href="<?php echo base_url(); ?>product/editproduct/<?php echo $data['product_id']; ?>" class="label label-success">Edit</a>
               <a href="<?php echo base_url(); ?>product/deleteproduct/<?php echo $data['product_id']; ?>" class="label label-danger">Delete</a>
             </td>
           </tr> 
         <?php } ?>
       </tr> 

     </table>

   </div><!-- /.box-body -->
 </div><!-- /.box -->

</div><!--/.col (full) -->
</div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php foreach ($products as $value) {;?>
  <?php
  $product_id = $value['product_id'];
  $product_name = $value['product_name'];
  $product_description = $value['product_description'];
  $product_matterials = $value['product_matterials'];
  $product_journey = $value['product_journey'];
  $product_catagory = $value['product_catagory'];
  $product_sub_catagory = $value['product_sub_catagory'];
  $collection_page = $value['collection_page'];
  $collection_sub_page = $value['collection_sub_page'];
  ?>
  <div class="modal fade" id="exampleModal<?php echo $product_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title" id="exampleModalLabel">Products Details</h3>
        </div>
        <div class="modal-body" style="max-height: 500px; overflow-y: auto;">
          <div class="form-group">
            <label>Product Name :</label>&nbsp;
            <?php echo $product_name;?>
          </div>
          <div class="form-group">
            <label>Product Description :</label>&nbsp;
            <?php echo $product_description;?>
          </div>
          <div class="form-group">
            <label>Product Matterials :</label>&nbsp;
            <?php echo $product_matterials;?>
          </div>
          <div class="form-group">
            <label>Product Journey :</label>&nbsp;
            <?php echo $product_journey;?>
          </div>
          <div class="form-group">
            <label>Product Category :</label>&nbsp;
            <?php foreach($category1 as $cate){?>
              <?php if($product_catagory==$cate['category_id']){
                echo $cate['category_name'];
              }?>
            <?php } ?>
          </div>
          <div class="form-group">
            <label>Product Sub Category :</label>&nbsp;
            <?php foreach($sub_catagory as $subcate){?>
              <?php if($product_sub_catagory==$subcate['sub_catagory_id']){
                echo $subcate['sub_catagory'];
              }?>
            <?php } ?>
          </div>
          <div class="form-group">
            <label>Collection page :</label>&nbsp;
            <?php foreach($celebrate1 as $collect){?>
              <?php if($collection_page==$collect['celebrate_id']){
                echo $collect['celebrate_name'];
              }?>
            <?php } ?>
          </div>
          <div class="form-group">
            <label>Images :</label>&nbsp;
            <?php
            foreach ($product_images as $key ) { 
              ?>
              <?php if($key['product_product_id']==$product_id){?>
                <img src="<?php echo base_url(); ?><?php echo $key['product_images_name']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
              <?php }?>
            <?php } ?>
          </div>
          <br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <?php }?>