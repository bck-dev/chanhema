<?php if(!empty($products)){ ?>
  <table class="table table-condensed" id="navTable">
    <tr>
      <th style="width: 10px">#</th>
      <th>Image</th>
      <th>Name</th>
      <th>Category</th>
      <th>Collection</th>
      <th>Celebrate</th>
      <th>Option</th>
    </tr>
    <!-- Defining variables -->
    <?php 
    $product_catagory='';
    $product_sub_catagory='';
    $collection_page_main='';
    $collection_page_sub='';
    ?>
    <!-- End defining variables -->
    <?php 
    $count = 0;
    foreach($products as $data) {
              //get category
      foreach ($category1 as $cat) {
        if($data['product_catagory']==$cat['category_id']){
          $product_catagory = $cat['category_name'];
          break;
        }
        else{
          $category = '_';
        }
      }
              //End get category

              //start product sub category 
              
              foreach ($sub_catagory as $subcat) {
                if($data['product_sub_catagory']==$subcat['sub_catagory_id']){
                  $product_sub_catagory = $subcat['sub_catagory'];
                  break;
                }
                else{
                  $product_sub_catagory = '_';
                }
              }
              
              //end product sub category

              //get collection
      foreach ($celebrate as $col) {
        if($data['collection_page']==$col['celebrate_id']){
          $collection_page_main = $col['celebrate_name'];
          break;
        }
        else{
          $collection_page_main = '_';
        }
      }
              //End get collection
    
    //get sub collection
      foreach ($collection_page_sub_catagory as $subcol) {
        if($data['collection_sub_page']==$subcol['page_sub_catagory_id']){
          $collection_page_sub = $subcol['page_catagory_name'];
          break;
        }
        else{
          $collection_page_sub = '_';
        }
      }
              //End get sub collection
              
              
      ?>

      <tr>
        <td><?php echo ++$count; ?>.</td>
        <td>
         <?php foreach ($product_images as $key ) { ?>
          <?php if($key['product_product_id']==$data['product_id']){?>
            <img src="<?php echo base_url(); ?>upload/product/<?php echo $key['product_images_name']; ?>" style="vertical-align:text-top;height: 100px;width: 100px;">
            <?php break; }?>
          <?php } ?>
        </td>
        <td><?php echo $data['product_name']; ?></td>
        <td><?php echo $product_catagory; ?></td>
        <td><?php echo $product_sub_catagory; ?></td>
        <td><?php echo $collection_page_main; ?></td>
        <td><?php echo $collection_page_sub; ?></td>
        <td>
         <a href="" class="label label-primary" data-toggle="modal" data-target="#exampleModal<?php echo $data['product_id'];?>" data-whatever="@mdo">View</a>
         <a href="<?php echo base_url(); ?>product/editproduct/<?php echo $data['product_id']; ?>" class="label label-success">Edit</a>
         <a href="" data-product-id="<?php echo $data['product_id']; ?>" class="label label-danger productConfirm">Delete</a>
       </td>
     </tr> 
   <?php } ?>
 </tr>
</table>
<?php } else{ ?>
  <tr>
    <th colspan="6" style="text-align: center; "><h3>No data found !</h3></th>
  </tr>
  <?php } ?>