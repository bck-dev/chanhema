<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Get values from table -->
  <?php foreach ($category as $data) {
      $category_id = $data['category_id'];
      $category_name = $data['category_name'];
      
  } ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('category/updatecategory/').$category_id; ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
              <div class="form-group">
                <label for="categoryname">Category Name</label>
                <input type="text" class="form-control" id="categoryname" placeholder="Enter Category Name" name="categoryname" value="<?php echo $category_name;?>" required>
              </div>
              
              <!-- Essential element for fontawsome font picker -->
              <button class="btn btn-danger action-destroy" style="display: none;">Destroy instances</button>
              <button class="btn btn-default action-create" style="display: none;">Create instances</button>  
              <!--End Essential element for fontawsome font picker -->                              
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update Category</button>
              <a class="btn btn-default" href="<?php echo base_url('category/viewcategory'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
