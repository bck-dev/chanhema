<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            View People
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">View People</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- table box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">
                            People List
                        </h3>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                        <?php echo $this->session->flashdata('msg'); ?>
                        <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                        <table class="table table-condensed">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Image</th>
                                <th>Full Name</th>
                                <th>Category</th>
                                <th>Options</th>
                            </tr>
                            <?php
                            $count = 0;
                            foreach($people as $data) {
                                ?>

                                <tr>
                                    <td><?php echo ++$count; ?>.</td>
                                    <td><img src="<?php echo base_url()?><?php echo $data['people_image']; ?>" height="100px" width="100px" ></td>
                                    <td><?php echo $data['people_first_name'].' '.$data['people_last_name'] ; ?></td>
                                    <td><?php  echo $data['people_category'];?></td>
                                    <td>
                                        <a href="" class="label label-primary" data-toggle="modal" data-target="#exampleModal<?php echo $data['people_id'];?>" data-whatever="@mdo">View</a>
                                        <a href="<?php echo base_url(); ?>people/editpeople/<?php echo $data['people_id']; ?>" class="label label-success">Edit</a>
                                        <a href="<?php echo base_url(); ?>people/deletepeople/<?php echo $data['people_id']; ?>" class="label label-danger">Delete</a>
                                    </td>
                                </tr>
                                    <?php } ?>


                                </table>

                            </div><!-- /.box-body -->
                        </div><!-- /.box -->

                    </div><!--/.col (full) -->
                </div>   <!-- /.row -->
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <?php foreach ($people as $value) { ?>
            <div class="modal fade" id="exampleModal<?php echo $value['people_id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="exampleModalLabel">People Details</h3>
                </div>
                <div class="modal-body" style="max-height: 500px; overflow-y: auto;">
                    <div class="form-group">
                      <label></label>&nbsp;
                      <img src="<?php echo base_url()?>upload/people/<?php echo $value['people_image']; ?>" style="vertical-align:text-top;height: 100px; margin-left: 3px;">
                  </div>
                  <div class="form-group">
                      <label>First Name :</label>&nbsp;
                      <?php echo $value['people_first_name'];?>
                  </div>
                  <div class="form-group">
                      <label>Last Name :</label>&nbsp;
                      <?php echo $value['people_last_name'];?>
                  </div>
                  <div class="form-group">
                      <label>Age :</label>&nbsp;
                      <?php echo $value['people_age'];?>
                  </div>
                  <div class="form-group">
                      <label>Address :</label>&nbsp;
                      <?php echo $value['people_address'];?>
                  </div>
                  <div class="form-group">
                      <label>Contact No. :</label>&nbsp;
                      <?php echo $value['people_contact_number'];?>
                  </div>
                  <div class="form-group">
                      <label>Email :</label>&nbsp;
                      <?php echo $value['people_email'];?>
                  </div>
                  <div class="form-group">
                      <label>Category :</label>&nbsp;
                      <?php if($data['people_category']==0){
                                echo 'guest designer';} 
                            else{
                                echo 'our talent';}; 
                        ?>
                  </div>
                  <div class="form-group">
                      <label>Description :</label>&nbsp;
                      <?php echo $value['people_description'];?>
                  </div>
                  <br>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php }?>

