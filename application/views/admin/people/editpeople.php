<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $pagetitle; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $pagetitle; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $pagetitle; ?></h3>
                    </div><!-- /.box-header -->

                    <?php foreach ($people as $value) {
                        $people_id = $value['people_id'];
                        $people_image = $value['people_image'];
                        $people_fname = $value['people_first_name'];
                        $people_lname = $value['people_last_name'];
                        $people_age = $value['people_age'];
                        $people_address = $value['people_address'];
                        $people_telephone = $value['people_contact_number'];
                        $people_email = $value['people_email'];
                        $people_description = $value['people_description'];
                        $people_category = $value['people_category'];


                    } ?>
                    <!-- form start -->
                    <form role="form" action="<?php echo base_url('people/updatepeople/'.$people_id); ?>" method="POST" enctype='multipart/form-data'>
                        <div class="box-body">
                            <?php echo $this->session->flashdata('msg'); ?>
                            <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                            <div class="form-group">
                                <label for="main_banner_image">People Image</label>
                                <input type="hidden" id="people_image" name="people_image"
                                       value="<?php echo $people_image; ?>">
                                <div class="image-group" style="position: relative; margin-top: 20px;">
                                    <img src="<?php echo base_url(); ?>/<?php echo $people_image; ?>"
                                         class="thumbnail" alt="<?php echo $people_image; ?>" style="width: 20%">
                                    <span class="img-close">&#10006;</span>
                                </div>
                                <input type="file" id="input-image" name="fileinput">

                                <p class="help-block">Image size should be below then 3mb and JPG, JPEG,PNG, GIF can be
                                    uploaded.</p>
                            </div>
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" id="people_fname" placeholder="Enter First Name for People" required  name="people_fname" value="<?php echo $people_fname;?>">
                            </div>

                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" id="people_lname" placeholder="Enter Last Name for People" required  name="people_lname" value="<?php echo $people_lname;?>">
                            </div>

                            <!-- <div class="form-group">
                                <label>Age</label>
                                <input type="text" class="form-control" id="people_age" placeholder="Enter Age for People" required  name="people_age" value="<?php echo $people_age;?>">
                            </div>

                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" id="people_address" placeholder="Enter Address for People" required  name="people_address" value="<?php echo $people_address;?>">
                            </div>

                            <div class="form-group">
                                <label>Contact Number</label>
                                <input type="text" class="form-control"  id="telephone" placeholder="Enter Contact number for People" required  name="telephone" value="<?php echo $people_telephone;?>">
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control"   id="people_email" placeholder="Enter Email for People" required  name="people_email" value="<?php echo $people_email;?>">
                            </div> -->

                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control note" rows="5" id="people_description" name="people_description" placeholder="Enter Description for People"><?php echo $people_description;?></textarea>
                            </div>

                            <div class="form-group">
                                <label>Title</label>
                                <select class="form-control" name="people_category" required>
                                    <option <?php if($people_category=="Master Craftsman"): echo 'selected="selected"'; endif; ?>>Master Craftsman</option>
                                </select>
                            </div>

                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Update People Details</button>
                            <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (full) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
