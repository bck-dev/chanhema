<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $pagetitle; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $pagetitle; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $pagetitle; ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="<?php echo base_url('people/insertpeople'); ?>" method="POST" enctype='multipart/form-data'>
                        <div class="box-body">
                            <?php echo $this->session->flashdata('msg'); ?>
                            <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                            <div class="form-group">
                                <label for="main_banner_image">People Image</label>
                                <input type="file" id="people_image" name="people_image" required>
                            </div>

                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" id="people_fname" placeholder="Enter First Name for People" required  name="people_fname">
                            </div>

                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" id="people_lname" placeholder="Enter Last Name for People" required  name="people_lname">
                            </div>

                            <!-- <div class="form-group">
                                <label>Age</label>
                                <input type="text" class="form-control" id="people_age" placeholder="Enter Age for People"   name="people_age">
                            </div>

                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" id="people_address" placeholder="Enter Address for People"   name="people_address">
                            </div>

                            <div class="form-group">
                                <label>Contact Number</label>
                                <input type="text" class="form-control" id="telephone" placeholder="Enter Contact number for People"   name="telephone">
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" id="people_email" placeholder="Enter Email for People"   name="people_email">
                            </div> -->

                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control note" rows="5" id="people_description" name="people_description" placeholder="Enter Description for People" required></textarea>
                            </div>

                            <div class="form-group">
                                <label>Title</label>
                                <select class="form-control" name="people_category" required>
                                    <option disabled="disabled" selected="selected">Select Title</option>
                                    <option>Master Craftsman</option>
                                </select>
                            </div>

                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Add People Details</button>
                            <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (full) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
