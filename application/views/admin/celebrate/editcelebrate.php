<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $pagetitle; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $pagetitle; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $pagetitle; ?></h3>
                    </div><!-- /.box-header -->
                    <?php foreach ($celebrate as $value) {
                        $celebrate_id = $value['celebrate_id'];
                        $celebrate_name= $value['celebrate_name'];

                    } ?>
                    <!-- form start -->
                    <form role="form" action="<?php echo base_url('Celebrate/updatecelebrate/'. $celebrate_id); ?>" method="POST" enctype='multipart/form-data'>
                        <div class="box-body">
                            <?php echo $this->session->flashdata('msg'); ?>
                            <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>


                            <div class="form-group">
                                <label for="home_page_event_title">Add Celebrate Name</label>
                                <input type="text" class="form-control" id="elebrate_name" placeholder="Enter Celebrate Name" name="celebrate_name" value="<?php echo $celebrate_name; ?>">
                            </div>


                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Update Celebrate</button>
                            <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (full) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
