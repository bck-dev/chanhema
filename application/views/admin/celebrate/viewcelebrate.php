<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            View Celebrate
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">View Celebrate</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- table box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">
                            Celebrate List
                        </h3>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-condensed">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Event Name</th>
                                <th>Option</th>
                            </tr>
                            <!--get data in viewhomeslider function using foreach-->
                            <?php
                            $count = 0;
                            foreach($celebrate_name as $data) {
                                ?>
                                <tr>
                                    <td><?php echo ++$count; ?>.</td>
                                    <td><?php echo $data['celebrate_name'];?></td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>celebrate/editcelebrate/<?php echo $data['celebrate_id']; ?>" class="label label-success">Edit</a>
                                        <a href="" data-maincelebrate-id="<?php echo $data['celebrate_id']; ?>" class="label label-danger celebrateConfirm">Delete</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!--/.col (full) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
