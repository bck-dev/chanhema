<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('Celebrate/insertpageSubCatagory'); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                
              <div class="form-group">
                <label for="home_page_event_title">Select Collection Page Catagory</label>
                <select class="form-control" id="catagory" name="pageCatagory" id="Catagory">
                  <option value="">-- Select Catagory --</option>
                   <?php foreach($celebrate as $cel): ?>
                   <option value="<?php echo $cel['celebrate_id']; ?>"><?php echo $cel['celebrate_name']; ?></option>
                   <?php endforeach; ?>
                </select>
             </div>
             
              <div class="form-group">
                <label for="home_page_event_title">Add Collection Sub Catagory</label>
                <input type="text" class="form-control" id="collection_name" placeholder="Enter Collection Sub Catagory" name="sub_catagory_name">
              </div>
             
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Create Collection Sub Catagory</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div>