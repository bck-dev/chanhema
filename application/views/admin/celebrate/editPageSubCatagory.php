<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Get values from table -->
  <?php foreach($pageSubCatagory as $data) {
      $sub_cat_id = $data['page_sub_catagory_id'];
      $sub_category_name = $data['page_catagory_name'];
      $catagory_id = $data['page_catagory_id'];
      
  } ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('celebrate/updateSubCatagory/').$sub_cat_id; ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
              
              <div class="form-group">
                <label for="categoryname">Category Name</label>
                <select class="form-control" name="categoryname_edit" id="">
                    <option value="">-- Select Catagory --</option>
                    <?php foreach($celebrate_name as $cat_edit): ?>
                        <option value="<?php echo $cat_edit['celebrate_id']; ?>" <?php echo ($catagory_id == $cat_edit['celebrate_id']) ? 'selected' : ''; ?>><?php echo $cat_edit['celebrate_name'] ?></option>
                    <?php endforeach; ?>
                </select>  
            </div>
              
              <div class="form-group">
                <label for="categoryname">Sub Category Name</label>
                <input type="text" class="form-control" id="categoryname" placeholder="Enter Category Name" name="sub_catagory_edit" value="<?php echo $sub_category_name;?>" required>
              </div>
              
              <!-- Essential element for fontawsome font picker -->
              <button class="btn btn-danger action-destroy" style="display: none;">Destroy instances</button>
              <button class="btn btn-default action-create" style="display: none;">Create instances</button>  
              <!--End Essential element for fontawsome font picker -->                          
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update Category</button>
              <a class="btn btn-default" href="<?php echo base_url('category/viewcategory'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
