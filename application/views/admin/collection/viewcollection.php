<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      View Collection
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">View Collection</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- table box -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
              Collection List
            </h3>
            <?php echo $this->session->flashdata('msg'); ?>
            <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
             
          </div><!-- /.box-header -->
          <div class="box-body no-padding">
            <table class="table table-condensed">
              <tr>
                <th style="width: 10px">#</th>
                <th>Collections</th>
                <th>Banner Image</th>
                <th>Options</th>
              </tr>
             <?php $count = 0; foreach($collections as $collection): ?>
              <tr>
                <td><?php echo ++$count; ?>.</td>
                <td>
                  <?php echo $collection['collection_name']; ?>
                </td>
                
                <td><img src="<?php echo base_url()?><?php echo $collection['banner_image']; ?>" width="200" ></td>
                
                <td>
                  <a href="" data-inquiry-id="<?php echo  $collection['collection_id']; ?>" data-action-id="3" data-toggle="modal" data-target="#vehicleModal<?php echo $collection['collection_id'];?>" class="label label-primary">view</a>
                  <a href="<?php echo base_url(); ?>admin/collection/editcollection/<?php echo $collection['collection_id']; ?>" class="label label-success">Edit</a>
                  <a href="<?php echo base_url(); ?>admin/collection/deletecollection/<?php echo $collection['collection_id']; ?>" class="label label-danger">Delete</a>
                </td>
              </tr> 
                      <?php endforeach; ?>                                        
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php foreach($collections as $data):
    $collection_id = $data['collection_id'];

?>
  <div class="modal fade" id="vehicleModal<?php echo $data['collection_id'];?>" role="dialog">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="exampleModalLabel">Collection Details</h3>
      </div>
      <div class="modal-body" style="max-height: 500px; overflow-y: auto;">
        <div class="form-group">
          <label>Collection Name :</label>&nbsp;
          <?php echo $data['collection_name']; ?>
        </div>
        <div class="form-group">
          <label>Banner Image :</label>&nbsp;
          <img src="<?php echo base_url(); ?><?php echo $data['banner_image']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
        </div>
        <div class="form-group">
          <label>Main Image :</label>&nbsp;
          <img src="<?php echo base_url(); ?><?php echo $data['first_image']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
        </div>
        <div class="form-group">
          <label>Description :</label>&nbsp;
          <?php echo $data['main_description']; ?>
        </div>        
        <div class="form-group">
          <label>Second Image :</label>&nbsp;
          <img src="<?php echo base_url(); ?><?php echo $data['second_image']; ?>" style="vertical-align:text-top;height: 61px;margin-top: 44px;margin-left: 3px;">
        </div>
         <div class="form-group">
          <label>Description :</label>&nbsp;
          <?php echo $data['second_description']; ?>
        </div>
        
          <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  </div>
<?php 
endforeach;
?>