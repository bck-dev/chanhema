<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
          </div><!-- /.box-header -->
         <?php 
            $collection_id = $collection[0]['collection_id'];
            $collection_name = $collection[0]['collection_name'];
            $banner_image = $collection[0]['banner_image'];
            $first_image = $collection[0]['first_image'];
            $frist_description = $collection[0]['main_description'];
            $second_image = $collection[0]['second_image'];
            $second_description = $collection[0]['second_description'];
           ?>
          <!-- form start -->
         <form role="form" action="<?php echo base_url('Collection/updatecollection/'.$collection_id); ?>" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
              
              <div class="form-group">                     
                <label for="home_page_grid_description">Collection Name</label>
                <span class="text-danger"> *</span>
                <br />
                <input type="text" class="form-control" name="collection_name" value="<?php echo $collection_name; ?>" placeholder="Collection Name" required>
              </div>

              <div class="form-group" id="banner_box">
                <label for="banner_image">Banner Image</label>
                    <br/>
                    <img src="<?php echo base_url(); ?><?php echo $banner_image; ?>" name="banner_image" id="banner_image" style="vertical-align:text-top;"  width="200" height="200">
                    <a href="#" style="cursor:pointer;" id="b_image_close" class="label label-danger">X</a>
              </div>
              
              <div class="form-group">
                <label>New Banner Image</label>
                <input type="file" name="banner_image" id="banner_image">
              </div>

              <div class="form-group">
                <label for="colecttion_name">First Image</label>
                    <br/>
                    <img src="<?php echo base_url(); ?><?php echo $first_image; ?>" name="first_image" id="first_image" style="vertical-align:text-top;"  width="200" height="200">
                    <a href="#" style="cursor:pointer;" id="f_image" data-imagedin-id="<?php echo $collection_id; ?>" class="label label-danger">X</a>
              </div>
              
              <div class="form-group">
                <label>New First Image</label>
                <input type="file" name="first_image" id="first_image">
              </div>

              <div class="form-group">                     
                <label for="collection_description">Main Description</label>
                <textarea class="form-control ne-except note" rows="3" id="" name="main_description" placeholder="Enter Description"><?php echo $frist_description;?></textarea>
              </div>
              
              <div class="form-group">
                <label for="colecttion_name">Second Image</label>
                    <br/>
                    <img src="<?php echo base_url(); ?><?php echo $second_image; ?>" name="second_image" id="second_image" style="vertical-align:text-top;"  width="200" height="200">
                    <a href="#" style="cursor:pointer;" id="s_image" data-imagedin-id="<?php echo $collection_id; ?>" class="label label-danger">X</a>
              </div>
                            
              <div class="form-group" style="display: block;">
                <label>New Second Image</label>
                <input type="file" name="second_image" id="second_image">
              </div>

              <div class="form-group" style="display: block;">                     
                <label for="collection_description">Second Description</label>
                <textarea class="form-control ne-except note" rows="3" name="second_description" placeholder="Enter Description"><?php echo $second_description;?></textarea>
              </div> 

            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update Collection</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
          
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
  $(document).ready(function() {
    $("#b_image_close").click(function() {
      console.log(1);
        $('#banner_box').hide();
    });
  });
</script>