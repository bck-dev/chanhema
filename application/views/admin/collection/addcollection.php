<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $pagetitle; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $pagetitle; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pagetitle; ?></h3>
            <br><p class="text-danger">Required *</p>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?php echo base_url('Collection/insertcollection'); ?>" id="collection_form" method="POST" enctype='multipart/form-data'>
            <div class="box-body">
              <?php echo $this->session->flashdata('msg'); ?>
              <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                            
              <div class="form-group">                     
                <label for="home_page_grid_description">Collection Name</label>
                <span class="text-danger"> *</span>
                <br />
                <input type="text" class="form-control" name="collection_name" placeholder="Collection Name" required>
              </div>
              
              <div class="form-group">                     
                <label for="home_page_grid_description">Add Banner Image</label>
                <span class="text-danger"> *</span>
                <input type="file" id="banner_image" name="banner_image" required><span id="banner_image_span" class="text-danger"></span>
              </div>
              
              <div class="form-group">                     
                <label for="home_page_grid_description">Add Main Image</label>
                <span class="text-danger"> *</span>
                <input type="file" id="main_image" name="main_image" required><span id="main_image_span" class="text-danger"></span>
              </div>
              
              <div class="form-group">
                <label for="home_page_grid_description">Add Main Description</label>
                <span class="text-danger"> *</span>
                <textarea class="form-control note" rows="3" id="collection_description" name="main_description" placeholder="Enter Second Description"></textarea>
                <span id="main_desciption_span" class="text-danger"></span>
              </div>
              
              <div class="form-group">
                  <p class="text-primary"><b>Note:</b> If you don't want to add a second section, leave below sections Blank</p>
              </div>
              
              <div class="form-group">                     
              <label for="home_page_grid_description">Add Second Image</label>
                <input type="file" id="second_image" name="second_image">
              </div>
              
              <div class="form-group">
                <label for="home_page_grid_description">Add Second Description</label>
                <textarea class="form-control note" rows="3" id="collection_description" name="second_description" placeholder="Enter Second Description"></textarea>
              </div>
             
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary" id="submitBTN">Create Collection</button>
              <a class="btn btn-default" href="<?php echo base_url('admin'); ?>">Cancel</a>
            </div>
          </form>
        </div><!-- /.box -->

      </div><!--/.col (full) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div>