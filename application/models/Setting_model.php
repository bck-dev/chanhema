<?php

class Setting_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	function Get_language()
	{
		$this->db->select('*');
		$this->db->from('tbl_language');
		$query = $this->db->get();
		$result = $query->result_array();
		foreach ($result as $row) {
			$data =  $row;
		}
		return $data['language'];
	}
	function Get_Single_Row($table_name, $where, $id)
	{
		$this->db->where($where, $id);
		$query = $this->db->get($table_name);
		return $result = $query->row();
	}

	function Get_All_joinAgent($table1, $table2, $where1, $where2, $where3, $orderby)
	{
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->join($table2, $where1, 'inner');
		$this->db->where_in($where2, $where3);
		if ($orderby != '') {
			$this->db->order_by($orderby, "desc");
		}

		$query = $this->db->get();
		return $query->result_array();
	}

	function Get_All($tablename)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$query = $this->db->get();
		return $query->result_array();
	}

	function Get_All_New($tablename, $orderby)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->order_by($orderby, 'DESC');
		return $this->db->get()->result();
	}

	function Get_All_Where($tablename, $field, $id)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where($field, $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function Get_All_Where_Object_Array($tablename, $field, $id, $orderby)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where($field, $id);
		$this->db->order_by($orderby, 'DESC');
		return $this->db->get()->result();
	}

	function Get_All_Where_Object_Array_Reverse($tablename, $field, $id, $orderby)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where($field, $id);
		$this->db->order_by($orderby, 'ASC');
		return $this->db->get()->result();
	}

	function Get_All_Where_Like($tablename, $field, $id)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->like($field, $id);
		return $this->db->get()->result();
	}

	function Get_All_Where_Limit($tablename, $field, $id)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where($field, $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function Get_Count($tablename)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$query = $this->db->get();
		return $query->num_rows();
	}


	function Get_Single($table_name, $where, $id)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($where, $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function update($data, $table_name, $where, $id)
	{
		$this->db->where($where, $id);
		$this->db->update($table_name, $data);
	}
	function insert($Data, $table_name)
	{
		$this->db->insert($table_name, $Data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}


	function delete($table_name, $where, $id)
	{
		$this->db->where($where, $id);
		$this->db->delete($table_name);
	}

	function Get_All_join($table1, $table2, $where1)
	{
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->join($table2, $where1, 'inner');
		$query = $this->db->get();
		return $query->result_array();
	}

	function Get_Single_join($table1, $table2, $where1, $where2 = NULL, $id = NULL)
	{
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->join($table2, $where1, 'inner');
		//$this->db->where($where2, $id);

		$query = $this->db->get();
		return $query->result_array();
	}

	function Get_agent_booking($tablename)
	{

		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where('agent_id !=', 'admin');
		$query = $this->db->get();
		return $query->result_array();
	}

	function upload($inputname, $path)
	{
		$config['upload_path'] = "./" . $path . "/"; // path where image will be saved
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		//$config['max_size'] = 3072;
		$config['encrypt_name'] = FALSE;
		$this->upload->initialize($config);
		$this->upload->do_upload($inputname);
		$data_upload_files = $this->upload->data();

		return $image = $data_upload_files['file_name'];
	}

	function uploadNew($inputname)
	{
		$date = date('d-m-Y');
		$path = './upload/' . $date;
		$db_path = 'upload/' . $date . '/';

		if (!is_dir('upload/' . $date)) {
			mkdir('./upload/' . $date, 0777, TRUE);
		}

		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['encrypt_name'] = FALSE;

		$this->upload->initialize($config);
		$this->upload->do_upload($inputname);

		$data_upload_files = $this->upload->data();

		if ($data_upload_files['file_name']) {
			return $db_path . $data_upload_files['file_name'];
		} else {
			return false;
		}
	}

	function deleteData($table, $field, $id)
	{
		$this->db->where($field, $id);
		$this->db->delete($table);
	}


	//upload multiple images to case study
	public function upload_image_services($inputdata, $filename, $covername)
	{
		$this->db->insert('services', $inputdata);
		$insert_id = $this->db->insert_id();

		if ($filename != '') {
			$filename1 = explode(',', $filename);
			foreach ($filename1 as $file) {
				$file_data = array(
					'service_image_name' => $file,
					'service_service_id' => $insert_id
				);
				$this->db->insert('service_images', $file_data);
			}
		}

		if ($covername != '') {
			$data = array('cover_image' => $covername);
			$this->db->update('services', $data, "service_id = $insert_id");
		}
	}

	//upload multiple images to events
	public function upload_image_events($inputdata, $filename)
	{
		$this->db->insert('news', $inputdata);
		$insert_id = $this->db->insert_id();

		if ($filename != '') {
			$filename1 = explode(',', $filename);
			foreach ($filename1 as $file) {
				$file_data = array(
					'news_image' => $file,
					'news_news_id' => $insert_id
				);
				$this->db->insert('news_images', $file_data);
			}
		}
	}

	//edit uploaded images for casestudy
	public function edit_upload_image_services($covername, $user_id, $inputdata, $filename = '')
	{

		$data = array(
			'service_description' => $inputdata['service_description'],
			'service_title' => $inputdata['service_title'],
			'category_fixed' => $inputdata['category_fixed']
		);
		$this->db->where('service_id', $user_id);
		$this->db->update('services', $data);

		if ($filename != '') {
			$filename1 = explode(',', $filename);
			foreach ($filename1 as $file) {
				$file_data = array(
					'service_image_name' => $file,
					'service_service_id' => $user_id
				);
				$this->db->insert('service_images', $file_data);
			}
		}

		if ($covername != '') {
			$data = array('cover_image' => $covername);
			$this->db->update('services', $data, "service_id = $user_id");
		}
	}

	//edit uploaded images for news
	public function edit_upload_image_news($user_id, $inputdata, $filename = '')
	{

		$data = array(
			'news_description' => $inputdata['news_description'],
			'news_title' => $inputdata['news_title']
		);
		$this->db->where('news_id', $user_id);
		$this->db->update('news', $data);

		if ($filename != '') {
			$filename1 = explode(',', $filename);
			foreach ($filename1 as $file) {
				$file_data = array(
					'news_image' => $file,
					'news_news_id' => $user_id
				);
				$this->db->insert('news_images', $file_data);
			}
		}
	}

	//delete data from table
	public function delete_data($tablename, $where, $user_id)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where($where, $user_id);
		$this->db->delete();
	}

	//delete data from table and folder
	public function delete_data_img($tablename, $where, $user_id, $name_img)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where($where, $user_id);
		unlink(FCPATH . "upload/gallery/" . $name_img);
		// 		$this->db->limit($limit);
		$this->db->delete();
	}

	//edit case study images from database
	public function edit_data_image_services($id)
	{
		$query = $this->db->query("SELECT *
			FROM services
			RIGHT JOIN service_images 
			ON service_id = service_service_id
			WHERE service_id = $id");
		return $query->result_array();
	}

	//edit news images from database
	public function edit_data_image_news($id)
	{
		$query = $this->db->query("SELECT *
			FROM news
			RIGHT JOIN news_images 
			ON news_id = news_news_id
			WHERE news_id = $id");
		return $query->result_array();
	}

	//multiple image upload (also using in edit too)
	public function multi_image_upload($files, $path)
	{
		//print_r($files);
		$count = count($_FILES['userfile']['name']);
		for ($i = 0; $i < $count; $i++) {
			$_FILES['userfile']['name'] = time() . $files['userfile']['name'][$i];
			$_FILES['userfile']['type'] = $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error'] = $files['userfile']['error'][$i];
			$_FILES['userfile']['size'] = $files['userfile']['size'][$i];
			$config['upload_path'] = './upload/' . $path . '/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = 3072;
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['max_width'] = '';
			$config['max_height'] = '';
			$this->upload->initialize($config);
			$this->upload->do_upload();
			$fileName = $_FILES['userfile']['name'];
			$images[] = $fileName;
		}
		$fileName = implode(',', $images);
		return $fileName;
	}

	public function vacancycategory()
	{
		$sql = "SELECT * FROM `vacancy_category` WHERE `vacancy_category_status`='1'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function select_sub_catagory($id)
	{

		$sql = "SELECT * FROM `collection_page_sub_catagory` WHERE `page_catagory_id`=$id LIMIT 4 ";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function award()
	{
		$sql = "SELECT * FROM `news` INNER JOIN `news_images` ON `news`.`news_id` = `news_images`.`news_news_id` WHERE `news`.`news_category` = 'award'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}


	function Get_All_by_id($table, $field, $id)
	{
		$this->db->where($field, $id);
		$query = $this->db->get($table);
		return $query->result_array();
	}

	function Get_Product_Count_By_Collection($collection_name)
	{
		$this->db->select('*');
		$this->db->from('products');
		$this->db->like('collection_page', $collection_name);
		$count = $this->db->count_all_results();
		return $count;
	}

	//feature products
	public function newarivals()
	{
		$sql = "SELECT * FROM `products` inner JOIN `product_category` ON `products`.`category_category_id` = `product_category`.`category_id` WHERE `product_category`.`category_id` = '6'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}
	public function clinical()
	{
		$sql = "SELECT * FROM `products` inner JOIN `product_category` ON `products`.`category_category_id` = `product_category`.`category_id` WHERE `product_category`.`category_id` = '9'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function homecare()
	{
		$sql = "SELECT * FROM `products` inner JOIN `product_category` ON `products`.`category_category_id` = `product_category`.`category_id` WHERE `product_category`.`category_id` = '10'";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	//add feature products

	public function addnewarival()
	{
		$query = $this->db->query("SELECT * FROM `feature_product` inner JOIN `products` ON `feature_product`.`product_id`=`products`.`product_id` WHERE `feature_product`.`category_id` = '6' ");

		return $query->result_array();
	}

	public function addclinical()
	{
		$query = $this->db->query("SELECT * FROM `feature_product` inner JOIN `products` ON `feature_product`.`product_id`=`products`.`product_id` WHERE `feature_product`.`category_id` = '9' ");

		return $query->result_array();
	}

	public function addhomecare()
	{
		$query = $this->db->query("SELECT * FROM `feature_product` inner JOIN `products` ON `feature_product`.`product_id`=`products`.`product_id` WHERE `feature_product`.`category_id` = '10' ");

		return $query->result_array();
	}

	//ajax search for products
	public function Get_Ajax_Search($tablename, $array)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		if (!empty($array)) {
			$this->db->where($array);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function Get_Ajax_Search_New($tablename, $category, $collection)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		if ($category != 0) {
			$this->db->where('product_catagory', $category);
		}
		if ($collection != '0') {
			$this->db->like('collection_page', $collection);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	function collection_join($col_id, $sub_id)
	{
		$query = $this->db->query("SELECT * FROM `collection` 
				LEFT JOIN `collection_page_main_category` ON `collection`.`catagory_id` = `collection_page_main_category`.`celebrate_id`
				LEFT JOIN `collection_page_sub_catagory` ON `collection`.`sub_catagory_id` = `collection_page_sub_catagory`.`page_sub_catagory_id` WHERE `collection`.`catagory_id` = $col_id AND `collection`.`sub_catagory_id` = $sub_id 
				");
		return $query->result_array();
	}

	function collection_join_catagory()
	{
		$query = $this->db->query("SELECT * FROM `collection` 
				LEFT JOIN `collection_page_main_category` ON `collection`.`catagory_id` = `collection_page_main_category`.`celebrate_id`
				LEFT JOIN `collection_page_sub_catagory` ON `collection`.`sub_catagory_id` = `collection_page_sub_catagory`.`page_sub_catagory_id` 
				");
		return $query->result_array();
	}

	function product_join($col_id, $sub_id)
	{

		$query = $this->db->query("SELECT * FROM `products` 
				LEFT JOIN `product_images` ON `products`.`product_id` = `product_images`.`product_product_id` WHERE `products`.`collection_page` = $col_id AND `products`.`collection_sub_page` = $sub_id
				");
		return $query->result_array();
	}


	public function newsjoin($id)
	{
		$sql = "SELECT * FROM `news` 
				LEFT JOIN `news_images` ON `news`.`news_id` = `news_images`.`news_news_id`
				WHERE `news`.`news_id` = {$id}";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	function productFilter($categoryId, $subCatagoryId, $collection)
	{
		$this->db->select('*');
		$this->db->from('products');
		if ($categoryId != '') {
			$this->db->where('product_catagory', $categoryId);
		}
		if ($subCatagoryId != '') {
			$this->db->where('product_sub_catagory', $subCatagoryId);
		}
		if ($collection != '') {
			$this->db->like('collection_page', $collection);
		}
		$this->db->order_by('product_id', 'DESC');
		return $this->db->get()->result();
	}

	function Get_sub_categories_with_products($categoryId = null)
	{
		$this->db->select('product_sub_catagory');
		$this->db->distinct('product_sub_catagory');
		if ($categoryId != '') {
			$this->db->where('product_catagory', $categoryId);
		}
		$unique_ids = $this->db->get('products')->result();

		$i = 0;
		$sub_cats = [];
		foreach ($unique_ids as $uid) {
			$sub_cats[$i] = $this->Get_Single_Row('product_sub_catagory', 'sub_catagory_id', $uid->product_sub_catagory);
			// echo $this->db->last_query();
			$i++;
		}

		return $sub_cats;
	}
}