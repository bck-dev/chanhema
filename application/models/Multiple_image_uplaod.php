<?php

class multiple_image_uplaod extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	 //multiple image upload (also using in edit too) --yasas vidanage
	public function multi_image_upload($files,$path){
		$date = date('d-m-Y');
		$newPath= $path.'/'.$date;

		if (!is_dir('upload/'.$newPath)) {
			mkdir('./upload/' . $newPath, 0777, TRUE);
		}
    	//print_r($files);
		$count = count($_FILES['userfile']['name']);
		for($i=0; $i<$count; $i++)
		{
			$_FILES['userfile']['name']= time().$files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			$config['upload_path'] = './upload/'.$newPath.'/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] ='' ;
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['max_width'] = '';
			$config['max_height'] = '';
			$config['encrypt_name'] = FALSE;
			$this->upload->initialize($config);
			$this->upload->do_upload();
			$fileName = $_FILES['userfile']['name'];
			$images[] = 'upload/'.$newPath.'/'.$fileName;
		}

		$fileName = implode(',',$images);
		return $fileName;
	}
//========================================Services Images Upload ===============================//
	//upload multiple images to services--yasas vidanage
	public function upload_image_services($inputdata,$filename)
	{
		$this->db->insert('services', $inputdata); 
		$insert_id = $this->db->insert_id();

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'services_images_name' => $file,
					'services_services_id' => $insert_id
				);
				$this->db->insert('services_images', $file_data);
			}
		}
	}

	//edit services images from database--yasas vidanage
	public function edit_data_image_services($id){
		$query=$this->db->query("SELECT *
			FROM services
			RIGHT JOIN services_images 
			ON services_id = services_services_id
			WHERE services_id = $id");
		return $query->result_array();
	}

	//edit uploaded images for services--yasas vidanage
	public function edit_upload_image_services($user_id,$inputdata,$filename ='')
	{

		$data = array(
			'services_name'=>$inputdata['services_name'],
			'services_description'=> $inputdata['services_description'],
			);

		$this->db->where('services_id', $user_id);
		$this->db->update('services', $data);

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'services_images_name' => $file,
					'services_services_id' => $user_id
				);
				$this->db->insert('services_images', $file_data);
			}
		}

		
	}
//=========================== End services Images Upload========================================//


//===========================news image upload ==============================================//

//upload multiple images to news--ishara sewwandi
	public function upload_image_news($filename,$dataid)
	{
		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'image_name' => $file,
					'news_news_id' => $dataid
				);
				$this->db->insert('news_images', $file_data);
			}
		}
	}

	public function edit_data_image_news($id){
		$query=$this->db->query("SELECT *
			FROM news
			RIGHT JOIN news_images 
			ON news_id = news_news_id
			WHERE news_id = $id");
		return $query->result_array();
	}

 public function edit_upload_image_news($filename,$dataid)
    {

       if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'image_name' => $file,
					'news_news_id' => $dataid
				);
				$this->db->insert('news_images', $file_data);
			}
		}


    }


//===========================End news image upload ==============================================//



//===========================start gallery images upload=================================//

public function upload_image_gallery($filename)
	{	
		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'image' => $file,
					
				);
				$this->db->insert('gallery', $file_data);
			}
		}
	}

//=========================== End gallery Images Upload========================================//
//===========================start best practices images upload=================================//

//upload multiple images to best practices--yasas vidanage
	public function upload_image_practices($inputdata,$filename)
	{
		$this->db->insert('best_practices', $inputdata); 
		$insert_id = $this->db->insert_id();

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'best_practices_images_name' => $file,
					'best_practices_best_id' => $insert_id
				);
				$this->db->insert('best_practices_images', $file_data);
			}
		}
	}

	//edit practices images from database--yasas vidanage
	public function edit_data_image_practices($id){
		$query=$this->db->query("SELECT *
			FROM  best_practices
			RIGHT JOIN best_practices_images 
			ON best_practices_id = best_practices_best_id
			WHERE best_practices_id = $id");
		return $query->result_array();
	}



	//edit uploaded images for practices--yasas vidanage
	public function edit_upload_image_practices($user_id,$inputdata,$filename ='')
	{

		$data = array(
			'best_practices_name'=>$inputdata['best_practices_name'],
			'best_practices_description'=> $inputdata['best_practices_description'],
			);

		$this->db->where('best_practices_id', $user_id);
		$this->db->update('best_practices', $data);

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'best_practices_images_name' => $file,
					'best_practices_best_id' => $user_id
				);
				$this->db->insert('best_practices_images', $file_data);
			}
		}

		
	}
//==========================End Practices Images Upload========================================//

	//edit uploaded images for stones--yasas vidanage
	public function upload_image_stones($inputdata,$filename)
	{
		$this->db->insert('stones', $inputdata); 
		$insert_id = $this->db->insert_id();

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'stones_images_name' => $file,
					'stones_stones_id' => $insert_id
				);
				$this->db->insert('stones_images', $file_data);
			}
		}
	}
//==========================End Stones Images Upload========================================//

	//edit stones images from database--yasas vidanage
	public function edit_data_image_stones($id){
		$query=$this->db->query("SELECT *
			FROM  stones
			RIGHT JOIN stones_images 
			ON stones_id = stones_stones_id
			WHERE stones_id = $id");
		return $query->result_array();
	}


	//edit uploaded images for practices--yasas vidanage
	public function edit_upload_image_stones($user_id,$inputdata,$filename ='')
	{

		$data = array(
			'stones_name' => $inputdata['stones_name'],
			'stones_status' => $inputdata['stones_status'],
			'stones_description'=> $inputdata['stones_description'],
			);

		$this->db->where('stones_id', $user_id);
		$this->db->update('stones', $data);

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'stones_images_name' => $file,
					'stones_stones_id' => $user_id
				);
				$this->db->insert('stones_images', $file_data);
			}
		}

		
	}

	//edit uploaded images for product--yasas vidanage
	public function upload_image_product($inputdata,$filename)
	{
		$comma_separated="";

		foreach($inputdata['collection_page'] as $collection){
			$comma_separated=$collection.", ".$comma_separated;
		}
		
		$inputdata['collection_page']=substr($comma_separated, 0, -2);;
		$this->db->insert('products', $inputdata); 
		$insert_id = $this->db->insert_id();

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'product_images_name' => $file,
					'product_product_id' => $insert_id
				);
				$this->db->insert('product_images', $file_data);
			}
		}
	}
	



	//edit product images from database--yasas vidanage
	public function edit_data_image_product($id){
		$query=$this->db->query("SELECT *
			FROM  products
			RIGHT JOIN product_images 
			ON product_id = product_product_id
			WHERE product_id = $id");
		return $query->result_array();
	}


	//edit uploaded images for product--yasas vidanage
	public function edit_upload_image_product($user_id,$inputdata,$filename ='')
	{
		$comma_separated="";

		foreach($inputdata['collection_page'] as $collection){
			$comma_separated=$collection.", ".$comma_separated;
		}
		
		$inputdata['collection_page']=substr($comma_separated, 0, -2);;
		$data = array(
			'product_name'=> $inputdata['product_name'],
			'product_description'=> $inputdata['product_description'],
			'product_matterials'=> $inputdata['product_matterials'],
			'product_journey'=> $inputdata['product_journey'],
			'product_catagory' => $inputdata['product_catagory'],
            'product_sub_catagory' => $inputdata['product_sub_catagory'],
            'collection_page' => $inputdata['collection_page'],
			);
		
		$this->db->where('product_id', $user_id);
		$this->db->update('products', $data);

		if($filename!='' ){
			$filename1 = explode(',',$filename);
			foreach($filename1 as $file){
				$file_data = array(
					'product_images_name' => $file,
					'product_product_id' => $user_id
				);
				$this->db->insert('product_images', $file_data);
			}
		}

		
	}

}